Changelog
==========

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

1.5.0

*****

* Update to v10.0.0 of the backend API which has several breaking changes. Refactored the results handling in the UI, extracting it all into new services and mappers. User experience should be unchanged.

1.4.2

*****

* fix: Update to latest makefile submodule to fix broken release process

1.4.1

*****

* fix: Changing the subarray or central frequency did not update the spectral resolution in velocity units or the effective resolution properly

1.4.0

*****

* Added spectropolarimetry results to both Mid and Low. Displayed below the existing results for continuum and zoom modes.
* Lid now supports subbands in continuum mode. Limited to 4 for performance reasons; expected to increase once changes are made to the Low sensitivity calculation.
