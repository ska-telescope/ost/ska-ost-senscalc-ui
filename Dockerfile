ARG CAR_OCI_REGISTRY_HOST=artefact.skao.int
ARG CAR_NPM_REPOSITORY_URL=https://artefact.skao.int/repository/npm-all/
ARG DUMMY_URL=http://dummy-url.tld

# Build stage

### STAGE 1: Build ###

# We label our stage as 'builder'
FROM node:18.17.1-alpine as builder
ARG CAR_NPM_REPOSITORY_URL
ARG DUMMY_URL
## Storing node modules on a separate layer will prevent unnecessary npm installs at each build
# RUN npm i && mkdir /app && cp -R ./node_modules ./app
WORKDIR /home/node/app

COPY package.json package-lock.json ./

# Set npm registry
ENV npm_config_registry=${CAR_NPM_REPOSITORY_URL}

RUN npm set progress=false && npm config set depth 0 && npm cache clean --force

RUN npm install

COPY . .

## Build the angular app in production mode and store the artifacts in dist folder
RUN npm run build -- --base-href=${DUMMY_URL}


### STAGE 2: Setup ###
## Setup an Nginx server that will acct as the webserver for the application
FROM nginx:1.23.3-alpine
WORKDIR /home/node/app
ARG DUMMY_URL

## Currently commented out due to version instability and we are
## currently not using integrity hashes (see conf/entrypoint.sh)
## If need for integrity hashes arises, we should install openssl
# RUN apk add --no-cache openssl=3.0.8-r3

COPY conf/nginx.conf /etc/nginx/conf.d/default.conf
COPY conf/entrypoint.sh /

## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*

## From 'builder' stage copy over the artifacts in dist folder to default nginx public folder
COPY --from=builder /home/node/app/dist/ska-ost-senscalc-ui /usr/share/nginx/html/

ENV DUMMY_URL=${DUMMY_URL}
ENV CONTEXT_URL=http://localhost

EXPOSE 80

ENTRYPOINT ["/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
