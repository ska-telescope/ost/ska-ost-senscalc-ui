# Project makefile for a ska-ost-senscalc-ui
CAR_OCI_REGISTRY_HOST ?= artefact.skao.int
CAR_OCI_REGISTRY_USERNAME ?= ska-telescope
PROJECT_NAME = ska-ost-senscalc-ui

# Set sphinx documentation build to fail on warnings (as it is configured
# in .readthedocs.yaml as well)
DOCS_SPHINXOPTS ?= -W --keep-going

# KUBE_NAMESPACE defines the Kubernetes Namespace that will be deployed to
# using Helm.  If this does not already exist it will be created
KUBE_NAMESPACE ?= ska-ost-senscalc-ui

# KUBE_HOST defines the IP address of the Minikube ingress.
KUBE_HOST ?= http://`minikube ip`

K8S_CHART ?= ska-ost-senscalc-ui-umbrella

# include makefile targets from the submodule
include .make/base.mk
include .make/oci.mk
include .make/k8s.mk
include .make/helm.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak

OCI_IMAGE_BUILD_CONTEXT = $(PWD)

up: oci-build ## Start up the UI inside a docker container (accessible at localhost:4200)
	docker run -p 4200:80 $(CAR_OCI_REGISTRY_HOST)/$(strip $(OCI_IMAGE)):$(VERSION)

pre_install_cleanup: #Remove old build reports and node modules
	rm -rf build/*
	rm -rf node_modules
	mkdir -p build/reports

install_dependencies: pre_install_cleanup   ## Install basic dependencies using npm
	npm install;

test: ## test the application and generate test coverage reports
	mkdir -p build/reports;
	npm run test:coverage;
	mv coverage/cobertura-coverage.xml build/reports/code-coverage.xml;
	mv junit.xml build/reports/unit-tests.xml;

lint: ## lint the application (static code analysis)
	mkdir -p build/reports;
	npm run code-analysis;
	mv linting.xml build/reports/linting.xml;

helm-post-set-release:
	sed -i 's/"currentVersion": [^\]*/"currentVersion": "${VERSION}",/' src/ska_ost_senscalc_ui/assets/configuration.json

# For the test, dev and integration environment, use the freshly built image in the GitLab registry
ENV_CHECK := $(shell echo $(CI_ENVIRONMENT_SLUG) | egrep 'test|dev|integration')
ifneq ($(ENV_CHECK),)
K8S_CHART_PARAMS = --set ska-ost-senscalc-ui.ui.image.tag=$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA) \
	--set ska-ost-senscalc-ui.ui.image.registry=$(CI_REGISTRY)/ska-telescope/ost/ska-ost-senscalc-ui
endif

# For the staging environment, make k8s-install-chart-car will pull the chart from CAR so we do not need to
# change any values


# For the production environment, make k8s-install-chart-car will pull the chart from CAR with the latest released version.
# We also need to allow the production ingress.
ENV_CHECK := $(shell echo $(CI_ENVIRONMENT_SLUG) | egrep production)
ifneq ($(ENV_CHECK),)
K8S_CHART_PARAMS = --set ska-ost-senscalc-ui.ingress.production="true"
K8S_CHART_PARAMS += --set ska-ost-senscalc.ingress.production="true"
endif

# These values should be set for all deployments, locally and CICD environments - see README for details on the URL
API_BACKEND_URL ?= $(KUBE_HOST)/$(KUBE_NAMESPACE)/senscalc
SENSCALC_UI_URL ?= $(KUBE_HOST)/$(KUBE_NAMESPACE)/senscalc

K8S_CHART_PARAMS += \
  --set ska-ost-senscalc-ui.ui.runtime.backendURL=$(API_BACKEND_URL) \
  --set ska-ost-senscalc-ui.ui.ingress.contextUrl=$(SENSCALC_UI_URL)

CODEGEN_TARGETS = \
	models,supportingFiles=models.ts   \
	apis,supportingFiles=api.ts		     \
	supportingFiles=api.module.ts      \
	supportingFiles=configuration.ts   \
	supportingFiles=encoder.ts         \
	supportingFiles=index.ts           \
	supportingFiles=param.ts           \
	supportingFiles=README.md          \
	supportingFiles=variables.ts

models:
	$(foreach var,$(CODEGEN_TARGETS),$(OCI_BUILDER) run --rm --user `id -u $$USER`:`id -g $$USER` --volume "$(MINIKUBE_NFS_SHARES_ROOT)$(PWD):/local" openapitools/openapi-generator-cli \
  	generate \
    -i /local/openapi-low.yaml \
    -g typescript-angular \
    -o /local/src/ \
		--package-name ska_ost_senscalc_ui.app.generated.api.low \
    --api-package ska_ost_senscalc_ui.app.generated.api.low.api \
		--model-package ska_ost_senscalc_ui.app.generated.api.low.model \
    --model-name-prefix Low \
  	--additional-properties "apiModulePrefix=Low,configurationPrefix=Low,ngVersion=14.0.0,useSingleRequestParameter=true" \
  	--global-property "$(var)" \
		--inline-schema-name-mappings	"ska_ost_senscalc_low_api_subarrays_200_response_inner=SubarraysResponse" \
		--inline-schema-name-mappings "ska_ost_senscalc_low_api_continuum_calculate_200_response=ContinuumSensitivityResponse" \
		--inline-schema-name-mappings "ska_ost_senscalc_low_api_continuum_calculate_200_response_continuum_subband_sensitivities_inner=SingleSubbandSensitivityResponse" \
		--inline-schema-name-mappings "ska_ost_senscalc_low_api_pss_calculate_200_response=PssSensitivityResponse" \
		--inline-schema-name-mappings "ska_ost_senscalc_low_api_zoom_calculate_200_response_inner=SingleZoomSensitivityResponse" ;)
	$(foreach var,$(CODEGEN_TARGETS),$(OCI_BUILDER) run --rm --user `id -u $$USER`:`id -g $$USER` --volume "$(MINIKUBE_NFS_SHARES_ROOT)$(PWD):/local" openapitools/openapi-generator-cli \
  	generate \
    -i /local/openapi-mid.yaml \
    -g typescript-angular \
    -o /local/src/ \
		--package-name ska_ost_senscalc_ui.app.generated.api.mid \
    --api-package ska_ost_senscalc_ui.app.generated.api.mid.api \
		--model-package ska_ost_senscalc_ui.app.generated.api.mid.model \
    --model-name-prefix Mid \
  	--additional-properties "apiModulePrefix=Mid,configurationPrefix=Mid,ngVersion=14.0.0,useSingleRequestParameter=true" \
		--inline-schema-name-mappings "ska_ost_senscalc_mid_api_continuum_calculate_200_response=ContinuumSensitivityResponse" \
		--inline-schema-name-mappings "ska_ost_senscalc_mid_api_continuum_calculate_200_response_continuum_subband_sensitivities_inner=SingleSubbandSensitivityResponse" \
		--inline-schema-name-mappings "ska_ost_senscalc_mid_api_continuum_calculate_200_response_continuum_subband_integration_times_inner=SingleSubbandIntegrationTimeResponse" \
		--inline-schema-name-mappings "ska_ost_senscalc_mid_api_zoom_calculate_200_response_inner=SingleZoomSensitivityResponse" \
		--inline-schema-name-mappings "ska_ost_senscalc_mid_api_subarrays_200_response_inner=SubarraysResponse" \
  	--global-property "$(var)" ;)
	echo "export * from './model/models';" >> src/ska_ost_senscalc_ui/app/generated/api/low/index.ts
	echo "export * from './model/models';" >> src/ska_ost_senscalc_ui/app/generated/api/mid/index.ts

