The Sensitivity Calculator is an application for calculating the sensitivity of
the SKA MID and SKA LOW telescopes.

This Project was generated with Angular CLI version 13.

## Local development and testing

### Installing project dependencies

Run `make install_dependencies` to install the latest project dependencies 
from package.json and package-lock.json

### Build a production deployment

Run `yarn build` or `npm run build` or `ng build` to build the project. The build artifacts will be stored in the
`dist/` directory.

### Rebuild the new Angular project structure class diagram for ReadTheDoc

Run `npm run arkit-diagram` or `yarn arkit-diagram` on the base directory of the project. It will
generate the new diagram 'angular_structure_class.svg' within the docs/src/_static/img/ directory.


### Running a front end development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The
app will automatically reload if you change any of the source files.

To interact with the backend API, set the `backendURL` in [runtime-configuration.json file](./src/ska_ost_senscalc_ui/assets/runtime-configuration.json)
to, for example, `http://$KUBE_HOST/ska-ost-senscalc/api/v1`

### Running unit tests

Run `make test` to execute the unit tests.

### Running static code analysis

Run `make lint` to lint the code.

### Generating API classes from YAML spec

If there are any changes to the back-end API definition, the API classes used by the front-end 
can be automatically generated from the back-end OpenAPI YAML specs with two steps:

1. Copy the required YAML files (openapi-mid-v1.yaml, openapi-low-v1.yaml and opeanpi-common-v1.yaml) 
from the ska-ost-senscalc project to the root of this project.
2. Run `make models`

This should regenerate the classes in `src/ska_ost_senscalc_ui/app/generated` folder to match 
the given specs.

## Deploying to Kubernetes  

To build the Docker image run the following  
  
```  
make oci-build-all  
```  
 
The umbrella Helm chart can then be deployed with  
  
```  
make k8s-install-chart
```  
  
and undeployed with 
```  
make k8s-uninstall-chart  
```  
  
Once deployed, the UI is available at 
`http://<HOST_IP>/<KUBE_NAMESPACE>/senscalc`, which will default to `/mid`

If using minikube, run `minikube ip` to find the host IP. `KUBE_NAMESPACE` is
set to `ska-ost-senscalc-ui` by default. The backend component will also be
deployed to a separate pod, which the web application will make requests to.

### Deployment URL configuration

Traditionally, the URL of an Angular application is known and declared at the
time the application is built, with the static URL configuration hardcoded
into the build artefacts. This is not desirable for SKA's Kubernetes
deployments, where a 'build once, deploy many' approach is preferred and the
desired endpoint may only be known at the time of deployment, e.g., for CI/CD
review deployments. To enable dynamic setting of endpoint URLs, the build 
procedure for this project declares that the application will be deployed to a
recognisable dummy URL, which is then replaced at runtime by a real endpoint
URL, known as the *context URL*. 

The dummy URL is declared at image build time via the `DUMMY_URL` Docker build
argument and has a default value of http://dummy-url.tld. At run time, this
dummy URL is replaced with the context URL, with dummy and context URLs
declared to the runtime system via two Helm chart values:

1. `ska-ost-senscalc-ui.ui.ingress.dummyUrl` (default=http://dummy-url.tld)
2. `ska-ost-senscalc-ui.ui.ingress.contextUrl` (default=http://<KUBE_HOST>/<KUBE_NAMESPACE>/senscalc)

While the dummy URL is exposed and made configurable, there should be no
reason to modify it. The context URL's default values should automatically
provide the correct behaviour for developer environments and CI/CD 
deployments.  Non-standard deployments may need to overwrite the contextUrl
value, setting it to the public URL where the application will be accessed.

If using Makefiles to deploy the application, you can use the
``SENSCALC_UI_URL`` variable to set the context URL. For example, with the
command below, the application would expect to be accessed at
http://skatelescope.org/science-users/calculator.

``SENSCALC_UI_URL=http://skatelescope.org/science-users/calculator make k8s-install-chart``


## Running integration (Cypress) tests

The integration tests check the end to end integration from the browser to the backend API.

Note: If running Cypress tests on Linux, there are some pre-requisites that have to be installed to run Cypress. See the list of packages to install on [Cypress installation instructions](https://docs.cypress.io/guides/getting-started/installing-cypress#Linux-Prerequisites).

1. A backend API must be running and the `backendURL` set in [runtime-configuration.json file](./src/ska_ost_senscalc_ui/assets/runtime-configuration.json)
2. The UI server also must be running and the `baseUrl` set in `cypress.json`
3. Run tests with `npm run cypress:run`

## Deploying to non-production environments

There are 3 different environments which are defined through the standard pipeline templates. They need to be manually triggered in the Gitlab UI.

1. `dev` - a temporary (4 hours) deployment from a feature branch, using the artefacts built in the branch pipeline
2. `integration` - a permanent deployment from the main branch, using the latest version of the artefacts built in the main pipeline
3. `staging` - a permanent deployment of the latest published artefact from CAR

To find the URL for the environment, see the 'info' job of the CICD pipeline stage, which should output the URL alongside the status of the Kubernetes pods.
Generally the UI URL should be available at  `https://k8s.stfc.skao.int/$KUBE_NAMESPACE/senscalc`

## Deploying to production environment

The production environment is publicly available at http://sensitivity-calculator.skao.int/

It is deployed from the `production` stage of a main pipeline and pulls the artefact from CAR. 
The deployment is to an AWS cluster, rather than the STFC k8s cluster the other environments use.

### Restrictions

The production environment is called `production-ska-ost-senscalc` in GitLab's
UI and can be seen from here: https://gitlab.com/ska-telescope/ost/ska-ost-senscalc/-/environments/11337767
Only the maintainers can make a deployment and all deployments should be
approved from the above interface when a new job is triggered for deployment.
These settings can be changed by the project owner or the team-system if
needed.


## Documentation

Documentation and a User Guide can be found in the ``docs`` folder. The documentation can be built using Python: 
first install the documentation build dependencies with ``pip install -r docs/requirements.txt`` and build 
the documentation with ``make docs-build html``.

## Further help

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-ost-senscalc-ui/badge/?version=latest)](https://developer.skao.int/projects/ska-ost-senscalc-ui/en/latest/?badge=latest)

To get more help on the Angular CLI use `ng help` or go check out the
[Angular CLI Overview and Command Reference](https://angular.io/cli) page.
