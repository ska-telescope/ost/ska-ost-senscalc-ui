
describe('The LOW calculator should meet accessibility requirements', () => {
  beforeEach(() => {
    cy.visit('/low')
    cy.wait(800)
  })

  it('should be able to tab to the mid/low toggle and change telescope', () => {
   cy.tab() // Start at the first element
     .tab() // Tab to the toggle
     .type('{enter}') // Toggle to mid

    cy.url().should('contain', "/mid")
  })

  it('should be able to tab from the input fields to the calculate button, and calculate using the enter key', () => {
    const input_and_expected_result = {
      "num_stations": "224",
      "freq_centre": "100",
      "bandwidth": "50",
      "ra": "08:00:00",
      "dec": "20:00:50",
      "minElevation": "20",
      "duration": "0.5",
      "continuumWeightedSensitivityResult": "59.97 uJy/beam (1.01)",
      "continuumSynthesizedBeamSizeResult": '1003.7" x 631.6"',
      "continuumSurfaceBrightnessSensitivityResult": " 38.96 K"
    }

    cy.tab() // Initial tab to the SKAO logo
     .tab() // Tab to mid toggle
     .tab() // Tab to low toggle
     .tab() // Tab to dark/light mode button
     .tab() // Tab to info button
     .tab().type( "{downArrow}") // Tab to subarray selector and choose AA4(core only) subarray (num of station is then disabled)
     .tab() // Tab to Degree toggle
     .tab().type(input_and_expected_result.ra)
     .tab().type(input_and_expected_result.dec)
     .tab().type(input_and_expected_result.minElevation)
     .tab().type("{enter}") // Tab to Continuum accordion
     .tab().type(input_and_expected_result.duration)
     .tab().type(input_and_expected_result.freq_centre)
     .tab().type(input_and_expected_result.bandwidth)
     .tab() // Tab to N Subbands
     .tab() // Tab to Spectral Resolution
     .tab() // Tab to Spectral Averaging
     .tab() // Tab to Effective resolution
     .tab().type( "{downArrow}").type( "{downArrow}") // Tab to Image Weighting and change to 'Briggs'
     .tab().type( "{downArrow}") // Change Robust Value to 1
     .tab() // Tab to results panel
     .tab() // Tab to closed zoom accordion
     .tab() // Tab to closed pss accordion
     .tab() // Tab to Calculate button
     .type('{enter}') // Send the request using the Enter key

    cy.getByLabel("Number of Stations").should("have.value", input_and_expected_result.num_stations)

    // Check the request send successfully results are displayed
      cy.get('#continuumWeightedSensitivity', {timeout: 25000}).should('contain.text', input_and_expected_result.continuumWeightedSensitivityResult);
      cy.get('#continuumSynthesizedBeamSize').should('contain.text', input_and_expected_result.continuumSynthesizedBeamSizeResult);
      cy.get('#continuumSurfaceBrightnessSensitivity').should('contain.text', input_and_expected_result.continuumSurfaceBrightnessSensitivityResult);
  })


})
