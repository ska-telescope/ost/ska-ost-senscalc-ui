let input_and_expected_results = [
  {
    "subarray_configuration": "AA4",
    "freq_centre": "100",
    "bandwidth": "50",
    "ra": "08:00:00",
    "dec": "20:00:50",
    "dec": "20:00:50",
    "minElevation": "20",
    "duration": "0.5",
    "imageWeighting": "Uniform",
    "continuumWeightedSensitivityResult": "413.56 uJy/beam (15.88)",
    "continuumConfusionNoiseResult": "13.20 uJy/beam",
    "continuumSensitivityResult": "413.77 uJy/beam",
    "continuumSynthesizedBeamSizeResult": "11.2\" x 5.5\"",
    "continuumSurfaceBrightnessSensitivityResult": "823.46 K",
  },
  {
    "subarray_configuration": "AA*",
    "freq_centre": "330",
    "bandwidth": "20",
    "ra": "00:00:00",
    "dec": "00:00:00",
    "minElevation": "50",
    "duration": "0.3",
    "imageWeighting": "Uniform",
    "continuumWeightedSensitivityResult": "896.18 uJy/beam (14.75)",
    "continuumConfusionNoiseResult": "186.67 nJy/beam",
    "continuumSensitivityResult": "896.18 uJy/beam",
    "continuumSynthesizedBeamSizeResult": "2.3\" x 2.1\"",
    "continuumSurfaceBrightnessSensitivityResult": "2085.09 K",
  },
  {
    "subarray_configuration": "AA4",
    "freq_centre": "200",
    "bandwidth": "300",
    "ra": "10:00:00",
    "dec": "-30:00:00",
    "minElevation": "20",
    "duration": "0.05",
    "imageWeighting": "Uniform",
    "continuumWeightedSensitivityResult": "201.85 uJy/beam (10.33)",
    "continuumConfusionNoiseResult": "632.81 nJy/beam",
    "continuumSensitivityResult": "201.85 uJy/beam",
    "continuumSynthesizedBeamSizeResult": "3.3\" x 2.5\"",
    "continuumSurfaceBrightnessSensitivityResult": "731.60 K",
  },
  {
    "subarray_configuration": "AA4",
    "freq_centre": "100",
    "bandwidth": "100",
    "ra": "00:00:00",
    "dec": "00:00:00",
    "minElevation": "35",
    "duration": "1",
    "imageWeighting": "Uniform",
    "continuumWeightedSensitivityResult": "178.91 uJy/beam (15.18)†",
    "continuumConfusionNoiseResult": "9.71 uJy/beam",
    "continuumSensitivityResult": "179.17 uJy/beam",
    "continuumSynthesizedBeamSizeResult": "7.8\" x 6.1\"",
    "continuumSurfaceBrightnessSensitivityResult": " 465.66 K",
  },
  {
    "subarray_configuration": "AA4",
    "freq_centre": "150",
    "bandwidth": "100",
    "ra": "00:00:00",
    "dec": "00:00:00",
    "minElevation": "20",
    "duration": "1",
    "imageWeighting": "Uniform",
    "continuumWeightedSensitivityResult": "111.71 uJy/beam (15.18)†",
    "continuumConfusionNoiseResult": "2.75 uJy/beam",
    "continuumSensitivityResult": "111.74 uJy/beam",
    "continuumSynthesizedBeamSizeResult": "5.2\" x 4.0\"",
    "continuumSurfaceBrightnessSensitivityResult": "290.40 K",
  },
  {
    "subarray_configuration": "AA4",
    "freq_centre": "300",
    "bandwidth": "100",
    "ra": "00:00:00",
    "dec": "00:00:00",
    "minElevation": "20",
    "duration": "1",
    "imageWeighting": "Uniform",
    "continuumWeightedSensitivityResult": "128.65 uJy/beam (15.18)†",
    "continuumConfusionNoiseResult": "227.21 nJy/beam",
    "continuumSensitivityResult": "128.65 uJy/beam",
    "continuumSynthesizedBeamSizeResult": "2.6\" x 2.0\"",
    "continuumSurfaceBrightnessSensitivityResult": "334.34 K",
  },
  {
    "subarray_configuration": "AA4",
    "freq_centre": "200",
    "bandwidth": "300",
    "ra": "00:00:00",
    "dec": "00:00:00",
    "minElevation": "20",
    "duration": "1",
    "imageWeighting": "Briggs",
    "robustValue": "2",
    "continuumWeightedSensitivityResult": "5.43 uJy/beam (1.00)†",
    "continuumConfusionNoiseResult": "N/A (non-Gaussian beam)",
    "continuumSensitivityResult": "N/A (non-Gaussian beam)",
    "continuumSynthesizedBeamSizeResult": "N/A (non-Gaussian beam)",
    "continuumSurfaceBrightnessSensitivityResult": "N/A (non-Gaussian beam)",
  },
  {
    "subarray_configuration": "AA4",
    "freq_centre": "200",
    "bandwidth": "300",
    "ra": "00:00:00",
    "dec": "00:00:00",
    "minElevation": "15",
    "duration": "1",
    "continuumSurfaceBrightnessSensitivityResult": "N/A (non-Gaussian beam)",
    "imageWeighting": "Natural",
    "continuumWeightedSensitivityResult": "5.43 uJy/beam (1.00)†",
    "continuumConfusionNoiseResult": "N/A (non-Gaussian beam)",
    "continuumSensitivityResult": "N/A (non-Gaussian beam)",
    "continuumSynthesizedBeamSizeResult": "N/A (non-Gaussian beam)",
  },
  {
    "subarray_configuration": "Custom",
    "freq_centre": "200",
    "bandwidth": "300",
    "ra": "00:00:00",
    "dec": "00:00:00",
    "minElevation": "20",
    "duration": "1",
    "continuumWeightedSensitivityResult": "5.43 uJy/beam (1.00)†",
    "continuumConfusionNoiseResult": "N/A (no beam information available)",
    "continuumSensitivityResult": "N/A (no beam information available)",
    "continuumSynthesizedBeamSizeResult": "N/A (no beam information available)",
    "continuumSurfaceBrightnessSensitivityResult": "N/A (no beam information available)",
  },
]



describe('Calculate LOW sensitivity and confusion noise in continuum mode', () => {
  beforeEach(() => {
    cy.visit('/low')
    cy.wait(300)
  })

  for (const params of input_and_expected_results) {

    it('should return a sensitivity in the results panel when valid inputs are given', () => {
      cy.getByLabel("Subarray Configuration").click().get('mat-option').contains(params.subarray_configuration).click()
      cy.getByLabel("Right Ascension").clear().type(params.ra)
      cy.getByLabel("Declination").clear().type(params.dec)
      cy.getByLabel("Minimum Elevation").clear().type(params.minElevation)

      cy.openPanel('#continuumExpansionPanel')

      cy.getByLabel("Integration Time").clear().type(params.duration)
      cy.getByLabel("Central Frequency").clear().type(params.freq_centre)
      cy.getByLabel("Continuum Bandwidth").clear().type(params.bandwidth)

      if(params.subarray_configuration!="Custom"){
        cy.getByLabel('Image Weighting').click().get('mat-option').contains(params.imageWeighting).click();
      }

      if(params.imageWeighting=="Briggs"){
        const robustRegExp = new RegExp("^\\s*" + params.robustValue + "\\s*$")
        cy.getByLabel('Robust Value').click().get('mat-option').contains(robustRegExp).click();
      }

      cy.contains("Calculate").click()

      cy.get('#continuumWeightedSensitivity', {timeout: 25000}).should('contain.text', params.continuumWeightedSensitivityResult);
      cy.get('#continuumConfusionNoise').should('contain.text', params.continuumConfusionNoiseResult);
      cy.get('#continuumTotalSensitivity').should('contain.text', params.continuumSensitivityResult);
      cy.get('#continuumSynthesizedBeamSize').should('contain.text', params.continuumSynthesizedBeamSizeResult);
      cy.get('#continuumSurfaceBrightnessSensitivity').should('contain.text', params.continuumSurfaceBrightnessSensitivityResult);
      cy.contains('Warning: You are approaching the confusion limit given the synthesized beam-size and frequency.').should('not.exist')
    })

  }


  it('should show the integration time different warning when different times are entered in the two accordions', () => {
    cy.openPanel('#continuumExpansionPanel');
    cy.openPanel('#zoomExpansionPanel');
    cy.getByLabel('Integration Time').clear().type('0.5');

    cy.contains("Calculate").click();

    cy.get('#contIntegrationTimeWarning').should('exist');
    cy.get('#contIntegrationTimeWarning')
        .should('be.visible')
        .contains('Warning: the integration times differ in other accordions.');

    cy.get('#zoomIntegrationTimeWarning').should('exist');
    cy.get('#zoomIntegrationTimeWarning')
        .should('be.visible')
        .contains('Warning: the integration times differ in other accordions.');
  })

  const sens = 13.66

  it('should return a spectral sensitivity improved by a factor 2 for a Spectral Averaging factor of 4', () => {
    cy.openPanel('#continuumExpansionPanel');
    cy.contains("Calculate").click();

    cy.get('#weightedSpectralSensitivity', {timeout: 25000}).should('contain.text', sens);
  })

  it('should return a spectral sensitivity improved by a factor 2 for a Spectral Averaging factor of 4', () => {
    cy.openPanel('#continuumExpansionPanel');
    cy.getByLabel('Spectral Averaging', '#continuumExpansionPanel').clear().type(4);
    cy.contains("Calculate").click();
    cy.get('#weightedSpectralSensitivity', {timeout: 25000}).should('contain.text', sens / 2.0);
  })
})

