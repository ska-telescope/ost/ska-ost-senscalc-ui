let input_and_expected_results = [
  {
    "subarray_configuration": "AA4 (core only)",
    "freq_centre": "150",
    "ra": "08:00:00",
    "dec": "20:00:50",
    "minElevation": "20",
    "duration": "0.5",
    "dispersion_measure": "0.0",
    "intrinsic_pulse_width": "0.004",
    "pulse_period": "33",
    "result": "49.60 uJy",
  }
]

describe('Calculate LOW sensitivity and confusion noise in continuum mode', () => {
  beforeEach(() => {
    cy.visit('/low')
    cy.wait(300)
  })

  for (const params of input_and_expected_results) {

    it('should return a sensitivity in the results panel when valid inputs are given', () => {
      cy.getByLabel("Subarray Configuration").click().get('mat-option').contains(params.subarray_configuration).click()
      cy.getByLabel("Right Ascension").clear().type(params.ra)
      cy.getByLabel("Declination").clear().type(params.dec)
      cy.getByLabel("Minimum Elevation").clear().type(params.minElevation)

      cy.openPanel('#pssExpansionPanel')

      cy.getByLabel("Integration Time", '#pssExpansionPanel').clear().type(params.duration)
      cy.getByLabel("Pulse Period").clear().type(params.pulse_period)
      cy.getByLabel("Intrinsic Pulse Width").clear().type(params.intrinsic_pulse_width)
      cy.getByLabel("Dispersion Measure").clear().type(params.dispersion_measure)

      cy.contains("Calculate").click()

      cy.get('#pssSensitivity', {timeout: 25000}).should('contain.text', params.result);
    })

  }

  it('should display error from back-end instead of a calculation result when pulse width is larger than pulse period', () => {
    // Check that no results or errors are currently displayed
    cy.get("#pssSensitivity").should('not.contain.text', 'Jy');
    cy.get('.mat-simple-snack-bar-content').should('not.exist');
    cy.getByLabel("Subarray Configuration").click().get('mat-option').contains("AA4 (core only)").click();

    cy.openPanel('#pssExpansionPanel');

    cy.getByLabel("Pulse Period").clear().type("7.2")
    cy.getByLabel("Intrinsic Pulse Width").clear().type("1")
    cy.getByLabel("Dispersion Measure").clear().type("1000")

    cy.contains("Calculate").click();

    cy.get('.mat-simple-snack-bar-content').should(
      'contain.text',
      'The effective pulse width (due to interstellar dispersion and scattering) is larger than the pulse period. Check your inputs.'
    );
    // No result should have been calculated
    cy.get("#pssSensitivity").should('not.contain.text', 'Jy');
  })

  it('should show the integration time different warning when different times are entered in accordions', () => {
    // Choose an array that allows PSS
    cy.getByLabel("Subarray Configuration").click().get('mat-option').contains("AA4 (core only)").click();
    cy.openPanel('#continuumExpansionPanel');
    cy.openPanel('#pssExpansionPanel');
    cy.getByLabel('Integration Time').clear().type('0.5');

    cy.contains("Calculate").click();

    cy.get('#contIntegrationTimeWarning').should('exist');
    cy.get('#contIntegrationTimeWarning')
        .should('be.visible')
        .contains('Warning: the integration times differ in other accordions.');

    cy.get('#pssIntegrationTimeWarning').should('exist');
    cy.get('#pssIntegrationTimeWarning')
        .should('be.visible')
        .contains('Warning: the integration times differ in other accordions.');

    cy.contains("Reset").click();

    // Reset has changed subarray and disabled PSS, choose correct subarray again and open PSS tab
    cy.getByLabel("Subarray Configuration").click().get('mat-option').contains("AA4 (core only)").click();
    cy.openPanel('#pssExpansionPanel');

    // Check that the warning has cleared
    cy.get('#pssIntegrationTimeWarning').should('not.exist');

    cy.closePanel('#continuumExpansionPanel');
    cy.openPanel('#zoomExpansionPanel');

    cy.getByLabel('Integration Time').clear().type('0.5');
    cy.contains("Calculate").click();

    cy.get('#zoomIntegrationTimeWarning').should('exist');
    cy.get('#zoomIntegrationTimeWarning')
        .should('be.visible')
        .contains('Warning: the integration times differ in other accordions.');

    cy.get('#pssIntegrationTimeWarning').should('exist');
    cy.get('#pssIntegrationTimeWarning')
        .should('be.visible')
        .contains('Warning: the integration times differ in other accordions.');
  })

  it('should show the bandwidth and chanel width fields are disabled and the rest enabled', () => {
    // Choose an array that allows PSS
    cy.getByLabel("Subarray Configuration").click().get('mat-option').contains("AA4 (core only)").click();
    cy.openPanel('#continuumExpansionPanel');
    cy.openPanel('#pssExpansionPanel');
    cy.getByLabel("Pulse Period").should('be.enabled');
    cy.getByLabel("Intrinsic Pulse Width").should('be.enabled');
    cy.getByLabel("Integration Time").should('be.enabled');
    cy.getByLabel("Central Frequency").should('be.enabled');
    cy.getByLabel("Bandwidth").should('be.disabled');
    cy.getByLabel("Channel Width").should('be.disabled');
    cy.getByLabel("Dispersion Measure").should('be.enabled');
  })
})

