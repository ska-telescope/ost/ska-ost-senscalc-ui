let data_input_and_expected_results = [
  {
    "subarray_configuration": "AA4 (core only)",
    "freq_centre": "100",
    "bandwidth_spectral_resolution": "3125.0 kHz (9368.5 km/s), 1808.4 Hz (5.4 km/s)",
    "ra": "00:00:00",
    "dec": "00:00:00",
    "minElevation": "20",
    "duration": "1",
    "spectral_averaging": "1",
    "imageWeighting": "Uniform",
    "zoomSpectralWeightedSensitivityResult": "9.96 mJy/beam (2.58)‡",
    "zoomConfusionNoiseResult": "52.61 mJy/beam",
    "zoomSensitivityResult": "53.54 mJy/beam",
    "zoomSynthesizedBeamSizeResult": "451.1\" x 403.1\"",
    "zoomSurfaceBrightnessSensitivityResult": "35.98 K"
  },
  {
    "subarray_configuration": "AA4 (core only)",
    "freq_centre": "150",
    "bandwidth_spectral_resolution": "3125.0 kHz (6245.7 km/s), 1808.4 Hz (3.6 km/s)",
    "ra": "00:00:00",
    "dec": "00:00:00",
    "minElevation": "20",
    "duration": "1",
    "spectral_averaging": "1",
    "imageWeighting": "Uniform",
    "zoomSpectralWeightedSensitivityResult": "9.84 mJy/beam (2.58)‡",
    "zoomConfusionNoiseResult": "14.53 mJy/beam",
    "zoomSensitivityResult": "17.55 mJy/beam",
    "zoomSynthesizedBeamSizeResult": "300.8\" x 268.7\"",
    "zoomSurfaceBrightnessSensitivityResult": "11.79 K",
  },
  {
    "subarray_configuration": "AA4 (core only)",
    "freq_centre": "300",
    "bandwidth_spectral_resolution": "3125.0 kHz (3122.8 km/s), 1808.4 Hz (1.8 km/s)",
    "ra": "00:00:00",
    "dec": "00:00:00",
    "minElevation": "20",
    "duration": "1",
    "spectral_averaging": "1",
    "imageWeighting": "Uniform",
    "zoomSpectralWeightedSensitivityResult": "10.58 mJy/beam (2.58)‡",
    "zoomConfusionNoiseResult": "1.56 mJy/beam",
    "zoomSensitivityResult": "10.70 mJy/beam",
    "zoomSynthesizedBeamSizeResult": "150.4\" x 134.4\"",
    "zoomSurfaceBrightnessSensitivityResult": "7.19 K",
  },
  {
    "subarray_configuration": "AA4 (core only)",
    "freq_centre": "100",
    "bandwidth_spectral_resolution": "3125.0 kHz (9368.5 km/s), 1808.4 Hz (5.4 km/s)",
    "ra": "00:00:00",
    "dec": "00:00:00",
    "minElevation": "20",
    "duration": "1",
    "spectral_averaging": "1",
    "imageWeighting": "Natural",
    "zoomSpectralWeightedSensitivityResult": "3.86 mJy/beam (1.00)‡",
    "zoomConfusionNoiseResult": "N/A (non-Gaussian beam)",
    "zoomSensitivityResult": "N/A (non-Gaussian beam)",
    "zoomSynthesizedBeamSizeResult": "N/A (non-Gaussian beam)",
    "zoomSurfaceBrightnessSensitivityResult": "N/A (non-Gaussian beam)",
  },
  {
    "subarray_configuration": "AA4 (core only)",
    "freq_centre": "100",
    "bandwidth_spectral_resolution": "3125.0 kHz (9368.5 km/s), 1808.4 Hz (5.4 km/s)",
    "ra": "00:00:00",
    "dec": "00:00:00",
    "minElevation": "20",
    "duration": "1",
    "spectral_averaging": "1",
    "imageWeighting": "Briggs",
    "robustValue": "2",
    "zoomSpectralWeightedSensitivityResult": "3.86 mJy/beam (1.00)‡",
    "zoomConfusionNoiseResult": "N/A (non-Gaussian beam)",
    "zoomSensitivityResult": "N/A (non-Gaussian beam)",
    "zoomSynthesizedBeamSizeResult": "N/A (non-Gaussian beam)",
    "zoomSurfaceBrightnessSensitivityResult": "N/A (non-Gaussian beam)",
  },
  {
    "subarray_configuration": "Custom",
    "freq_centre": "100",
    "bandwidth_spectral_resolution": "3125.0 kHz (9368.5 km/s), 1808.4 Hz (5.4 km/s)",
    "ra": "00:00:00",
    "dec": "00:00:00",
    "minElevation": "20",
    "duration": "1",
    "spectral_averaging": "1",
    "zoomSpectralWeightedSensitivityResult": "1.68 mJy/beam (1.00)‡",
    "zoomConfusionNoiseResult": "N/A (no beam information available)",
    "zoomSensitivityResult": "N/A (no beam information available)",
    "zoomSynthesizedBeamSizeResult": "N/A (no beam information available)",
    "zoomSurfaceBrightnessSensitivityResult": "N/A (no beam information available)",
  },
  {
    "subarray_configuration": "AA4 (core only)",
    "freq_centre": "100",
    "bandwidth_spectral_resolution": "3125.0 kHz (9368.5 km/s), 1808.4 Hz (5.4 km/s)",
    "ra": "00:00:00",
    "dec": "00:00:00",
    "minElevation": "20",
    "duration": "1",
    "spectral_averaging": "4",
    "imageWeighting": "Natural",
    "zoomSpectralWeightedSensitivityResult": "1.93 mJy/beam (1.00)‡",
    "zoomConfusionNoiseResult": "N/A (non-Gaussian beam)",
    "zoomSensitivityResult": "N/A (non-Gaussian beam)",
    "zoomSynthesizedBeamSizeResult": "N/A (non-Gaussian beam)",
    "zoomSurfaceBrightnessSensitivityResult": "N/A (non-Gaussian beam)",
  },
]

describe('Calculate LOW sensitivity and confusion noise in zoom window mode', () => {
  beforeEach(() => {
    cy.visit('/low')
    cy.wait(300)
  })

  for (const params of data_input_and_expected_results) {

    it('should return a sensitivity in the results panel when valid inputs are given', () => {
      cy.getByLabel("Subarray Configuration").click().get('mat-option').contains(params.subarray_configuration).click()
      cy.getByLabel("Right Ascension").clear().type(params.ra)
      cy.getByLabel("Declination").clear().type(params.dec)
      cy.getByLabel("Minimum Elevation").clear().type(params.minElevation)

      cy.openPanel('#zoomExpansionPanel')

      cy.getByLabel('Integration Time', '#zoomExpansionPanel').clear().type(params.duration)

      cy.getByLabel('Central Frequency', '#zoomExpansionPanel').clear().type(params.freq_centre)
      cy.getByLabel("Bandwidth, Spectral Resolution").click().get('mat-option').contains(params.bandwidth_spectral_resolution).click()
      cy.getByLabel('Spectral Averaging', '#zoomExpansionPanel').clear().type(params.spectral_averaging)

      if (params.subarray_configuration != "Custom"){
        cy.getByLabel('Image Weighting', '#zoomExpansionPanel').click().get('mat-option').contains(params.imageWeighting).click();
      }

      if(params.imageWeighting=="Briggs"){
        const robustRegExp = new RegExp("^\\s*" + params.robustValue + "\\s*$")
        cy.getByLabel('Robust Value', '#zoomExpansionPanel').click().get('mat-option').contains(robustRegExp).click();
      }

      cy.contains("Calculate").click()

      cy.get('#spectralWeightedSensitivity', {timeout: 25000}).should('contain.text', params.zoomSpectralWeightedSensitivityResult);
      cy.get('#spectralConfusionNoise').should('contain.text', params.zoomConfusionNoiseResult);
      cy.get('#spectralTotalSensitivity').should('contain.text', params.zoomSensitivityResult);
      cy.get('#spectralSynthesizedBeamSize').should('contain.text', params.zoomSynthesizedBeamSizeResult);
      cy.get('#spectralSurfaceBrightnessSensitivity').should('contain.text', params.zoomSurfaceBrightnessSensitivityResult);

      if(params.subarray_configuration=="Custom" || params.imageWeighting=="Natural" || params.imageWeighting=="Briggs"){
        cy.contains('Warning: You are approaching the confusion limit given the synthesized beam-size and frequency.').should('not.exist')
      }
    })
  }
})
