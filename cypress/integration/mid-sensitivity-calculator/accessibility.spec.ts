
describe('The MID calculator should meet accessibility requirements', () => {
  beforeEach(() => {
    cy.visit('/mid')
    cy.wait(800)
  })

  it('should be able to tab to the mid/low toggle and change telescope', () => {
   cy.tab() // Start at the first element
     .tab() // Tab to mid button
     .tab() // Tab to low button
     .type('{enter}') // Toggle to low

    cy.url().should('contain', "/low")
  })

    it('should be able to tab through the fields, enter some values, move to the calculate button and calculate using the enter key', () => {
    const input = {
      "subBands": 2,
      "ra": "08:00:00",
      "dec": "20:00:50",
      "elevation": 39.2,
    }
    cy.tab() // Initial tab to the SKAO logo
     .tab() // Tab to mid toggle
     .tab() // Tab to low toggle
     .tab() // Tab to advanced toggle
     .tab() // Tab to dark/light mode button
     .tab() // Tab to info button
     .tab().type( "{downArrow}") // Tab to observing band field, change option down 1
     .tab() // Tab to weather PWV input field
     .tab().type( "{downArrow}") // Tab to array/configuration field, change option down 1
     .tab() // Tab to Degree toggle
     .tab().type(input.ra) // (number of antenna both disabled)
     .tab().type(input.dec)
     .tab().type(input.elevation) // Tab to elevation input field
     .tab().type('{enter}')  // Tab to continuum panel
     .tab() // Tab to supplied field
     .tab() // Tab to integration time input field
     .tab() // Tab to integration time dropdown unit field
     .type("{enter}", {force: true}) // Open the integration time units
     .type( "{downArrow}", {force: true}) // change to us units
     .type("{enter}", {force: true}) // select the units and close the menu
     .tab() // Tab to Central Frequency input field
     .tab() // Tab to Central Frequency dropdown unit field
     .tab() // Tab to continuum bandwidth input field
     .tab() // Tab to continuum bandwidth unit field
     .tab().type(input.subBands)// Tab to sub-bands number input field
     .tab() // Tab to spectral resolution input field
     .tab() // Tab to spectral averaging input field
     .tab() // Tab to effective resolution input field
     .tab() // Tab to image weighting dropdowm field
     .tab() // Tab to tapering dropdown field
     .tab() // Tab to zoom panel
     .tab() // Tab to Reset button
     .tab().type('{enter}') // Tab to Calculate button and send the request using the Enter key

      cy.contains('button', 'Calculate').should('not.be.disabled')
      cy.get('#continuumExpansionPanel > .mat-expansion-panel-header')
        .invoke('attr','aria-expanded')
        .should('eq','true')
      cy.get('#zoomExpansionPanel > .mat-expansion-panel-header')
        .invoke('attr','aria-expanded')
        .should('eq','false')
  });

//  assuming the continuum panel is default opened and zoom is closed
  it('should be able to tab through the fields, close the continuum panel. This should disable the calculate button', () => {

    cy.tab() // Initial tab to the SKAO logo
     .tab() // Tab to mid button
     .tab() // Tab to low button
     .tab() // Tab to advanced toggle
     .tab() // Tab to dark/light mode button
     .tab() // Tab to info button
     .tab() // Tab to observing band
     .tab() // Tab to weather PWV input field
     .tab() // Tan tp array config
     .tab() // Tab to Degree toggle
     .tab().type("08:00:00") // Tab to RA (number of antenna both disabled)
     .tab().type("00:00:00") // Tab to dec
     .tab() // Tab to elevation

    // Check the calculate button is disabled by default (no accordions open)
    cy.contains('button', 'Calculate').should('be.disabled')
    cy.tab().should('contain.text', 'Continuum').type(' ')  // open continuum accordion

    // Check the calculate button is now enabled
    cy.contains('button', 'Calculate').should('be.enabled')
   });

  it('should be able to tab through the fields, close the continnum panel, open the zoom panel and move to the calculate button, and calculate using the enter key', () => {
    const input = {
      "ra": "08:00:00",
      "dec": "20:00:50",
      "elevation": 39.2,
    }

    cy.tab() // Initial tab to the SKAO logo
     .tab() // Tab to mid button
     .tab() // Tab to low button
     .tab() // Tab to advanced toggle
     .tab() // Tab to dark/light mode button
     .tab() // Tab to info button
     .tab().type( "{downArrow}") // Tab to observing band field, change option down 1
     .tab() // Tab to weather PWV input field
     .tab().type( "{downArrow}") // Tab to array/configuration field, change option down 1
     .tab() // Tab to Degree toggle
     .tab().type(input.ra)
     .tab().type(input.dec)
     .tab().type(input.elevation) // Tab to elevation input field
     .tab() // Tab to continuum panel
     .tab().type(' ')  // Tab to zoom panel and open panel
      .tab().type( "{downArrow}") // Tab to supplied field, change option down to sensitivity
     .tab() // Tab to sensitivity input field
     .tab() // Tab to sensitivity dropdown unit field
     .tab() // Tab to Central Frequency input field
     .tab() // Tab to Central Frequency dropdown unit field
     .tab()// Tab to continuum bandwidth input field
     .tab() // Tab to spectral averaging dropdown field
     .tab() // Tab to effective resolution input field
     .tab() // Tab to image weighting dropdown   field
     .tab() // Tab to tapering dropdown field
     .tab() // Tab to Reset button
     .tab() // Tab to Calculate button
     .type('{enter}') // Send the request using the Enter key

    // Check the request send successfully
      cy.contains('button', 'Calculate').should('not.be.disabled')
      cy.get('#continuumExpansionPanel > .mat-expansion-panel-header')
        .invoke('attr','aria-expanded')
        .should('eq','false')
      cy.get('#zoomExpansionPanel > .mat-expansion-panel-header')
        .invoke('attr','aria-expanded')
        .should('eq','true')
  });


  it('should be able to tab through the fields, open the zoom panel and move to the calculate button, and calculate using the enter key', () => {
    const input = {
      "ra": "08:00:00",
      "dec": "20:00:50",
      "elevation": 39.2,
      "subBands": 2,
      "result": 26.386
    }

    cy.tab() // Initial tab to the SKAO logo
     .tab() // Tab to mid toggle
     .tab() // Tab to low toggle
     .tab() // Tab to advanced toggle
     .tab() // Tab to dark/light mode button
     .tab() // Tab to info button
     .tab().type( "{downArrow}") // Tab to observing band field, change option down 1
     .tab() // Tab to weather PWV input field
     .tab().type( "{downArrow}") // Tab to array/configuration field, change option down 1 (number of antenna both disabled)
     .tab() // Tab to Degree toggle
     .tab().type(input.ra)
     .tab().type(input.dec)
     .tab().type(input.elevation) // Tab to elevation input field
     .tab().type("{enter}")  // Tab to continuum panel
     .tab().type( "{downArrow}") // Tab to supplied field, change option down to sensitivity
     .tab() // Tab to integration time input field
     .tab() // Tab to integration time dropdown unit field
     .tab() // Tab to Central Frquency input field
     .tab() // Tab to Central Frquency dropdown unit field
     .tab() // Tab to continuum bandwidth input field
     .tab() // Tab to continuum bandwidth unit field
     .tab().type(input.subBands)// Tab to sub-bands number input field
     .tab() // Tab to spectral resolution input field
     .tab() // Tab to spectral averaging input field
     .tab() // Tab to effective resolution input field
     .tab() // Tab to image weighting dropdown field
     .tab() // Tab to tapering dropdown field
     .tab() // Tab to Continuum Results panel
     .tab().type(' ')  // Tab to zoom panel and open panel
     .tab().type( "{downArrow}") // Tab to supplied field, change option down to sensitivity
     .tab() // Tab to integration time input field
     .tab() // Tab to integration time dropdown unit field
     .tab() // Tab to Central Frequency input field
     .tab() // Tab to Central Frequency dropdown unit field
     .tab()// Tab to continuum bandwidth input field
     .tab() // Tab to spectral averaging dropdown field
     .tab() // Tab to effective resolution input field
     .tab() // Tab to image weighting dropdowm field
     .tab() // Tab to tapering dropdown field
     .tab() // Tab to Zoom Results panel
     .tab() // Tab to Reset button
     .tab() // Tab to Calculate button
     .type('{enter}') // Send the request using the Enter key

    // Check the request sent successfully

      cy.contains('button', 'Calculate').should('not.be.disabled')
      cy.get('#continuumExpansionPanel > .mat-expansion-panel-header')
        .invoke('attr','aria-expanded')
        .should('eq','true')
      cy.get('#zoomExpansionPanel > .mat-expansion-panel-header')
        .invoke('attr','aria-expanded')
        .should('eq','true')
  });
});
