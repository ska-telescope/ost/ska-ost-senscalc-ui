import responses from '../../fixtures/responses'

// This spec covers tests for calculating the sensitivity in continuum panel

describe('Calculate sensitivity in continuum panel', () => {
  // This is the default page of the application execute before every test
  beforeEach(() => {
    cy.visit('/mid');
    cy.wait(800);
  })

  it('Verify sensitivity and time output on pre selected default input values', () => {
    // This test calculate the sensitivity in continuum panel with the
    // default input values.
    cy.getByLabel('Subarray Configuration').click().get('mat-option').contains(/^\sAA4\s*$/).click()
    // open the continnum panel if it is closed
    cy.openPanel('#continuumExpansionPanel')
    cy.getByLabel('Right Ascension').clear().type(responses.target.ra);
    cy.getByLabel('Declination').clear().type(responses.target.dec);
    cy.getByLabel('Number of sub-bands').clear().type(responses.subBand.numberOfSubBands);
    // click on the calculate button
    cy.contains("Calculate").click();
    // verify result output widget is rendered
    cy.get('app-continuum-results > .mat-card', {timeout: 3000}).should('be.visible');
    // verify title of the result output
    cy.get('.mat-card > .mat-card-title').contains('Results');
    // verify the content of the result output
    cy.get('#weightedContSensitivity').should('contain.text', responses.continuum.sensitivity);
    cy.get('#contSynthBeamSize').should('contain.text', responses.continuum.beamSize);
    cy.get('#contConfusionNoise').should('contain.text', responses.continuum.confusionNoise);
    cy.get('#totContSensitivity').should('contain.text', responses.continuum.totalSensitivity);
    cy.get('#contSurfaceBrightSensitivity').should('contain.text', responses.continuum.sbSensitivity);

    // verify sub-band section
    cy.get('#weightedContinuumSensitivityRange').should('contain.text', responses.subBand.sensitivity);
    cy.get('#confusionNoiseRange').should('contain.text', responses.subBand.confusionNoise);
    cy.get('#continuumSensitivityRange').should('contain.text', responses.subBand.totalSensitivity);
    cy.get('#synthesizedBeamSizeRange').should('contain.text', responses.subBand.beamSize);
    cy.get('#surfaceBrightnessSensitivityRange').should('contain.text', responses.subBand.sbSensitivity);

    cy.get('#weightedSpectralSensitivity').should('contain.text', responses.continuum.spectralSensitivity);
    cy.get('#spectralSynthBeamSize').should('contain.text', responses.continuum.spectralBeamSize);
    cy.get('#spectralConfNoise').should('contain.text', responses.continuum.spectralConfusionNoise);
    cy.get('#totSpectralSensitivity').should('contain.text', responses.continuum.totalSpectralSensitivity);
    cy.get('#spectralSurfaceBrightSensitivity').should('contain.text', responses.continuum.spectralSBSensitivity);
    cy.get('#sensLimitWarning').should('not.exist')
  })

  it('Verify sensitivity output when supplied input is selected as sensitivity', () => {
    // This test calculate the integration time in continuum panel with default input values.
    cy.getByLabel('Subarray Configuration').click().get('mat-option').contains(/^\sAA4\s*$/).click()
    // open the continnum panel if it is closed
    cy.openPanel('#continuumExpansionPanel')
    cy.getByLabel('Right Ascension').clear().type(responses.target.ra);
    cy.getByLabel('Declination').clear().type(responses.target.dec);
    //select supplied input as sensitivity
    cy.getByLabel('Supplied').click().get('mat-option').contains('Sensitivity').click()
    // click on the calculate button
    cy.contains("Calculate").click()
    cy.wait(300)
    // verify result output widget is rendered
    cy.get('app-continuum-results > .mat-card').should('be.visible');
    // verify title of the result output
    cy.get('.mat-card > .mat-card-title').contains('Results');
    // verify the content of the result output
    cy.get('#contSynthBeamSize').should('contain.text', responses.continuum.beamSize);
    cy.get('#contConfusionNoise').should('contain.text', responses.continuum.confusionNoise);
    cy.get('#contIntTime').should('contain.text', responses.continuum.integrationTime);

    cy.get('#spectralSynthBeamSize').should('contain.text', responses.continuum.spectralBeamSize);
    cy.get('#spectralConfNoise').should('contain.text', responses.continuum.spectralConfusionNoise);
    cy.get('#spectralIntTime').should('contain.text', responses.continuum.spectralIntegrationTime);
    cy.get('#sensLimitWarning').should('not.exist')
  })

  it('Verify confusion limit warning is displayed on selected input values', () => {
    // This test calculate the sensitivity in continuum panel with the
    // default input values.
    cy.getByLabel('Subarray Configuration').click().get('mat-option').contains(/^\sAA4\s*$/).click()
    // open the continnum panel if it is closed
    cy.openPanel('#continuumExpansionPanel')
    cy.getByLabel('Right Ascension').clear().type('01:00:00');
    cy.getByLabel('Declination').clear().type('-30:00:00');
    cy.getByLabel('Observing Band').click().get('mat-option').contains('Band 2').click();
    cy.getByLabel('Integration Time').clear().type('1800');
    cy.getByLabel('Central Frequency').clear().type('1.4');
    cy.getByLabel('Continuum Bandwidth').clear().type('0.5');
    cy.getByLabel('Image Weighting').click().get('mat-option').contains('Briggs').click();
    cy.getByLabel('Tapering').click().get('mat-option').contains('4.000').click();
    // click on the calculate button
    cy.contains("Calculate").click();
    cy.get('#sensLimitWarning').should('exist')
    cy.get('#sensLimitWarning')
        .should('be.visible')
        .contains('Warning: You are approaching the confusion limit given the synthesized beam-size and frequency.')
    cy.getByLabel('Supplied').click().get('mat-option').contains('Sensitivity').click();
    cy.getByLabel('Sensitivity').clear({force: true}).type('0.0000025');
    cy.contains("Calculate").click();
    cy.get('#sensLimitWarning').should('exist')
    cy.get('#sensLimitWarning')
        .should('be.visible')
        .contains('Warning: You are approaching the confusion limit (1.87 uJy/beam) given the synthesized beam-size and frequency.')
  })

  it('Verify confusion limit warning is displayed on decimal ra dec selected input values', () => {
    // This test calculate the sensitivity in continuum panel with the
    // default input values plus decimal Ra/Dec values.
    cy.getByLabel('Subarray Configuration').click().get('mat-option').contains(/^\sAA4\s*$/).click()
    // open the continnum panel if it is closed
    cy.openPanel('#continuumExpansionPanel')
    cy.get('#mat-slide-toggle-2 > .mat-slide-toggle-label').click();
    cy.getByLabel('Right Ascension').clear().type('15.0');
    cy.getByLabel('Declination').clear().type('-30.0');
    cy.getByLabel('Observing Band').click().get('mat-option').contains('Band 2').click();
    cy.getByLabel('Integration Time').clear().type('1800');
    cy.getByLabel('Central Frequency').clear().type('1.4');
    cy.getByLabel('Continuum Bandwidth').clear().type('0.5');
    cy.getByLabel('Image Weighting').click().get('mat-option').contains('Briggs').click();
    cy.getByLabel('Tapering').click().get('mat-option').contains('4.000').click();
    // click on the calculate button
    cy.contains("Calculate").click();
    cy.get('#sensLimitWarning').should('exist')
    cy.get('#sensLimitWarning')
        .should('be.visible')
        .contains('Warning: You are approaching the confusion limit given the synthesized beam-size and frequency.')
    cy.getByLabel('Supplied').click().get('mat-option').contains('Sensitivity').click();
    cy.getByLabel('Sensitivity').clear({force: true}).type('0.0000025');
    cy.contains("Calculate").click();
    cy.get('#sensLimitWarning').should('exist')
    cy.get('#sensLimitWarning')
        .should('be.visible')
        .contains('Warning: You are approaching the confusion limit (1.87 uJy/beam) given the synthesized beam-size and frequency.')
  })

  it('should show the confusion noise error when a sensitivity that is too small is entered', () => {
    cy.getByLabel('Subarray Configuration').click().get('mat-option').contains(/^\sAA4\s*$/).click()
    // open the continnum panel if it is closed
    cy.openPanel('#continuumExpansionPanel')
    cy.getByLabel('Right Ascension').clear().type('01:00:00');
    cy.getByLabel('Declination').clear().type('-30:00:00');
    cy.getByLabel('Observing Band').click().get('mat-option').contains('Band 2').click();
    cy.getByLabel('Supplied').click().get('mat-option').contains('Sensitivity').click();
    cy.getByLabel('Sensitivity').clear({force: true}).type('0.0000025');
    cy.getByLabel('Central Frequency').clear().type('1.4');
    cy.getByLabel('Continuum Bandwidth').clear().type('0.5');
    cy.getByLabel('Image Weighting').click().get('mat-option').contains('Briggs').click();
    cy.getByLabel('Tapering').click().get('mat-option').contains('256.000').click();
    cy.contains("Calculate").click();
    cy.get('#errorMsg').should('exist')
    cy.get('#errorMsg')
        .should('be.visible')
        .contains('Error: the entered sensitivity is less than or equal to the confusion noise (2.008 mJy/beam)')
  })

  it('should show the integration time different warning when different times are entered in the two accordions', () => {
    cy.openPanel('#continuumExpansionPanel');
    cy.openPanel('#zoomExpansionPanel');
    cy.getByLabel('Integration Time').clear().type('1800');

    cy.contains("Calculate").click();

    cy.get('#contIntegrationTimeWarning').should('exist');
    cy.get('#contIntegrationTimeWarning')
        .should('be.visible')
        .contains('Warning: the integration times differ in other accordions.');

    cy.get('#zoomIntegrationTimeWarning').should('exist');
    cy.get('#zoomIntegrationTimeWarning')
        .should('be.visible')
        .contains('Warning: the integration times differ in other accordions.');
  })
})
