const config = require('src/ska_ost_senscalc_ui/assets/configuration.json')

describe('Incorrect input parameters display an error',
  () => {
    // This is the default page of the application execute before every test
    beforeEach(() => {
      cy.visit('/mid')
      cy.wait(1000)
    })

    const bands = ['Band 1', 'Band 2', 'Band 5a', 'Band 5b'];

    // Examples for array strings displayed in the UI with only SKA antennas,
    // only MeerKAT antennas and with both antennas
    const arrayExamples = {
      'ska': 'AA* (15-m antennas only)',
      'meerkat': 'AA*/AA4 (13.5-m antennas only)',
      'mixed': /^\sAA4\s*$/
    }

    bands.forEach((band: string) => {
      it(`Verify all the populated input values on selection of ${band} in continuum mode`, () => {
        const arrays = getBandArrays(band)
        arrays.forEach((array: string) => {
          const [centralFreq, bandwidth, resolution] = getBandContDefaults(band, array);
          cy.getByLabel('Subarray Configuration').click().get('mat-option').contains(arrayExamples[array]).click()
          cy.inactivateInput();
          cy.getByLabel('Observing Band').click().get('mat-option').contains(band).click()
          cy.openPanel('#continuumExpansionPanel')

          cy.getByLabel('Central Frequency').should('have.value', centralFreq)
          cy.getByLabel('Continuum Bandwidth').should('have.value', bandwidth)
          cy.getByLabel('Spectral Resolution').should('have.value', resolution)

          //integration time should be the same for all bands
          cy.getByLabel('Integration Time').should('have.value', '600')
        })
      })
    })

    bands.forEach((band: string) => {
      it(`Verify all the populated input values on selection of ${band} in zoom mode`, () => {
        const arrays = getBandArrays(band)
        arrays.forEach((array: string) => {
          cy.getByLabel('Subarray Configuration').click().get('mat-option').contains(arrayExamples[array]).click()
          cy.inactivateInput();
          cy.getByLabel('Observing Band').click().get('mat-option').contains(band).click()
          cy.openPanel('#zoomExpansionPanel')

          const freqDefault = getZoomFreqDefault(band, array)
          cy.getByLabel('Central Frequency', '#zoomExpansionPanel').should('have.value', freqDefault)
          // Integration time should be the same for all bands
          cy.getByLabel('Integration Time').should('have.value', '600')
        })
      });
    });

    it('Verify PWV input is within limits', () => {
      // This test confirms that error messages are displayed when the PWV
      // input is out of range.

      const pwv_range = config.midCalcFormFields.find((e: any) => e.field == 'fieldWeatherPWV')!.defaultValue

      // test outside lower limit
      cy.get('#txtWeatherPWV').clear().type((pwv_range.min - 0.01).toString())
      cy.inactivateInput();
      cy.get('#errWeatherPWV')
        .should('be.visible')
        .and('contain.text', 'Please enter a number between 3 and 25')

      // test outside upper limit
      cy.get('#txtWeatherPWV').clear().type((pwv_range.max + 0.01).toString())
      cy.inactivateInput();
      cy.get('#errWeatherPWV')
        .should('be.visible')
        .and('contain.text', 'Please enter a number between 3 and 25')
    })

    it('Verify elevation input is within limits', () => {
      // This test confirms that error messages are displayed when the
      // elevation input is out of range.

      const el_range = config.midCalcFormFields.find((e: any) => e.field == 'fieldElevation')!.defaultValue
      cy.getByLabel('Declination').clear().type("-31:00:00")

      // test outside lower limit
      // force clear elevation field as Cypress sometimes incorrectly assumes the field is
      // still disabled and throws an error on clear() if not forced
      cy.getByLabel('Elevation').clear({force: true}).type((el_range.min - 1).toString())
      cy.inactivateInput();
      cy.getErrorByLabel('Elevation')
        .should('be.visible')
        .contains('Please enter a number between 15 and')

      // test outside upper limit
      cy.getByLabel('Elevation').clear().type((el_range.max + 1).toString())
      cy.inactivateInput();
      cy.getErrorByLabel('Elevation')
        .should('be.visible')
        .contains('Please enter a number between 15 and')
    })

    it('Verify Elevation field is inactive if illegal Declination value is entered', () => {
      // Test to verify Elevation behaviour
      cy.getByLabel('Declination').clear().type('+60:00:00')
      cy.inactivateInput();
      cy.getByLabel('Elevation').should('be.disabled')
    })

    it('Verify invalid declination input displays error', () => {
      cy.getByLabel('Declination').clear().type('0:0:0:0:0')
      cy.inactivateInput();
      cy.getErrorByLabel('Declination')
        .should('be.visible')
        .contains('Input formatted incorrectly')

      cy.getByLabel('Declination').clear().type('+60:00:00')
      cy.inactivateInput();
      cy.getErrorByLabel('Declination')
        .should('be.visible')
        .contains('Target does not reach minimum elevation of 15 degrees')
    })

    it('Verify Declination and Right Ascension values are padded with a trailing "0" if number ends in "."', () => {
      cy.getByLabel('Right Ascension').clear().type('01:02:03.')
      cy.inactivateInput();
      cy.getByLabel('Right Ascension').should('have.value','01:02:03.0');

      cy.getByLabel('Declination').clear().type('-60:00:00.')
      cy.inactivateInput();
      cy.getByLabel('Declination').should('have.value','-60:00:00.0')
    })

    it('Verify decimal Right Ascension values are correctly formatted', () => {
      cy.get('[data-cy=sexDecToggle]').click();
      cy.getByLabel('Right Ascension').clear().type('1.')
      cy.inactivateInput();
      cy.getByLabel('Right Ascension').should('have.value','1.0');
      cy.getByLabel('Right Ascension').clear().type('1')
      cy.inactivateInput();
      cy.getByLabel('Right Ascension').should('have.value','1.0');
      cy.getByLabel('Right Ascension').clear().type('1.234')
      cy.inactivateInput();
      cy.getByLabel('Right Ascension').should('have.value','1.234');
      cy.getByLabel('Right Ascension').clear().type('aaa')
      cy.inactivateInput();
      cy.getByLabel('Right Ascension').should('have.value','aaa'); // Dont format junk
      cy.getErrorByLabel('Right Ascension')
      .should('be.visible')
      .contains('Input formatted incorrectly')

      cy.getByLabel('Declination').clear().type('1.')
      cy.inactivateInput();
      cy.getByLabel('Declination').should('have.value','1.0');
      cy.getByLabel('Declination').clear().type('1')
      cy.inactivateInput();
      cy.getByLabel('Declination').should('have.value','1.0');
      cy.getByLabel('Declination').clear().type('1.234')
      cy.inactivateInput();
      cy.getByLabel('Declination').should('have.value','1.234');
      cy.getByLabel('Declination').clear().type('aaa')
      cy.inactivateInput();
      cy.getByLabel('Declination').should('have.value','aaa'); // Dont format junk
      cy.getErrorByLabel('Declination')
      .should('be.visible')
      .contains('Input formatted incorrectly')
    })

    it('Verify Right Ascension value in correct format', () => {
      // Test to verify Right Ascension value
      cy.getByLabel('Right Ascension').clear().type('0:0:0:0:0')
      cy.inactivateInput();
      cy.getErrorByLabel('Right Ascension')
        .should('be.visible')
        .contains('Input formatted incorrectly')
    })

    it('Verify Integration Time value must not be empty', () => {
      cy.openPanel('#continuumExpansionPanel')
      cy.getByLabel('Integration Time').clear()
      cy.inactivateInput();
      cy.getErrorByLabel('Integration Time')
        .should('be.visible')
        .contains('Field cannot be empty')
    })

    it('Verify Central Frequency value within limit', () => {
      cy.openPanel('#continuumExpansionPanel')
      // Test to verify Central Frequency value
      cy.getByLabel('Central Frequency').clear()
      cy.inactivateInput();
      cy.getErrorByLabel('Central Frequency')
        .should('be.visible')
        .contains('Please enter a number')
      cy.getByLabel('Central Frequency').clear().type('10')
      cy.inactivateInput();
      cy.getErrorByLabel('Central Frequency')
        .should('be.visible')
        .contains('Observing frequency out of range')
      cy.getByLabel('Central Frequency').clear().type('0.1')
      cy.inactivateInput();
      cy.getErrorByLabel('Central Frequency')
        .should('be.visible')
        .contains('Observing frequency out of range')
    })

    it('Verify Continuum Bandwidth value within limit', () => {
      cy.openPanel('#continuumExpansionPanel')
      // Test to verify Continuum Bandwidth Frequency value
      cy.getByLabel('Continuum Bandwidth').clear()
      cy.inactivateInput();
      cy.getErrorByLabel('Continuum Bandwidth')
        .should('be.visible')
        .contains('Please enter a number')
      cy.getByLabel('Continuum Bandwidth').clear().type('0.8')
      cy.inactivateInput();
      cy.getErrorByLabel('Continuum Bandwidth')
        .should('be.visible')
        .contains('Bandwidth must be fully contained within the band')
    })

    it('Verify Number of sub-bands value positive number', () => {
      cy.openPanel('#continuumExpansionPanel')
      // Test to verify Number of sub-bands value
      cy.getByLabel('Number of sub-bands').clear().type('0')
      cy.inactivateInput();
      cy.getErrorByLabel('Number of sub-bands')
        .should('be.visible')
        .contains('Please enter a number between 1 and 32')
    })
  })

/**
 * Returns the default frequency, bandwidth and resolution defined in the configuration
 * file for the given observing band and array type.
 *
 * @param bandStr : string The observing band to get default values for
 * @param arrayStr : string The array type to get default values for. Allowed inputs are 'ska'/'meerkat'/'mixed'
 */
function getBandContDefaults(bandStr: string, arrayStr: string): [string, string, string] {
  const bandDefaults = getBandDefaults(bandStr)
  const freq = bandDefaults.contFreqInput.find((f: any) => f.type == arrayStr).value
  const bw = bandDefaults.contBandInput.find((b: any) => b.type == arrayStr).value
  const res = config.midCalcFormFields.find((r: any) => r.field == "fieldResolution").defaultValue
  return [freq, bw, res]
}


/**
 * Returns default the zoom frequency defined in the configuration
 * file for the given observing band and array type ('ska'/'meerkat'/'mixed').
 *
 * @param bandStr : string The observing band to get default values for
 * @param arrayStr : string The array type to get default values for
 */
function getZoomFreqDefault(bandStr: string, arrayStr: string): string {
  const bandDefaults = getBandDefaults(bandStr)
  return bandDefaults.zoomFreqInput.find((f: any) => f.type == arrayStr).value
}

/**
 * This method returns a list of array types that are allowed for the given observing band.
 * The current array types used in the configuration file are 'ska', 'meerkat' and 'mixed'.
 *
 * @param bandStr : string The observing band to get array types for
 */
function getBandArrays(bandStr: string): string[] {
  const bandDefaults = getBandDefaults(bandStr)
  let bandArrays: string[] = []
  bandDefaults.bandLimits.forEach((defaultVal) => {
    bandArrays.push(defaultVal.type as string)
  })
  return bandArrays
}


/**
 * This method returns an object containing all default values for the
 * given observing band. The object should have the following fields:
 *  mode - the observing band
 *  description - observing band ranges to display to the user
 *  contFreqInput - the default central frequencies for the band
 *  contBandInput - the default bandwidths for the band
 *  bandLimits - upper and lower limits for the band
 *  zoomFreqInput - the default central frequency for the band in zoom mode
 *
 * @param bandStr : string The observing band to get array types for
 */
function getBandDefaults(bandStr: string): {} {
  return config.midCalcFormFields.find((e: any) => e.field == 'fieldObservingMode')!.defaultValue.find((b: any) => b.mode == bandStr)
}
