import responses from '../../fixtures/responses'

// This spec covers tests for calculating the sensitivity in zoom panel

describe('Calculate sensitivity in zoom panel', () => {
  // This is the default page of the application execute before every test
  beforeEach(() => {
    cy.visit('/mid');
    cy.wait(800);
  })

  const testCases = responses.zoom
  testCases.forEach((testCase) => {
//    Skip until Confusion noise is no longer displayed for natural weighting
    it('Verify sensitivity output for array  ' + testCase.array + ' and band ' + testCase.band, () => {
      // This test calculate the sensitivity in zoom panel with the
      cy.getByLabel('Right Ascension').clear().type(responses.target.ra);
      cy.getByLabel('Declination').clear().type(responses.target.dec);
      const arrayRegEx = new RegExp("^\\s*" + testCase.array + "\\s*$")
      cy.getByLabel('Subarray Configuration').click().get('mat-option').contains(arrayRegEx).click()
      cy.getByLabel('Observing Band').click().get('mat-option').contains(testCase.band).click()

      // open the zoom panel if it is closed
      cy.openPanel('#zoomExpansionPanel')

      // click on the calculate button
      cy.contains("Calculate").click();
      // verify result output widget is rendered
      cy.get('app-zoom-results > .mat-card', {timeout: 500}).should('be.visible');
      // verify title of the result output
      cy.get('.mat-card > .mat-card-title').contains('Results');
      // // verify result output content
      cy.get('#zoomExpansionPanel').within(() => { // Need to check within as some elements of the cont panel have the same ids
        cy.get('#zoomWeightedSpectralSensitivity').should('contain.text', testCase.sensitivity);
        cy.get('#zoomSpectralSynthesizedBeamSize').should('contain.text', testCase.beamSize);
        cy.get('#zoomSpectralConfusionNoise').should('contain.text', testCase.confusionNoise);
        cy.get('#zoomTotalSpectralSensitivity').should('contain.text', testCase.totalSensitivity);
        cy.get('#zoomSpectralSurfaceBrightnessSensitivity').should('contain.text', testCase.sbSensitivity);
        cy.get('#zoomSensLimitWarning').should('not.exist')
      })
    })

    // Skip until Confusion noise is no longer displayed for natural weighting
    it('Verify integration time output for array  ' + testCase.array + ' and band ' + testCase.band, () => {
      // This test calculate the integration time in zoom panel with default input values.
      cy.getByLabel('Right Ascension').clear().type(responses.target.ra);
      cy.getByLabel('Declination').clear().type(responses.target.dec);
      const arrayRegEx = new RegExp("^\\s*" + testCase.array + "\\s*$")
      cy.getByLabel('Subarray Configuration').click().get('mat-option').contains(arrayRegEx).click()
      cy.getByLabel('Observing Band').click().get('mat-option').contains(testCase.band).click()

      // open the zoom panel if it is closed
      cy.openPanel('#zoomExpansionPanel')
      //select supplied input as sensitivity
      cy.getByLabel('Supplied', '#zoomExpansionPanel').click({force: true}).get('mat-option').contains('Sensitivity').click()
      // click on the calculate button
      cy.contains("Calculate").click()
      // verify result output widget is rendered
      cy.get('app-zoom-results > .mat-card', {timeout: 500}).should('be.visible');
      // verify title of the result output
      cy.get('.mat-card > .mat-card-title').contains('Results');
      // verify result output content
      cy.get('#zoomSpectralSynthesizedBeamSize').should('contain.text', testCase.beamSize);
      cy.get('#zoomSpectralConfusionNoise').should('contain.text', testCase.confusionNoise);
      cy.get('#zoomSpectralIntegrationTime').should('contain.text', testCase.integrationTime);
      cy.get('#zoomSensLimitWarning').should('not.exist')
    })
  })

  it('Verify sensitivity and time output on selected input values gives the warning message', () => {
    // This test calculate the sensitivity in continuum panel with the
    // default input values.
    cy.getByLabel('Subarray Configuration').click().get('mat-option').contains(/^\sAA4\s*$/).click()
    // open the continnum panel if it is closed
    cy.openPanel('#zoomExpansionPanel')
    cy.getByLabel('Right Ascension').clear().type('01:00:00');
    cy.getByLabel('Declination').clear().type('-72:00:00');
    cy.getByLabel('Integration Time', '#zoomExpansionPanel').clear().type('518400');
    cy.getByLabel('Central Frequency', '#zoomExpansionPanel').clear().type('0.7');
    cy.getByLabel('Image Weighting', '#zoomExpansionPanel').click().get('mat-option').contains('Briggs').click();
    cy.getByLabel('Robust', '#zoomExpansionPanel').click().get('mat-option').contains(/^1$/).click();
    cy.getByLabel('Tapering', '#zoomExpansionPanel').click().get('mat-option').contains('32.000').click();
    // click on the calculate button
    cy.contains("Calculate").click();
    cy.get('#zoomSensLimitWarning').should('exist')
    cy.get('#zoomSensLimitWarning')
      .should('be.visible')
      .contains('Warning: You are approaching the confusion limit given the synthesized beam-size and frequency.')
    cy.getByLabel('Supplied', '#zoomExpansionPanel').click().get('mat-option').contains('Sensitivity').click();
    cy.getByLabel('Sensitivity', '#zoomExpansionPanel').clear({force: true}).type('0.00045');
    cy.contains("Calculate").click();
    cy.get('#zoomSensLimitWarning').should('exist')
    cy.get('#zoomSensLimitWarning')
      .should('be.visible')
      .contains('Warning: You are approaching the confusion limit (225.39 uJy/beam) given the synthesized beam-size and frequency.')
  })
});
