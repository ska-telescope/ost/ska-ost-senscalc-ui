import responses from '../fixtures/responses.json';

describe('Changing between calculator modes should preserve data', () => {
  const modeCases = [
    {first: 'low', then: 'mid'},
    {first: 'mid', then: 'low'}
  ]
  modeCases.forEach((mc) => {
    it(`should be possible to enter data in ${mc.first} mode, switch to ${mc.then} and return to find the same values`, () => {
      cy.visit(`/${mc.first}`);
      cy.wait(500);
      cy.getByLabel('Right Ascension').clear().type(responses.target.ra);
      cy.getByLabel('Declination').clear().type(responses.target.dec);
      cy.contains('button', mc.then, {matchCase: false}).click();
      cy.url().should('contain', `/${mc.then}`);
      cy.contains('button', mc.first, {matchCase: false}).click();
      cy.url().should('contain', `/${mc.first}`);
      cy.getByLabel('Declination').should('have.value', responses.target.dec);
      cy.getByLabel('Right Ascension').should('have.value', responses.target.ra);
    })
  })
})
