// ***********************************************
// This example namespace declaration will help
// with Intellisense and code completion in your
// IDE or Text Editor.
// ***********************************************
import Chainable = Cypress.Chainable;

declare namespace Cypress {
  interface Chainable<Subject = any> {
    getByLabel(param: any): Chainable;
    getErrorByLabel(param: any): Chainable;
  }
}

function getByLabel(label: any, insideSelector: any = null): Chainable {
  // If insideSelector is given then the label will be searched for only within that element.
  // If not it will be searched from the root.
  const elementToSearch = insideSelector ? cy.get(insideSelector) : cy
  return elementToSearch.contains('label', new RegExp("^" + label + "[ \*]*$"))
    .invoke('attr', 'for')
    .then((id) => {
      cy.get('#' + id)
    })
}

function getErrorByLabel(label: any): Chainable {
  return cy.contains('label', new RegExp("^" + label + "[ \*]*$"))
    .invoke('attr', 'for')
    .then((id) => {
      cy.get('#' + id)
      .invoke('attr', 'aria-describedby')
      .then((error_id) => {
        cy.get('#' + error_id)
      })
    })
}

Cypress.Commands.add('getByLabel', getByLabel);
Cypress.Commands.add('getErrorByLabel', getErrorByLabel);

function openPanel(id: string): void {
  // open the panel if it is closed
  cy.get(id + ' > .mat-expansion-panel-header')
    .invoke('attr', 'aria-expanded')
    .then((isExpanded) => {
      if (isExpanded === 'false') {
        // Click to open the panel
        cy.get(id).click();

        cy.get(id + ' > .mat-expansion-panel-header')
          .invoke('attr', 'aria-expanded')
          .then((isExpanded) => {
            // Assert that the panel is open
            expect(isExpanded).to.equal('true');
          });
      }
  });
}

function closePanel(id: string): void {
  // open the panel if it is closed
  cy.get(id + ' > .mat-expansion-panel-header')
    .invoke('attr', 'aria-expanded')
    .then((isExpanded) => {
      if (isExpanded === 'true') {
        // Click to open the panel
        cy.get(id).click();

        cy.get(id + ' > .mat-expansion-panel-header')
          .invoke('attr', 'aria-expanded')
          .then((isExpanded) => {
            // Assert that the panel is closed
            expect(isExpanded).to.equal('false');
          });
      }
  });
}

Cypress.Commands.add('openPanel', openPanel);
Cypress.Commands.add('closePanel', openPanel);

function inactivateInput(): void {
  cy.get('body').click(0,0)
}

Cypress.Commands.add('inactivateInput', inactivateInput);

// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
