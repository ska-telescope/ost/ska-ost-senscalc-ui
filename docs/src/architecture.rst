High Level Architecture
.......................

.. image:: _static/img/angular_structure_class.svg

**Figure 2** . Flowchart diagram showing the different parent and child angular components that make up the UI structure

.. image:: _static/img/angular_structure_high_level.png

**Figure 3** . High level diagram showing the different parent and child angular components that make up the UI structure in a nested format
