.. _confusionnoiselow:

===========================================
Confusion Noise implementation
===========================================

Feature Overview
-----------------
| This feature displays the confusion noise and total sensitivity for Low on Continuum and Zoom panels.
| The confusion noise is returned from the API and the total sensitivity is calculated using confusion noise and weighted sensitivity.

Files:
.......
|  `src/ska_ost_senscalc_ui/app/low-sensitivity-calculator/continuum-panel/continuum-results/continuum-results.component.ts`
|  `src/ska_ost_senscalc_ui/app/low-sensitivity-calculator/zoom-panel/zoom-results/zoom-results.component.ts`
|  `src/ska_ost_senscalc_ui/app/shared/utils/helpers.ts`
|  `src/ska_ost_senscalc_ui/app/shared/utils/constants.ts`

| These files contain several functions for performing calculations and data processing to display results for the low continuum and zoom panels.
| Altough 2 separate files, `continuum-results` and `zoom-results` for low are extremely similar. 
| The differences are mainly in the names of the variables, such as continuumConfusionNoise vs spectralConfusionNoise. Therefore this documentation for the implentation of confusion noise in Low covers both files.


Function: getContinuumConfusionNoise & getSpectralConfusionNoise
-----------------------------------------------------------------
This function returns the confusion noise value.

Parameters
.............
weightingResponse : LowWeightingCalculationResult
    The weighting response object.

Returns
........
number or undefined 
    A value of type `any`.

Usage
......
::

    const confusionNoise = getContinuumConfusionNoise(weightingResponse);
    const confusionNoise = getSpectralConfusionNoise(weightingResponse);

Notes
......

| The function first checks if the beam size is smaller than the minimum beam size limit. The minimum beam size limit for low is declared in constants.helpers.minGaussianBeamLow and is currently set at 1.5. This is the minimum beam size in the look-up table in the back-end and could potentially change in the future.
| If the beam size is smaller than the minimum limit, the confusion noise is set to 0. If not, the value returned from the API is used.
| This function should return a `number` value for the confusion noise, but could return `undefined` if the confusionNoise property doesn't exist on the `weightingResponse` object.


Function: getContinuumSurfaceBrightnessSensitivity & getSpectralSurfaceBrightnessSensitivity
---------------------------------------------------------------------------------------------
This function uses the total sensitivity and the surface brightness sensitivity conversion factor (sbs_conv_factor) from the `weightingResponse` to calculate the surface brightness sensitivity and returns it.

Parameters
.............
weightingResponse : LowWeightingCalculationResult
    The weighting response object.
continuumTotalSensitivity or spectralTotalSensitivity : number
    The calculated total sensitivity

Returns
........
number 

Usage
......
::

    const continuumSurfaceBrightnessSensitivity = this.getContinuumSurfaceBrightnessSensitivity(weightingResponse, continuumTotalSensitivity);
    const zoomSurfaceBrightnessSensitivity = this.getContinuumSurfaceBrightnessSensitivity(weightingResponse, spectralTotalSensitivity)


Function: minBeamSizeCheck
--------------------------------------------------------------------------
This function checks if the beam size in arcseconds is smaller than the minimum beam size limit.

Parameters
............
beam_min_scaled : number 
    The minor beam size in degrees
beam_maj_scaled : number
    The major beam size in degrees
precision : number
    The number of decimal places to display the result to

Returns
............
number  
    The calculated beam size in square arcseconds

Usage
............
::

    helpers.calculate.minBeamSizeCheck(weightingResponse, 1)

Notes
............
| The function first call helpers.calculate.calculateBeamSizeInArcseconds() to get the beam size in arcseconds, using the beam size info from the weightingResponse parameter.
| The minimum beam size limit for low is declared in constants.helpers.minGaussianBeamLow and is currently set at 1.5. This is the minimum beam size in the look-up table in the back-end and could potentially change in the future.


Function: displayResult
---------------------------
This function displays the results of the low continuum sensitivity calculation in the UI.

Parameters
............
calculateResponse : LowContinuumSensitivityResponse
    The response object from the low continuum sensitivity calculation.
weightingResponse : LowWeightingCalculationResult
    The weighting response object. 
showBeamAndSBS : boolean
    A flag indicating whether to display the synthesized beam size and surface brightness sensitivity.

Returns
............
None

Usage
............
::

    displayResult(calculateResponse, weightingResponse, showBeamAndSBS);

Notes
............
| This function is called after the low continuum sensitivity calculation is completed to display the results in the UI.
| It retrieves and formats the values to be displayed in the results panels.
| The showBeamAndSBS parameter is used to assess whether or not to display the continuumConfusionNoise / spectralConfusionNoise, continuumSynthesizedBeamSize / spectralSynthesizedBeamSize and continuumSurfaceBrightnessSensitivity / zoomSurfaceBrightnessSensitivity values.
| If showBeamAndSBS is false, the confusion noise needs to be retrieved and the total sensitivity needs to be calculated using the confusion noise value retrieved from the weightingResponse.
| To get the total sensitivity, the function helpers.calculate.sqrtOfSumSqs() is called with confusion noise and weighted sensitivity as parameters.
| The function selectSensitivityUnit() is also called to convert the total sensitivity into a display format with correct units to show in the UI.
| After calculating the results, the function sets the corresponding display values in the UI and checks for any warning messages in the calculateResponse object.
| Finally, the function sets the newCalc flag to true to prepare for any parameter changes.


Function: selectSensitivityUnit
------------------------------------
| This function selects the appropriate unit for displaying sensitivity values based on the input value. 
| It uses the convertValueNew function to convert the value to the appropriate unit.

Parameters
..............
value : any 
    The input value to be converted.

Returns
.........
string 
    The converted value with the appropriate unit and "/beam" appended to it.

Usage
......
::

    selectSensitivityUnit(value);


Function: convertValueNew
------------------------------------
| This function converts a value to the specified unit based on the provided configuration. 
| The configuration is a JSON object that contains a list of dropDownValues, each of which specifies a type, value, operator, and multiplier.

Parameters
............
conversionType : string
    The type of value being converted (time, frequency, or sensitivity).

conversionUnit : string
    The unit to convert to.

value : any
    The value to be converted.

digitsAfterDecimal : number
    The number of digits after the decimal point to be displayed. Default value is 2.

Returns
..........
string 
    The converted value with the appropriate unit.

Usage
.......
::

    convertValueNew(conversionType, conversionUnit, value, digitsAfterDecimal);


Function: displayResultWithoutWeighting
........................................
This function displays the results of the low continuum sensitivity calculation in the UI without taking into account the weighting factor. This is run when custom array is selected.

Parameters
...........
calculateResponse : LowContinuumSensitivityResponse
    The response object from the low continuum sensitivity calculation.

Returns
........
None

Usage
.......
::

    displayResultWithoutWeighting(calculateResponse);

Notes
.......
| This function is called after the low continuum sensitivity calculation is completed to display the results in the UI.
| The function does not take into account the weighting factor in the calculation of the continuum sensitivity or total continuum sensitivity.
| The newCalc flag is used to ensure that the results are updated properly when the input parameters change.
| For confusion noise and total sensitivity, the noBeamAvailable value is displayed.



===============================================
Confusion Limit Warning implementation
===============================================

| This explains the new feature for checking whether the total sensitivity is approaching the confusion noise limit on Low.
| This is implemented for both continuum confusion noise and spectral confusion noise (zoom).

Feature Overview
-----------------
| This feature checks whether the total sensitivity is approaching the confusion noise limit on Low. It does this by comparing the total sensitivity to twice the confusion noise, and displaying a warning message if the sensitivity is near the limit.
| The check is initiated in the displayResult function for both zoom and continuum.
| If custom array is selected, calling displayResultWithoutWeighting, sensitivityLimitCheck() should always return false as confusion noise is a string containing the following: "N/A (no beam information available)". Therefore, no confusion limit warning message should be displayed.
| For non-Gaussian beam, we should not display a warning message even if the sensitivity is near the confusion limit. Therefore, sensLimitReached is always set to false and sensitivityLimitCheck() is not called. 


Function: sensitivityLimitCheck
------------------------------------
This function checks whether the total sensitivity is approaching the confusion noise limit. The confusion noise limit is when the total sensitivity is less than twice the confusion noise, so we define approaching the limit as any sensitivity within 2 * confusion noise.

Parameters
...........
confusionNoise : number | string | typeof NaN
    The calculated confusion noise for the observation. 
sensitivity : number | string
    The total sensitivity that has been calculated using the square of summs of confusion noise and weighted sensitivity.

Returns
...........
boolean 
    Returns true if the sensitivity is near the limit and false otherwise.

Usage
...........
::

    const sensLimitReached = helpers.calculate.sensitivityLimitCheck(continuumConfusionNoise, continuumWeightedSensitivity);

Notes
...........
| Located in helpers.ts.
| This is was an existing function that is also used for Mid.
| This function is used to check if the total sensitivity is approaching the confusion noise limit.
| If the confusion noise is a number and is greater than zero, the function checks if the weighted sensitivity is between confusion noise and 2 * confusion noise.
| If the sensitivity is near the limit, the function returns true. Otherwise, it returns false.


Function: sensLimitWarning
------------------------------------
This function returns a warning message when the sensitivity limit is reached.

Returns
...........
string 
    The warning message for the sensitivity limit.

Usage
...........
::

    const warningMessage = sensLimitWarning();

Notes
...........
| Located in continuum-results.ts and zoom-results.ts.
| This function is called to display a warning message when the sensitivity limit is reached.
| The function calls helpers.messages.sensLimitWarningLow() to get the specific warning message for Low.

Usage in HTML
..............
::

    <mat-error *ngIf="sensLimitReached" id="sensLimitWarning">
        {{sensLimitWarning()}}
    </mat-error>

This code displays a warning message when the sensitivity limit is reached, as determined by the sensitivityLimitCheck function.
