.. _sensitivity_model:

=================
Sensitivity Model
=================

Please refer to the `back-end documentation <https://developer.skao.int/projects/ska-ost-senscalc/en/latest/>`_ for details of the sensitivity calculations.
