.. _user_guide:

==========
User Guide
==========

User guides for the Low and Mid Calculators are stored as Google documents and are maintained by the Science Operations team.

`Mid Calculator User Guide <https://docs.google.com/document/d/1ppt2T2d67HB3iZLb6pXAjAG3oiw5nZ3NmtTmJROqJsg/edit#heading=h.gjdgxs>`_

`Low Calculator User Guide <https://docs.google.com/document/d/1tB-5ZwpYdlavyTyELrZWpFnEJgPv2Qc4azONxlU5kXM/edit#heading=h.fn3j4uie4zpn>`_
