module.exports = {
  moduleNameMapper: {
    "@core/(.*)": "<rootDir>/src/app/core/$1",
  },
  preset: "jest-preset-angular",
  setupFilesAfterEnv: ["<rootDir>/setup-jest.ts"],
  transform: {
    "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$":
      "<rootDir>/fileTransformer.js",
  },
  testPathIgnorePatterns: [
    "<rootDir>/cypress/",
    "<rootDir>/.cache/Cypress"
  ],
  testResultsProcessor: "jest-junit",
  coverageReporters: ["text", "cobertura", "html"],
  coverageThreshold: {
    global: {
      branches: 75,
      functions: 75,
      lines: 75,
      statements: 75,
    },
  },
  collectCoverageFrom: [
    "src/**/*.ts",
    "!src/**/*.spec.ts",
    "!src/ska_ost_senscalc_ui/app/generated/**/*.ts",
    "!src/ska_ost_senscalc_ui/app/ng-material/**/*.ts",
    "!src/ska_ost_senscalc_ui/app/shared/shared.module.ts"
  ]
};
