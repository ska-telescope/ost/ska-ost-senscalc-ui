import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class AppConfiguration {

    private config!: Configuration;
    private runtimeConfig!: RuntimeConfiguration;

    constructor(private http: HttpClient) {
    }

    public loadConfig() {
        return new Promise<void>((resolve) => {
            this.http.get<Configuration>("assets/configuration.json").subscribe((data) => {
                this.config = data;
                resolve();
            });
        });
    }

    public getConfig(): Configuration {
        return this.config;
    }

    public loadRuntimeConfig() {
        return new Promise<void>((resolve) => {
            this.http.get<RuntimeConfiguration>("assets/runtime-configuration.json").subscribe((data) => {
                this.runtimeConfig = data;
                resolve();
            });
        });
    }

    public getRuntimeConfig(): RuntimeConfiguration {
        return this.runtimeConfig;
    }
}

export interface RuntimeConfiguration {
    backendURL?: string;
}

export interface Configuration {
    appTitle?: ConfigurationAppTitle[];
    currentVersion: string;
    documentationMidURL?: string;
    documentationLowURL?: string;
    skaoHomepage?: string;
    midCalcFormFields?: ConfigurationMidCalculatorFormField[];
    advancedModeFields?: ConfigurationMidCalculatorFormField[];
    dropDownValues?: ConfigurationDropDownValue[];
    subarrays?: ArrayDropDownValue[];
    validationErrors: ConfigurationValidationErrorMessage[];
    resolutionDropdownValues?: ConfigurationResolutionDropDownValue[];
    spectralAveragingValues?: ConfigurationSpectralAvgDropDownValue[];
    pulsarSearchArrays: PulsarSearchArrayValue[];
    advancedModeActive: boolean;
}

export interface ConfigurationAppTitle {
    name: string;
}

export interface ConfigurationMidCalculatorFormField {
    field: string;
    title: string;
    description: string;
    placeholder?: string;
    defaultValue?: any;
}

export interface ConfigurationDropDownValue {
    type: string;
    value: string;
    operator?: string | null;
    multiplier?: number | null;
    selected?: boolean | null;
}

export interface ConfigurationValidationErrorMessage {
    errorName: string;
    errorMessage: string;
}

export interface ConfigurationResolutionDropDownValue {
    value: number;
    displayValue: string;
    selected?: boolean | null;
    bandwidth: number;
}

export interface ConfigurationSpectralAvgDropDownValue {
    value: number;
    selected?: boolean | null;
}

export interface ConfigurationImageWeightingValue {
    option: string;
    value: string;
    selected: boolean;
    robust: boolean;
}

export interface ArrayDropDownValue {
    name: string;
    label: string;
    n_ska: number;
    n_meer: number;
}

export interface PulsarSearchArrayValue {
    label: string;
}

export type MidSubarrayId = 'AA0.5' | 'AA1' | 'AA2' | 'AA*' | 'AA4' | 'AA4 (13.5 m antennas only)' | 'AA4 (15 m antennas only)' | 'Custom';
