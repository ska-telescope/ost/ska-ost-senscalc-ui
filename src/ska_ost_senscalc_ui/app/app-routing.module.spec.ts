import { ActivatedRouteSnapshot, DetachedRouteHandle } from '@angular/router';
import { AlwaysReuseStrategy } from "./app-routing.module";

describe("AlwaysReuseStrategy behaves as expected", () => {
  let routeReuseStrategy: AlwaysReuseStrategy;
  const mockRoute = {routeConfig: {"path": "/"}} as ActivatedRouteSnapshot;
  const mockHandle = {} as DetachedRouteHandle;

  beforeEach(() => {
    routeReuseStrategy = new AlwaysReuseStrategy();
  });

  test("shouldDeatch() returns true", () => {
    expect(routeReuseStrategy.shouldDetach(mockRoute)).toBe(true);
  });

  test("shouldAttach() returns true when route stored", () => {
    routeReuseStrategy.store(mockRoute, mockHandle);
    expect(routeReuseStrategy.shouldAttach(mockRoute)).toBe(true);
  });

  test("shouldAttach() returns false when no route stored", () => {
    expect(routeReuseStrategy.shouldAttach(mockRoute)).toBe(false);
  });

  test("store() persists a route handle", () => {
    routeReuseStrategy.store(mockRoute, mockHandle);
    // Because handlers is private:
    const handlers = (routeReuseStrategy as any).handlers;
    expect(handlers.size).toBe(1);
  });

  test("retrieve() returns a route handle", () => {
    routeReuseStrategy.store(mockRoute, mockHandle);
    const result = routeReuseStrategy.retrieve(mockRoute);
    expect(result).toEqual(mockHandle);
  });

  test("retrieve() returns null for non-stored route", () => {
    const result = routeReuseStrategy.retrieve(mockRoute);
    expect(result).toBe(null);
  });

  test("shouldReuseRoute() returns false when navigating to different page", () => {
    const nextRoute = {routeConfig: {"path": "/next"}} as ActivatedRouteSnapshot;
    expect(routeReuseStrategy.shouldReuseRoute(nextRoute, mockRoute)).toBe(false);
  });

  test("shouldReuseRoute() returns true when on same page", () => {
    expect(routeReuseStrategy.shouldReuseRoute(mockRoute, mockRoute)).toBe(true);
  });

});
