import { NgModule } from '@angular/core';
import { ActivatedRouteSnapshot, DetachedRouteHandle, Route, RouteReuseStrategy, RouterModule, Routes } from '@angular/router';

/**
 * The "AlwaysReuseStrategy" allows switching back and forth between MID and LOW
 * calculators without losing data entered.
 * Flagrantly cribbed from:
 * https://www.auroria.io/angular-route-reuse-strategy/
 * https://stackoverflow.com/a/71292990/845210
 * https://github.com/angular/angular/issues/44383#issuecomment-996361898
 */
export class AlwaysReuseStrategy implements RouteReuseStrategy {
  private handlers: Map<Route, DetachedRouteHandle> = new Map();

  constructor() {}

  /**
   * Determines if this route (and its subtree) should be detached to be reused later.
   * As we always want to reuse the calculator for state to be retained, this is
   * hardcoded to true.
   */
  public shouldDetach(_route: ActivatedRouteSnapshot): boolean {
    return true;
  }

  /**
   * This method is invoked only if the shouldDetach returns true. As shouldDetach ALWAYS
   * returns true, this method is called for every route change, we cache the old route we're
   * navigating away from, with the route's configuration (path, params, etc) as a key, ready
   * to be restored if the the user revisits that same route.
   */
  public store(
    route: ActivatedRouteSnapshot,
    handle: DetachedRouteHandle
  ): void {
    if (!route.routeConfig) {
      return;
    }
    this.handlers.set(route.routeConfig, handle);
  }

  /**
   * Called when loading a new route, this method checks to see if we already
   * have cached a detached route handler for this route. Basically, did the
   * user already visit this page? If so, then we should reattach that,
   * because it potentially contains values entered by the user.
   */
  public shouldAttach(route: ActivatedRouteSnapshot): boolean {
    return !!route.routeConfig && !!this.handlers.get(route.routeConfig);
  }

  /**
   * This method is called if shouldAttach returns true. It grabs the saved
   * DeatchedRouteHandle (the component for this route, basically) from our
   * store and returns it.
   */
  public retrieve(route: ActivatedRouteSnapshot): DetachedRouteHandle|null {
    if (!route.routeConfig || !this.handlers.has(route.routeConfig)) {
      return null;
    }
    return this.handlers.get(route.routeConfig)!;
  }

  /**
   * Called first when navgiating between pages, this method determines
   * whether we even need a new route, or if we can just keep using the same
   * route/compnent tree we've already got. Imagine navigating from
   * /product/123 to /product/456 -> we'd just reuse the "product" route.
   * If this method returns true, none of the others are called.
   */
  public shouldReuseRoute(
    future: ActivatedRouteSnapshot,
    curr: ActivatedRouteSnapshot
  ): boolean {
    return future.routeConfig === curr.routeConfig;
  }
}

const routes: Routes = [
  {
    path: 'mid',
    loadChildren: () => import('./mid-sensitivity-calculator/mid-sensitivity-calculator.module').then(m => m.MidSensitivityCalculatorModule)
  },
  {
    path: 'low',
    loadChildren: () => import('./low-sensitivity-calculator/low-sensitivity-calculator.module').then(m => m.LowSensitivityCalculatorModule)
  },
  {
    path: '',
    redirectTo: '/mid',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '/mid'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
