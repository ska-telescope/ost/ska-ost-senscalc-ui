import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import {Component} from "@angular/core";
import {HeaderComponent} from "./shared/header/header.component";
import {FooterComponent} from "./shared/footer/footer.component";

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        RouterTestingModule
      ],
      declarations: [
        AppComponent,
        StubFooterComponent,
        StubHeaderComponent
      ]
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'ska-ost-senscalc-ui'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    expect(fixture.componentInstance.title).toEqual('ska-ost-senscalc-ui');
  });

  it(`checkThemePreference should have classList 'skao-theme'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.componentInstance.checkThemePreference();
    expect(document.documentElement.classList.contains('skao-theme')).toBeFalsy();
    expect(document.documentElement.classList.contains('dark-theme')).toBeFalsy();
  });

  it(`selectDarkTheme should have classList 'dark-theme'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.componentInstance.selectDarkTheme();
    expect(document.documentElement.classList.contains('skao-theme')).toBeFalsy();
    expect(document.documentElement.classList.contains('dark-theme')).toBeTruthy();
  });

  it(`selectDarkTheme should have classList 'dark-theme'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.componentInstance.selectLightTheme();
    expect(document.documentElement.classList.contains('dark-theme')).toBeFalsy();
  });
});

@Component({
  selector: 'app-header',
  template: '',
  providers: [
    {
      provide: HeaderComponent,
      useClass: StubHeaderComponent
    }
  ]
})
class StubHeaderComponent extends HeaderComponent {
}

@Component({
  selector: 'app-footer',
  template: '',
  providers: [
    {
      provide: FooterComponent,
      useClass: StubFooterComponent
    }
  ]
})
class StubFooterComponent extends FooterComponent {
}
