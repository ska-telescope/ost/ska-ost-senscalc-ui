import { Component } from '@angular/core';
import { SharedService } from './shared/service/shared.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  static readonly DARK_THEME_CLASS = 'dark-theme';
  static readonly LOCAL_STORAGE_TITLE = 'skao-theme';

  title = 'ska-ost-senscalc-ui';

  constructor(
    private sharedService: SharedService
  ) {
    this.checkThemePreference();
    this.sharedService.lightMode.subscribe(() => this.selectLightTheme());
    this.sharedService.darkMode.subscribe(() => this.selectDarkTheme());
  }

  checkThemePreference(): void {
    const currentTheme = localStorage.getItem(AppComponent.LOCAL_STORAGE_TITLE);
    if (currentTheme == AppComponent.DARK_THEME_CLASS) {
      document.documentElement.classList.add(AppComponent.DARK_THEME_CLASS);
    }
  }

  selectDarkTheme(): void {
    localStorage.setItem(
      AppComponent.LOCAL_STORAGE_TITLE,
      AppComponent.DARK_THEME_CLASS
    );
    document.documentElement.classList.add(AppComponent.DARK_THEME_CLASS);
  }

  selectLightTheme(): void {
    localStorage.removeItem(AppComponent.LOCAL_STORAGE_TITLE);
    document.documentElement.classList.remove(AppComponent.DARK_THEME_CLASS);
  }
}
