import { NgModule } from '@angular/core';

import { MidCalculatorService } from './mid-calculator.service';
import { ValidatorsService } from './validators.service';
import { LowApiModule } from "../generated/api/low";

@NgModule({
  imports: [
    LowApiModule
  ],
  declarations: [],
  providers: [
    MidCalculatorService,
    ValidatorsService
  ],
  exports: []
})
export class CoreModule {
}
