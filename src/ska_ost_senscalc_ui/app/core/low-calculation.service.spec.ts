import { TestBed } from '@angular/core/testing';
import {firstValueFrom, of} from 'rxjs';
import {
  BASE_PATH,
  DefaultService,
  LowApiModule,
  LowContinuumSensitivityResponse,
  LowContinuumWeightingResult, LowPssSensitivityResponse, LowSingleWeightingZoomResult,
  LowSingleZoomSensitivityResponse,
  LowSpectropolarimetryResults
} from '../generated/api/low';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {LowCalculationService} from "./low-calculation.service";
import {LowFormInput} from "../low-sensitivity-calculator/types";
import {SpectropolarimetryResults} from "./types";
import constants from "../shared/utils/constants";

describe('LowCalculationService', () => {
  let fixture: LowCalculationService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, LowApiModule],
      providers: [
        {
          provide: BASE_PATH,
          useValue: 'http://backend'
        }
      ]
    }).compileComponents();

    fixture = TestBed.inject(LowCalculationService);
  });

  const spectropolarimetryResults: LowSpectropolarimetryResults = {
    fwhm_of_the_rmsf: {value: 18, unit: 'rad/m^2'},
    max_faraday_depth_extent: {value: 1800, unit: 'rad/m^2'},
    max_faraday_depth: {value: 36, unit: 'rad/m^2'}
  };

  const displaySpectropolarimetryResults: SpectropolarimetryResults = {
    fwhmOfTheRMSF: '18.0 rad/m^2',
    maximumFaradayDepth: '36.0 rad/m^2',
    maximumFaradayDepthExtent: '1800.0 rad/m^2'
  };

  describe('When calculateContinuumResult is called ', () => {
    const continuumCalculateResult: LowContinuumSensitivityResponse = {
      continuum_sensitivity: {value: 20, unit: 'uJy'},
      spectral_sensitivity: {value: 1.2, unit: 'uJy'},
      spectropolarimetry_results: spectropolarimetryResults
    };

    const continuumWeightingResult: LowContinuumWeightingResult = {
      weighting_factor: 1.5,
      confusion_noise: {value: 1.23e-6, limit_type: 'value'},
      sbs_conv_factor: 4,
      beam_size: {beam_pa: 1, beam_min_scaled: 2, beam_maj_scaled: 3}
    };

    beforeEach(() => {});

    it('should call the calculate and weighting end points delegate to the full mapper', async () => {
      const fakeApi = {
        skaOstSenscalcLowApiContinuumCalculate: () => of(continuumCalculateResult),
        skaOstSenscalcLowApiContinuumWeighting: () => of(continuumWeightingResult)
      };
      fixture = new LowCalculationService(fakeApi as unknown as DefaultService);

      jest.spyOn(fakeApi, 'skaOstSenscalcLowApiContinuumCalculate');
      jest.spyOn(fakeApi, 'skaOstSenscalcLowApiContinuumWeighting');
      const lowFormInput: LowFormInput = {
        common: {
          num_stations: 512,
          ra_str: '10:00:00',
          dec_str: '-20:00:00',
          min_elevation: 30,
          subarray: 'LOW_AA05_all'
        },
        continuum: {
          imageWeighting: 'uniform',
          duration: 1,
          frequency: 1e6,
          bandwidth: 2e6,
          spectralMode: 'continuum',
          nSubbands: 1,
          averaging: 1
        }

      };
      const result = await firstValueFrom(fixture.calculateContinuumResult(lowFormInput, false));
      expect(fakeApi.skaOstSenscalcLowApiContinuumCalculate).toHaveBeenCalled();
      expect(fakeApi.skaOstSenscalcLowApiContinuumWeighting).toHaveBeenCalledTimes(2); // Once for spectral_mode=continuum and once for spectral_mode=line
      // The testing of the combination of the API responses into the actual results is done in the mapper specs, so we don't do a full assertion here
      expect(result.spectropolarimetryResults).toEqual(displaySpectropolarimetryResults);
    });

    it('should call the calculate and weighting end points delegate to the non Gaussian mapper', async () => {
      const fakeApi = {
        skaOstSenscalcLowApiContinuumCalculate: () => of(continuumCalculateResult),
        skaOstSenscalcLowApiContinuumWeighting: () => of(continuumWeightingResult)
      };
      fixture = new LowCalculationService(fakeApi as unknown as DefaultService);
      jest.spyOn(fakeApi, 'skaOstSenscalcLowApiContinuumCalculate');
      jest.spyOn(fakeApi, 'skaOstSenscalcLowApiContinuumWeighting');

      const lowFormInput: LowFormInput = {
        common: {
          num_stations: 512,
          ra_str: '10:00:00',
          dec_str: '-20:00:00',
          min_elevation: 30,
          subarray: 'LOW_AA05_all'
        },
        continuum: {
          imageWeighting: 'natural', // should mean the beam is not Gaussian
          duration: 1,
          frequency: 1e6,
          bandwidth: 2e6,
          spectralMode: 'continuum',
          nSubbands: 1,
          averaging: 1
        }

      };
      const result = await firstValueFrom(fixture.calculateContinuumResult(lowFormInput, false));
      expect(fakeApi.skaOstSenscalcLowApiContinuumCalculate).toHaveBeenCalled();
      expect(fakeApi.skaOstSenscalcLowApiContinuumWeighting).toHaveBeenCalledTimes(2); // Once for spectral_mode=continuum and once for spectral_mode=line
      // The testing of the combination of the API responses into the actual results is done in the mapper specs, so we don't do a full assertion here
      expect(result.spectralSurfaceBrightnessSensitivity).toEqual(constants.result.nonGaussianBeam);
      expect(result.spectropolarimetryResults).toEqual(displaySpectropolarimetryResults);
    });

    it('should call only the calculate end point and delegate to the no beam available mapper', async () => {
      const fakeApi = {
        skaOstSenscalcLowApiContinuumCalculate: () => of(continuumCalculateResult),
        skaOstSenscalcLowApiContinuumWeighting: () => of(continuumWeightingResult)
      };
      fixture = new LowCalculationService(fakeApi as unknown as DefaultService);
      jest.spyOn(fakeApi, 'skaOstSenscalcLowApiContinuumCalculate');
      jest.spyOn(fakeApi, 'skaOstSenscalcLowApiContinuumWeighting');

      const lowFormInput: LowFormInput = {
        common: {
          num_stations: 512,
          ra_str: '10:00:00',
          dec_str: '-20:00:00',
          min_elevation: 30,
          subarray: 'Custom'
        },
        continuum: {
          imageWeighting: 'natural',
          duration: 1,
          frequency: 1e6,
          bandwidth: 2e6,
          spectralMode: 'continuum',
          nSubbands: 1,
          averaging: 1
        }

      };
      const result = await firstValueFrom(fixture.calculateContinuumResult(lowFormInput, false));
      expect(fakeApi.skaOstSenscalcLowApiContinuumCalculate).toHaveBeenCalled();
      expect(fakeApi.skaOstSenscalcLowApiContinuumWeighting).not.toHaveBeenCalled();
      // The testing of the combination of the API responses into the actual results is done in the mapper specs, so we don't do a full assertion here
      expect(result.spectralSurfaceBrightnessSensitivity).toEqual(constants.result.noBeamAvailable);
      expect(result.spectropolarimetryResults).toEqual(displaySpectropolarimetryResults);
    });
  });

  describe('When calculateZoomResult is called ', () => {
    const zoomCalculateResult: LowSingleZoomSensitivityResponse[] = [{
      spectral_sensitivity: {value: 1.2, unit: 'uJy'},
      spectropolarimetry_results: spectropolarimetryResults
    }];

    const zoomWeightingResult: LowSingleWeightingZoomResult[] = [{
      weighting_factor: 1.5,
      confusion_noise: {value: 1.23e-6, limit_type: 'value'},
      sbs_conv_factor: 4,
      beam_size: {beam_pa: 1, beam_min_scaled: 2, beam_maj_scaled: 3}
    }];

    beforeEach(() => {});

    it('should call the calculate and weighting end points delegate to the full mapper', async () => {
      const fakeApi = {
        skaOstSenscalcLowApiZoomCalculate: () => of(zoomCalculateResult),
        skaOstSenscalcLowApiZoomWeighting: () => of(zoomWeightingResult)
      };
      fixture = new LowCalculationService(fakeApi as unknown as DefaultService);

      jest.spyOn(fakeApi, 'skaOstSenscalcLowApiZoomCalculate');
      jest.spyOn(fakeApi, 'skaOstSenscalcLowApiZoomWeighting');
      const lowFormInput: LowFormInput = {
        common: {
          num_stations: 512,
          ra_str: '10:00:00',
          dec_str: '-20:00:00',
          min_elevation: 30,
          subarray: 'LOW_AA05_all'
        },
        zoom: {
          imageWeighting: 'briggs',
          robust: 1,
          duration: 1,
          frequency: 1e6,
          totalBandwidthKhz: 2e6
        }

      };
      const result = await firstValueFrom(fixture.calculateZoomResult(lowFormInput, false));
      expect(fakeApi.skaOstSenscalcLowApiZoomCalculate).toHaveBeenCalled();
      expect(fakeApi.skaOstSenscalcLowApiZoomWeighting).toHaveBeenCalled();
      // The testing of the combination of the API responses into the actual results is done in the mapper specs, so we don't do a full assertion here
      expect(result.spectropolarimetryResults).toEqual(displaySpectropolarimetryResults);
    });

    it('should call the calculate and weighting end points delegate to the non Gaussian mapper', async () => {
      const fakeApi = {
        skaOstSenscalcLowApiZoomCalculate: () => of(zoomCalculateResult),
        skaOstSenscalcLowApiZoomWeighting: () => of(zoomWeightingResult)
      };
      fixture = new LowCalculationService(fakeApi as unknown as DefaultService);

      jest.spyOn(fakeApi, 'skaOstSenscalcLowApiZoomCalculate');
      jest.spyOn(fakeApi, 'skaOstSenscalcLowApiZoomWeighting');
      const lowFormInput: LowFormInput = {
        common: {
          num_stations: 512,
          ra_str: '10:00:00',
          dec_str: '-20:00:00',
          min_elevation: 30,
          subarray: 'LOW_AA05_all'
        },
        zoom: {
          imageWeighting: 'natural',
          duration: 1,
          frequency: 1e6,
          totalBandwidthKhz: 2e6
        }

      };
      const result = await firstValueFrom(fixture.calculateZoomResult(lowFormInput, false));
      expect(fakeApi.skaOstSenscalcLowApiZoomCalculate).toHaveBeenCalled();
      expect(fakeApi.skaOstSenscalcLowApiZoomWeighting).toHaveBeenCalled();
      // The testing of the combination of the API responses into the actual results is done in the mapper specs, so we don't do a full assertion here
      expect(result.spectralSurfaceBrightnessSensitivity).toEqual(constants.result.nonGaussianBeam);
      expect(result.spectropolarimetryResults).toEqual(displaySpectropolarimetryResults);
    });

    it('should call only the calculate end point and delegate to the no beam available mapper', async () => {
            const fakeApi = {
        skaOstSenscalcLowApiZoomCalculate: () => of(zoomCalculateResult),
        skaOstSenscalcLowApiZoomWeighting: () => of(zoomWeightingResult)
      };
      fixture = new LowCalculationService(fakeApi as unknown as DefaultService);

      jest.spyOn(fakeApi, 'skaOstSenscalcLowApiZoomCalculate');
      jest.spyOn(fakeApi, 'skaOstSenscalcLowApiZoomWeighting');
      const lowFormInput: LowFormInput = {
        common: {
          num_stations: 512,
          ra_str: '10:00:00',
          dec_str: '-20:00:00',
          min_elevation: 30,
          subarray: 'Custom'
        },
        zoom: {
          imageWeighting: 'natural',
          duration: 1,
          frequency: 1e6,
          totalBandwidthKhz: 2e6
        }

      };
      const result = await firstValueFrom(fixture.calculateZoomResult(lowFormInput, false));
      expect(fakeApi.skaOstSenscalcLowApiZoomCalculate).toHaveBeenCalled();
      expect(fakeApi.skaOstSenscalcLowApiZoomWeighting).not.toHaveBeenCalled();
      // The testing of the combination of the API responses into the actual results is done in the mapper specs, so we don't do a full assertion here
      expect(result.spectralSurfaceBrightnessSensitivity).toEqual(constants.result.noBeamAvailable);
      expect(result.spectropolarimetryResults).toEqual(displaySpectropolarimetryResults);
    });

  });

  describe('When calculatePssResult is called ', () => {
    const pssCalculateResult: LowPssSensitivityResponse = {
      folded_pulse_sensitivity: {value: 1.2, unit: 'uJy'}
    };

    beforeEach(() => {});

    it('should call the calculate and weighting end points delegate to the full mapper', async () => {
      const fakeApi = {
        skaOstSenscalcLowApiPssCalculate: () => of(pssCalculateResult)
      };
      fixture = new LowCalculationService(fakeApi as unknown as DefaultService);

      jest.spyOn(fakeApi, 'skaOstSenscalcLowApiPssCalculate');
      const lowFormInput: LowFormInput = {
        common: {
          num_stations: 512,
          ra_str: '10:00:00',
          dec_str: '-20:00:00',
          min_elevation: 30,
          subarray: 'LOW_AA05_all'
        },
        pss: {
          pulsePeriod: 1,
          pulseWidth: 1,
          chanelWidth: 1,
          bandwidth: 1e6,
          frequency: 1e6,
          duration: 1,
          dispersionMeasure: 1
        }

      };
      const result = await firstValueFrom(fixture.calculatePssResult(lowFormInput, false));
      expect(fakeApi.skaOstSenscalcLowApiPssCalculate).toHaveBeenCalled();
      // The testing of the combination of the API responses into the actual results is done in the mapper specs, so we don't do a full assertion here
      expect(result.sensitivity).toEqual('1.20 uJy');
    });
  });
});
