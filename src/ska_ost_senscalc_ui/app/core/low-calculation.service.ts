import { Injectable } from "@angular/core";
import { LowFormInput } from "../low-sensitivity-calculator/types";
import { combineLatest, map, Observable } from "rxjs";
import {
  DefaultService,
  SkaOstSenscalcLowApiContinuumCalculateRequestParams,
  SkaOstSenscalcLowApiContinuumWeightingRequestParams,
  SkaOstSenscalcLowApiZoomWeightingRequestParams,
  SkaOstSenscalcLowApiZoomCalculateRequestParams,
  SkaOstSenscalcLowApiPssCalculateRequestParams
} from "../generated/api/low";
import helpers from "../shared/utils/helpers";
import {
  lowContinuumResponseToSensitivityResultsWithoutWeighting,
  lowContinuumResponseToSensitivityFullResults,
  lowContinuumResponseToSensitivityResultsNoneGaussian
} from "./mappers/low-continuum-results.mapper";
import { ContinuumResults, PssResults, ZoomResults } from "./types";
import {
  lowZoomResponseToSensitivityResultsWithoutWeighting,
  lowZoomResponseToSensitivityFullResults,
  lowZoomResponseToSensitivityResultsNoneGaussian
} from "./mappers/low-zoom-results.mapper";
import {lowPssResponseToResults} from "./mappers/low-pss-results.mapper";

/**
 * This service tries to encapsulate all the complexity of the calculator, by offering methods which take the form input and return Observables
 * of the final result data, ready to be displayed as strings.
 * It handles deciding which APIs to call, how the form inputs are mapped to the params and then the combining of the results, which is delegated to the mappers.
 */
@Injectable({
  providedIn: "root"
})
export class LowCalculationService {

  constructor(private lowApi: DefaultService) {}

  calculateContinuumResult(lowFormInput: LowFormInput, integrationTimesDifferent: boolean): Observable<ContinuumResults> {
    // Request /continuum/calculate
    const calculateRequestParams: SkaOstSenscalcLowApiContinuumCalculateRequestParams =
      {
        integrationTimeH: lowFormInput.continuum?.duration,
        pointingCentre:
          lowFormInput.common.ra_str + " " + lowFormInput.common.dec_str,
        freqCentreMhz: lowFormInput.continuum!.frequency,
        elevationLimit: lowFormInput.common.min_elevation,
        spectralAveragingFactor: lowFormInput.continuum!.averaging,
        bandwidthMhz: lowFormInput.continuum!.bandwidth,
        nSubbands: lowFormInput.continuum!.nSubbands
      };

    if (lowFormInput.common.subarray == "Custom") {
      calculateRequestParams.numStations = lowFormInput.common.num_stations;
    } else {
      calculateRequestParams.subarrayConfiguration = lowFormInput.common.subarray;
    }

    const continuumCalculateResult =
      this.lowApi.skaOstSenscalcLowApiContinuumCalculate(
        calculateRequestParams
      );

    if (lowFormInput.common.subarray == "Custom") {
      return continuumCalculateResult.pipe(
        map((continuumCalculate) =>
          lowContinuumResponseToSensitivityResultsWithoutWeighting(continuumCalculate, integrationTimesDifferent)
        )
      );
    }

    // Request /continuum/weighting for continuum
    const weightingRequestParams: SkaOstSenscalcLowApiContinuumWeightingRequestParams =
      {
        spectralMode: "continuum",
        // The  backend expects 'robust' for the value we display as 'Briggs'
        weightingMode: lowFormInput.continuum!.imageWeighting
          ? lowFormInput.continuum!.imageWeighting == "briggs"
            ? "robust"
            : lowFormInput.continuum!.imageWeighting
          : "natural",
        subarrayConfiguration: lowFormInput.common
          .subarray as SkaOstSenscalcLowApiContinuumWeightingRequestParams["subarrayConfiguration"],
        pointingCentre:
          lowFormInput.common.ra_str + " " + lowFormInput.common.dec_str,
        freqCentreMhz: lowFormInput.continuum!.frequency
      };
    if (
      lowFormInput.continuum!.imageWeighting == "briggs" &&
      lowFormInput.continuum!.robust != undefined
    ) {
      weightingRequestParams.robustness = lowFormInput.continuum!.robust;
    }
    if (
      lowFormInput.continuum?.nSubbands &&
      lowFormInput.continuum.nSubbands > 1
    ) {
      weightingRequestParams.subbandFreqCentresMhz =
        helpers.calculate.subBandtoFrequencyArray(
          lowFormInput.continuum.nSubbands,
          lowFormInput.continuum.bandwidth,
          lowFormInput.continuum.frequency
        );
    }

    const continuumWeightingResult =
      this.lowApi.skaOstSenscalcLowApiContinuumWeighting(
        weightingRequestParams
      );
    // Request /continuum/weighting for line
    weightingRequestParams.spectralMode = "line";
    const lineWeightingResult =
      this.lowApi.skaOstSenscalcLowApiContinuumWeighting(
        weightingRequestParams
      );

    const showBeamAndSBS = helpers.option.showBeamAndSBSForLow(
      lowFormInput.continuum!.imageWeighting,
      lowFormInput.continuum!.robust!
    );

    return combineLatest(continuumCalculateResult, continuumWeightingResult, lineWeightingResult)
      .pipe(
        map(([continuumCalculate, continuumWeighting, lineWeighting]) => showBeamAndSBS
          ? lowContinuumResponseToSensitivityFullResults(continuumCalculate, continuumWeighting, lineWeighting, integrationTimesDifferent)
          : lowContinuumResponseToSensitivityResultsNoneGaussian(continuumCalculate, continuumWeighting, lineWeighting, integrationTimesDifferent)
      ));
  }

  calculateZoomResult(
    lowFormInput: LowFormInput,
    integrationTimesDifferent = false
  ): Observable<ZoomResults> {
    const calculateRequestParams: SkaOstSenscalcLowApiZoomCalculateRequestParams =
      {
        integrationTimeH: lowFormInput.zoom!.duration,
        pointingCentre:
          lowFormInput.common.ra_str + " " + lowFormInput.common.dec_str,
        freqCentresMhz: [lowFormInput.zoom!.frequency],
        elevationLimit: lowFormInput.common.min_elevation,
        spectralAveragingFactor: lowFormInput.zoom!.spectralAveraging,
        totalBandwidthsKhz: [lowFormInput.zoom!.totalBandwidthKhz],
        spectralResolutionsHz: [lowFormInput.zoom!.spectralResolutionHz!]
      };

    if (lowFormInput.common.subarray == "Custom") {
      calculateRequestParams.numStations = lowFormInput.common.num_stations;
    } else {
      calculateRequestParams.subarrayConfiguration = lowFormInput.common
        .subarray as SkaOstSenscalcLowApiZoomCalculateRequestParams["subarrayConfiguration"];
    }

    const zoomCalculateResult = this.lowApi.skaOstSenscalcLowApiZoomCalculate(
      calculateRequestParams
    );

    if (lowFormInput.common.subarray == "Custom") {
      return zoomCalculateResult.pipe(
        map((zoomCalculate) =>
          lowZoomResponseToSensitivityResultsWithoutWeighting(
            zoomCalculate,
            integrationTimesDifferent
          )
        )
      );
    }

    const weightingRequestParams: SkaOstSenscalcLowApiZoomWeightingRequestParams =
      {
        // The  backend expects 'robust' for the value we display as 'Briggs'
        weightingMode: lowFormInput.zoom!.imageWeighting == "briggs"
          ? "robust"
          : lowFormInput.zoom!.imageWeighting,
        subarrayConfiguration: lowFormInput.common
          .subarray as SkaOstSenscalcLowApiZoomWeightingRequestParams["subarrayConfiguration"],
        pointingCentre:
          lowFormInput.common.ra_str + " " + lowFormInput.common.dec_str,
        freqCentresMhz: [lowFormInput.zoom!.frequency]
      };
    if (
      lowFormInput.zoom!.imageWeighting == "briggs" &&
      lowFormInput.zoom!.robust != undefined
    ) {
      weightingRequestParams.robustness = lowFormInput.zoom!.robust;
    }

    const zoomWeightingResult = this.lowApi.skaOstSenscalcLowApiZoomWeighting(
      weightingRequestParams
    );

    const showBeamAndSBS = helpers.option.showBeamAndSBSForLow(
      lowFormInput.zoom!.imageWeighting,
      lowFormInput.zoom!.robust!
    );

    return combineLatest(zoomCalculateResult, zoomWeightingResult)
      .pipe(
        map(([zoomCalculate, zoomWeighting]) => showBeamAndSBS
          ? lowZoomResponseToSensitivityFullResults(zoomCalculate, zoomWeighting, integrationTimesDifferent)
          : lowZoomResponseToSensitivityResultsNoneGaussian(zoomCalculate, zoomWeighting, integrationTimesDifferent)
      ));
  }

  /**
   * Calculate SKA LOW pss sensitivity.
   *
   * @param formInput form input to process
   */
  calculatePssResult(
    formInput: LowFormInput,
    integrationTimesDifferent: boolean
  ): Observable<PssResults> {
    const requestParams: SkaOstSenscalcLowApiPssCalculateRequestParams = {
      integrationTimeH: formInput.pss!.duration,
      pointingCentre: formInput.common.ra_str + " " + formInput.common.dec_str,
      freqCentreMhz: formInput.pss!.frequency,
      elevationLimit: formInput.common.min_elevation,
      dm: formInput.pss!.dispersionMeasure,
      intrinsicPulseWidth: formInput.pss!.pulseWidth,
      pulsePeriod: formInput.pss!.pulsePeriod
    };

    if (formInput.common.subarray == "Custom") {
      requestParams.numStations = formInput.common.num_stations;
    } else {
      requestParams.subarrayConfiguration = formInput.common
        .subarray as SkaOstSenscalcLowApiPssCalculateRequestParams["subarrayConfiguration"];
    }

    return this.lowApi
      .skaOstSenscalcLowApiPssCalculate(requestParams)
      .pipe(
        map((pssCalculateResponse) =>
          lowPssResponseToResults(
            pssCalculateResponse,
            integrationTimesDifferent
          )
        )
      );
  }
}
