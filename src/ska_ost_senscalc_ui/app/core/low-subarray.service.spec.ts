import { TestBed } from '@angular/core/testing';
import { LowSubarrayService } from './low-subarray.service';
import { from, throwError } from 'rxjs';
import {
  BASE_PATH,
  DefaultService,
  LowApiModule,
  LowSubarraysResponse
} from '../generated/api/low';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('LowQueryService', () => {
  let fixture: LowSubarrayService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, LowApiModule],
      providers: [
        {
          provide: BASE_PATH,
          useValue: 'http://backend'
        }
      ]
    }).compileComponents();

    fixture = TestBed.inject(LowSubarrayService);
  });

  describe('When instantiated', () => {
    const subarray: LowSubarraysResponse = {
      name: 'foo',
      label: 'bar',
      n_stations: 123
    };
    const customSubarray: LowSubarraysResponse = {
      name: 'Custom',
      label: 'Custom',
      n_stations: 512
    };

    beforeEach(() => {});

    it('populates the subarrays$ Observable', () => {
      const fakeApi = {
        skaOstSenscalcLowApiSubarrays: () => from([[subarray]])
      };
      fixture = new LowSubarrayService(fakeApi as unknown as DefaultService);

      expect(fixture.subarrays$.getValue()).toEqual([subarray, customSubarray]);
    });

    it('adds Custom subarray as an option', () => {
      const fakeApi = {
        skaOstSenscalcLowApiSubarrays: () => from([[]])
      };
      fixture = new LowSubarrayService(fakeApi as unknown as DefaultService);

      expect(fixture.subarrays$.getValue()).toEqual([customSubarray]);
    });

    it('leaves subQueryFailed$ as false if the subarray query succeeds', () => {
      const fake = {
        skaOstSenscalcLowApiSubarrays: () => from([[subarray]])
      };
      fixture = new LowSubarrayService(fake as unknown as DefaultService);

      expect(fixture.subQueryFailed$.getValue()).toBe(false);
    });

    it('sets subQueryFailed$ to true if the subarray query fails', () => {
      const fake = {
        skaOstSenscalcLowApiSubarrays: () => throwError(() => new Error())
      };
      fixture = new LowSubarrayService(fake as unknown as DefaultService);

      expect(fixture.subQueryFailed$.getValue()).toBe(true);
    });
  });
});
