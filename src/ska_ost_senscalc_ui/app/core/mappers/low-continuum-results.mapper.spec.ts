import {ContinuumResults} from "../types";
import {
  lowContinuumResponseToSensitivityResultsWithoutWeighting,
  lowContinuumResponseToSensitivityFullResults,
  lowContinuumResponseToSensitivityResultsNoneGaussian
} from "./low-continuum-results.mapper";
import constants from "../../shared/utils/constants";
import {
  LowContinuumSensitivityResponse,
  LowContinuumWeightingResult,
  LowWeightingSubbandResult
} from "../../generated/api/low";

const continuumCalculateResult: LowContinuumSensitivityResponse = {
  continuum_sensitivity: {value: 2.567, unit: 'uJy'},
  spectral_sensitivity: {value: 1.234, unit: 'uJy'},
  spectropolarimetry_results: {
    fwhm_of_the_rmsf: {value: 5, unit: 'rad/m^2'},
    max_faraday_depth_extent: {value: 6, unit: 'rad/m^2'},
    max_faraday_depth: {value: 7, unit: 'rad/m^2'}
  },
  continuum_subband_sensitivities: [
    {sensitivity: {value: 5.6, unit: 'uJy'}},
    {sensitivity: {value: 7.8, unit: 'uJy'}}
  ],
  warning: 'test error'
};

const singleSubbandWeighting: LowWeightingSubbandResult = {
  weighting_factor: 1.5,
  confusion_noise: {value: 1.23e-6, limit_type: 'value'}, // confusion noise is near to sensitivity so result should have sensLimitReached true
  sbs_conv_factor: 4,
  beam_size: {beam_pa: 1, beam_min_scaled: 2, beam_maj_scaled: 3}
};

const continuumWeightingResult: LowContinuumWeightingResult = {
  weighting_factor: 1.5,
  confusion_noise: {value: 1.23e-6, limit_type: 'value'}, // confusion noise is near to sensitivity so result should have sensLimitReached true
  sbs_conv_factor: 4,
  beam_size: {beam_pa: 1, beam_min_scaled: 2, beam_maj_scaled: 3},
  subbands: [singleSubbandWeighting, singleSubbandWeighting]
};

const lineWeightingResult: LowContinuumWeightingResult = {
  weighting_factor: 1.2,
  confusion_noise: {value: 0.9e-6, limit_type: 'value'}, // confusion noise is near to sensitivity so result should have sensLimitReached true
  sbs_conv_factor: 2,
  beam_size: {beam_pa: 0.5, beam_min_scaled: 1.5, beam_maj_scaled: 2.5}
};

describe('The low continuum results mapper functions', () => {
  it('should map the Low continuum responses to the results with full weighting', () => {
    const expectedContinuumResults: ContinuumResults = {
      weightedContinuumSensitivity: '3.85 uJy/beam (1.50)',
      continuumConfusionNoise: '1.23 uJy/beam',
      totalContinuumSensitivity: '4.04 uJy/beam',
      continuumSynthesizedBeamSize: '10800.0" x 7200.0"',
      continuumSurfaceBrightnessSensitivity: '16.17 uK',
      weightedSpectralSensitivity: '1.48 uJy/beam (1.20)',
      spectralConfusionNoise: '900.00 nJy/beam',
      totalSpectralSensitivity: '1.73 uJy/beam',
      spectralSynthesizedBeamSize: '9000.0" x 5400.0"',
      spectralSurfaceBrightnessSensitivity: '3.47 uK',
      spectropolarimetryResults: {
        fwhmOfTheRMSF: '5.0 rad/m^2',
        maximumFaradayDepth: '7.0 rad/m^2',
        maximumFaradayDepthExtent: '6.0 rad/m^2'
      },
      weightedSensitivityPerSubBand: '8.40 uJy/beam - 11.70 uJy/beam (1.50)',
      confusionNoisePerSubBand: '1.23 uJy/beam - 1.23 uJy/beam',
      totalSensitivityPerSubBand: '8.49 uJy/beam - 11.76 uJy/beam',
      synthesizedBeamSizePerSubBand: '10800.0" x 7200.0" - 10800.0" x 7200.0"',
      surfaceBrightnessSensitivityPerSubBand: '33.96 uK - 47.06 uK',
      warnings: {
        errorMsg: 'Warning: test error',
        integrationTimesDifferent: false,
        sensLimitReached: {reached: true}
      }
    };

    expect(
      lowContinuumResponseToSensitivityFullResults(continuumCalculateResult, continuumWeightingResult, lineWeightingResult)
    ).toStrictEqual(expectedContinuumResults);
  });

  it('should map the Low continuum responses to the results with non Gaussian weighting', () => {
    const expectedContinuumResults: ContinuumResults = {
      weightedContinuumSensitivity: '3.85 uJy/beam (1.50)',
      continuumConfusionNoise: constants.result.nonGaussianBeam,
      totalContinuumSensitivity: constants.result.nonGaussianBeam,
      continuumSynthesizedBeamSize: constants.result.nonGaussianBeam,
      continuumSurfaceBrightnessSensitivity: constants.result.nonGaussianBeam,
      weightedSpectralSensitivity: '1.48 uJy/beam (1.20)',
      spectralConfusionNoise: constants.result.nonGaussianBeam,
      totalSpectralSensitivity: constants.result.nonGaussianBeam,
      spectralSynthesizedBeamSize: constants.result.nonGaussianBeam,
      spectralSurfaceBrightnessSensitivity: constants.result.nonGaussianBeam,
      spectropolarimetryResults: {
        fwhmOfTheRMSF: '5.0 rad/m^2',
        maximumFaradayDepth: '7.0 rad/m^2',
        maximumFaradayDepthExtent: '6.0 rad/m^2'
      },
      weightedSensitivityPerSubBand: '8.40 uJy/beam - 11.70 uJy/beam (1.50)',
      confusionNoisePerSubBand: constants.result.nonGaussianBeam,
      totalSensitivityPerSubBand: constants.result.nonGaussianBeam,
      synthesizedBeamSizePerSubBand: constants.result.nonGaussianBeam,
      surfaceBrightnessSensitivityPerSubBand: constants.result.nonGaussianBeam,
      warnings: {
        errorMsg: 'Warning: test error',
        integrationTimesDifferent: false,
        sensLimitReached: {reached: false}
      }
    };

    expect(
      lowContinuumResponseToSensitivityResultsNoneGaussian(continuumCalculateResult, continuumWeightingResult, lineWeightingResult)
    ).toStrictEqual(expectedContinuumResults);
  });

    it('should map the Low continuum responses to the results when weighting is not available', () => {
    const expectedContinuumResults: ContinuumResults = {
      weightedContinuumSensitivity: '2.57 uJy/beam (1.00)',
      continuumConfusionNoise: constants.result.noBeamAvailable,
      totalContinuumSensitivity: constants.result.noBeamAvailable,
      continuumSynthesizedBeamSize: constants.result.noBeamAvailable,
      continuumSurfaceBrightnessSensitivity: constants.result.noBeamAvailable,
      weightedSpectralSensitivity: '1.23 uJy/beam (1.00)',
      spectralConfusionNoise: constants.result.noBeamAvailable,
      totalSpectralSensitivity: constants.result.noBeamAvailable,
      spectralSynthesizedBeamSize: constants.result.noBeamAvailable,
      spectralSurfaceBrightnessSensitivity: constants.result.noBeamAvailable,
      spectropolarimetryResults: {
        fwhmOfTheRMSF: '5.0 rad/m^2',
        maximumFaradayDepth: '7.0 rad/m^2',
        maximumFaradayDepthExtent: '6.0 rad/m^2'
      },
      weightedSensitivityPerSubBand: '5.60 uJy/beam - 7.80 uJy/beam (1.00)',
      confusionNoisePerSubBand: constants.result.noBeamAvailable,
      totalSensitivityPerSubBand: constants.result.noBeamAvailable,
      synthesizedBeamSizePerSubBand: constants.result.noBeamAvailable,
      surfaceBrightnessSensitivityPerSubBand: constants.result.noBeamAvailable,
      warnings: {
        errorMsg: 'Warning: test error',
        integrationTimesDifferent: true,
        sensLimitReached: {reached: false}
      }
    };

    expect(
      lowContinuumResponseToSensitivityResultsWithoutWeighting(continuumCalculateResult, true)
    ).toStrictEqual(expectedContinuumResults);
  });
});
