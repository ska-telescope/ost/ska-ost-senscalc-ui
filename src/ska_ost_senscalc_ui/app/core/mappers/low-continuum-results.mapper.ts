import {
  LowContinuumSensitivityResponse,
  LowContinuumWeightingResult
} from "../../generated/api/low";
import helpers from "../../shared/utils/helpers";
import constants from "../../shared/utils/constants";
import {ContinuumResults} from "../types";
import {
  getConfusionNoiseRawValueInuJy,
  getFirstSubbandConfusionNoiseInuJy,
  getLastSubbandConfusionNoiseInuJy, getNoBeamAvailableContinuumResults,
  getNonGaussianBeamContinuumResults,
  getSpectropolarimetryResults,
  getSurfaceBrightnessSensitivity,
  getSynthesizedBeamSize, getWeightedSensitivityWithWeightedFactor, lowBeamSizePrecision
} from "./mapper.utils";

export const lowContinuumResponseToSensitivityFullResults = (continuumCalculateResult: LowContinuumSensitivityResponse, continuumWeightingResult: LowContinuumWeightingResult, lineWeightingResult: LowContinuumWeightingResult, integrationTimesDifferent = false): ContinuumResults => {
  const weightedContinuumSensitivityRawValue = continuumCalculateResult.continuum_sensitivity!.value! * continuumWeightingResult.weighting_factor;
  const continuumConfusionNoiseRawValue = getConfusionNoiseRawValueInuJy(continuumWeightingResult);
  const totalContinuumSensitivityRawValue = helpers.calculate.sqrtOfSumSqs(continuumConfusionNoiseRawValue, weightedContinuumSensitivityRawValue);

  const weightedContinuumSensitivity = getWeightedSensitivityWithWeightedFactor(weightedContinuumSensitivityRawValue, continuumWeightingResult);
  const continuumConfusionNoise = helpers.format.convertSensitivityToDisplayValue(continuumConfusionNoiseRawValue);
  const totalContinuumSensitivity = helpers.format.convertSensitivityToDisplayValue(totalContinuumSensitivityRawValue);
  const continuumSynthesizedBeamSize = getSynthesizedBeamSize(continuumWeightingResult, lowBeamSizePrecision);
  const continuumSurfaceBrightnessSensitivity = getSurfaceBrightnessSensitivity(continuumWeightingResult, totalContinuumSensitivityRawValue);

  const continuumSensLimitReached = helpers.calculate.sensitivityLimitCheck(Number(continuumConfusionNoiseRawValue), totalContinuumSensitivityRawValue);

  const weightedSpectralSensitivityRawValue= continuumCalculateResult.spectral_sensitivity!.value! * lineWeightingResult.weighting_factor;
  const spectralConfusionNoiseRawValue = getConfusionNoiseRawValueInuJy(lineWeightingResult);
  const totalSpectralSensitivityRawValue = helpers.calculate.sqrtOfSumSqs(spectralConfusionNoiseRawValue, weightedSpectralSensitivityRawValue);

  const weightedSpectralSensitivity = getWeightedSensitivityWithWeightedFactor(weightedSpectralSensitivityRawValue, lineWeightingResult);
  const spectralConfusionNoise = helpers.format.convertSensitivityToDisplayValue(spectralConfusionNoiseRawValue);
  const totalSpectralSensitivity = helpers.format.convertSensitivityToDisplayValue(totalSpectralSensitivityRawValue);
  const spectralSynthesizedBeamSize = getSynthesizedBeamSize(lineWeightingResult, lowBeamSizePrecision);
  const spectralSurfaceBrightnessSensitivity = getSurfaceBrightnessSensitivity(lineWeightingResult, totalSpectralSensitivityRawValue);

 const lineSensLimitReached = helpers.calculate.sensitivityLimitCheck(Number(spectralConfusionNoiseRawValue), totalSpectralSensitivityRawValue);

  let subBandData = {};
  let maxSubbandSenseLimitReached = false;
  let minSubbandSenseLimitReached = false;
  if (continuumCalculateResult.continuum_subband_sensitivities?.length && continuumWeightingResult.subbands?.length) {
    const n_subbands = continuumCalculateResult.continuum_subband_sensitivities.length;
    const maxWeightedSensitivity = (continuumCalculateResult.continuum_subband_sensitivities[0].sensitivity!.value!) * continuumWeightingResult.weighting_factor;
    const minWeightedSensitivity = (continuumCalculateResult.continuum_subband_sensitivities[n_subbands - 1].sensitivity!.value!) * continuumWeightingResult.weighting_factor;

    const maxConfusionNoise = getFirstSubbandConfusionNoiseInuJy(continuumWeightingResult);
    const minConfusionNoise = getLastSubbandConfusionNoiseInuJy(continuumWeightingResult);

    const maxTotalSensitivity = helpers.calculate.sqrtOfSumSqs(maxConfusionNoise, maxWeightedSensitivity);
    const minTotalSensitivity = helpers.calculate.sqrtOfSumSqs(minConfusionNoise, minWeightedSensitivity);

    const maxSynthesizedBeam = getSynthesizedBeamSize(continuumWeightingResult.subbands[0], lowBeamSizePrecision);
    const minSynthesizedBeam = getSynthesizedBeamSize(continuumWeightingResult.subbands[n_subbands - 1], lowBeamSizePrecision);

    const maxSbSensitivity = getSurfaceBrightnessSensitivity(continuumWeightingResult.subbands[0], maxTotalSensitivity);
    const minSbSensitivity = getSurfaceBrightnessSensitivity(continuumWeightingResult.subbands[n_subbands - 1], minTotalSensitivity);

    maxSubbandSenseLimitReached = helpers.calculate.sensitivityLimitCheck(Number(maxConfusionNoise), maxTotalSensitivity);
    minSubbandSenseLimitReached = helpers.calculate.sensitivityLimitCheck(Number(minConfusionNoise), minTotalSensitivity);
    subBandData = {
      weightedSensitivityPerSubBand: `${helpers.format.convertSensitivityToDisplayValue(maxWeightedSensitivity)} - ${helpers.format.convertSensitivityToDisplayValue(minWeightedSensitivity)} (${continuumWeightingResult.weighting_factor.toFixed(2)})`,
      confusionNoisePerSubBand: `${helpers.format.convertSensitivityToDisplayValue(maxConfusionNoise)} - ${helpers.format.convertSensitivityToDisplayValue(minConfusionNoise)}`,
      totalSensitivityPerSubBand: `${helpers.format.convertSensitivityToDisplayValue(maxTotalSensitivity)} - ${helpers.format.convertSensitivityToDisplayValue(minTotalSensitivity)}`,
      synthesizedBeamSizePerSubBand: `${maxSynthesizedBeam} - ${minSynthesizedBeam}`,
      surfaceBrightnessSensitivityPerSubBand: `${maxSbSensitivity} - ${minSbSensitivity}`
    };
  }

  const warnings = {
      sensLimitReached: { reached: continuumSensLimitReached || lineSensLimitReached || maxSubbandSenseLimitReached || minSubbandSenseLimitReached },
      integrationTimesDifferent: integrationTimesDifferent,
      errorMsg: continuumCalculateResult.warning ? `Warning: ${continuumCalculateResult.warning}` : undefined
    };

  return {weightedContinuumSensitivity, continuumConfusionNoise, totalContinuumSensitivity, continuumSynthesizedBeamSize, continuumSurfaceBrightnessSensitivity,
    weightedSpectralSensitivity, spectralConfusionNoise, totalSpectralSensitivity, spectralSynthesizedBeamSize, spectralSurfaceBrightnessSensitivity,
    ...subBandData, ...getSpectropolarimetryResults(continuumCalculateResult), warnings

  };

};

export const lowContinuumResponseToSensitivityResultsNoneGaussian = (continuumCalculateResult: LowContinuumSensitivityResponse, continuumWeightingResult: LowContinuumWeightingResult, lineWeightingResult: LowContinuumWeightingResult, integrationTimesDifferent = false): ContinuumResults => {
  const weightedContinuumSensitivityRawValue = continuumCalculateResult.continuum_sensitivity!.value! * continuumWeightingResult.weighting_factor;

  const weightedContinuumSensitivity = getWeightedSensitivityWithWeightedFactor(weightedContinuumSensitivityRawValue, continuumWeightingResult);

  const weightedSpectralSensitivityRawValue= continuumCalculateResult.spectral_sensitivity!.value! * lineWeightingResult.weighting_factor;

  const weightedSpectralSensitivity = getWeightedSensitivityWithWeightedFactor(weightedSpectralSensitivityRawValue, lineWeightingResult);

  let subBandData = {};
  if (continuumCalculateResult.continuum_subband_sensitivities?.length && continuumWeightingResult.subbands?.length) {
    const n_subbands = continuumCalculateResult.continuum_subband_sensitivities.length;
    const maxWeightedSensitivity = continuumCalculateResult.continuum_subband_sensitivities[0].sensitivity!.value! * continuumWeightingResult.weighting_factor;
    const minWeightedSensitivity = continuumCalculateResult.continuum_subband_sensitivities[n_subbands - 1].sensitivity!.value! * continuumWeightingResult.weighting_factor;

    const weightedSensitivityPerSubBand = `${helpers.format.convertSensitivityToDisplayValue(maxWeightedSensitivity)} - ${helpers.format.convertSensitivityToDisplayValue(minWeightedSensitivity)} (${continuumWeightingResult.weighting_factor.toFixed(2)})`;
    subBandData = {
      weightedSensitivityPerSubBand,
      confusionNoisePerSubBand: constants.result.nonGaussianBeam,
      totalSensitivityPerSubBand: constants.result.nonGaussianBeam,
      synthesizedBeamSizePerSubBand: constants.result.nonGaussianBeam,
      surfaceBrightnessSensitivityPerSubBand: constants.result.nonGaussianBeam
    };
  }

  const warnings = {
      sensLimitReached: { reached: false },
      integrationTimesDifferent: integrationTimesDifferent,
      errorMsg: continuumCalculateResult.warning ? `Warning: ${continuumCalculateResult.warning}` : undefined
    };

  return {weightedContinuumSensitivity, weightedSpectralSensitivity, ...getNonGaussianBeamContinuumResults(),
    ...subBandData, ...getSpectropolarimetryResults(continuumCalculateResult), warnings
  };
};

export const lowContinuumResponseToSensitivityResultsWithoutWeighting = (continuumCalculateResult: LowContinuumSensitivityResponse, integrationTimesDifferent = false): ContinuumResults => {
  const weightedContinuumSensitivity = getWeightedSensitivityWithWeightedFactor(continuumCalculateResult.continuum_sensitivity!.value!);

  const weightedSpectralSensitivity = getWeightedSensitivityWithWeightedFactor(continuumCalculateResult.spectral_sensitivity!.value!);

  let subBandData = {};
  if (continuumCalculateResult.continuum_subband_sensitivities?.length) {
      const maxWeightedSensitivity = helpers.format.convertSensitivityToDisplayValue(continuumCalculateResult.continuum_subband_sensitivities[0].sensitivity!.value!);
      const minWeightedSensitivity = helpers.format.convertSensitivityToDisplayValue(continuumCalculateResult.continuum_subband_sensitivities[continuumCalculateResult.continuum_subband_sensitivities.length - 1].sensitivity!.value!);

     subBandData = {
       weightedSensitivityPerSubBand: `${maxWeightedSensitivity} - ${minWeightedSensitivity} (1.00)`,
       confusionNoisePerSubBand: constants.result.noBeamAvailable,
       totalSensitivityPerSubBand: constants.result.noBeamAvailable,
       synthesizedBeamSizePerSubBand: constants.result.noBeamAvailable,
       surfaceBrightnessSensitivityPerSubBand: constants.result.noBeamAvailable
     };
    }

  const warnings = {
    sensLimitReached: {reached: false},
    integrationTimesDifferent: integrationTimesDifferent,
    errorMsg: continuumCalculateResult.warning ? `Warning: ${continuumCalculateResult.warning}` : undefined
  };

  return {weightedContinuumSensitivity, weightedSpectralSensitivity, ...getNoBeamAvailableContinuumResults(),
    ...subBandData, ...getSpectropolarimetryResults(continuumCalculateResult), warnings

  };
};
