import {PssResults} from "../types";
import {
  lowPssResponseToResults
} from "./low-pss-results.mapper";
import {
  LowPssSensitivityResponse
} from "../../generated/api/low";

const continuumCalculateResult: LowPssSensitivityResponse = {
  folded_pulse_sensitivity: {value: 2.567, unit: 'uJy'},
  warning: 'test error'
};

describe('The low pss results mapper functions', () => {
  it('should map the Low pss responses to the results', () => {
    const expectedContinuumResults: PssResults = {
      sensitivity: '2.57 uJy',
      warnings: {
        errorMsg: 'Warning: test error',
        integrationTimesDifferent: true,
        sensLimitReached: {reached: false}
      }
    };

    expect(
      lowPssResponseToResults(continuumCalculateResult, true)
    ).toStrictEqual(expectedContinuumResults);
  });
});
