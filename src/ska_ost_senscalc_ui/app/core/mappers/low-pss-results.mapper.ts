import {
  LowPssSensitivityResponse
} from "../../generated/api/low";
import helpers from "../../shared/utils/helpers";
import {PssResults} from "../types";

export const lowPssResponseToResults = (pssCalculateResult: LowPssSensitivityResponse, integrationTimesDifferent = false): PssResults => {
  const sensitivity = helpers.format.convertSensitivityUnit((pssCalculateResult.folded_pulse_sensitivity!.value!));

  const warnings = {
    sensLimitReached: {reached: false},
    integrationTimesDifferent: integrationTimesDifferent,
    errorMsg: pssCalculateResult.warning ? `Warning: ${pssCalculateResult.warning}` : undefined
  };
  return {sensitivity, warnings};
};
