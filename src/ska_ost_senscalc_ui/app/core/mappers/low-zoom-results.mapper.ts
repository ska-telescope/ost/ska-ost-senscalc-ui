import {
  LowSingleZoomSensitivityResponse, LowSingleWeightingZoomResult
} from "../../generated/api/low";
import helpers from "../../shared/utils/helpers";
import {ZoomResults} from "../types";
import {
  getSurfaceBrightnessSensitivity,
  getSynthesizedBeamSize,
  getSpectropolarimetryResults,
  getConfusionNoiseRawValueInuJy,
  getNonGaussianBeamZoomResults,
  getNoBeamAvailableZoomResults,
  lowBeamSizePrecision,
  getWeightedSensitivityWithWeightedFactor
} from "./mapper.utils";

export const lowZoomResponseToSensitivityFullResults = (zoomCalculateResult: Array<LowSingleZoomSensitivityResponse>, zoomWeightingResult: Array<LowSingleWeightingZoomResult>, integrationTimesDifferent = false): ZoomResults => {
  // The API supports multiple zoom windows so returns and array, but the UI only supports one, so we take the first element
  const calculateResult = zoomCalculateResult[0];
  const weightingResult = zoomWeightingResult[0];

  const weightedSpectralSensitivityRawValue= calculateResult.spectral_sensitivity!.value! * weightingResult.weighting_factor;
  const spectralConfusionNoiseRawValue = getConfusionNoiseRawValueInuJy(weightingResult);
  const totalSpectralSensitivityRawValue = helpers.calculate.sqrtOfSumSqs(spectralConfusionNoiseRawValue, weightedSpectralSensitivityRawValue);

  const weightedSpectralSensitivity = getWeightedSensitivityWithWeightedFactor(weightedSpectralSensitivityRawValue, weightingResult);
  const spectralConfusionNoise = helpers.format.convertSensitivityToDisplayValue(spectralConfusionNoiseRawValue);
  const totalSpectralSensitivity = helpers.format.convertSensitivityToDisplayValue(totalSpectralSensitivityRawValue);
  const spectralSynthesizedBeamSize = getSynthesizedBeamSize(weightingResult, lowBeamSizePrecision);
  const spectralSurfaceBrightnessSensitivity = getSurfaceBrightnessSensitivity(weightingResult, totalSpectralSensitivityRawValue);

  const warnings = {
    sensLimitReached: {reached: helpers.calculate.sensitivityLimitCheck(spectralConfusionNoiseRawValue, totalSpectralSensitivityRawValue)},
    integrationTimesDifferent: integrationTimesDifferent,
    errorMsg: calculateResult.warning ? `Warning: ${calculateResult.warning}` : undefined
  };

  return {weightedSpectralSensitivity, spectralConfusionNoise, totalSpectralSensitivity, spectralSynthesizedBeamSize, spectralSurfaceBrightnessSensitivity, ...getSpectropolarimetryResults(calculateResult), warnings};
};

export const lowZoomResponseToSensitivityResultsNoneGaussian = (zoomCalculateResult: Array<LowSingleZoomSensitivityResponse>, zoomWeightingResult: Array<LowSingleWeightingZoomResult>, integrationTimesDifferent = false): ZoomResults => {
  // The API supports multiple zoom windows so returns and array, but the UI only supports one, so we take the first element
  const calculateResult = zoomCalculateResult[0];
  const weightingResult = zoomWeightingResult[0];

  const weightedSpectralSensitivityRawValue= calculateResult.spectral_sensitivity!.value! * weightingResult.weighting_factor;

  const weightedSpectralSensitivity = getWeightedSensitivityWithWeightedFactor(weightedSpectralSensitivityRawValue, weightingResult);

  const warnings = {
    sensLimitReached: {reached: false},
    integrationTimesDifferent: integrationTimesDifferent,
    errorMsg: calculateResult.warning ? `Warning: ${calculateResult.warning}` : undefined
  };

  return {weightedSpectralSensitivity, ...getNonGaussianBeamZoomResults(), ...getSpectropolarimetryResults(calculateResult), warnings};
};

export const lowZoomResponseToSensitivityResultsWithoutWeighting = (zoomCalculateResult: Array<LowSingleZoomSensitivityResponse>, integrationTimesDifferent = false) => {
  // The API supports multiple zoom windows so returns and array, but the UI only supports one, so we take the first element
  const calculateResult = zoomCalculateResult[0];

  const weightedSpectralSensitivity = getWeightedSensitivityWithWeightedFactor(calculateResult.spectral_sensitivity!.value!);

  const warnings = {
    sensLimitReached: {reached: false},
    integrationTimesDifferent: integrationTimesDifferent,
    errorMsg: calculateResult.warning ? `Warning: ${calculateResult.warning}` : undefined
  };

  return {weightedSpectralSensitivity, ...getNoBeamAvailableZoomResults(), ...getSpectropolarimetryResults(calculateResult), warnings};
};
