import {
  MidContinuumSensitivityResponse,
  MidContinuumWeightingResult,
  MidSingleWeightingZoomResult
} from "../../generated/api/mid";
import {ContinuumResults, SpectropolarimetryResults, ZoomResults} from "../types";
import helpers from "../../shared/utils/helpers";
import {
  LowContinuumSensitivityResponse,
  LowContinuumWeightingResult,
  LowSingleWeightingZoomResult
} from "../../generated/api/low";
import constants from "../../shared/utils/constants";

type ContinuumSensitivityResult = LowContinuumSensitivityResponse | MidContinuumSensitivityResponse;
type ContinuumWeightingResult = LowContinuumWeightingResult | MidContinuumWeightingResult;
type WeightingResult = ContinuumWeightingResult | LowSingleWeightingZoomResult | MidSingleWeightingZoomResult;

export const getSpectropolarimetryResults = (calculateResult: ContinuumSensitivityResult): {spectropolarimetryResults: SpectropolarimetryResults} => {
  return {
    spectropolarimetryResults: {
      fwhmOfTheRMSF: helpers.format.displayValueWithUnit(calculateResult.spectropolarimetry_results!.fwhm_of_the_rmsf!),
      maximumFaradayDepthExtent: helpers.format.displayValueWithUnit(calculateResult.spectropolarimetry_results!.max_faraday_depth_extent!),
      maximumFaradayDepth: helpers.format.displayValueWithUnit(calculateResult.spectropolarimetry_results!.max_faraday_depth!)
    }
  };
};

export const getWeightedSensitivityWithWeightedFactor = (weightedSensitivityRawValueInuJy: number, weightingResult?: WeightingResult): string => (
  `${helpers.format.convertSensitivityToDisplayValue(weightedSensitivityRawValueInuJy)} (${weightingResult?.weighting_factor?.toFixed(2) ?? '1.00'})`
);

export const lowBeamSizePrecision = 1;
export const midBeamSizePrecision = 3;

export const getSynthesizedBeamSize = (weightingResult: WeightingResult, toFixed = midBeamSizePrecision): string => {
    const min = weightingResult.beam_size.beam_min_scaled * 3600;
    const major = weightingResult.beam_size.beam_maj_scaled * 3600;
    return `${major.toFixed(toFixed)}" x ${min.toFixed(toFixed)}"`;
};

export const getSurfaceBrightnessSensitivity = (weightingResponse: WeightingResult, totalSensitivityRawValue: number): string => {
  return helpers.format.convertKelvinsToDisplayValue(totalSensitivityRawValue * weightingResponse.sbs_conv_factor);
};

// if beam size is smaller than minimum limit, set confusion noise as 0, if not get confusion noise value from weighting response
export const getConfusionNoiseRawValueInuJy = (weightingResult: WeightingResult): number =>
  // Confusion noise is returned from the API in uJy, but the other sensitivities are uJy, so we convert here (and hope some future refactoring makes the unit handling more robust!)
  helpers.calculate.minBeamSizeCheck(weightingResult, 1) ? 0 : weightingResult.confusion_noise.value * 1e6;

export const getFirstSubbandConfusionNoiseInuJy = (continuumWeightingResult: ContinuumWeightingResult): number =>
  continuumWeightingResult.subbands![0].confusion_noise.value  * 1e6;

export const getLastSubbandConfusionNoiseInuJy = (continuumWeightingResult: ContinuumWeightingResult): number =>
  continuumWeightingResult.subbands![continuumWeightingResult.subbands!.length - 1].confusion_noise.value  * 1e6;

export const getNonGaussianBeamContinuumResults = (onlyIntegrationTime = false): ContinuumResults => getContinuumResultsWithStaticMessage(constants.result.nonGaussianBeam, onlyIntegrationTime);

export const getNoBeamAvailableContinuumResults = (onlyIntegrationTime = false): ContinuumResults => getContinuumResultsWithStaticMessage(constants.result.noBeamAvailable, onlyIntegrationTime);

export const getNonGaussianBeamZoomResults = (onlyIntegrationTime = false): ZoomResults => getZoomResultsWithStaticMessage(constants.result.nonGaussianBeam, onlyIntegrationTime);

export const getNoBeamAvailableZoomResults = (onlyIntegrationTime = false): ZoomResults => getZoomResultsWithStaticMessage(constants.result.noBeamAvailable, onlyIntegrationTime);

const getContinuumResultsWithStaticMessage = (message: string, onlyIntegrationTime: boolean): ContinuumResults => (
  onlyIntegrationTime ? {
    continuumConfusionNoise: message,
    continuumSynthesizedBeamSize: message,
    spectralConfusionNoise: message,
    spectralSynthesizedBeamSize: message
  } : {
    continuumConfusionNoise: message,
    totalContinuumSensitivity:message,
    continuumSynthesizedBeamSize: message,
    continuumSurfaceBrightnessSensitivity: message,
    spectralConfusionNoise: message,
    totalSpectralSensitivity: message,
    spectralSynthesizedBeamSize: message,
    spectralSurfaceBrightnessSensitivity: message
  });

const getZoomResultsWithStaticMessage = (message: string, onlyIntegrationTime: boolean): ZoomResults => (
  onlyIntegrationTime ? {
    spectralConfusionNoise: message,
    spectralSynthesizedBeamSize: message
  } : {
    spectralConfusionNoise: message,
    totalSpectralSensitivity: message,
    spectralSynthesizedBeamSize: message,
    spectralSurfaceBrightnessSensitivity: message
  });
