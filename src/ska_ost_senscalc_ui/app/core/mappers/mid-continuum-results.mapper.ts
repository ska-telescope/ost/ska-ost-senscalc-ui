import {
  MidContinuumSensitivityResponse,
  MidContinuumWeightingResult
} from "../../generated/api/mid";
import helpers from "../../shared/utils/helpers";
import constants from "../../shared/utils/constants";
import {ContinuumResults, Warnings} from "../types";
import {
  getSurfaceBrightnessSensitivity,
  getSynthesizedBeamSize,
  getSpectropolarimetryResults,
  getConfusionNoiseRawValueInuJy,
  getFirstSubbandConfusionNoiseInuJy,
  getLastSubbandConfusionNoiseInuJy,
  getNonGaussianBeamContinuumResults, getNoBeamAvailableContinuumResults, getWeightedSensitivityWithWeightedFactor
} from "./mapper.utils";

export const midContinuumResponseToSensitivityFullResults = (continuumCalculateResult: MidContinuumSensitivityResponse, continuumWeightingResult: MidContinuumWeightingResult, lineWeightingResult: MidContinuumWeightingResult, integrationTimesDifferent = false): ContinuumResults => {
    const weightedContinuumSensitivityRawValue = continuumCalculateResult.continuum_sensitivity!.value! * continuumWeightingResult.weighting_factor * 1e6;
    const continuumConfusionNoiseRawValue = getConfusionNoiseRawValueInuJy(continuumWeightingResult);
    const totalContinuumSensitivityRawValue = helpers.calculate.sqrtOfSumSqs(continuumConfusionNoiseRawValue, weightedContinuumSensitivityRawValue);

        // We start with the flag to check whether the sensitivity is approaching the confusion noise limit
    // as false, then set it to true if any of the sets of results fail the check.
    // For time -> sensitivity, we don't display the value in the warning message as any or all of the
    // sensitivities (cont, subband, spectral) could be near the limit
    const warnings: Warnings = { sensLimitReached: {reached: false}, integrationTimesDifferent: integrationTimesDifferent };

      const continuumResults = {
        weightedContinuumSensitivity: getWeightedSensitivityWithWeightedFactor(weightedContinuumSensitivityRawValue, continuumWeightingResult),
        continuumConfusionNoise: helpers.format.convertSensitivityToDisplayValue(continuumConfusionNoiseRawValue),
        totalContinuumSensitivity: helpers.format.convertSensitivityToDisplayValue(totalContinuumSensitivityRawValue),
        continuumSynthesizedBeamSize: getSynthesizedBeamSize(continuumWeightingResult),
        continuumSurfaceBrightnessSensitivity: getSurfaceBrightnessSensitivity(continuumWeightingResult, totalContinuumSensitivityRawValue)
      };
      const continuumSensLimitReached = helpers.calculate.sensitivityLimitCheck(continuumConfusionNoiseRawValue, totalContinuumSensitivityRawValue);
      if (continuumSensLimitReached) {
        warnings.sensLimitReached!.reached = true;
      }

      const weightedSpectralSensitivityRawValue= continuumCalculateResult.spectral_sensitivity!.value! * lineWeightingResult.weighting_factor * 1e6;
      const spectralConfusionNoiseRawValue = getConfusionNoiseRawValueInuJy(lineWeightingResult);
      const totalSpectralSensitivityRawValue = helpers.calculate.sqrtOfSumSqs(spectralConfusionNoiseRawValue, weightedSpectralSensitivityRawValue);

      const spectralResults = {
        weightedSpectralSensitivity: getWeightedSensitivityWithWeightedFactor(weightedSpectralSensitivityRawValue, lineWeightingResult),
        spectralConfusionNoise:  helpers.format.convertSensitivityToDisplayValue(spectralConfusionNoiseRawValue),
        totalSpectralSensitivity:  helpers.format.convertSensitivityToDisplayValue(totalSpectralSensitivityRawValue),
        spectralSynthesizedBeamSize: getSynthesizedBeamSize(lineWeightingResult),
        spectralSurfaceBrightnessSensitivity: getSurfaceBrightnessSensitivity(lineWeightingResult, totalSpectralSensitivityRawValue)
      };

      const lineSensLimitReached = helpers.calculate.sensitivityLimitCheck(spectralConfusionNoiseRawValue, totalSpectralSensitivityRawValue);
      if (lineSensLimitReached) {
        warnings.sensLimitReached!.reached = true;
      }

      let subBandResults = {};
      if (continuumCalculateResult.continuum_subband_sensitivities && continuumWeightingResult.subbands) {
          // If beamNotApplicableMessage is not truthy, ie beam weighting cannot be applied, then we shouldn't calculate weighted values
      const n_subbands = continuumCalculateResult.continuum_subband_sensitivities.length;
      let maxSubbandSenseLimitReached = false;
      let minSubbandSenseLimitReached = false;

      const maxWeightedSensitivity = (continuumCalculateResult.continuum_subband_sensitivities[0].sensitivity!.value!) * continuumWeightingResult.weighting_factor * 1e6;
      const minWeightedSensitivity = (continuumCalculateResult.continuum_subband_sensitivities[n_subbands - 1].sensitivity!.value!) * continuumWeightingResult.weighting_factor * 1e6;

      const maxConfusionNoise = getFirstSubbandConfusionNoiseInuJy(continuumWeightingResult);
      const minConfusionNoise = getLastSubbandConfusionNoiseInuJy(continuumWeightingResult);

      const maxTotalSensitivity = helpers.calculate.sqrtOfSumSqs(maxConfusionNoise, maxWeightedSensitivity);
      const minTotalSensitivity = helpers.calculate.sqrtOfSumSqs(minConfusionNoise, minWeightedSensitivity);

      const maxSynthesizedBeam = getSynthesizedBeamSize(continuumWeightingResult.subbands[0]);
      const minSynthesizedBeam = getSynthesizedBeamSize(continuumWeightingResult.subbands[n_subbands - 1]);

      const maxSbSensitivity = getSurfaceBrightnessSensitivity(continuumWeightingResult.subbands[0], maxTotalSensitivity);
      const minSbSensitivity = getSurfaceBrightnessSensitivity(continuumWeightingResult.subbands[n_subbands - 1], minTotalSensitivity);

      subBandResults = {
        weightedSensitivityPerSubBand: `${helpers.format.convertSensitivityToDisplayValue(maxWeightedSensitivity)} - ${helpers.format.convertSensitivityToDisplayValue(minWeightedSensitivity)} (${continuumWeightingResult.weighting_factor.toFixed(2)})`,
        confusionNoisePerSubBand: `${helpers.format.convertSensitivityToDisplayValue(maxConfusionNoise)} - ${helpers.format.convertSensitivityToDisplayValue(minConfusionNoise)}`,
        totalSensitivityPerSubBand: `${helpers.format.convertSensitivityToDisplayValue(maxTotalSensitivity)} - ${helpers.format.convertSensitivityToDisplayValue(minTotalSensitivity)}`,
        synthesizedBeamSizePerSubBand: `${maxSynthesizedBeam} - ${minSynthesizedBeam}`,
        surfaceBrightnessSensitivityPerSubBand: `${maxSbSensitivity} - ${minSbSensitivity}`
      };
      maxSubbandSenseLimitReached =  helpers.calculate.sensitivityLimitCheck(maxConfusionNoise, maxTotalSensitivity);
      minSubbandSenseLimitReached = helpers.calculate.sensitivityLimitCheck(minConfusionNoise, minTotalSensitivity);

      if (maxSubbandSenseLimitReached || minSubbandSenseLimitReached) {
         warnings.sensLimitReached!.reached = true;
        }
      }

      return {...continuumResults, ...spectralResults, ...subBandResults, ...getSpectropolarimetryResults(continuumCalculateResult), warnings};
};

export const midContinuumResponseToSensitivityResultsNoneGaussian = (continuumCalculateResult: MidContinuumSensitivityResponse, continuumWeightingResult: MidContinuumWeightingResult, lineWeightingResult: MidContinuumWeightingResult, integrationTimesDifferent = false): ContinuumResults => {
    const weightedContinuumSensitivityRawValue = continuumCalculateResult.continuum_sensitivity!.value! * continuumWeightingResult.weighting_factor * 1e6;

    const warnings: Warnings = { sensLimitReached: {reached: false}, integrationTimesDifferent: integrationTimesDifferent };

    const weightedContinuumSensitivity = getWeightedSensitivityWithWeightedFactor(weightedContinuumSensitivityRawValue, continuumWeightingResult);

    const weightedSpectralSensitivityRawValue= continuumCalculateResult.spectral_sensitivity!.value! * lineWeightingResult.weighting_factor * 1e6;

    const weightedSpectralSensitivity = getWeightedSensitivityWithWeightedFactor(weightedSpectralSensitivityRawValue, lineWeightingResult);

    let subBandResults = {};
    if (continuumCalculateResult.continuum_subband_sensitivities && continuumWeightingResult.subbands) {
    const n_subbands = continuumCalculateResult.continuum_subband_sensitivities.length;
    const maxSensitivity = (continuumCalculateResult.continuum_subband_sensitivities[0].sensitivity!.value!) * 1e6;
    const minSensitivity = (continuumCalculateResult.continuum_subband_sensitivities[n_subbands - 1].sensitivity!.value!) * 1e6;

    subBandResults = {
        weightedSensitivityPerSubBand: `${helpers.format.convertSensitivityToDisplayValue(maxSensitivity)} - ${helpers.format.convertSensitivityToDisplayValue(minSensitivity)} (1.00)`,
        confusionNoisePerSubBand: constants.result.nonGaussianBeam,
        totalSensitivityPerSubBand: constants.result.nonGaussianBeam,
        synthesizedBeamSizePerSubBand: constants.result.nonGaussianBeam,
        surfaceBrightnessSensitivityPerSubBand: constants.result.nonGaussianBeam
      };

    }

    return {weightedContinuumSensitivity, weightedSpectralSensitivity, ...getNonGaussianBeamContinuumResults(), ...subBandResults, ...getSpectropolarimetryResults(continuumCalculateResult), warnings};
};

export const midContinuumResponseToSensitivityResultsWithoutWeighting = (continuumCalculateResult: MidContinuumSensitivityResponse, integrationTimesDifferent = false): ContinuumResults => {
    const weightedContinuumSensitivityRawValue = continuumCalculateResult.continuum_sensitivity!.value! * 1e6;

    const warnings: Warnings = { sensLimitReached: {reached: false}, integrationTimesDifferent: integrationTimesDifferent };

    const weightedContinuumSensitivity= getWeightedSensitivityWithWeightedFactor(weightedContinuumSensitivityRawValue);

    const weightedSpectralSensitivityRawValue= continuumCalculateResult.spectral_sensitivity!.value! * 1e6;

    const weightedSpectralSensitivity = getWeightedSensitivityWithWeightedFactor(weightedSpectralSensitivityRawValue);

    let subBandResults = {};
    if (continuumCalculateResult.continuum_subband_sensitivities) {
    const n_subbands = continuumCalculateResult.continuum_subband_sensitivities.length;
    const maxSensitivity = (continuumCalculateResult.continuum_subband_sensitivities[0].sensitivity!.value!) * 1e6;
    const minSensitivity = (continuumCalculateResult.continuum_subband_sensitivities[n_subbands - 1].sensitivity!.value!) * 1e6;

    subBandResults = {
        weightedSensitivityPerSubBand: `${helpers.format.convertSensitivityToDisplayValue(maxSensitivity)} - ${helpers.format.convertSensitivityToDisplayValue(minSensitivity)} (1.00)`,
        confusionNoisePerSubBand: constants.result.noBeamAvailable,
        totalSensitivityPerSubBand: constants.result.noBeamAvailable,
        synthesizedBeamSizePerSubBand: constants.result.noBeamAvailable,
        surfaceBrightnessSensitivityPerSubBand: constants.result.noBeamAvailable
      };
    }
    return {weightedContinuumSensitivity, weightedSpectralSensitivity, ...getNoBeamAvailableContinuumResults(), ...subBandResults, ...getSpectropolarimetryResults(continuumCalculateResult), warnings};
};

export const midContinuumResponseToIntegrationTimeFullResults = (inputSensitivityJy: number, continuumCalculateResult: MidContinuumSensitivityResponse, lineCalculateResult: MidContinuumSensitivityResponse, continuumWeightingResult: MidContinuumWeightingResult, lineWeightingResult: MidContinuumWeightingResult): ContinuumResults => {
    const continuumConfusionNoiseRawValue = getConfusionNoiseRawValueInuJy(continuumWeightingResult);

    // We start with the flag to check whether the sensitivity is approaching the confusion noise limit
    // as false, then set it to true if any of the sets of results fail the check.
    // For sensitivity -> time, we want to display the maximum value in the warning message
    const warnings: Warnings = { sensLimitReached: {reached: false}, integrationTimesDifferent: false };

    const continuumResults = {
      continuumSynthesizedBeamSize: getSynthesizedBeamSize(continuumWeightingResult),
      continuumConfusionNoise: helpers.format.convertSensitivityToDisplayValue(continuumConfusionNoiseRawValue),
      continuumIntegrationTime: helpers.format.convertTimeToDisplayUnit(continuumCalculateResult.continuum_integration_time!)
    };

    const spectralConfusionNoiseRawValue = getConfusionNoiseRawValueInuJy(lineWeightingResult);

    const spectralResults = {
      spectralSynthesizedBeamSize: getSynthesizedBeamSize(lineWeightingResult),
      spectralConfusionNoise:  helpers.format.convertSensitivityToDisplayValue(spectralConfusionNoiseRawValue),
      spectralIntegrationTime: helpers.format.convertTimeToDisplayUnit(lineCalculateResult.spectral_integration_time!)
    };

    let subBandResults;
    if (continuumCalculateResult.continuum_subband_integration_times && continuumWeightingResult.subbands) {
      const n_subbands = continuumCalculateResult.continuum_subband_integration_times.length;

      const maxTime = helpers.format.convertTimeToDisplayUnit(continuumCalculateResult.continuum_subband_integration_times[0].integration_time!);
      const minTime = helpers.format.convertTimeToDisplayUnit(continuumCalculateResult.continuum_subband_integration_times[n_subbands-1].integration_time!);

      const maxConfusionNoise = continuumWeightingResult.subbands[0].confusion_noise.value * 1e6;
      const minConfusionNoise = continuumWeightingResult.subbands[n_subbands - 1].confusion_noise.value * 1e6;

      const maxSynthesizedBeam = getSynthesizedBeamSize(continuumWeightingResult.subbands[0]);
      const minSynthesizedBeam = getSynthesizedBeamSize(continuumWeightingResult.subbands[n_subbands - 1]);

      subBandResults = {
      confusionNoisePerSubBand: `${helpers.format.convertSensitivityToDisplayValue(maxConfusionNoise)} - ${helpers.format.convertSensitivityToDisplayValue(minConfusionNoise)}`,
      synthesizedBeamSizePerSubBand: `${maxSynthesizedBeam} - ${minSynthesizedBeam}`,
        integrationTimePerSubBand: maxTime + " - "  + minTime
      };
    }
    // For sensitivity -> integration time we need to compare the entered (total) sensitivity with the up to
    // four confusion noise values that might be shown (full bandwidth, two confusion-noise values and spectral)
    const maxConfusionNoiseJy = continuumCalculateResult.continuum_subband_integration_times || continuumCalculateResult.continuum_subband_sensitivities
      ? Math.max(
        continuumWeightingResult.confusion_noise.value,
        lineWeightingResult.confusion_noise.value,
        continuumWeightingResult.subbands![0].confusion_noise.value,
        continuumWeightingResult.subbands![continuumWeightingResult.subbands!.length - 1].confusion_noise.value
      )
      : Math.max(
        continuumWeightingResult.confusion_noise.value,
        lineWeightingResult.confusion_noise.value
      );

    if (helpers.calculate.sensitivityLimitCheck(maxConfusionNoiseJy, inputSensitivityJy)) {
      warnings.sensLimitReached = {
        reached: true,
        value: helpers.format.convertSensitivityToDisplayValue(maxConfusionNoiseJy*1e6)
      };
    }
    return {...continuumResults, ...spectralResults, ...subBandResults, warnings, ...getSpectropolarimetryResults(continuumCalculateResult) };

};

export const midContinuumResponseToIntegrationTimeResultsNoneGaussian = (continuumCalculateResult: MidContinuumSensitivityResponse, lineCalculateResult: MidContinuumSensitivityResponse): ContinuumResults => {
    // Beam is not applicable => there is no confusion noise => limit not reached so leave it as false default
    const warnings: Warnings = { sensLimitReached: {reached: false}, integrationTimesDifferent: false };

    const continuumIntegrationTime = helpers.format.convertTimeToDisplayUnit(continuumCalculateResult.continuum_integration_time!);

    const spectralIntegrationTime = helpers.format.convertTimeToDisplayUnit(lineCalculateResult.spectral_integration_time!);

    let subBandResults;
    if (continuumCalculateResult.continuum_subband_integration_times) {
      const n_subbands = continuumCalculateResult.continuum_subband_integration_times.length;

      const maxTime = helpers.format.convertTimeToDisplayUnit(continuumCalculateResult.continuum_subband_integration_times[0].integration_time!);
      const minTime = helpers.format.convertTimeToDisplayUnit(continuumCalculateResult.continuum_subband_integration_times[n_subbands-1].integration_time!);

      subBandResults = {
      confusionNoisePerSubBand: constants.result.nonGaussianBeam,
      synthesizedBeamSizePerSubBand: constants.result.nonGaussianBeam,
        integrationTimePerSubBand: maxTime + " - "  + minTime
      };
    }
    // Beam is not applicable => there is no confusion noise => limit not reached so leave it as false default

    return {continuumIntegrationTime, spectralIntegrationTime, ...getNonGaussianBeamContinuumResults(true), ...subBandResults, warnings, ...getSpectropolarimetryResults(continuumCalculateResult) };
};

export const midContinuumResponseToIntegrationTimeResultsWithoutWeighting = (continuumCalculateResult: MidContinuumSensitivityResponse): ContinuumResults => {

 // Beam is not applicable => there is no confusion noise => limit not reached so leave it as false default
    const warnings: Warnings = { sensLimitReached: {reached: false}, integrationTimesDifferent: false };

    const continuumIntegrationTime = helpers.format.convertTimeToDisplayUnit(continuumCalculateResult.continuum_integration_time!);

    const spectralIntegrationTime = helpers.format.convertTimeToDisplayUnit(continuumCalculateResult.spectral_integration_time!);

    let subBandResults;
    if (continuumCalculateResult.continuum_subband_integration_times) {
      const n_subbands = continuumCalculateResult.continuum_subband_integration_times.length;

      const maxTime = helpers.format.convertTimeToDisplayUnit(continuumCalculateResult.continuum_subband_integration_times[0].integration_time!);
      const minTime = helpers.format.convertTimeToDisplayUnit(continuumCalculateResult.continuum_subband_integration_times[n_subbands-1].integration_time!);

      subBandResults = {
      confusionNoisePerSubBand: constants.result.noBeamAvailable,
      synthesizedBeamSizePerSubBand: constants.result.noBeamAvailable,
        integrationTimePerSubBand: maxTime + " - "  + minTime
      };
    }

    return {continuumIntegrationTime, spectralIntegrationTime, ...getNoBeamAvailableContinuumResults(true), ...subBandResults, warnings, ...getSpectropolarimetryResults(continuumCalculateResult) };

};
