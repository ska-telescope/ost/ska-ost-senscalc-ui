import {MidSingleWeightingZoomResult, MidSingleZoomSensitivityResponse} from "../../generated/api/mid";
import {ZoomResults} from "../types";
import {
  midZoomResponseToSensitivityResultsWithoutWeighting,
  midZoomResponseToSensitivityFullResults,
  midZoomResponseToSensitivityResultsNoneGaussian,
  midZoomResponseToIntegrationTimeResultsWithoutWeighting,
  midZoomResponseToIntegrationTimeResultsNoneGaussian, midZoomResponseToIntegrationTimeFullResults
} from "./mid-zoom-results.mapper";
import constants from "../../shared/utils/constants";

const zoomSensitivityCalculateResult: MidSingleZoomSensitivityResponse[] = [{
  freq_centre: {value: 100, unit: 'GHz'},
  spectral_sensitivity: {value: 1.234e-6, unit: 'Jy'},
  spectropolarimetry_results: {
    fwhm_of_the_rmsf: {value: 5, unit: 'rad/m^2'},
    max_faraday_depth_extent: {value: 6, unit: 'rad/m^2'},
    max_faraday_depth: {value: 7, unit: 'rad/m^2'}
  }
}];

const zoomIntegrationTimeCalculateResult: MidSingleZoomSensitivityResponse[] = [{
  freq_centre: {value: 100, unit: 'GHz'},
  spectral_integration_time: {value: 123, unit: 's'},
  spectropolarimetry_results: {
    fwhm_of_the_rmsf: {value: 5, unit: 'rad/m^2'},
    max_faraday_depth_extent: {value: 6, unit: 'rad/m^2'},
    max_faraday_depth: {value: 7, unit: 'rad/m^2'}
  }
}];

const zoomWeightingResult: MidSingleWeightingZoomResult[] = [{
  freq_centre: {value: 100, unit: 'GHz'},
  weighting_factor: 1.5,
  confusion_noise: {value: 1.23e-6, limit_type: 'value'}, // confusion noise is near to sensitivity so result should have sensLimitReached true
  sbs_conv_factor: 4,
  beam_size: {beam_pa: 1, beam_min_scaled: 2, beam_maj_scaled: 3}
}];

describe('The mid zoom results mapper functions for a sensitivity calculation', () => {
  it('should map the Mid zoom responses to the results with full weighting', () => {
    const expectedZoomResults: ZoomResults = {
      weightedSpectralSensitivity: '1.85 uJy/beam (1.50)',
      spectralConfusionNoise: '1.23 uJy/beam',
      totalSpectralSensitivity: '2.22 uJy/beam',
      spectralSynthesizedBeamSize: '10800.000" x 7200.000"',
      spectralSurfaceBrightnessSensitivity: '8.89 uK',
      spectropolarimetryResults: {
        fwhmOfTheRMSF: '5.0 rad/m^2',
        maximumFaradayDepth: '7.0 rad/m^2',
        maximumFaradayDepthExtent: '6.0 rad/m^2'
      },
      warnings: {
        integrationTimesDifferent: true,
        sensLimitReached: {reached: true}
      }
    };

    expect(
      midZoomResponseToSensitivityFullResults(zoomSensitivityCalculateResult, zoomWeightingResult, true)
    ).toStrictEqual(expectedZoomResults);
  });

  it('should map the Mid zoom responses to the results with non Gaussian weighting', () => {
    const expectedZoomResults: ZoomResults = {
      weightedSpectralSensitivity: '1.85 uJy/beam (1.50)',
      spectralConfusionNoise: constants.result.nonGaussianBeam,
      totalSpectralSensitivity: constants.result.nonGaussianBeam,
      spectralSynthesizedBeamSize: constants.result.nonGaussianBeam,
      spectralSurfaceBrightnessSensitivity: constants.result.nonGaussianBeam,
      spectropolarimetryResults: {
        fwhmOfTheRMSF: '5.0 rad/m^2',
        maximumFaradayDepth: '7.0 rad/m^2',
        maximumFaradayDepthExtent: '6.0 rad/m^2'
      },
      warnings: {
        integrationTimesDifferent: false,
        sensLimitReached: {reached: false}
      }
    };

    expect(
      midZoomResponseToSensitivityResultsNoneGaussian(zoomSensitivityCalculateResult, zoomWeightingResult)
    ).toStrictEqual(expectedZoomResults);
  });

    it('should map the Mid zoom responses to the results when weighting is not available', () => {
    const expectedZoomResults: ZoomResults = {
      weightedSpectralSensitivity: '1.23 uJy/beam (1.00)',
      spectralConfusionNoise: constants.result.noBeamAvailable,
      totalSpectralSensitivity: constants.result.noBeamAvailable,
      spectralSynthesizedBeamSize: constants.result.noBeamAvailable,
      spectralSurfaceBrightnessSensitivity: constants.result.noBeamAvailable,
      spectropolarimetryResults: {
        fwhmOfTheRMSF: '5.0 rad/m^2',
        maximumFaradayDepth: '7.0 rad/m^2',
        maximumFaradayDepthExtent: '6.0 rad/m^2'
      },
      warnings: {
        integrationTimesDifferent: false,
        sensLimitReached: {reached: false}
      }
    };

    expect(
      midZoomResponseToSensitivityResultsWithoutWeighting(zoomSensitivityCalculateResult)
    ).toStrictEqual(expectedZoomResults);
  });
});

describe('The mid zoom results mapper functions for an integration time calculation', () => {
  it('should map the Mid zoom responses to the results with full weighting', () => {
    const expectedZoomResults: ZoomResults = {
      spectralConfusionNoise: '1.23 uJy/beam',
      spectralSynthesizedBeamSize: '10800.000" x 7200.000"',
      spectralIntegrationTime: '123.00 s',
      spectropolarimetryResults: {
        fwhmOfTheRMSF: '5.0 rad/m^2',
        maximumFaradayDepth: '7.0 rad/m^2',
        maximumFaradayDepthExtent: '6.0 rad/m^2'
      },
      warnings: {
        integrationTimesDifferent: false,
        sensLimitReached: {reached: false}
      }
    };

    expect(
      midZoomResponseToIntegrationTimeFullResults(1e6, zoomIntegrationTimeCalculateResult, zoomWeightingResult)
    ).toStrictEqual(expectedZoomResults);
  });

  it('should map the Mid zoom responses to the results with non Gaussian weighting', () => {
    const expectedZoomResults: ZoomResults = {
      spectralIntegrationTime: '123.00 s',
      spectralConfusionNoise: constants.result.nonGaussianBeam,
      spectralSynthesizedBeamSize: constants.result.nonGaussianBeam,
      spectropolarimetryResults: {
        fwhmOfTheRMSF: '5.0 rad/m^2',
        maximumFaradayDepth: '7.0 rad/m^2',
        maximumFaradayDepthExtent: '6.0 rad/m^2'
      },
      warnings: {
        integrationTimesDifferent: false,
        sensLimitReached: {reached: false}
      }
    };

    expect(
      midZoomResponseToIntegrationTimeResultsNoneGaussian(zoomIntegrationTimeCalculateResult)
    ).toStrictEqual(expectedZoomResults);
  });

    it('should map the Mid zoom responses to the results when weighting is not available', () => {
    const expectedZoomResults: ZoomResults = {
      spectralIntegrationTime: '123.00 s',
      spectralConfusionNoise: constants.result.noBeamAvailable,
      spectralSynthesizedBeamSize: constants.result.noBeamAvailable,
      spectropolarimetryResults: {
        fwhmOfTheRMSF: '5.0 rad/m^2',
        maximumFaradayDepth: '7.0 rad/m^2',
        maximumFaradayDepthExtent: '6.0 rad/m^2'
      },
      warnings: {
        integrationTimesDifferent: false,
        sensLimitReached: {reached: false}
      }
    };

    expect(
      midZoomResponseToIntegrationTimeResultsWithoutWeighting(zoomIntegrationTimeCalculateResult)
    ).toStrictEqual(expectedZoomResults);
  });
});
