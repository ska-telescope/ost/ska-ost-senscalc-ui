import {
  MidSingleWeightingZoomResult, MidSingleZoomSensitivityResponse
} from "../../generated/api/mid";
import helpers from "../../shared/utils/helpers";
import {ZoomResults, Warnings} from "../types";
import {
  getSurfaceBrightnessSensitivity,
  getSynthesizedBeamSize,
  getSpectropolarimetryResults,
  getConfusionNoiseRawValueInuJy,
  getNoBeamAvailableZoomResults,
  getNonGaussianBeamZoomResults,
  getWeightedSensitivityWithWeightedFactor
} from "./mapper.utils";

export const midZoomResponseToSensitivityFullResults = (zoomCalculateResult: Array<MidSingleZoomSensitivityResponse>, zoomWeightingResult: Array<MidSingleWeightingZoomResult>, integrationTimesDifferent = false): ZoomResults => {
  // The API supports multiple zoom windows so returns and array, but the UI only supports one, so we take the first element
  const calculateResult = zoomCalculateResult[0];
  const weightingResult = zoomWeightingResult[0];

  const weightedSpectralSensitivityRawValue= calculateResult.spectral_sensitivity!.value! * weightingResult.weighting_factor * 1e6;
  const spectralConfusionNoiseRawValue = getConfusionNoiseRawValueInuJy(weightingResult);
  const totalSpectralSensitivityRawValue = helpers.calculate.sqrtOfSumSqs(spectralConfusionNoiseRawValue, weightedSpectralSensitivityRawValue);

  const weightedSpectralSensitivity = getWeightedSensitivityWithWeightedFactor(weightedSpectralSensitivityRawValue, weightingResult);
  const spectralConfusionNoise = helpers.format.convertSensitivityToDisplayValue(spectralConfusionNoiseRawValue);
  const totalSpectralSensitivity = helpers.format.convertSensitivityToDisplayValue(totalSpectralSensitivityRawValue);
  const spectralSynthesizedBeamSize = getSynthesizedBeamSize(weightingResult);
  const spectralSurfaceBrightnessSensitivity = getSurfaceBrightnessSensitivity(weightingResult, totalSpectralSensitivityRawValue);

  const warnings: Warnings = {
    // For time -> sensitivity, we don't display the value in the warning message to be consistent with continuum
    sensLimitReached: {reached: helpers.calculate.sensitivityLimitCheck(spectralConfusionNoiseRawValue, totalSpectralSensitivityRawValue)},
    integrationTimesDifferent: integrationTimesDifferent
  };

  return {weightedSpectralSensitivity, spectralConfusionNoise, totalSpectralSensitivity, spectralSynthesizedBeamSize, spectralSurfaceBrightnessSensitivity, ...getSpectropolarimetryResults(calculateResult), warnings};
};

export const midZoomResponseToSensitivityResultsNoneGaussian = (zoomCalculateResult: Array<MidSingleZoomSensitivityResponse>, zoomWeightingResult: Array<MidSingleWeightingZoomResult>, integrationTimesDifferent = false): ZoomResults => {
  // The API supports multiple zoom windows so returns and array, but the UI only supports one, so we take the first element
  const calculateResult = zoomCalculateResult[0];
  const weightingResult = zoomWeightingResult[0];

  const weightedSpectralSensitivityRawValue= calculateResult.spectral_sensitivity!.value! * weightingResult.weighting_factor * 1e6;

  const weightedSpectralSensitivity = `${helpers.format.convertSensitivityToDisplayValue(weightedSpectralSensitivityRawValue)} (${weightingResult.weighting_factor.toFixed(2)})`;

  const warnings: Warnings = {
    sensLimitReached: {reached: false},
    integrationTimesDifferent: integrationTimesDifferent
  };

  return {weightedSpectralSensitivity, ...getNonGaussianBeamZoomResults(), warnings, ...getSpectropolarimetryResults(calculateResult)};
};

export const midZoomResponseToSensitivityResultsWithoutWeighting = (zoomCalculateResult: Array<MidSingleZoomSensitivityResponse>, integrationTimesDifferent = false): ZoomResults => {
  // The API supports multiple zoom windows so returns and array, but the UI only supports one, so we take the first element
  const calculateResult = zoomCalculateResult[0];

  const weightedSpectralSensitivity = getWeightedSensitivityWithWeightedFactor(calculateResult.spectral_sensitivity!.value!*1e6);

  const warnings: Warnings = {
    sensLimitReached: {reached: false},
    integrationTimesDifferent: integrationTimesDifferent
  };

  return {weightedSpectralSensitivity, ...getNoBeamAvailableZoomResults(), warnings, ...getSpectropolarimetryResults(calculateResult)};
};

export const midZoomResponseToIntegrationTimeFullResults = (inputSensitivityJy: number, zoomCalculateResult: Array<MidSingleZoomSensitivityResponse>, zoomWeightingResult: Array<MidSingleWeightingZoomResult>): ZoomResults => {
  // The API supports multiple zoom windows so returns and array, but the UI only supports one, so we take the first element
  const calculateResult = zoomCalculateResult[0];
  const weightingResult = zoomWeightingResult[0];

  const spectralConfusionNoiseRawValue = getConfusionNoiseRawValueInuJy(weightingResult);

  const spectralIntegrationTime = helpers.format.convertTimeToDisplayUnit(calculateResult.spectral_integration_time!);
  const spectralConfusionNoise = helpers.format.convertSensitivityToDisplayValue(spectralConfusionNoiseRawValue);
  const spectralSynthesizedBeamSize = getSynthesizedBeamSize(weightingResult);

  const warnings: Warnings = {
    integrationTimesDifferent: false
  };
  if (helpers.calculate.sensitivityLimitCheck(spectralConfusionNoiseRawValue, inputSensitivityJy*1e6)) {
    warnings.sensLimitReached = {reached: true, value: spectralConfusionNoise};
  } else {
    warnings.sensLimitReached = {reached: false};
  }

  return {spectralIntegrationTime, spectralConfusionNoise, spectralSynthesizedBeamSize, ...getSpectropolarimetryResults(calculateResult), warnings};
};

export const midZoomResponseToIntegrationTimeResultsNoneGaussian = (zoomCalculateResult: Array<MidSingleZoomSensitivityResponse>): ZoomResults => {
  // The API supports multiple zoom windows so returns and array, but the UI only supports one, so we take the first element
  const calculateResult = zoomCalculateResult[0];

  const spectralIntegrationTime = helpers.format.convertTimeToDisplayUnit(calculateResult.spectral_integration_time!);

  const warnings: Warnings = {
    sensLimitReached: {reached: false},
    integrationTimesDifferent: false
  };

  return {spectralIntegrationTime, ...getNonGaussianBeamZoomResults(true), ...getSpectropolarimetryResults(calculateResult), warnings};
};

export const midZoomResponseToIntegrationTimeResultsWithoutWeighting = (zoomCalculateResult: Array<MidSingleZoomSensitivityResponse>): ZoomResults => {
  // The API supports multiple zoom windows so returns and array, but the UI only supports one, so we take the first element
  const calculateResult = zoomCalculateResult[0];

  const spectralIntegrationTime = helpers.format.convertTimeToDisplayUnit(calculateResult.spectral_integration_time!);

  const warnings: Warnings = {
    sensLimitReached: {reached: false},
    integrationTimesDifferent: false
  };

  return {spectralIntegrationTime, ...getNoBeamAvailableZoomResults(true), ...getSpectropolarimetryResults(calculateResult), warnings};
};
