import { TestBed } from '@angular/core/testing';
import {firstValueFrom, of} from 'rxjs';
import {
  BASE_PATH,
  DefaultService,
  MidApiModule,
  MidContinuumSensitivityResponse,
  MidContinuumWeightingResult, MidSingleWeightingZoomResult,
  MidSingleZoomSensitivityResponse,
  MidSpectropolarimetryResults
} from '../generated/api/mid';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {MidCalculationService} from "./mid-calculation.service";
import {MidFormInput} from "../mid-sensitivity-calculator/types";
import {SpectropolarimetryResults} from "./types";
import constants from "../shared/utils/constants";

describe('MidCalculationService', () => {
  let fixture: MidCalculationService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, MidApiModule],
      providers: [
        {
          provide: BASE_PATH,
          useValue: 'http://backend'
        }
      ]
    }).compileComponents();

    fixture = TestBed.inject(MidCalculationService);
  });

  const spectropolarimetryResults: MidSpectropolarimetryResults = {
    fwhm_of_the_rmsf: {value: 18, unit: 'rad/m^2'},
    max_faraday_depth: {value: 1800, unit: 'rad/m^2'},
    max_faraday_depth_extent: {value: 36, unit: 'rad/m^2'}
  };

  const displaySpectropolarimetryResults: SpectropolarimetryResults = {
    fwhmOfTheRMSF: '18.0 rad/m^2',
    maximumFaradayDepthExtent: '36.0 rad/m^2',
    maximumFaradayDepth: '1800.0 rad/m^2'
  };

  describe('When calculateContinuumResult is called with an integration time', () => {
    const continuumCalculateResult: MidContinuumSensitivityResponse = {
      continuum_sensitivity: {value: 20, unit: 'uJy'},
      spectral_sensitivity: {value: 1.2, unit: 'uJy'},
      spectropolarimetry_results: spectropolarimetryResults
    };

    const continuumWeightingResult: MidContinuumWeightingResult = {
      weighting_factor: 1.5,
      confusion_noise: {value: 1.23e-6, limit_type: 'value'},
      sbs_conv_factor: 4,
      beam_size: {beam_pa: 1, beam_min_scaled: 2, beam_maj_scaled: 3}
    };

    beforeEach(() => {});

    it('should call the calculate and weighting end points delegate to the full mapper', async () => {
      const fakeApi = {
        skaOstSenscalcMidApiContinuumCalculate: () => of(continuumCalculateResult),
        skaOstSenscalcMidApiContinuumWeighting: () => of(continuumWeightingResult)
      };
      fixture = new MidCalculationService(fakeApi as unknown as DefaultService);

      jest.spyOn(fakeApi, 'skaOstSenscalcMidApiContinuumCalculate');
      jest.spyOn(fakeApi, 'skaOstSenscalcMidApiContinuumWeighting');
      const midFormInput: MidFormInput = {
        common: {
          rx_band: 'Band 1',
          ra_str: '10:00:00',
          dec_str: '-20:00:00',
          el: '30',
          pwv: '1',
          array_configuration: 'AA4',
          n_meer: null,
          n_ska: null
        },
        continuum: {
          weighting: 'briggs',
          integrationTime: 1,
          frequency: 1e6,
          bandwidth: 2e6,
          calculator_mode: 'continuum',
          n_subbands: 2,
          spectralAveragingFactor: 1,
          spectralResolution: 1
        }

      };
      const result = await firstValueFrom(fixture.calculateContinuumResult(midFormInput, false));
      expect(fakeApi.skaOstSenscalcMidApiContinuumCalculate).toHaveBeenCalled();
      expect(fakeApi.skaOstSenscalcMidApiContinuumWeighting).toHaveBeenCalledTimes(2); // Once for spectral_mode=continuum and once for spectral_mode=line
      // The testing of the combination of the API responses into the actual results is done in the mapper specs, so we don't do a full assertion here
      expect(result.spectropolarimetryResults).toEqual(displaySpectropolarimetryResults);
    });

    it('should call the calculate and weighting end points delegate to the non Gaussian mapper', async () => {
      const fakeApi = {
        skaOstSenscalcMidApiContinuumCalculate: () => of(continuumCalculateResult),
        skaOstSenscalcMidApiContinuumWeighting: () => of(continuumWeightingResult)
      };
      fixture = new MidCalculationService(fakeApi as unknown as DefaultService);
      jest.spyOn(fakeApi, 'skaOstSenscalcMidApiContinuumCalculate');
      jest.spyOn(fakeApi, 'skaOstSenscalcMidApiContinuumWeighting');

      const midFormInput: MidFormInput = {
        common: {
          rx_band: 'Band 1',
          ra_str: '10:00:00',
          dec_str: '-20:00:00',
          el: '30',
          pwv: '1',
          array_configuration: 'AA4',
          n_meer: null,
          n_ska: null
        },
        continuum: {
          weighting: 'natural',
          taper: 0, // so beam non Gaussian
          integrationTime: 1,
          frequency: 1e6,
          bandwidth: 2e6,
          calculator_mode: 'continuum',
          n_subbands: 1,
          spectralAveragingFactor: 1,
          spectralResolution: 1
        }

      };
      const result = await firstValueFrom(fixture.calculateContinuumResult(midFormInput, false));
      expect(fakeApi.skaOstSenscalcMidApiContinuumCalculate).toHaveBeenCalled();
      expect(fakeApi.skaOstSenscalcMidApiContinuumWeighting).toHaveBeenCalledTimes(2); // Once for spectral_mode=continuum and once for spectral_mode=line
      // The testing of the combination of the API responses into the actual results is done in the mapper specs, so we don't do a full assertion here
      expect(result.spectralSurfaceBrightnessSensitivity).toEqual(constants.result.nonGaussianBeam);
      expect(result.spectropolarimetryResults).toEqual(displaySpectropolarimetryResults);
    });

    it('should call only the calculate end point and delegate to the no beam available mapper', async () => {
      const fakeApi = {
        skaOstSenscalcMidApiContinuumCalculate: () => of(continuumCalculateResult),
        skaOstSenscalcMidApiContinuumWeighting: () => of(continuumWeightingResult)
      };
      fixture = new MidCalculationService(fakeApi as unknown as DefaultService);
      jest.spyOn(fakeApi, 'skaOstSenscalcMidApiContinuumCalculate');
      jest.spyOn(fakeApi, 'skaOstSenscalcMidApiContinuumWeighting');

      const midFormInput: MidFormInput = {
        common: {
          rx_band: 'Band 1',
          ra_str: '10:00:00',
          dec_str: '-20:00:00',
          el: '30',
          pwv: '1',
          array_configuration: 'Custom', // so beam not available
          n_meer: null,
          n_ska: null
        },
        continuum: {
          weighting: 'uniform',
          integrationTime: 1,
          frequency: 1e6,
          bandwidth: 2e6,
          calculator_mode: 'continuum',
          n_subbands: 1,
          spectralAveragingFactor: 1,
          spectralResolution: 1
        }

      };
      const result = await firstValueFrom(fixture.calculateContinuumResult(midFormInput, false));
      expect(fakeApi.skaOstSenscalcMidApiContinuumCalculate).toHaveBeenCalled();
      expect(fakeApi.skaOstSenscalcMidApiContinuumWeighting).not.toHaveBeenCalled();
      // The testing of the combination of the API responses into the actual results is done in the mapper specs, so we don't do a full assertion here
      expect(result.spectralSurfaceBrightnessSensitivity).toEqual(constants.result.noBeamAvailable);
      expect(result.spectropolarimetryResults).toEqual(displaySpectropolarimetryResults);
    });
  });

  describe('When calculateContinuumResult is called with a sensitivity', () => {
    const continuumCalculateResult: MidContinuumSensitivityResponse = {
      continuum_integration_time: {value: 20, unit: 's'},
      spectral_integration_time: {value: 1.2, unit: 's'},
      spectropolarimetry_results: spectropolarimetryResults
    };

    const continuumWeightingResult: MidContinuumWeightingResult = {
      weighting_factor: 1.5,
      confusion_noise: {value: 1.23e-6, limit_type: 'value'},
      sbs_conv_factor: 4,
      beam_size: {beam_pa: 1, beam_min_scaled: 2, beam_maj_scaled: 3}
    };

    beforeEach(() => {});

    it('should call the calculate and weighting end points delegate to the full mapper', async () => {
      const fakeApi = {
        skaOstSenscalcMidApiContinuumCalculate: () => of(continuumCalculateResult),
        skaOstSenscalcMidApiContinuumWeighting: () => of(continuumWeightingResult)
      };
      fixture = new MidCalculationService(fakeApi as unknown as DefaultService);

      jest.spyOn(fakeApi, 'skaOstSenscalcMidApiContinuumCalculate');
      jest.spyOn(fakeApi, 'skaOstSenscalcMidApiContinuumWeighting');
      const midFormInput: MidFormInput = {
        common: {
          rx_band: 'Band 1',
          ra_str: '10:00:00',
          dec_str: '-20:00:00',
          el: '30',
          pwv: '1',
          array_configuration: 'AA4',
          n_meer: null,
          n_ska: null
        },
        continuum: {
          weighting: 'briggs',
          sensitivity: 1,
          frequency: 1e6,
          bandwidth: 2e6,
          calculator_mode: 'continuum',
          spectralAveragingFactor: 1,
          spectralResolution: 1,
          n_subbands: 1
        }

      };
      const result = await firstValueFrom(fixture.calculateContinuumResult(midFormInput, false));
      expect(fakeApi.skaOstSenscalcMidApiContinuumCalculate).toHaveBeenCalledTimes(2); // once for the continuum thermal sensitiviy and once for the spectral
      expect(fakeApi.skaOstSenscalcMidApiContinuumWeighting).toHaveBeenCalledTimes(2); // Once for spectral_mode=continuum and once for spectral_mode=line
      // The testing of the combination of the API responses into the actual results is done in the mapper specs, so we don't do a full assertion here
      expect(result.continuumIntegrationTime).toEqual('20.00 s');
      expect(result.spectropolarimetryResults).toEqual(displaySpectropolarimetryResults);
    });

    it('should call the calculate and weighting end points delegate to the non Gaussian mapper', async () => {
      const fakeApi = {
        skaOstSenscalcMidApiContinuumCalculate: () => of(continuumCalculateResult),
        skaOstSenscalcMidApiContinuumWeighting: () => of(continuumWeightingResult)
      };
      fixture = new MidCalculationService(fakeApi as unknown as DefaultService);
      jest.spyOn(fakeApi, 'skaOstSenscalcMidApiContinuumCalculate');
      jest.spyOn(fakeApi, 'skaOstSenscalcMidApiContinuumWeighting');

      const midFormInput: MidFormInput = {
        common: {
          rx_band: 'Band 1',
          ra_str: '10:00:00',
          dec_str: '-20:00:00',
          el: '30',
          pwv: '1',
          array_configuration: 'AA4',
          n_meer: null,
          n_ska: null
        },
        continuum: {
          weighting: 'briggs',
          robustness: 2,
          taper: 0, // so beam non Gaussian
          sensitivity: 1,
          frequency: 1e6,
          bandwidth: 2e6,
          calculator_mode: 'continuum',
          n_subbands: 1,
          spectralAveragingFactor: 1,
          spectralResolution: 1
        }

      };
      const result = await firstValueFrom(fixture.calculateContinuumResult(midFormInput, false));
      expect(fakeApi.skaOstSenscalcMidApiContinuumCalculate).toHaveBeenCalledTimes(2); // once for the continuum thermal sensitiviy and once for the spectral
      expect(fakeApi.skaOstSenscalcMidApiContinuumWeighting).toHaveBeenCalledTimes(2); // Once for spectral_mode=continuum and once for spectral_mode=line
      // The testing of the combination of the API responses into the actual results is done in the mapper specs, so we don't do a full assertion here
      expect(result.spectralConfusionNoise).toEqual(constants.result.nonGaussianBeam);
      expect(result.spectropolarimetryResults).toEqual(displaySpectropolarimetryResults);
    });

    it('should call only the calculate end point and delegate to the no beam available mapper', async () => {
      const fakeApi = {
        skaOstSenscalcMidApiContinuumCalculate: () => of(continuumCalculateResult),
        skaOstSenscalcMidApiContinuumWeighting: () => of(continuumWeightingResult)
      };
      fixture = new MidCalculationService(fakeApi as unknown as DefaultService);
      jest.spyOn(fakeApi, 'skaOstSenscalcMidApiContinuumCalculate');
      jest.spyOn(fakeApi, 'skaOstSenscalcMidApiContinuumWeighting');

      const midFormInput: MidFormInput = {
        common: {
          rx_band: 'Band 1',
          ra_str: '10:00:00',
          dec_str: '-20:00:00',
          el: '30',
          pwv: '1',
          array_configuration: 'Custom', // so beam not available
          n_meer: null,
          n_ska: null
        },
        continuum: {
          weighting: 'uniform',
          sensitivity: 1,
          frequency: 1e6,
          bandwidth: 2e6,
          calculator_mode: 'continuum',
          n_subbands: 2,
          spectralAveragingFactor: 1,
          spectralResolution: 1
        }

      };
      const result = await firstValueFrom(fixture.calculateContinuumResult(midFormInput, false));
      expect(fakeApi.skaOstSenscalcMidApiContinuumCalculate).toHaveBeenCalled();
      expect(fakeApi.skaOstSenscalcMidApiContinuumWeighting).not.toHaveBeenCalled();
      // The testing of the combination of the API responses into the actual results is done in the mapper specs, so we don't do a full assertion here
      expect(result.spectralIntegrationTime).toEqual('1.20 s');
      expect(result.spectropolarimetryResults).toEqual(displaySpectropolarimetryResults);
    });
  });

  describe('When calculateZoomResult is called with an integration time', () => {
    const zoomCalculateResult: MidSingleZoomSensitivityResponse[] = [{
      spectral_sensitivity: {value: 1.2, unit: 'uJy'},
      spectropolarimetry_results: spectropolarimetryResults
    }];

    const zoomWeightingResult: MidSingleWeightingZoomResult[] = [{
      weighting_factor: 1.5,
      confusion_noise: {value: 1.23e-6, limit_type: 'value'},
      sbs_conv_factor: 4,
      beam_size: {beam_pa: 1, beam_min_scaled: 2, beam_maj_scaled: 3}
    }];

    beforeEach(() => {});

    it('should call the calculate and weighting end points delegate to the full mapper', async () => {
      const fakeApi = {
        skaOstSenscalcMidApiZoomCalculate: () => of(zoomCalculateResult),
        skaOstSenscalcMidApiZoomWeighting: () => of(zoomWeightingResult)
      };
      fixture = new MidCalculationService(fakeApi as unknown as DefaultService);

      jest.spyOn(fakeApi, 'skaOstSenscalcMidApiZoomCalculate');
      jest.spyOn(fakeApi, 'skaOstSenscalcMidApiZoomWeighting');
      const midFormInput: MidFormInput = {
        common: {
          rx_band: 'Band 1',
          ra_str: '10:00:00',
          dec_str: '-20:00:00',
          el: '30',
          pwv: '1',
          array_configuration: 'AA4',
          n_meer: null,
          n_ska: null
        },
        line: {
          weighting: 'robust',
          robustness: 1,
          integrationTime: 1,
          zoomFrequency: 1e6,
          zoomBandwidth: 2e6,
          calculator_mode: 'continuum',
          spectralAveragingFactor: 1,
          spectralResolution: 1
        }

      };
      const result = await firstValueFrom(fixture.calculateZoomResult(midFormInput, false));
      expect(fakeApi.skaOstSenscalcMidApiZoomCalculate).toHaveBeenCalled();
      expect(fakeApi.skaOstSenscalcMidApiZoomWeighting).toHaveBeenCalled();
      // The testing of the combination of the API responses into the actual results is done in the mapper specs, so we don't do a full assertion here
      expect(result.spectropolarimetryResults).toEqual(displaySpectropolarimetryResults);
    });

    it('should call the calculate and weighting end points delegate to the non Gaussian mapper', async () => {
      const fakeApi = {
        skaOstSenscalcMidApiZoomCalculate: () => of(zoomCalculateResult),
        skaOstSenscalcMidApiZoomWeighting: () => of(zoomWeightingResult)
      };
      fixture = new MidCalculationService(fakeApi as unknown as DefaultService);

      jest.spyOn(fakeApi, 'skaOstSenscalcMidApiZoomCalculate');
      jest.spyOn(fakeApi, 'skaOstSenscalcMidApiZoomWeighting');
      const midFormInput: MidFormInput = {
        common: {
          rx_band: 'Band 1',
          ra_str: '10:00:00',
          dec_str: '-20:00:00',
          el: '30',
          pwv: '1',
          array_configuration: 'AA4',
          n_meer: null,
          n_ska: null
        },
        line: {
          weighting: 'natural',
          taper: 0, // so beam non Gaussian
          integrationTime: 1,
          zoomFrequency: 1e6,
          zoomBandwidth: 2e6,
          calculator_mode: 'continuum',
          spectralAveragingFactor: 1,
          spectralResolution: 1
        }

      };
      const result = await firstValueFrom(fixture.calculateZoomResult(midFormInput, false));
      expect(fakeApi.skaOstSenscalcMidApiZoomCalculate).toHaveBeenCalled();
      expect(fakeApi.skaOstSenscalcMidApiZoomWeighting).toHaveBeenCalled();
      // The testing of the combination of the API responses into the actual results is done in the mapper specs, so we don't do a full assertion here
      expect(result.spectralSurfaceBrightnessSensitivity).toEqual(constants.result.nonGaussianBeam);
      expect(result.spectropolarimetryResults).toEqual(displaySpectropolarimetryResults);
    });

    it('should call only the calculate end point and delegate to the no beam available mapper', async () => {
        const fakeApi = {
        skaOstSenscalcMidApiZoomCalculate: () => of(zoomCalculateResult),
        skaOstSenscalcMidApiZoomWeighting: () => of(zoomWeightingResult)
      };
      fixture = new MidCalculationService(fakeApi as unknown as DefaultService);

      jest.spyOn(fakeApi, 'skaOstSenscalcMidApiZoomCalculate');
      jest.spyOn(fakeApi, 'skaOstSenscalcMidApiZoomWeighting');
      const midFormInput: MidFormInput = {
        common: {
          rx_band: 'Band 1',
          ra_str: '10:00:00',
          dec_str: '-20:00:00',
          el: '30',
          pwv: '1',
          array_configuration: 'Custom',
          n_meer: null,
          n_ska: null
        },
        line: {
          weighting: 'uniform',
          integrationTime: 1,
          zoomFrequency: 1e6,
          zoomBandwidth: 2e6,
          calculator_mode: 'continuum',
          spectralAveragingFactor: 1,
          spectralResolution: 1
        }

      };
      const result = await firstValueFrom(fixture.calculateZoomResult(midFormInput, false));
      expect(fakeApi.skaOstSenscalcMidApiZoomCalculate).toHaveBeenCalled();
      expect(fakeApi.skaOstSenscalcMidApiZoomWeighting).not.toHaveBeenCalled();
      // The testing of the combination of the API responses into the actual results is done in the mapper specs, so we don't do a full assertion here
      expect(result.spectralSurfaceBrightnessSensitivity).toEqual(constants.result.noBeamAvailable);
      expect(result.spectropolarimetryResults).toEqual(displaySpectropolarimetryResults);
    });

  });

  describe('When calculateZoomResult is called with a sensitivity', () => {
    const zoomCalculateResult: MidSingleZoomSensitivityResponse[] = [{
      spectral_integration_time: {value: 1.2, unit: 's'},
      spectropolarimetry_results: spectropolarimetryResults
    }];

    const zoomWeightingResult: MidSingleWeightingZoomResult[] = [{
      weighting_factor: 1.5,
      confusion_noise: {value: 1.23e-6, limit_type: 'value'},
      sbs_conv_factor: 4,
      beam_size: {beam_pa: 1, beam_min_scaled: 2, beam_maj_scaled: 3}
    }];

    beforeEach(() => {});

    it('should call the calculate and weighting end points delegate to the full mapper', async () => {
      const fakeApi = {
        skaOstSenscalcMidApiZoomCalculate: () => of(zoomCalculateResult),
        skaOstSenscalcMidApiZoomWeighting: () => of(zoomWeightingResult)
      };
      fixture = new MidCalculationService(fakeApi as unknown as DefaultService);

      jest.spyOn(fakeApi, 'skaOstSenscalcMidApiZoomCalculate');
      jest.spyOn(fakeApi, 'skaOstSenscalcMidApiZoomWeighting');
      const midFormInput: MidFormInput = {
        common: {
          rx_band: 'Band 1',
          ra_str: '10:00:00',
          dec_str: '-20:00:00',
          el: '30',
          pwv: '1',
          array_configuration: 'AA4',
          n_meer: null,
          n_ska: null
        },
        line: {
          weighting: 'uniform',
          sensitivity: 1,
          zoomFrequency: 1e6,
          zoomBandwidth: 2e6,
          calculator_mode: 'continuum',
          spectralAveragingFactor: 1,
          spectralResolution: 1
        }

      };
      const result = await firstValueFrom(fixture.calculateZoomResult(midFormInput, false));
      expect(fakeApi.skaOstSenscalcMidApiZoomCalculate).toHaveBeenCalled();
      expect(fakeApi.skaOstSenscalcMidApiZoomWeighting).toHaveBeenCalled();
      // The testing of the combination of the API responses into the actual results is done in the mapper specs, so we don't do a full assertion here
      expect(result.spectralIntegrationTime).toEqual("1.20 s");
      expect(result.spectropolarimetryResults).toEqual(displaySpectropolarimetryResults);
    });

    it('should call the calculate and weighting end points delegate to the non Gaussian mapper', async () => {
      const fakeApi = {
        skaOstSenscalcMidApiZoomCalculate: () => of(zoomCalculateResult),
        skaOstSenscalcMidApiZoomWeighting: () => of(zoomWeightingResult)
      };
      fixture = new MidCalculationService(fakeApi as unknown as DefaultService);

      jest.spyOn(fakeApi, 'skaOstSenscalcMidApiZoomCalculate');
      jest.spyOn(fakeApi, 'skaOstSenscalcMidApiZoomWeighting');
      const midFormInput: MidFormInput = {
        common: {
          rx_band: 'Band 1',
          ra_str: '10:00:00',
          dec_str: '-20:00:00',
          el: '30',
          pwv: '1',
          array_configuration: 'AA4',
          n_meer: null,
          n_ska: null
        },
        line: {
          weighting: 'robust',
          robustness: 2,
          taper: 0, // so beam non Gaussian
          sensitivity: 1,
          zoomFrequency: 1e6,
          zoomBandwidth: 2e6,
          calculator_mode: 'continuum',
          spectralAveragingFactor: 1,
          spectralResolution: 1
        }

      };
      const result = await firstValueFrom(fixture.calculateZoomResult(midFormInput, false));
      expect(fakeApi.skaOstSenscalcMidApiZoomCalculate).toHaveBeenCalled();
      expect(fakeApi.skaOstSenscalcMidApiZoomWeighting).toHaveBeenCalled();
      expect(result.spectralIntegrationTime).toEqual('1.20 s');
      expect(result.spectralConfusionNoise).toEqual(constants.result.nonGaussianBeam);
      expect(result.spectropolarimetryResults).toEqual(displaySpectropolarimetryResults);
    });

    it('should call only the calculate end point and delegate to the no beam available mapper', async () => {
      const fakeApi = {
        skaOstSenscalcMidApiZoomCalculate: () => of(zoomCalculateResult),
        skaOstSenscalcMidApiZoomWeighting: () => of(zoomWeightingResult)
      };
      fixture = new MidCalculationService(fakeApi as unknown as DefaultService);

      jest.spyOn(fakeApi, 'skaOstSenscalcMidApiZoomCalculate');
      jest.spyOn(fakeApi, 'skaOstSenscalcMidApiZoomWeighting');
      const midFormInput: MidFormInput = {
        common: {
          rx_band: 'Band 1',
          ra_str: '10:00:00',
          dec_str: '-20:00:00',
          el: '30',
          pwv: '1',
          array_configuration: 'Custom',
          n_meer: null,
          n_ska: null
        },
        line: {
          weighting: 'uniform',
          sensitivity: 1,
          zoomFrequency: 1e6,
          zoomBandwidth: 2e6,
          calculator_mode: 'continuum',
          spectralAveragingFactor: 1,
          spectralResolution: 1
        }

      };
      const result = await firstValueFrom(fixture.calculateZoomResult(midFormInput, false));
      expect(fakeApi.skaOstSenscalcMidApiZoomCalculate).toHaveBeenCalled();
      expect(fakeApi.skaOstSenscalcMidApiZoomWeighting).not.toHaveBeenCalled();
      // The testing of the combination of the API responses into the actual results is done in the mapper specs, so we don't do a full assertion here
      expect(result.spectralIntegrationTime).toEqual('1.20 s');
      expect(result.spectralConfusionNoise).toEqual(constants.result.noBeamAvailable);
      expect(result.spectropolarimetryResults).toEqual(displaySpectropolarimetryResults);
    });

  });
});
