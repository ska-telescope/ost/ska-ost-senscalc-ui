import {Injectable} from "@angular/core";
import {
  DefaultService,
  MidContinuumWeightingResult,
  MidSingleWeightingZoomResult,
  SkaOstSenscalcMidApiContinuumWeightingRequestParams,
  SkaOstSenscalcMidApiZoomCalculateRequestParams,
  SkaOstSenscalcMidApiZoomWeightingRequestParams
} from "../generated/api/mid";
import {MidFormInput} from "../mid-sensitivity-calculator/types";
import {combineLatest, map, mergeMap, Observable, of} from "rxjs";
import {SkaOstSenscalcMidApiContinuumCalculateRequestParams} from "../generated/api/mid";
import helpers from "../shared/utils/helpers";
import {
  midZoomResponseToSensitivityFullResults,
  midZoomResponseToSensitivityResultsNoneGaussian,
  midZoomResponseToSensitivityResultsWithoutWeighting,
  midZoomResponseToIntegrationTimeFullResults,
  midZoomResponseToIntegrationTimeResultsNoneGaussian,
  midZoomResponseToIntegrationTimeResultsWithoutWeighting
} from "./mappers/mid-zoom-results.mapper";
import {
  midContinuumResponseToSensitivityFullResults,
  midContinuumResponseToSensitivityResultsNoneGaussian,
  midContinuumResponseToSensitivityResultsWithoutWeighting,
  midContinuumResponseToIntegrationTimeFullResults,
  midContinuumResponseToIntegrationTimeResultsNoneGaussian,
  midContinuumResponseToIntegrationTimeResultsWithoutWeighting
} from "./mappers/mid-continuum-results.mapper";
import {ContinuumResults, ZoomResults} from "./types";

/**
 * This service tries to encapsulate all the complexity of the calculator, by offering methods which take the form input
 * and return Observables of the final result data, ready to be displayed as strings.
 * It handles deciding which APIs to call, how the form inputs are mapped to the params and then the combining of the results, which is delegated to the mappers.
 */
@Injectable({
  providedIn: 'root'
})
export class MidCalculationService {

  constructor(private midApi: DefaultService) {}

  calculateContinuumResult(midFormInput: MidFormInput, integrationTimesDifferent: boolean): Observable<ContinuumResults> {
    const calculateSensitivity = !!midFormInput.continuum!.integrationTime;

    return calculateSensitivity
      ? this.calculateContinuumSensitivityResult(midFormInput, integrationTimesDifferent)
      : this.calculateContinuumIntegrationTimeResult(midFormInput);
  }

  calculateZoomResult(midFormInput: MidFormInput, integrationTimesDifferent: boolean): Observable<ZoomResults> {
    const calculateSensitivity = !!midFormInput.line!.integrationTime;

    return calculateSensitivity
      ? this.calculateZoomSensitivityResult(midFormInput, integrationTimesDifferent)
      : this.calculateZoomIntegrationTimeResult(midFormInput);
  }

  private calculateContinuumSensitivityResult(midFormInput: MidFormInput, integrationTimesDifferent: boolean): Observable<ContinuumResults> {
    // Request /continuum/calculate
    const calculateRequestParams: SkaOstSenscalcMidApiContinuumCalculateRequestParams = {
      integrationTimeS: midFormInput.continuum!.integrationTime!,
      freqCentreHz: midFormInput.continuum!.frequency,
      bandwidthHz: midFormInput.continuum!.bandwidth,
      rxBand: midFormInput.common.rx_band,
      pointingCentre: midFormInput.common.ra_str + ' ' + midFormInput.common.dec_str,
      spectralAveragingFactor: midFormInput.continuum!.spectralAveragingFactor as SkaOstSenscalcMidApiContinuumCalculateRequestParams['spectralAveragingFactor'],
      pwv: Number(midFormInput.common.pwv),
      el: Number(midFormInput.common.el),
      nSubbands: Number(midFormInput.continuum!.n_subbands),
      ...midFormInput.advanced
    };

    if (midFormInput.common.array_configuration == 'Custom') {
      calculateRequestParams.nSka = midFormInput.common.n_ska!;
      calculateRequestParams.nMeer = midFormInput.common.n_meer!;
    } else {
      calculateRequestParams.subarrayConfiguration = midFormInput.common.array_configuration as SkaOstSenscalcMidApiContinuumCalculateRequestParams['subarrayConfiguration'];
    }

    const continuumCalculateResult = this.midApi.skaOstSenscalcMidApiContinuumCalculate(calculateRequestParams);

    if (midFormInput.common.array_configuration == 'Custom') {
       return continuumCalculateResult.pipe(map((continuumCalculate) => midContinuumResponseToSensitivityResultsWithoutWeighting(continuumCalculate, integrationTimesDifferent)));
    }

    // Request /continuum/weighting for continuum
    const weightingRequestParams: SkaOstSenscalcMidApiContinuumWeightingRequestParams = {
      spectralMode: "continuum",
      // The  backend expects 'robust' for the value we display as 'Briggs'
      weightingMode: (midFormInput.continuum!.weighting == 'briggs' ? 'robust' : midFormInput.continuum!.weighting),
      subarrayConfiguration: midFormInput.common.array_configuration as SkaOstSenscalcMidApiContinuumWeightingRequestParams['subarrayConfiguration'],
      pointingCentre: midFormInput.common.ra_str + ' ' + midFormInput.common.dec_str,
      freqCentreHz: midFormInput.continuum!.frequency,
      taper: midFormInput.continuum?.taper
    };
    if (weightingRequestParams.weightingMode == "robust" && midFormInput.continuum!.robustness != undefined) {
      weightingRequestParams.robustness = midFormInput.continuum!.robustness;
    }
    if (midFormInput.continuum?.n_subbands && midFormInput.continuum.n_subbands > 1) {
      weightingRequestParams.subbandFreqCentresHz = helpers.calculate.subBandtoFrequencyArray(Number(midFormInput.continuum.n_subbands),
        midFormInput.continuum.bandwidth,
        midFormInput.continuum.frequency);
    }

    const continuumWeightingResult = this.midApi.skaOstSenscalcMidApiContinuumWeighting(
      weightingRequestParams
    );

    // Request /continuum/weighting for line
    weightingRequestParams.spectralMode = "line";
    const lineWeightingResult = this.midApi.skaOstSenscalcMidApiContinuumWeighting(
      weightingRequestParams
    );

    // combine results to display value
    return combineLatest(continuumCalculateResult, continuumWeightingResult, lineWeightingResult)
      .pipe(
        map(([continuumCalculate, continuumWeighting, lineWeighting]) => helpers.option.continuumBeamNonGaussian(midFormInput)
          ? midContinuumResponseToSensitivityResultsNoneGaussian(continuumCalculate, continuumWeighting, lineWeighting, integrationTimesDifferent)
          : midContinuumResponseToSensitivityFullResults(continuumCalculate, continuumWeighting, lineWeighting, integrationTimesDifferent)
      ));
  }

  private calculateContinuumIntegrationTimeResult(midFormInput: MidFormInput): Observable<ContinuumResults> {
    // The sensitivity -> integration time case is harder as we need to send the **thermal** sensitivity to the calculate endpoint,
    // which means we need to get the weighting response first in order to turn the user input sensitivity into the thermal sensitivity
    // We also need to send two calculate requests as the weighting for continuum and spectral results is different, so the thermal sensitivity will be different.

    if (midFormInput.common.array_configuration == 'Custom') {
       return this.calculateContinuumIntegrationTimesWithoutWeighting(midFormInput);
    }

    const weightingRequestParams: SkaOstSenscalcMidApiContinuumWeightingRequestParams = {
      spectralMode: "continuum",
      // The  backend expects 'robust' for the value we display as 'Briggs'
      weightingMode: midFormInput.continuum!.weighting == 'briggs' ? 'robust' : midFormInput.continuum!.weighting,
      subarrayConfiguration: midFormInput.common.array_configuration as SkaOstSenscalcMidApiContinuumWeightingRequestParams['subarrayConfiguration'],
      pointingCentre: midFormInput.common.ra_str + ' ' + midFormInput.common.dec_str,
      freqCentreHz: midFormInput.continuum!.frequency,
      taper: midFormInput.continuum?.taper
    };
    if (weightingRequestParams.weightingMode == "robust" && midFormInput.continuum!.robustness != undefined) {
      weightingRequestParams.robustness = midFormInput.continuum!.robustness;
    }
    if (midFormInput.continuum?.n_subbands && midFormInput.continuum.n_subbands > 1) {
      weightingRequestParams.subbandFreqCentresHz = helpers.calculate.subBandtoFrequencyArray(Number(midFormInput.continuum.n_subbands),
        midFormInput.continuum.bandwidth,
        midFormInput.continuum.frequency);
    }

    const continuumWeightingResult = this.midApi.skaOstSenscalcMidApiContinuumWeighting(
      weightingRequestParams
    );

    // Request /continuum/weighting for line
    weightingRequestParams.spectralMode = "line";
    const lineWeightingResult = this.midApi.skaOstSenscalcMidApiContinuumWeighting(
      weightingRequestParams
    );

    return combineLatest(continuumWeightingResult, lineWeightingResult)
      .pipe(
        mergeMap(([continuumWeighting, lineWeighting]) => this.calculateContinuumIntegrationTimesGivenWeightingResponses(midFormInput, continuumWeighting, lineWeighting))
      );

  }

  private calculateZoomSensitivityResult(midFormInput: MidFormInput, integrationTimesDifferent: boolean) {
    const calculateRequestParams: SkaOstSenscalcMidApiZoomCalculateRequestParams = {
      integrationTimeS: midFormInput.line!.integrationTime!,
      freqCentresHz: [midFormInput.line!.zoomFrequency],
      totalBandwidthsHz: [midFormInput.line!.zoomBandwidth],
      spectralResolutionsHz: [midFormInput.line!.spectralResolution!],
      rxBand: midFormInput.common.rx_band,
      pointingCentre: midFormInput.common.ra_str + ' ' + midFormInput.common.dec_str,
      spectralAveragingFactor: midFormInput.line!.spectralAveragingFactor! as SkaOstSenscalcMidApiZoomCalculateRequestParams['spectralAveragingFactor'],
      pwv: Number(midFormInput.common.pwv),
      el: Number(midFormInput.common.el)
    };

    if (midFormInput.common.array_configuration == 'Custom') {
      calculateRequestParams.nSka = midFormInput.common.n_ska!;
      calculateRequestParams.nMeer = midFormInput.common.n_meer!;
    } else {
      calculateRequestParams.subarrayConfiguration = midFormInput.common.array_configuration as SkaOstSenscalcMidApiZoomCalculateRequestParams['subarrayConfiguration'];
    }

    const zoomCalculateResult = this.midApi.skaOstSenscalcMidApiZoomCalculate(calculateRequestParams);

    if (midFormInput.common.array_configuration == 'Custom') {
       return zoomCalculateResult.pipe(map((zoomCalculate) => midZoomResponseToSensitivityResultsWithoutWeighting(zoomCalculate, integrationTimesDifferent)));
    }

    const weightingRequestParams: SkaOstSenscalcMidApiZoomWeightingRequestParams = {
      // The  backend expects 'robust' for the value we display as 'Briggs'
      weightingMode: midFormInput.line!.weighting
        ? (midFormInput.line!.weighting == 'briggs' ? 'robust' : midFormInput.line!.weighting as SkaOstSenscalcMidApiZoomWeightingRequestParams['weightingMode'])
        : "natural",
      subarrayConfiguration: midFormInput.common.array_configuration as SkaOstSenscalcMidApiZoomWeightingRequestParams['subarrayConfiguration'],
      pointingCentre: midFormInput.common.ra_str + ' ' + midFormInput.common.dec_str,
      freqCentresHz: [midFormInput.line!.zoomFrequency],
      taper: midFormInput.line?.taper as SkaOstSenscalcMidApiZoomWeightingRequestParams['taper']
    };
    if (weightingRequestParams.weightingMode == "robust" && midFormInput.line?.robustness != undefined) {
      weightingRequestParams.robustness = midFormInput.line.robustness as SkaOstSenscalcMidApiZoomWeightingRequestParams['robustness'];
    }

    const zoomWeightingResult = this.midApi.skaOstSenscalcMidApiZoomWeighting(
      weightingRequestParams
    );

    return combineLatest(zoomCalculateResult, zoomWeightingResult)
      .pipe(
        map(([zoomCalculate, zoomWeighting]) => helpers.option.zoomBeamNonGaussian(midFormInput)
          ? midZoomResponseToSensitivityResultsNoneGaussian(zoomCalculate, zoomWeighting, integrationTimesDifferent)
          : midZoomResponseToSensitivityFullResults(zoomCalculate, zoomWeighting, integrationTimesDifferent)
      ));

  }

  private calculateZoomIntegrationTimeResult(midFormInput: MidFormInput): Observable<ZoomResults> {
    // For the zoom mode integration time, only one call to the API is required, with the zoom related parameters populated.

    if (midFormInput.common.array_configuration == 'Custom') {
       return this.calculateZoomIntegrationTimesWithoutWeighting(midFormInput);
    }

    const weightingRequestParams: SkaOstSenscalcMidApiZoomWeightingRequestParams = {
      // The  backend expects 'robust' for the value we display as 'Briggs'
      weightingMode: midFormInput.line!.weighting
        ? (midFormInput.line!.weighting == 'briggs' ? 'robust' : midFormInput.line!.weighting as SkaOstSenscalcMidApiZoomWeightingRequestParams['weightingMode'])
        : "natural",
      subarrayConfiguration: midFormInput.common.array_configuration as SkaOstSenscalcMidApiZoomWeightingRequestParams['subarrayConfiguration'],
      pointingCentre: midFormInput.common.ra_str + ' ' + midFormInput.common.dec_str,
      freqCentresHz: [midFormInput.line!.zoomFrequency],
      taper: midFormInput.line?.taper as SkaOstSenscalcMidApiZoomWeightingRequestParams['taper']
    };
      if (weightingRequestParams.weightingMode == "robust" && midFormInput.line?.robustness != undefined) {
        weightingRequestParams.robustness = midFormInput.line.robustness as SkaOstSenscalcMidApiZoomWeightingRequestParams['robustness'];
      }

    const zoomWeightingResult = this.midApi.skaOstSenscalcMidApiZoomWeighting(
      weightingRequestParams
    );

    return zoomWeightingResult.pipe(mergeMap((zoomWeighting) => this.calculateZoomIntegrationTimesGivenWeightingResponse(midFormInput, zoomWeighting)));
  }

  private calculateContinuumIntegrationTimesGivenWeightingResponses(midFormInput: MidFormInput, continuumWeightingResult: MidContinuumWeightingResult, lineWeightingResult: MidContinuumWeightingResult): Observable<ContinuumResults> {

    // Request /continuum/calculate for the continuum thermal sensitiv
    const continuumSensitivityJy = helpers.calculate.sensitivityOnUnit(midFormInput.continuum!.selectedSensitivityUnit!, midFormInput.continuum!.sensitivity!, continuumWeightingResult.sbs_conv_factor);
    const confusionNoise = isIncludeConfusionNoise(midFormInput) ? continuumWeightingResult.confusion_noise.value : 0;
    if (confusionNoise >= continuumSensitivityJy) {
      return of({warnings: {errorMsg: helpers.messages.sensLimitError(helpers.format.convertSensitivityToDisplayValue(confusionNoise*1e6,3))}});
    }
    const thermalSensitivity = helpers.calculate.thermalSensitivity(continuumSensitivityJy, confusionNoise, continuumWeightingResult.weighting_factor);
    const calculateRequestParams: SkaOstSenscalcMidApiContinuumCalculateRequestParams = {
      sensitivityJy: thermalSensitivity,
      freqCentreHz: midFormInput.continuum!.frequency,
      bandwidthHz: midFormInput.continuum!.bandwidth,
      rxBand: midFormInput.common.rx_band,
      pointingCentre: midFormInput.common.ra_str + ' ' + midFormInput.common.dec_str,
      spectralAveragingFactor: midFormInput.continuum!.spectralAveragingFactor as SkaOstSenscalcMidApiContinuumCalculateRequestParams['spectralAveragingFactor'],
      pwv: Number(midFormInput.common.pwv),
      el: Number(midFormInput.common.el),
      nSubbands: Number(midFormInput.continuum!.n_subbands ?? 1),
      ...midFormInput.advanced
    };

    if (midFormInput.common.array_configuration == 'Custom') {
      calculateRequestParams.nSka = midFormInput.common.n_ska!;
      calculateRequestParams.nMeer = midFormInput.common.n_meer!;
    } else {
      calculateRequestParams.subarrayConfiguration = midFormInput.common.array_configuration as SkaOstSenscalcMidApiContinuumCalculateRequestParams['subarrayConfiguration'];
    }
    if (Number(midFormInput.continuum!.n_subbands ?? 1) > 1) {
      // As the confusion noise will always be greatest for the sub-band with the lowest frequency, in principle we
      // only need to compare the entered sensitivity with the confusion noise of the first sub-band.
      const firstSubBandConfusionNoise = isIncludeConfusionNoise(midFormInput) ? continuumWeightingResult.subbands![0].confusion_noise.value: 0;
      if (firstSubBandConfusionNoise > continuumSensitivityJy) {
        return of({warnings: {errorMsg: helpers.messages.sensLimitError(helpers.format.convertSensitivityToDisplayValue(confusionNoise*1e6, 3))}});
      }

      calculateRequestParams.subbandSensitivitiesJy = continuumWeightingResult.subbands!.map((subband_response) => {
        const subBandConfusionNoise = isIncludeConfusionNoise(midFormInput) ? subband_response.confusion_noise.value : 0;
        return helpers.calculate.thermalSensitivity(continuumSensitivityJy, subBandConfusionNoise, subband_response.weighting_factor);
      });
    }
    const continuumCalculateResult = this.midApi.skaOstSenscalcMidApiContinuumCalculate(calculateRequestParams);

    // Request /continuum/calculate for the spectral thermal sensitivity
    const spectralSensitivityJy = helpers.calculate.sensitivityOnUnit(midFormInput.continuum!.selectedSensitivityUnit!, midFormInput.continuum!.sensitivity!, lineWeightingResult.sbs_conv_factor);
    const spectralConfusionNoise = isIncludeConfusionNoise(midFormInput) ? lineWeightingResult.confusion_noise.value : 0;
    if (spectralConfusionNoise >= spectralSensitivityJy) {
      return of({warnings: {errorMsg: helpers.messages.sensLimitError(helpers.format.convertSensitivityToDisplayValue(spectralConfusionNoise*1e6, 3))}});
    }
    // The second call to calculate is to calculate the spectral integration time using the sensitivity combined with the line mode weighting factor and confusion noise.
    // This isn't very intuative, as the first call returns a spectral_integration_time that we do not use..
    calculateRequestParams.sensitivityJy = helpers.calculate.thermalSensitivity(spectralSensitivityJy, spectralConfusionNoise, lineWeightingResult.weighting_factor);
    const lineCalculateResult = this.midApi.skaOstSenscalcMidApiContinuumCalculate(calculateRequestParams);

    // combine results to display value
    return combineLatest(continuumCalculateResult, lineCalculateResult)
      .pipe(
        map(([continuumCalculate, lineCalculateResult]) => helpers.option.continuumBeamNonGaussian(midFormInput)
          ? midContinuumResponseToIntegrationTimeResultsNoneGaussian(continuumCalculate, lineCalculateResult)
          : midContinuumResponseToIntegrationTimeFullResults(continuumSensitivityJy, continuumCalculate, lineCalculateResult, continuumWeightingResult, lineWeightingResult)
        )
      );
  }

  private calculateZoomIntegrationTimesGivenWeightingResponse(midFormInput: MidFormInput, zoomWeightingResult: Array<MidSingleWeightingZoomResult>): Observable<ZoomResults> {
    // The API supports multiple zoom windows so returns and array, but the UI only supports one, so we take the first element
    const weightingResult = zoomWeightingResult[0];

    const lineSensitivityJy = helpers.calculate.sensitivityOnUnit(midFormInput.line!.selectedSensitivityUnit!, midFormInput.line!.sensitivity!, weightingResult.sbs_conv_factor);
    const lineConfusionNoise = isIncludeConfusionNoise(midFormInput, true) ? weightingResult.confusion_noise.value : 0;
    if (lineConfusionNoise >= lineSensitivityJy) {
      return of({warnings: {errorMsg: helpers.messages.sensLimitError(helpers.format.convertSensitivityToDisplayValue(lineConfusionNoise*1e6, 3))}});
    }
    const thermalSensitivity = helpers.calculate.thermalSensitivity(lineSensitivityJy, lineConfusionNoise, weightingResult.weighting_factor);

     const calculateRequestParams: SkaOstSenscalcMidApiZoomCalculateRequestParams = {
      sensitivitiesJy: [thermalSensitivity],
      freqCentresHz: [midFormInput.line!.zoomFrequency],
      totalBandwidthsHz: [midFormInput.line!.zoomBandwidth],
      spectralResolutionsHz: [midFormInput.line!.spectralResolution!],
      rxBand: midFormInput.common.rx_band,
      pointingCentre: midFormInput.common.ra_str + ' ' + midFormInput.common.dec_str,
      spectralAveragingFactor: midFormInput.line!.spectralAveragingFactor! as SkaOstSenscalcMidApiZoomCalculateRequestParams['spectralAveragingFactor'],
      pwv: Number(midFormInput.common.pwv),
      el: Number(midFormInput.common.el),
       ...midFormInput.advanced
    };

    if (midFormInput.common.array_configuration == 'Custom') {
      calculateRequestParams.nSka = midFormInput.common.n_ska!;
      calculateRequestParams.nMeer = midFormInput.common.n_meer!;
    } else {
      calculateRequestParams.subarrayConfiguration = midFormInput.common.array_configuration as SkaOstSenscalcMidApiZoomCalculateRequestParams['subarrayConfiguration'];
    }

    return this.midApi.skaOstSenscalcMidApiZoomCalculate(calculateRequestParams)
      .pipe(
        map((zoomCalculate) => helpers.option.zoomBeamNonGaussian(midFormInput)
          ? midZoomResponseToIntegrationTimeResultsNoneGaussian(zoomCalculate)
          : midZoomResponseToIntegrationTimeFullResults(lineSensitivityJy, zoomCalculate, zoomWeightingResult)
      ));
  }

  private calculateZoomIntegrationTimesWithoutWeighting(midFormInput: MidFormInput): Observable<ZoomResults> {
    const lineSensitivityJy = helpers.format.convertSensitivityToJy(midFormInput.line!.sensitivity!, midFormInput.line!.selectedSensitivityUnit!);

     const calculateRequestParams: SkaOstSenscalcMidApiZoomCalculateRequestParams = {
      sensitivitiesJy: [lineSensitivityJy],
      freqCentresHz: [midFormInput.line!.zoomFrequency],
      totalBandwidthsHz: [midFormInput.line!.zoomBandwidth],
      spectralResolutionsHz: [midFormInput.line!.spectralResolution!],
      rxBand: midFormInput.common.rx_band,
      pointingCentre: midFormInput.common.ra_str + ' ' + midFormInput.common.dec_str,
      spectralAveragingFactor: midFormInput.line!.spectralAveragingFactor! as SkaOstSenscalcMidApiZoomCalculateRequestParams['spectralAveragingFactor'],
      pwv: Number(midFormInput.common.pwv),
      el: Number(midFormInput.common.el),
       ...midFormInput.advanced
    };

    if (midFormInput.common.array_configuration == 'Custom') { // This method is only called for custom subarrays currently, but leave this check in for future proofing
      calculateRequestParams.nSka = midFormInput.common.n_ska!;
      calculateRequestParams.nMeer = midFormInput.common.n_meer!;
    } else {
      calculateRequestParams.subarrayConfiguration = midFormInput.common.array_configuration as SkaOstSenscalcMidApiZoomCalculateRequestParams['subarrayConfiguration'];
    }

    return this.midApi.skaOstSenscalcMidApiZoomCalculate(calculateRequestParams)
      .pipe(
        map(midZoomResponseToIntegrationTimeResultsWithoutWeighting)
      );
  }

  private calculateContinuumIntegrationTimesWithoutWeighting(midFormInput: MidFormInput): Observable<ZoomResults> {
  const continuumSensitivityJy = helpers.format.convertSensitivityToJy(midFormInput.continuum!.sensitivity!, midFormInput.continuum!.selectedSensitivityUnit!);

   const calculateRequestParams: SkaOstSenscalcMidApiContinuumCalculateRequestParams = {
    sensitivityJy: continuumSensitivityJy,
    freqCentreHz: midFormInput.continuum!.frequency,
    bandwidthHz: midFormInput.continuum!.bandwidth,
    rxBand: midFormInput.common.rx_band,
    pointingCentre: midFormInput.common.ra_str + ' ' + midFormInput.common.dec_str,
    spectralAveragingFactor: midFormInput.continuum!.spectralAveragingFactor as SkaOstSenscalcMidApiContinuumCalculateRequestParams['spectralAveragingFactor'],
    pwv: Number(midFormInput.common.pwv),
    el: Number(midFormInput.common.el),
    nSubbands: Number(midFormInput.continuum!.n_subbands ?? 1),
    ...midFormInput.advanced
  };

  if (midFormInput.common.array_configuration == 'Custom') { // This method is only called for custom subarrays currently, but leave this check in for future proofing
    calculateRequestParams.nSka = midFormInput.common.n_ska!;
    calculateRequestParams.nMeer = midFormInput.common.n_meer!;
  } else {
    calculateRequestParams.subarrayConfiguration = midFormInput.common.array_configuration as SkaOstSenscalcMidApiZoomCalculateRequestParams['subarrayConfiguration'];
  }
  return this.midApi.skaOstSenscalcMidApiContinuumCalculate(calculateRequestParams)
    .pipe(
      map(midContinuumResponseToIntegrationTimeResultsWithoutWeighting)
    );
}
}

export const isIncludeConfusionNoise = (midFormInput: MidFormInput, zoom = false): boolean => {

  const {array_configuration} = midFormInput.common;
  const {weighting, taper, robustness} = zoom ? midFormInput.line! : midFormInput.continuum!;
      // Delegate to the common helper method, which returns a string if weighting cannot be applied and null if it can
  return !helpers.option.beamNotApplicableMessageForMid(weighting, taper!, robustness!, array_configuration);

  };
