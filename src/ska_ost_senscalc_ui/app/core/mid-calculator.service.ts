import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Configuration } from '../app-configuration';

// TODO this needs to be tidyed up or removed
@Injectable({
  providedIn: 'root'
})
export class MidCalculatorService {

  private config: Configuration | null;

  private observingModeObservable = new BehaviorSubject("");

  constructor() {
    this.config = null;
  }

  setConfig(config: Configuration) {
    this.config = config;
  }

  setObservingModeValue(mode: string) {
    this.observingModeObservable.next(mode);
  }

  getObservingMode(): Observable<string> {
    return this.observingModeObservable;
  }

  getDisplayedTaper(taper: number, selectedFrequencyInGHz?: number) {
    if (taper === 0) {
      return "No tapering";
    }
    if (selectedFrequencyInGHz && selectedFrequencyInGHz !== 0) {
      return (taper * (1.4 / selectedFrequencyInGHz)).toFixed(3);
    }
    return taper.toFixed(3);
  }
}
