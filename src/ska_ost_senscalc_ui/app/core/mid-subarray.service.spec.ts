import {TestBed} from '@angular/core/testing';
import {from, throwError} from 'rxjs';
import {
  BASE_PATH,
  DefaultService,
  MidApiModule,
  MidSubarraysResponse
} from '../generated/api/mid';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MidSubarrayService} from './mid-subarray.service';

describe('MidQueryService', () => {
  let fixture: MidSubarrayService;

  const customSubarray: MidSubarraysResponse = {
    name: 'Custom',
    label: 'Custom',
    n_ska: 133,
    n_meer: 64
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, MidApiModule],
      providers: [
        {
          provide: BASE_PATH,
          useValue: 'http://backend'
        }
      ]
    }).compileComponents();

    fixture = TestBed.inject(MidSubarrayService);
  });

  describe('When instantiated', () => {
    const subarray: MidSubarraysResponse = {
      name: 'foo',
      label: 'bar',
      n_ska: 10,
      n_meer: 20
    };

    beforeEach(() => {});

    it('populates the subarrays$ Observable', () => {
      const fakeApi = {
        skaOstSenscalcMidApiSubarrays: () => from([[subarray]])
      };
      fixture = new MidSubarrayService(fakeApi as unknown as DefaultService);

      expect(fixture.subarrays$.getValue()).toEqual([subarray, customSubarray]);
    });

    it('adds Custom subarray as an option', () => {
      const fakeApi = {
        skaOstSenscalcMidApiSubarrays: () => from([[]])
      };
      fixture = new MidSubarrayService(fakeApi as unknown as DefaultService);

      expect(fixture.subarrays$.getValue()).toEqual([customSubarray]);
    });

    it('leaves subQueryFailed$ as false if the subarray query succeeds', () => {
      const fake = {
        skaOstSenscalcMidApiSubarrays: () => from([[subarray]])
      };
      fixture = new MidSubarrayService(fake as unknown as DefaultService);

      expect(fixture.subQueryFailed$.getValue()).toBe(false);
    });

    it('sets subQueryFailed$ to true if the subarray query fails', () => {
      const fake = {
        skaOstSenscalcMidApiSubarrays: () => throwError(() => new Error())
      };
      fixture = new MidSubarrayService(fake as unknown as DefaultService);

      expect(fixture.subQueryFailed$.getValue()).toBe(true);
    });
  });

});
