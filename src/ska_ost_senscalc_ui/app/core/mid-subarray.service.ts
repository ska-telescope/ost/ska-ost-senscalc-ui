import { Injectable, OnDestroy } from '@angular/core';
import {
  BehaviorSubject,
  Subscription
} from 'rxjs';
import {
  DefaultService,
  MidSubarraysResponse
} from '../generated/api/mid';

@Injectable({
  providedIn: 'root'
})
export class MidSubarrayService implements OnDestroy {
  // populated with the list of subarray objects as the response is returned
  subarrays$ = new BehaviorSubject<MidSubarraysResponse[]>([]);

  // flag that is set if the subarray query fails
  subQueryFailed$ = new BehaviorSubject<boolean>(false);

  // list of subscriptions to be unsubscribed on service destruction
  private subscriptions: Subscription[] = [];

  constructor(private midApi: DefaultService) {
    // custom subarray to be appended to any successful query response
    const customSubarray: MidSubarraysResponse = {
      name: 'Custom',
      label: 'Custom',
      n_ska: 133,
      n_meer: 64
    };

    // populate the subarrays attribute, setting the subQueryFailed flag if
    // any error occurs.
    const subarrays$ = midApi.skaOstSenscalcMidApiSubarrays();
    this.subscriptions.push(
      subarrays$.subscribe({
        next: (value) => this.subarrays$.next(value.concat(customSubarray)),
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        error: (_) => this.subQueryFailed$.next(true)
      })
    );
  }

  private dispose() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  ngOnDestroy() {
    this.dispose();
  }
}
