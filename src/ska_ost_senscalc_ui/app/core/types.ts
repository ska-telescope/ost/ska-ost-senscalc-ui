export interface Warnings {
  sensLimitReached?: {reached: boolean, value?: string};
  integrationTimesDifferent?: boolean;
  errorMsg?: string;
}

export interface SpectropolarimetryResults {
  fwhmOfTheRMSF: string;
  maximumFaradayDepthExtent: string;
  maximumFaradayDepth: string;
}

export interface ContinuumResults {
  weightedContinuumSensitivity?: string;
  continuumConfusionNoise?: string;
  totalContinuumSensitivity?: string;
  continuumSynthesizedBeamSize?: string;
  continuumSurfaceBrightnessSensitivity?: string;
  continuumIntegrationTime?: string;

  weightedSpectralSensitivity?: string;
  spectralConfusionNoise?: string;
  totalSpectralSensitivity?: string;
  spectralSynthesizedBeamSize?: string;
  spectralSurfaceBrightnessSensitivity?: string;
  spectralIntegrationTime?: string;

  weightedSensitivityPerSubBand?: string;
  confusionNoisePerSubBand?: string;
  totalSensitivityPerSubBand?: string;
  synthesizedBeamSizePerSubBand?: string;
  surfaceBrightnessSensitivityPerSubBand?: string;
  integrationTimePerSubBand?: string;

  spectropolarimetryResults?: SpectropolarimetryResults

  warnings?: Warnings
}

export interface ZoomResults {
  weightedSpectralSensitivity?: string;
  spectralConfusionNoise?: string;
  totalSpectralSensitivity?: string;
  spectralSynthesizedBeamSize?: string;
  spectralSurfaceBrightnessSensitivity?: string;
  spectralIntegrationTime?: string;

  spectropolarimetryResults?: SpectropolarimetryResults

  warnings?: Warnings
}

export interface PssResults {
  sensitivity: string

  warnings: Warnings
}
