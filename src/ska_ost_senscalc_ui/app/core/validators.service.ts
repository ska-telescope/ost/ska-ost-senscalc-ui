import { Injectable } from '@angular/core';
import {
  ArrayDropDownValue,
  Configuration,
  ConfigurationDropDownValue, MidSubarrayId
} from '../app-configuration';
import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';
import {
  LowSubarraysResponse,
  SkaOstSenscalcLowApiContinuumCalculateRequestParams,
  SkaOstSenscalcLowApiContinuumWeightingRequestParams
} from "../generated/api/low";

@Injectable({
  providedIn: 'root'
})
export class ValidatorsService {
  protected config: Configuration | null;
  midMinimumChannelWidthHz = 13.44e3; // This is the fundamental limit of the bandwidth provided by SKA MID
  lowMinimumChannelWidthHz = 24 * 781.25e3 / 3456; // This is the fundamental limit of the bandwidth provided by SKA LOW

  // For the subarrays not listed here, the full bandwidth is allowed
  midMaxContBandwidthHzForSubarray = new Map<MidSubarrayId, number>([
      ["AA0.5", 800e6],
      ["AA1", 800e6],
      ["AA2", 800e6]
  ]);
  lowMaxContBandwidthHzForSubarray = new Map<SkaOstSenscalcLowApiContinuumCalculateRequestParams["subarrayConfiguration"], number>([
      ["LOW_AA05_all", 75e6],
      ["LOW_AA1_all", 75e6],
      ["LOW_AA2_all", 150e6],
      ["LOW_AA2_core_only", 150e6]
  ]);

  constructor() {
    this.config = null;
  }

  setConfig(config: Configuration) {
    this.config = config;
  }

  // Validate right ascension input
  validatorRightAscension(decimal: boolean): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const value: string = control.value as string;
      const test = decimal ? !this.parseDecRA(value) : !this.parseRA(value);

      const message = this.config?.validationErrors!.find(e => e.errorName === 'raError')?.errorMessage;
      return test ? { errorMessage: message } : null;
    };
  }

  validatorMinReceiverNum(nSka: string, nMeer: string): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const nSkaValue = parseInt(control.parent?.get(nSka)?.value, 10);
      const nMeerValue = parseInt(control.parent?.get(nMeer)?.value, 10);

      if (nSkaValue + nMeerValue < 2 ) {
        const message = this.config?.validationErrors!.find(e => e.errorName === 'minReceiverError')?.errorMessage;
        return { errorMessage: message };
      }

      return null;
    };
  }

  // Validate declination input
  validatorDeclination(source: string, decimal: boolean): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const value: string = control.value as string;

      if (decimal ? !this.parseDecDec(value) : !this.parseDec(value)) {
        const validationMessage = this.config?.validationErrors!.find(e => e.errorName === 'decError')!.errorMessage;
        return {errorMessage: validationMessage};
      }

      if (!this.isVisible(value, source)) {
        const isVisibleMessage =
          this.config?.validationErrors!.find(e => e.errorName === 'isVisibleError')?.errorMessage;
        return {errorMessage: isVisibleMessage};
      }

      return null;
    };
  }

  // Validate pulse width input
  validatorPulseWidth(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if (parseFloat(control.value) > parseFloat(control.parent?.get("pulsePeriod")?.value)) {
        const validationMessage = this.config?.validationErrors!.find(e => e.errorName === 'pulseWidthError')?.errorMessage;
        return {errorMessage: validationMessage};
      }

      return null;
    };
  }

  // Validate right ascension input
  validatorInteger(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if (control.value) {
        const value = control.value as number;

        if (!this.isNumeric(value)) {
          const message = this.config?.validationErrors!.find(e => e.errorName === 'numericError')?.errorMessage;
          return { errorMessage: message };
        }

        if (value < 0) {
          const message = this.config?.validationErrors!.find(e => e.errorName === 'negativeError')?.errorMessage;
          return { errorMessage: message };
        }

        if (!Number.isInteger(value)) {
          const message = this.config?.validationErrors!.find(e => e.errorName === 'integerError')?.errorMessage;
          return { errorMessage: message };
        }
        return null;
      }
      return null;
    };
  }

  validatorEfficiency(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if (control.value || control.value === 0) {
        const value = parseFloat(control.value as string);
        const message = this.config?.validationErrors!.find(e => e.errorName === 'efficiencyError')?.errorMessage;

        if (!this.isNumeric(value)) {
          return { errorMessage: message };
        }
        if ((value <= 0) || (value > 1)) {
          return { errorMessage: message };
        }
      }
      return null;
    };
  }

  validatorElevation(min: number, decField: string, latitude: number): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const declination = control.parent?.get(decField)?.value;
      // Check specifically for 0 to differentiate from null.
      if ((control.value === 0 || control.value) && declination) {
        const sexaDec = this.sexa2Dec(declination);
        const max = Math.floor((90 - (Math.abs(sexaDec - latitude))) * 10) / 10;
        if((control.value < min) || (control.value > max)){
          let message = this.config?.validationErrors!.find(e => e.errorName === 'minmaxError')?.errorMessage;
          message = message?.replace("%s", min.toString()).replace("%s", max.toString());
          return { errorMessage: message };
        }
      }
      return null;
    };
  }

  validatorElevationMid(min: number, decField: string): ValidatorFn {
    const latitude = this.sexa2Dec('-30:43:16.068');
    return this.validatorElevation(min, decField, latitude);
  }

  validatorElevationLow(min: number, decField: string): ValidatorFn {
    const latitude = -26.82472208;
    return this.validatorElevation(min, decField, latitude);
  }

  //validate Min/Max Number
  validatorNumberMinMax(min: number, max: number): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {

      if (control.value || control.value === 0) {
        const value = parseFloat(control.value as string);
        let message = this.config?.validationErrors!.find(e => e.errorName === 'minmaxError')?.errorMessage;
        message = message?.replace("%s", min.toString()).replace("%s", max.toString());

        if (!this.isNumeric(value)) {
          return { errorMessage: message };
        }
        if ((value < min) || (value > max)) {
          return { errorMessage: message };
        }
      }
      return null;
    };

  }
  // General function to validate an input that needs no special treatment
  validatorGeneral(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {

      const value = control.value as string;
      if (!value) {
        return null;
      }

      if (!this.isNumeric(value) || value == "") {
        const message = this.config?.validationErrors!.find(e => e.errorName === 'numericError')?.errorMessage;
        return { errorMessage: message };
      }

      if (parseFloat(value) <= 0) {
        const message = this.config?.validationErrors!.find(e => e.errorName === 'greaterThanZeroError')?.errorMessage;
        return { errorMessage: message };
      }

      return null;
    };
  }

  minimumValue(minimum: number): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if (!control.value) {
        return null;
      }

      if (!this.isNumeric(control.value)) {
        const message = this.config?.validationErrors!.find(e => e.errorName === 'numericError')?.errorMessage;
        return { errorMessage: message };
      }

      if (control.value < minimum) {
        let message = this.config?.validationErrors!.find(e => e.errorName === 'minimumValueError')?.errorMessage;
        message = message?.replace("%s", minimum.toString());
        return { errorMessage: message };
      }

      return null;
    };
  }

  parseRA(value: string): boolean {
    return ((/^\d{1,2}:\d{1,2}:\d{1,2}((\.?)|(\.\d+))$/.test(value)) &&
           (/(?:[01]\d|2[0-3]):([0-5]\d):([0-5]\d)/.test(value)));
  }

  parseDec(value: string): boolean {
    return ((/^[-+]?\d{1,2}:\d{1,2}:\d{1,2}((\.?)|(\.\d+))$/.test(value)) &&
           (/((([0-8]\d):([0-5]\d):([0-5]\d))|90:00:00(\.0+)?$)/.test(value)));
  }

  parseDecRA(value: string): boolean {
    // Validate its a valid number
    if (!(/^[0-9]*\.?[0-9]+$/.test(value))) { return false; }
    const number = parseFloat(value);
    // Validate decimal right ascension range.
    return (number >= 0 && number < 360) ? true : false;
  }

  parseDecDec(value: string): boolean {
    // Validate its a valid number
    if (!(/^[-+]?[0-9]*\.?[0-9]+$/.test(value))) { return false; }
    const number = parseFloat(value);
    // Validate decimal declination range.
    return (number >= -90 && number <=90) ? true : false;
  }

  isVisible(dec: string, source: string): boolean {
    // Determine which telescope
    const skaLatitude = source === "Mid" ? this.sexa2Dec("-30:43:16.068") : this.sexa2Dec("-26:41:49.3");
    const elevationLimit = 15.0;
    if (this.sexa2Dec(dec) < (90.0 + skaLatitude - elevationLimit)) {
      return true;
    }
    return false;
  }

  // function to convert Sexagesimal declination to decimal for source validation
  sexa2Dec(dec: string): number {
    if(/^[-+]?[0-9]*\.?[0-9]+$/.test(dec)){
      return parseFloat(dec);
    }
    const decSplit = dec.split(':', 3);

    if (Number(decSplit[0]) < 0) {
      return (Number(decSplit[0]) - Number(decSplit[1]) / 60 - Number(decSplit[2]) / 3600);
    }
    return (Number(decSplit[0]) + Number(decSplit[1]) / 60 + Number(decSplit[2]) / 3600);
  }

  isNumeric(val: any): boolean {
    return !(val instanceof Array) && (val - parseFloat(val as string) + 1) >= 0;
  }

  // Validate a frequency input, making sure it is not empty and is a number.
  centralFrequencyValidator(freqScale: ConfigurationDropDownValue, observingMode: string, subarrayType: ArrayDropDownValue): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const value = control.value as number;
      const message = this.config?.validationErrors!.find(e => e.errorName === 'numericError')?.errorMessage;
      const rangeMessage = this.config?.validationErrors!.find(e => e.errorName === 'rangeError')?.errorMessage;
      if (!this.isNumeric(value)) {
        control?.setErrors({ 'numericError': message });
        return { 'numericError': message };
      }
      const scaledFrequency = this.getScaledValue(value, freqScale.multiplier!, freqScale.operator!);
      if (this.isFreqContained(scaledFrequency.toString(), observingMode, subarrayType)) {
        control?.setErrors(null);
        return null;
      }
      control.setErrors({ 'rangeError': rangeMessage });
      return { 'rangeError': rangeMessage };
    };
  }

  midContinuumBandwidthValidator(freqScale: ConfigurationDropDownValue, bandwidthScale: ConfigurationDropDownValue, observingMode: string, subarrayType: ArrayDropDownValue): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const frequency = control.get('fieldCentralFrequency');
      const bandwidth = control.get('fieldBandwidth');
      const isNumericError = !this.isNumeric(bandwidth?.value);
      if (isNumericError) {
        const numericErrorMessage = {
          'numericError': this.config?.validationErrors!.find(e => e.errorName === 'numericError')?.errorMessage
        };
        bandwidth?.setErrors(numericErrorMessage);
        return numericErrorMessage;
      }
      const scaledFrequency = this.getScaledValue(frequency?.value, freqScale.multiplier!, freqScale.operator!);
      const scaledBandwidth = this.getScaledValue(bandwidth?.value, bandwidthScale.multiplier!, bandwidthScale.operator!);
      const errors = this.bandwidthFieldValidator(scaledFrequency, scaledBandwidth, observingMode, subarrayType);
      bandwidth?.setErrors(errors);
      return errors;
    };
  }

  midZoomBandwidthValidator(freqScale: ConfigurationDropDownValue, observingMode: string, subarrayType: ArrayDropDownValue): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const frequency = control.get('fieldZoomFrequency');
      const resolution = control.get('fieldZoomResolution');
      const isNumericError = !this.isNumeric(resolution?.value);
      if (isNumericError) {
        const numericErrorMessage = {
          'numericError': this.config?.validationErrors!.find(e => e.errorName === 'numericError')?.errorMessage
        };
        resolution?.setErrors(numericErrorMessage);
        return numericErrorMessage;
      }
      const bandwidth = this.config!.resolutionDropdownValues!.find(e => e.value === resolution?.value)!.bandwidth;
      const scaledFrequency = this.getScaledValue(frequency?.value, freqScale.multiplier!, freqScale.operator!);
      const errors = this.bandwidthFieldValidator(scaledFrequency, bandwidth, observingMode, subarrayType);
      resolution?.setErrors(errors);
      return errors;
    };
  }

  bandwidthFieldValidator(frequency: number, bandwidth: number, observingMode: string, subarrayType: ArrayDropDownValue) {
    const rangeErrorMessage = this.config?.validationErrors!.find(e => e.errorName === "bandwidthRangeError")?.errorMessage;
    const minimumChannelWidthErrorMessage = this.config?.validationErrors!.find(e => e.errorName === "bandwidthSmallerThanChannel")?.errorMessage;
    const contBandwidthMaximumExceededMessage = this.config?.validationErrors!.find(e => e.errorName === "contBandwidthMaximumExceeded")?.errorMessage;
    if (bandwidth < this.midMinimumChannelWidthHz) {
      return { 'minimumChannelWidthError': minimumChannelWidthErrorMessage };
    }

    const maxContBandwidthHz = this.midMaxContBandwidthHzForSubarray.get(subarrayType.label as MidSubarrayId);
    if (maxContBandwidthHz && bandwidth > maxContBandwidthHz) {
      const message = contBandwidthMaximumExceededMessage?.replace("%s", (maxContBandwidthHz * 1e-6).toString());
      return { 'contBandwidthMaximumExceeded': message };
    }

    if (!this.isBandwidthContained(frequency, bandwidth, observingMode, subarrayType)) {
      return { 'bandwidthRangeError': rangeErrorMessage };
    }
    return null;
  }

  lowContinuumBandwidthValidator(subarray?: LowSubarraysResponse) : ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const frequency = control.parent?.get('centralFrequency')?.value;
      const bandwidth = control.value;
      const errors = this.lowBandwidthValidator(bandwidth, frequency, subarray);
      control.setErrors(errors);
      return errors;
    };
  }

  lowZoomBandwidthValidator(subarray?: LowSubarraysResponse) : ValidatorFn {
    return (control: AbstractControl) : ValidationErrors | null => {
      const frequency = control.parent?.get('centralFrequency')?.value;
      const bandwidthMHz = control.value.totalBandwidthKhz  * 1e-3;
      const errors = this.lowBandwidthValidator(bandwidthMHz, frequency, subarray);
      control.setErrors(errors);
      return errors;
    };
  }

  lowPssBandwidthValidator() : ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const frequency = control.value;
      const bandwidth = control.parent?.get('totalBandwidth')?.value;
      const errors = this.lowBandwidthValidator(bandwidth, frequency);
      control.setErrors(errors);
      return errors;
    };
  }

  lowBandwidthValidator(bandwidth: number, frequency: number, subarray: LowSubarraysResponse | undefined = undefined) {
    const numericErrorMessage = this.config?.validationErrors!.find(e => e.errorName === 'numericError')?.errorMessage;
    const rangeErrorMessage = this.config?.validationErrors!.find(e => e.errorName === "bandwidthRangeError")?.errorMessage;
    const minimumChannelWidthErrorMessage = "Bandwidth must be larger than the channel width (5.43 kHz)";
    const contBandwidthMaximumExceededMessage = this.config?.validationErrors!.find(e => e.errorName === "contBandwidthMaximumExceeded")?.errorMessage;
    const isNumericError = !this.isNumeric(bandwidth);
    if (isNumericError) {
      return { 'numericError': numericErrorMessage };
    }
    if ((bandwidth * 1e6) < this.lowMinimumChannelWidthHz) {
      return { 'minimumChannelWidthError': minimumChannelWidthErrorMessage };
    }

    if (subarray) {
      const maxContBandwidthHz = this.lowMaxContBandwidthHzForSubarray.get(subarray.name as SkaOstSenscalcLowApiContinuumWeightingRequestParams["subarrayConfiguration"]);
      if (maxContBandwidthHz && ((bandwidth * 1e6) > maxContBandwidthHz)) {
        const message = contBandwidthMaximumExceededMessage?.replace("%s", (maxContBandwidthHz * 1e-6).toString());
        return { 'contBandwidthMaximumExceeded': message };
      }
    }

    if (!this.isBandwidthContainedLow(frequency, bandwidth)){
      return {'bandwidthRangeError': rangeErrorMessage};
    }
    return null;
  }

  //Function to convert a given frequency/time/sensitivity according to the units
  getScaledValue(value: any, multiplier: number, operator: string): number {
    let val_scaled = 0;
    switch (operator) {
      case "*": val_scaled = value * multiplier; break;
      case "/": val_scaled = value / multiplier; break;
      default: val_scaled = value;
    }

    return val_scaled;
  }

  // Function to check if a given frequency is contained within an acceptable band
  isFreqContained(observingFrequency: string, observingMode: string, subarrayType: ArrayDropDownValue): boolean {
    const limits = this.getBandLimits(observingMode, subarrayType);
    const parsedFreqScaled = parseFloat(observingFrequency);
    return limits.length > 0 ? parsedFreqScaled >= limits[0] && parsedFreqScaled <= limits[1] : false;
  }

  // Function to check if given bandwidth is entirely contained within the band.
  isBandwidthContained(scaledFrequency: number, scaledBandwidth: number, observingMode: string, subarrayType: ArrayDropDownValue): boolean {
    const scaledFrequencyNum = Number(scaledFrequency);
    const halfBandwidth = Number(scaledBandwidth) / 2.0;

    const lowerBound: number = scaledFrequencyNum - halfBandwidth;
    const upperBound: number = scaledFrequencyNum + halfBandwidth;

    const bandLimits = this.getBandLimits(observingMode, subarrayType);

    return !(lowerBound < bandLimits[0] || upperBound > bandLimits[1]);
  }

  // This was created for the LOW calculator, but in future we should merge this function and the one above
  isBandwidthContainedLow(centralFrequency: number, continuumBandwidth: number): boolean {
    const frequency = Number(centralFrequency);
    const halfBandwidth = Number(continuumBandwidth) / 2.0;

    const lowerBound: number = frequency - halfBandwidth;
    const upperBound: number = frequency + halfBandwidth;

    return !(lowerBound < 50.0 || upperBound > 350);
  }

  getBandLimits(observingMode: string, subarrayType: ArrayDropDownValue): number[] {
    const fieldObservingMode = this.config?.midCalcFormFields?.find(e => e.field === 'fieldObservingMode');
    if (!fieldObservingMode) {
      return [];
    }

    const bandLimits = fieldObservingMode.defaultValue.find((e: any) => e.mode === observingMode).bandLimits;

    const hasSKA = subarrayType.n_ska > 0;
    const hasMeerkat = subarrayType.n_meer > 0;

    let key: string;
    if (hasMeerkat && !hasSKA) {
      key = "meerkat";
    } else if (hasSKA && !hasMeerkat) {
      key = "ska";
    } else {
      key = "mixed";
    }
    return bandLimits.find((e: any) => e.type === key).limits;
  }

  getErrorMessage(control: AbstractControl | null) {
    if (control === null || control.errors === null) {
      return null;
    }

    let msg = "";
    // Get the key to the first error in the list as we only want to report one error at a time.
    const [errorKey] = Object.keys(control.errors);

    if (errorKey === 'required') {
      // Error messages which have a key of "required" have the message "true"
      // so return a more appropriate message.
      msg = "Field cannot be empty";
    } else {
      msg = control.errors[errorKey];
    }

    return msg;
  }

  subBandBandwidthValidator(bandwidthScale: ConfigurationDropDownValue) {
    // The sub-band bandwidth defined by the bandwidth of the observation divided by the number of
    // sub-bands should be greater than the minimum allowed bandwidth of SKA MID.
    return (control: AbstractControl): ValidationErrors | null => {
      const bandwidth = control.get('fieldBandwidth');
      const scaledBandwidth = this.getScaledValue(bandwidth?.value, bandwidthScale.multiplier!, bandwidthScale.operator!);
      const nSubBands = control.get("fieldNoOfSubBands");
      const subBandBandwidthErrorMessage  = "Sub-band bandwidth is too low - reduce number of sub-bands";
      if (nSubBands?.value != null && (scaledBandwidth / nSubBands.value) < this.midMinimumChannelWidthHz) {
        nSubBands.setErrors({ 'subBandBandwidthError': subBandBandwidthErrorMessage });
        return { 'subBandBandwidthError': subBandBandwidthErrorMessage };
      }
      return null;
      };
  }

}
