import { NgModule, ModuleWithProviders, SkipSelf, Optional } from '@angular/core';
import { LowConfiguration } from './configuration';
import { HttpClient } from '@angular/common/http';


@NgModule({
  imports:      [],
  declarations: [],
  exports:      [],
  providers: []
})
export class LowApiModule {
    public static forRoot(configurationFactory: () => LowConfiguration): ModuleWithProviders<LowApiModule> {
        return {
            ngModule: LowApiModule,
            providers: [ { provide: LowConfiguration, useFactory: configurationFactory } ]
        };
    }

    constructor( @Optional() @SkipSelf() parentModule: LowApiModule,
                 @Optional() http: HttpClient) {
        if (parentModule) {
            throw new Error('LowApiModule is already loaded. Import in your base AppModule only.');
        }
        if (!http) {
            throw new Error('You need to import the HttpClientModule in your AppModule! \n' +
            'See also https://github.com/angular/angular/issues/20575');
        }
    }
}
