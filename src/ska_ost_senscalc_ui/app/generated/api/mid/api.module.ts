import { NgModule, ModuleWithProviders, SkipSelf, Optional } from '@angular/core';
import { MidConfiguration } from './configuration';
import { HttpClient } from '@angular/common/http';


@NgModule({
  imports:      [],
  declarations: [],
  exports:      [],
  providers: []
})
export class MidApiModule {
    public static forRoot(configurationFactory: () => MidConfiguration): ModuleWithProviders<MidApiModule> {
        return {
            ngModule: MidApiModule,
            providers: [ { provide: MidConfiguration, useFactory: configurationFactory } ]
        };
    }

    constructor( @Optional() @SkipSelf() parentModule: MidApiModule,
                 @Optional() http: HttpClient) {
        if (parentModule) {
            throw new Error('MidApiModule is already loaded. Import in your base AppModule only.');
        }
        if (!http) {
            throw new Error('You need to import the HttpClientModule in your AppModule! \n' +
            'See also https://github.com/angular/angular/issues/20575');
        }
    }
}
