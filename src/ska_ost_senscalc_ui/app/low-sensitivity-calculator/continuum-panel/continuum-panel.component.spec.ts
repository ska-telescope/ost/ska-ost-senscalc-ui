import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MatCardModule } from "@angular/material/card";
import { UntypedFormBuilder, FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TestbedHarnessEnvironment } from "@angular/cdk/testing/testbed";
import { MatInputHarness } from "@angular/material/input/testing";
import { MatFormFieldHarness } from "@angular/material/form-field/testing";
import { HarnessLoader } from "@angular/cdk/testing";
import { HttpClientModule } from "@angular/common/http";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatSelectModule } from "@angular/material/select";

import { ContinuumPanelComponent } from './continuum-panel.component';
import {ValidatorsService} from "../../core/validators.service";

describe('ContinuumPanelComponent', () => {
  let fixture: ComponentFixture<ContinuumPanelComponent>;
  let component: ContinuumPanelComponent;
  let loader: HarnessLoader;

  const getFormInputHarness = async (input_name: string): Promise<MatInputHarness> => {
    // Allow asterix in label in case the field is mandatory
    const input_reg = new RegExp(input_name + "( *)");

    const control = await loader.getHarness(
      MatFormFieldHarness.with({ floatingLabelText: input_reg })
    );
    return await control.getControl() as MatInputHarness;
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ContinuumPanelComponent
      ],
      imports: [
        HttpClientModule,
        NoopAnimationsModule,
        MatCardModule,
        MatSnackBarModule,
        MatFormFieldModule,
        MatSelectModule,
        MatInputModule,
        ReactiveFormsModule,
        FormsModule
      ],
      providers: [
        UntypedFormBuilder,
        ValidatorsService
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
    .compileComponents();
    fixture = TestBed.createComponent(ContinuumPanelComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(fixture.componentInstance).toBeTruthy();
  });

  it('should have a required Integration Time field', async () => {
    const input_field = await getFormInputHarness('Integration Time');
    expect(input_field.isRequired()).toBeTruthy();
  });

  it('should have a required Integration Time field', async () => {
    const input_field = await getFormInputHarness('Central Frequency');
    expect(input_field.isRequired()).toBeTruthy();
  });

  it('should have a required Integration Time field', async () => {
    const input_field = await getFormInputHarness('Continuum Bandwidth');
    expect(input_field.isRequired()).toBeTruthy();
  });

  it('should set all continuum fields to defaults or empty when resetForm is called ', async () => {
    const int_time_input = await getFormInputHarness('Integration Time');
    await int_time_input.setValue("5");

    const cent_freq_input = await getFormInputHarness('Central Frequency');
    await cent_freq_input.setValue("123");

    const bandwidth_input = await getFormInputHarness('Continuum Bandwidth');
    await bandwidth_input.setValue("30");

    // Call the function rather than use the button as the button is part of the parent component
    component.resetForm();

    expect(await int_time_input.getValue()).toBe('1');
    expect(await cent_freq_input.getValue()).toBe('200');
    expect(await bandwidth_input.getValue()).toBe('300');
  });

  it('should return the data from the continuum forms when getFormData is called', async () => {
    const int_time_input = await getFormInputHarness('Integration Time');
    await int_time_input.setValue("5");

    const cent_freq_input = await getFormInputHarness('Central Frequency');
    await cent_freq_input.setValue("123");

    const bandwidth_input = await getFormInputHarness('Continuum Bandwidth');
    await bandwidth_input.setValue("30");

    component.imageWeighting?.setValue("Briggs");
    component.robust?.setValue(-2);
    component.showRobustness = true;

    const result = fixture.componentInstance.getFormData();

    expect(result.duration).toBe('5');
    expect(result.frequency.toString()).toBe("123");
    expect(result.bandwidth.toString()).toBe("30");
    expect(result.imageWeighting).toBe("briggs");
    expect(result.robust?.toString()).toBe("-2");
  });

  it('should have a Robust Value field with a value of 0 when Image Weighting is Briggs', async () => {
    // Set the Image Weighting to Briggs. Ideally would do this using the harness set the value on the UI element,
    // however it doesn't seem to work nicely with MatSelect.
    component.imageWeighting?.setValue("Briggs");
    component.weightingChanged();

    const input_field = await getFormInputHarness('Robust Value');
    expect(await input_field.isRequired()).toBeFalsy();
    expect(component.robust?.value).toBe(0);
  });

  it('should set the image weighting to Natural and disable the field', async () => {
    component.imageWeighting?.setValue("Briggs");
    component.disableImageWeighting();

    const input_field = await getFormInputHarness('Image Weighting');
    expect(await input_field.isDisabled()).toBeTruthy();
    expect(component.imageWeighting?.value).toBe('Natural');

    component.enableImageWeighting();
    expect(await input_field.isDisabled()).toBeFalsy();
  });

});
