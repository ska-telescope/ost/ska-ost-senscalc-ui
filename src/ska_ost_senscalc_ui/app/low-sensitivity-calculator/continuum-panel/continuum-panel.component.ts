import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from "@angular/core";
import {LowContinuumInput} from '../types';
import {AbstractControl, UntypedFormBuilder, Validators} from '@angular/forms';
import {ValidatorsService} from '../../core/validators.service';
import {ContinuumResultsComponent} from "./continuum-results/continuum-results.component";
import {
  LowSubarraysResponse,
  SkaOstSenscalcLowApiContinuumWeightingRequestParams
} from "../../generated/api/low";
import helpers from "../../shared/utils/helpers";
import {ContinuumResults} from "../../core/types";

@Component({
  selector: 'app-continuum-panel',
  templateUrl: './continuum-panel.component.html',
  styleUrls: ['./continuum-panel.component.scss']
})
export class ContinuumPanelComponent implements OnInit, OnChanges, AfterViewInit  {

  @ViewChild(ContinuumResultsComponent) output!: ContinuumResultsComponent;

  _selectedSubarray!: LowSubarraysResponse;

  @Input() set selectedSubarray(value: LowSubarraysResponse) {
    this._selectedSubarray = value;
  }

  @Input() lowContinuumResults?: ContinuumResults | null;

  formInitialValues: any;
  showRobustness!: boolean;
  imageWeightingOptions = ['Natural', 'Uniform', 'Briggs'];
  defaultImageWeighting = 'Uniform';
  robustOptions  = [-2, -1, 0, 1, 2];
  continuumBandwidthDefaultValue = 300;
  spectralResolutionkHz = 5.43;
  resolutionDefaultValue = `${this.spectralResolutionkHz} kHz`;
  spectralMode: 'continuum' | 'line' = 'continuum';
  nSubbandsLimits = {min: 1, max: 4};
  centralFrequencyLimits = {min: 50, max: 350};
  spectralAveragingLimits = {min: 1, max: 864};

  @Output() changeEvent = new EventEmitter();

  constructor(
    private validatorService: ValidatorsService,
    private fb: UntypedFormBuilder
  ) {}

  ngOnInit(): void {
  }

  continuumForm = this.fb.group({
    integrationTime: [1, [Validators.required, this.validatorService.validatorGeneral()]],
    centralFrequency: [200, [Validators.required, this.validatorService.validatorNumberMinMax(this.centralFrequencyLimits.min, this.centralFrequencyLimits.max)]],
    continuumBandwidth: [this.continuumBandwidthDefaultValue,
                         [Validators.required,
                          this.validatorService.lowContinuumBandwidthValidator(this._selectedSubarray)]],
    spectralResolution: [this.resolutionDefaultValue, []],
    spectralAveraging: [1, [Validators.required, this.validatorService.validatorNumberMinMax(this.spectralAveragingLimits.min, this.spectralAveragingLimits.max)]],
    effectiveResolution: [this.resolutionDefaultValue, []],
    imageWeighting: [this.defaultImageWeighting, [Validators.required]],
    robust: [0, []],
    nSubbands: [1, [Validators.required, this.validatorService.validatorNumberMinMax(this.nSubbandsLimits.min, this.nSubbandsLimits.max)]]
  });

  ngAfterViewInit(): void {
    // Save the initial values for reset purposes.
    this.formInitialValues = this.continuumForm.value;
    this.setEffectiveResolution();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.updateValidators();
    if (changes['selectedSubarray']) {
      this.onSubarrayChange();
    }
  }

  paramChange() {
    this.updateValidators();
    this.output?.paramChange();
  }

  /**
   * Some of the field level validators depend on other fields, so need to be reset if that other field changes.
   *
   * This method handles all of those validator updates.
   */
  updateValidators() {
    this.continuumForm.get('continuumBandwidth')?.setValidators(
      this.validatorService.lowContinuumBandwidthValidator(
        this._selectedSubarray
      )
    );
    this.continuumForm.get('continuumBandwidth')!.updateValueAndValidity();

    const currentSpectralResolution = this.spectralResolutionkHz / 1000;
    const currentContinuumBandwidth = this.continuumForm.get('continuumBandwidth')?.value;
    const newSpectralAveragingMax = currentContinuumBandwidth / currentSpectralResolution / 2;
    this.spectralAveragingLimits.max = newSpectralAveragingMax;
    this.continuumForm.get('spectralAveraging')?.setValidators(
      this.validatorService.validatorNumberMinMax(1, Math.floor(newSpectralAveragingMax))
    );
    this.continuumForm.get('spectralAveraging')!.updateValueAndValidity();
  }

  /**
   * This method is called when the subarray changes in the parent component, through ngChanges
   *
   * It handles any behaviour that needs to happen as a result of that change, for example the default value
   * of the bandwidth changing to a valid default
   **/
  onSubarrayChange(): void {
    if (this.continuumForm.get('continuumBandwidth')?.touched) {
      // If the user has changed the bandwidth then goes back to change the subarray, we do not want to overwrite
      // their changes, even if that leaves the form in an invalid state with an error message
      return;
    }
    const defaultContBandwidthHz = this.validatorService.lowMaxContBandwidthHzForSubarray.get(this._selectedSubarray?.name as SkaOstSenscalcLowApiContinuumWeightingRequestParams["subarrayConfiguration"]);
    const defaultContBandwidthMHz = defaultContBandwidthHz
      ? defaultContBandwidthHz * 1.e-6
      : this.continuumBandwidthDefaultValue;
    this.continuumForm.get('continuumBandwidth')?.setValue(defaultContBandwidthMHz);
  }

  freqParamChange() {
    // Only validate Bandwidth if Frequency is valid
    if (this.centralFrequency?.valid) {
      this.continuumBandwidth?.markAsTouched();
      this.continuumBandwidth?.updateValueAndValidity();
    }
    else {
      // Reset bandwidth errors if frequency is invalid
      this.continuumBandwidth?.setErrors(null);
    }
    this.setEffectiveResolution();
    this.paramChange();
  }

  weightingChanged(): void {
    this.paramChange();
    this.showRobustness = this.imageWeighting?.value == "Briggs";
  }

  get integrationTime() {
    return this.continuumForm.get('integrationTime');
  }

  get centralFrequency() {
    return this.continuumForm.get('centralFrequency');
  }

  get continuumBandwidth() {
    return this.continuumForm.get('continuumBandwidth');
  }

  get spectralResolution() {
    return this.continuumForm.get('spectralResolution');
  }

  get spectralAveraging() {
    return this.continuumForm.get('spectralAveraging');
  }

  get imageWeighting() {
    return this.continuumForm.get('imageWeighting');
  }

  get robust() {
    return this.continuumForm.get('robust');
  }

  get nSubbands() {
    return this.continuumForm.get('nSubbands');
  }

  getErrorMessage(control: AbstractControl | null) {
    return this.validatorService.getErrorMessage(control);
  }

  resetForm() {
    this.continuumForm.reset(this.formInitialValues);
    this.continuumForm.markAsUntouched();
    this.imageWeighting?.setValue(this.defaultImageWeighting);
    this.weightingChanged();
    this.output?.resetForm();
  }

  clearResultPanel() {
    this.loadingResults(false);
  }

  loadingResults(show: boolean) {
    this.output.loadingResults(show);
  }

  getFormData(): LowContinuumInput {
    const data: LowContinuumInput = {
      duration: this.integrationTime!.value,
      frequency: this.centralFrequency!.value,
      bandwidth: this.continuumBandwidth!.value,
      averaging: this.spectralAveraging!.value,
      imageWeighting: this.imageWeighting?.value.toLowerCase(),
      spectralMode: this.spectralMode,
      nSubbands: this.nSubbands?.value
    };
    if (this.showRobustness) {
      data.robust = this.robust!.value;
    }
    return data;
  }

  disableImageWeighting() {
    // In custom array mode, weighting should be set to natural and disabled as we can't calculate the weighting factors
    this.imageWeighting?.setValue('Natural');
    this.weightingChanged();
    this.imageWeighting?.disable();
  }

  enableImageWeighting() {
    this.imageWeighting?.enable();
    this.weightingChanged();
  }

  spectralAveragingChanged(): void {
    this.output.paramChange();
    this.spectralAveraging?.markAsTouched();
    this.setEffectiveResolution();
  }

  setEffectiveResolution(): void {
    const spectralAverage = this.continuumForm.get('spectralAveraging')!.value;
    const centralFreq = this.validatorService.getScaledValue(this.continuumForm.get('centralFrequency')!.value, 1000000, "*");
    const velocity = '(' + helpers.calculate.calculateVelocity(this.spectralResolutionkHz * spectralAverage * 1000, centralFreq) + ')';
    this.continuumForm.get('effectiveResolution')?.setValue(`${(spectralAverage * this.spectralResolutionkHz).toFixed(2)} kHz ${velocity}`);
  }

  getSpectralResolutionVelocityDisplayValue(): string {
    const centralFreq = this.validatorService.getScaledValue(this.continuumForm.get('centralFrequency')!.value, 1000000, "*");
    return '(' + helpers.calculate.calculateVelocity(this.spectralResolutionkHz * 1000, centralFreq) + ')';
  }
}
