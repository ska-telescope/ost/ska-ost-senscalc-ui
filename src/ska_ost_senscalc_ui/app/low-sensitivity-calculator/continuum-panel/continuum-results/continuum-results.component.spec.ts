import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from "@angular/material/card";

import { ContinuumResultsComponent } from './continuum-results.component';
import { AppConfiguration } from '../../../app-configuration';
import config from '../../../../assets/configuration.json';

describe('ContinuumResultsComponent', () => {
  let component: ContinuumResultsComponent;
  let fixture: ComponentFixture<ContinuumResultsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ContinuumResultsComponent
      ],
      imports: [
        MatCardModule
      ],
      providers: [
        { provide: AppConfiguration, useValue: { getConfig: () => config } }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContinuumResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show that the results message are loading', () => {
    component.loadingResults(true);
    expect(component.resultsHeaderMsg).toBe('Calculating Results...');
  });

  it('should display param change', () => {
    component.lowContinuumResults = {warnings: {}};
    component.parametersChanged = false;
    component.paramChange();
    expect(component.parametersChanged).toBe(true);
  });

});
