import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import helpers from "../../../shared/utils/helpers";
import constants from "../../../shared/utils/constants";
import {ContinuumResults} from "../../../core/types";

@Component({
  selector: 'app-continuum-results',
  templateUrl: './continuum-results.component.html',
  styleUrls: ['./continuum-results.component.scss']
})
export class ContinuumResultsComponent implements OnInit, OnChanges {

  @Input() nSubbands!: number;
  @Input() lowContinuumResults?: ContinuumResults | null;

  differentIntegrationTimeMessage = constants.messages.differentIntegrationTime;

  resultsHeaderMsg!: string;
  parametersChanged!: boolean; // true if the input parameters have changed since the last calculation results shown

  constructor() {}

  ngOnInit(): void {
    this.resetControlVariables();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['lowContinuumResults']) {
      this.resetControlVariables();
    }
  }

  resetForm() {
    this.lowContinuumResults = undefined;
    this.resetControlVariables();
  }

  resetControlVariables() {
    this.parametersChanged = false;
    this.loadingResults(false);
  }
  paramChange() {
    // Only want to warn after a calculation has been made and not
    // while user is entering at the start of a new calculation.
    if (this.lowContinuumResults) {
      this.parametersChanged = true;
    }
  }

  loadingResults(loading: boolean): void {
    this.resultsHeaderMsg = helpers.result.header(loading);
  }

  sensLimitWarning() {
    return helpers.messages.sensLimitWarning();
  }
}
