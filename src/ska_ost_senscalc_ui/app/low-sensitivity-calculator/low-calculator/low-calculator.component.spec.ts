import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LowCalculatorComponent } from './low-calculator.component';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { HarnessLoader } from '@angular/cdk/testing';
import * as config from '../../../assets/configuration.json';
import {HttpClientModule, HttpErrorResponse} from '@angular/common/http';
import { MatInputModule } from '@angular/material/input';
import {
  FormsModule,
  ReactiveFormsModule,
  UntypedFormBuilder
} from '@angular/forms';
import { ValidatorsService } from '../../core/validators.service';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputHarness } from '@angular/material/input/testing';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { LowContinuumInput, LowFormInput } from '../types';
import { MatFormFieldHarness } from '@angular/material/form-field/testing';
import { MatExpansionPanelHarness } from "@angular/material/expansion/testing";
import { MatButtonHarness } from '@angular/material/button/testing';
import {Component, Input} from '@angular/core';
import {BehaviorSubject, throwError} from 'rxjs';
import { AppConfiguration } from '../../app-configuration';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSelectModule } from '@angular/material/select';
import {
  LowSubarraysResponse
} from '../../generated/api/low';
import { LowSubarrayService } from '../../core/low-subarray.service';
import { MatSelectHarness } from '@angular/material/select/testing';
import {MatSnackBarHarness} from "@angular/material/snack-bar/testing";
import {ContinuumPanelComponent} from "../continuum-panel/continuum-panel.component";
import {ZoomPanelComponent} from "../zoom-panel/zoom-panel.component";
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { PssPanelComponent } from '../pss-panel/pss-panel.component';

describe('LowCalculatorComponent', () => {
  let fixture: ComponentFixture<LowCalculatorComponent>;
  let component: LowCalculatorComponent;
  let loader: HarnessLoader;
  let mockLowApi: MockLowQueryService;

  const getFormInputHarness = async (
    inputName: string
  ): Promise<MatInputHarness> => {
    // Allow asterix in label in case the field is mandatory
    const inputRegEx = new RegExp(inputName + '( *)');

    const control = await loader.getHarness(
      MatFormFieldHarness.with({ floatingLabelText: inputRegEx })
    );
    return (await control.getControl()) as MatInputHarness;
  };

  const openContinuumPanel = async () => {
    const continuumAccordion = await loader.getHarness(
      MatExpansionPanelHarness.with({title: "Continuum"})
    );
    await continuumAccordion.expand();
  };

  const openZoomPanel = async () => {
    const zoomAccordion = await loader.getHarness(
      MatExpansionPanelHarness.with({title: "Zoom Window"})
    );
    await zoomAccordion.expand();
  };

  const openPssPanel = async () => {
    const pssAccordion = await loader.getHarness(
      MatExpansionPanelHarness.with({title: "Pulsar Search (PSS)"})
    );
    await pssAccordion.expand();
  };

  const setValidForm = async () => {
    // Set a fake subarray response object so that form
    // can be considered valid
    const subarrays: LowSubarraysResponse[] = [
      {
        name: 'foo',
        label: 'bar',
        n_stations: 123
      }
    ];
    mockLowApi.subarrays$.next(subarrays);

    const stnInput = await getFormInputHarness('Number of Stations');
    await stnInput.setValue('5');

    const decInput = await getFormInputHarness('Declination');
    await decInput.setValue('-43:01:09.00');

    const raInput = await getFormInputHarness('Right Ascension');
    await raInput.setValue('13:25:27.60');
  };

  beforeEach(async () => {
    mockLowApi = new MockLowQueryService();

    await TestBed.configureTestingModule({
      declarations: [
        LowCalculatorComponent,
        MockLowCalcContPanelComponent,
        MockZoomPanelComponent,
        MockPssPanelComponent],
      imports: [
        NoopAnimationsModule,
        FormsModule,
        HttpClientModule,
        MatCardModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatSnackBarModule,
        MatSlideToggleModule,
        ReactiveFormsModule
      ],
      providers: [
        AppConfiguration,
        UntypedFormBuilder,
        ValidatorsService,
        MatSnackBar,
        { provide: LowSubarrayService, useValue: mockLowApi }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(LowCalculatorComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    fixture.componentInstance.configuration = config;
  });

  beforeEach(() => {
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a required Number of stations field', async () => {
    const stnInput = await getFormInputHarness('Number of Stations');
    expect(stnInput.isRequired()).toBeTruthy();
  });

  it('should have required coordinate fields', async () => {
    const decInput = await getFormInputHarness('Declination');
    const raInput = await getFormInputHarness('Right Ascension');
    expect(decInput.isRequired()).toBeTruthy();
    expect(raInput.isRequired()).toBeTruthy();
  });

  it('should set all fields to defaults or empty on reset ', async () => {
    const stnInput = await getFormInputHarness('Number of Stations');
    await stnInput.setValue('5');

    const decInput = await getFormInputHarness('Declination');
    await decInput.setValue('-43:01:09.00');

    const raInput = await getFormInputHarness('Right Ascension');
    await raInput.setValue('13:25:27.60');

    const minElevationInput = await getFormInputHarness('Minimum Elevation');
    await raInput.setValue('21');

    const resetButton = await loader.getHarness(
      MatButtonHarness.with({ text: 'Reset' })
    );
    await resetButton.click();

    expect(await stnInput.getValue()).toBe('512');
    expect(await raInput.getValue()).toBe('00:00:00.0');
    expect(await decInput.getValue()).toBe('00:00:00.0');
    expect(await minElevationInput.getValue()).toBe('20');
  });

  it.each([
    ['Number of Stations', '600'],
    ['Declination', '90:00'],
    ['Right Ascension', '-10:00:00'],
    ['Minimum Elevation', '10']
  ])(
    'should disable submit button on invalid value for %s',
    async (fieldName: string, invalidInput: string) => {
      // This test only needs to set common inputs, validity of continuum form will be ignored
      // it has not been defined in the component.
      await openContinuumPanel();
      await setValidForm();

      const submitButton = await loader.getHarness(
        MatButtonHarness.with({ text: 'Calculate' })
      );

      // All inputs should be valid so Calculate button should be enabled
      expect(await submitButton.isDisabled()).toBeFalsy();

      const invalidInputHarness = await getFormInputHarness(fieldName);
      component.onSelectSubarray({
        name: 'Custom',
        label: 'Custom',
        n_stations: 512
      });
      await invalidInputHarness.setValue(invalidInput);

      expect(await submitButton.isDisabled()).toBeTruthy();
    }
  );

  it.each([
    ['Declination', '80:00:00', 'Target does not reach minimum elevation of 15 degrees'],
    ['Declination', '4700:00', 'Input formatted incorrectly'],
    ['Right Ascension', '0:0:0:0:0', 'Input formatted incorrectly'],
    ['Minimum Elevation', '14', 'Please enter a number between 15 and 63.1'],
    ['Minimum Elevation', '80', 'Please enter a number between 15 and 63.1']
  ])(
    'should display validation errors for %s',
    async (fieldName: string, invalidInput: string, errorMessage: string) => {
      const invalidInputHarness = await getFormInputHarness(fieldName);
      await invalidInputHarness.setValue(invalidInput);
      await invalidInputHarness.blur();

      const control = await loader.getHarness(
        MatFormFieldHarness.with({
          floatingLabelText: new RegExp(fieldName + '( *)')
        })
      );
      expect(await control.hasErrors()).toBeTruthy();
      const errors = await control.getTextErrors();
      expect(errors[0]).toBe(errorMessage);
    }
  );

  describe('When the declination changes', () => {
    it('revalidates the elevation', async () => {
      await setValidForm();

      const control = await loader.getHarness(
        MatFormFieldHarness.with({
          floatingLabelText: 'Minimum Elevation *'
        })
      );
      expect(await control.hasErrors()).toBeFalsy();

      // set dec to a value that invalidates the default elevation
      const decInput = await getFormInputHarness('Declination');
      await decInput.setValue('45:00:00.00');

      expect(await control.hasErrors()).toBeTruthy();
    });
  });

  describe('When the page loads', () => {
    it('displays an error modal if the backend query fails', async () => {
      // disable snackbar duration. See https://github.com/angular/components/issues/19290
      component["snackbarDuration"] = -1;

      mockLowApi.subQueryFailed$.next(true);

      // use the root loader to get the harnesses for the entire document
      // because the snack bar is rendered outside of the component
      const rootLoader = TestbedHarnessEnvironment.documentRootLoader(fixture);

      const dialog = await rootLoader.getHarness<MatSnackBarHarness>(MatSnackBarHarness);
      expect(await dialog.getMessage()).toMatch(/Communication error/i);
    });
  });

  describe('When the subarray selector is presented', () => {
    let selectHarness: MatSelectHarness;
    let resetButton: MatButtonHarness;

    beforeEach(async () => {
      selectHarness = await loader.getHarness<MatSelectHarness>(
        MatSelectHarness
      );
      resetButton = await loader.getHarness(
        MatButtonHarness.with({ text: 'Reset' })
      );
    });

    it('presents subarrays by label', async () => {
      const subarrays: LowSubarraysResponse[] = [
        {
          name: 'foo',
          label: 'bar',
          n_stations: 123
        }
      ];
      mockLowApi.subarrays$.next(subarrays);

      await selectHarness.open();
      const options = await selectHarness.getOptions();
      // The Promise.all(iterable) method returns a promise that resolves when
      // all of the promises in the iterable argument have resolved, or rejects
      // with the reason of the first passed promise that rejects.
      const labels = await Promise.all(options.map(async (o) => o.getText()));
      expect(labels).toEqual(expect.arrayContaining(['bar']));
    });

    it('selects the AA4 subarray by default', async () => {
      mockLowApi.subarrays$.next([
        {
          name: 'name 1',
          label: 'label 1',
          n_stations: 10
        },
        {
          name: 'name 2',
          label: 'AA4',
          n_stations: 20
        },
        {
          name: 'name 3',
          label: 'label 3',
          n_stations: 30
        }
      ]);

      const selected = await selectHarness.getValueText();
      expect(selected).toEqual('AA4');
    });

    it.each([
      {
        name: 'LOW_AA05_all',
        label: 'label 1',
        n_stations: 10
      },
      {
        name: 'LOW_AA1_all',
        label: 'label 1',
        n_stations: 10
      }])('disables zoom mode for the subarray', (subarray) => {
      mockLowApi.subarrays$.next([subarray]);

      expect(component.zoomModeDisabled).toBeTruthy();
      expect(component.panelZoomOpened).toBeFalsy();
    });

    it('resets to the AA4 subarray when AA4 is available', async () => {
      await setValidForm();

      // Overwrite the subarrays set in the 'setValidForm' function
      mockLowApi.subarrays$.next([
        {
          name: 'val 1',
          label: 'label 1',
          n_stations: 10
        },
        {
          name: 'val 2',
          label: 'AA4',
          n_stations: 20
        },
        {
          name: 'val 3',
          label: 'label 3',
          n_stations: 30
        }
      ]);

      await selectHarness.clickOptions({ text: 'label 1' });
      expect(await selectHarness.getValueText()).toBe('label 1');

      expect(await resetButton.isDisabled()).toBe(false);
      await resetButton.click();
      expect(await selectHarness.getValueText()).toBe('AA4');
    });

    it('is disabled if the backend query fails', async () => {
      mockLowApi.subQueryFailed$.next(true);
      expect(await selectHarness.isDisabled()).toBe(true);
    });

    it('still allows the calculate button to be clicked if query fails', async () => {
      mockLowApi.subQueryFailed$.next(true);

      await openContinuumPanel();

      const stnInput = await getFormInputHarness('Number of Stations');
      await stnInput.setValue('5');

      const decInput = await getFormInputHarness('Declination');
      await decInput.setValue('-43:01:09.00');

      const raInput = await getFormInputHarness('Right Ascension');
      await raInput.setValue('13:25:27.60');

      const submitButton = await loader.getHarness(
        MatButtonHarness.with({ text: 'Calculate' })
      );

      // All inputs should be valid so Calculate button should be enabled
      expect(await submitButton.isDisabled()).toBeFalsy();
    }, 10000);

    it('is enabled if the backend query succeeds', async () => {
      mockLowApi.subQueryFailed$.next(false);
      expect(await selectHarness.isDisabled()).toBe(false);
    });

    it('leaves reset disabled after the subarray options are initialised', async () => {
      mockLowApi.subarrays$.next([
        {
          name: '_',
          label: '_',
          n_stations: 123
        }
      ]);
      expect(await resetButton.isDisabled()).toBe(true);
    });

    it('sets the number of stations when a subarray is selected', async () => {
      mockLowApi.subarrays$.next([
        {
          name: '_',
          label: '_',
          n_stations: 123
        }
      ]);

      const fieldName = 'Number of Stations';
      const numStationsComponent = await getFormInputHarness(fieldName);
      const numStations = await numStationsComponent.getValue();
      expect(numStations).toEqual('123');
    });

  });

  it('should display min/max error for Number of Stations', async () => {
    const message = 'Please enter a number between 2 and 512';
    const fieldName = 'Number of Stations';
    const invalidInputHarness = await getFormInputHarness(fieldName);
    await invalidInputHarness.setValue('0');
    await invalidInputHarness.blur();

    const control = await loader.getHarness(
      MatFormFieldHarness.with({
        floatingLabelText: new RegExp(fieldName + '( *)')
      })
    );
    expect(await control.hasErrors()).toBeTruthy();
    let errors = await control.getTextErrors();
    expect(errors[0]).toBe(message);

    // Set back to valid value and check errors are cleared
    await invalidInputHarness.setValue('2');
    await invalidInputHarness.blur();
    expect(await control.hasErrors()).toBeFalsy();

    await invalidInputHarness.setValue('600');

    expect(await control.hasErrors()).toBeTruthy();
    errors = await control.getTextErrors();
    expect(errors[0]).toBe(message);
  });

  it('should display HTTP Errors from backend', async () => {
    // Set Low backend to throw an error
    jest
      .spyOn(
        (component as any).calculationService,
        'calculateContinuumResult'
      )
      .mockReturnValue(
        throwError(() => new HttpErrorResponse({ error: { detail: 'foo' } }))
      );

    jest
      .spyOn(
        (component as any).calculationService,
        'calculateZoomResult'
      )
      .mockReturnValue(
        throwError(() => new HttpErrorResponse({ error: { detail: 'foo' } }))
      );

    jest
      .spyOn(
        (component as any).calculationService,
        'calculatePssResult'
      )
      .mockReturnValue(
        throwError(() => new HttpErrorResponse({ error: { detail: 'foo' } }))
      );

    const errorMock = jest.spyOn(component, 'showErrorModal');

    await openContinuumPanel();
    await openZoomPanel();
    await openPssPanel();
    await setValidForm();

    const submitButton = await loader.getHarness(
      MatButtonHarness.with({ text: 'Calculate' })
    );
    await submitButton.click();

    expect(errorMock).toHaveBeenCalledWith('foo');
  }, 50000);

  it('onRaDecBlur format period checking', async () => {
    const decInput = await getFormInputHarness('Declination');
    await decInput.setValue('00:00:00');
    component.onRaDecBlur('dec');
    expect(await decInput.getValue()).toBe('00:00:00.0');

    await decInput.setValue('00:00:00.');
    component.onRaDecBlur('dec');
    expect(await decInput.getValue()).toBe('00:00:00.0');

    const raInput = await getFormInputHarness('Right Ascension');
    await raInput.setValue('00:00:00');
    component.onRaDecBlur('ra');
    expect(await raInput.getValue()).toBe('00:00:00.0');

    await raInput.setValue('00:00:00.');
    component.onRaDecBlur('ra');
    expect(await raInput.getValue()).toBe('00:00:00.0');
  });

  it('onSexDecChange toggle change', async () => {
    const decInput = await getFormInputHarness('Declination');
    const raInput = await getFormInputHarness('Right Ascension');

    component.onSexDecChange({ checked: true });
    expect(await raInput.getValue()).toBe('0.0');
    expect(await decInput.getValue()).toBe('0.0');

    component.onSexDecChange({ checked: false });
    expect(await raInput.getValue()).toBe('00:00:00.0');
    expect(await decInput.getValue()).toBe('00:00:00.0');
  });

  describe('compareEnteredIntegrationTimes', () => {
    it('should return false if no accordions are open', () => {
      expect(component.compareEnteredIntegrationTimes()).toBe(false);
    });

    it('should return false if only the zoom accordion is open', async () => {
      const zoomPanel = await loader.getHarness(
        MatExpansionPanelHarness.with({selector: "#zoomExpansionPanel"})
      );
      await zoomPanel.expand();
      expect(component.compareEnteredIntegrationTimes()).toBe(false);
    });

    it('should return false if only the continuum accordion is open', async () => {
      const contPanel = await loader.getHarness(
        MatExpansionPanelHarness.with({selector: "#continuumExpansionPanel"})
      );
      await contPanel.expand();
      expect(component.compareEnteredIntegrationTimes()).toBe(false);
    });

    it('should return false if both accordions are open and have the same integration times', async () => {
      const zoomPanel = await loader.getHarness(
        MatExpansionPanelHarness.with({selector: "#zoomExpansionPanel"})
      );
      await zoomPanel.expand();
      const contPanel = await loader.getHarness(
        MatExpansionPanelHarness.with({selector: "#continuumExpansionPanel"})
      );
      await contPanel.expand();
      expect(component.compareEnteredIntegrationTimes()).toBe(false);
    });

    it('should return true if both accordions are open and have different integration times', async () => {
      const zoomPanel = await loader.getHarness(
        MatExpansionPanelHarness.with({selector: "#zoomExpansionPanel"})
      );
      await zoomPanel.expand();
      const contPanel = await loader.getHarness(
        MatExpansionPanelHarness.with({selector: "#continuumExpansionPanel"})
      );
      await contPanel.expand();

      jest
        .spyOn(
          (component as any).contPanel,
          'getFormData'
        )
        .mockReturnValue({duration: 2,frequency: 100,bandwidth: 10} as LowContinuumInput);

      expect(component.compareEnteredIntegrationTimes()).toBe(true);
    });
  });
});

@Component({
  selector: 'app-continuum-panel',
  template: '',
  providers: [
    {
      provide: ContinuumPanelComponent,
      useClass: MockLowCalcContPanelComponent
    }
  ]
})

class MockLowCalcContPanelComponent {
  @Input() selectedSubarray: LowSubarraysResponse = {
    name: 'foo',
    label: 'bar',
    n_stations: 123
  };
  getFormData() {
    return {duration: 1,frequency: 100,bandwidth: 10} as LowContinuumInput;
  }
  clearResultPanel() { }
  loadingResultPanel(show: boolean) {
    show;
  }
  displayResult(response: any): void {
    response;
  }

  displayResultWithoutWeighting(response: any): void {
    response;
  }

  paramChange() {}

  resetForm() {}

  disableImageWeighting() {}

  enableImageWeighting() {}

  loadingResults() {}
}

@Component({
  selector: 'app-zoom-panel',
  template: '',
  providers: [
    {
      provide: ZoomPanelComponent,
      useClass: MockZoomPanelComponent
    }
  ]
})
class MockZoomPanelComponent extends MockLowCalcContPanelComponent {

}

@Component({
  selector: 'app-pss-panel',
  template: '',
  providers: [
    {
      provide: PssPanelComponent,
      useClass: MockPssPanelComponent
    }
  ]
})
class MockPssPanelComponent extends MockLowCalcContPanelComponent {

}

class MockLowQueryService {
  subarrays$ = new BehaviorSubject<Array<LowSubarraysResponse>>([]);

  subQueryFailed$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  calculateSensitivity(input: LowFormInput): void {
    input;
  }

  calculateZoomSensitivity(input: LowFormInput): void {
    input;
  }

  calculateZoomWeighting(input: LowFormInput): void {
    input;
  }

  calculateContinuumWeighting(input: LowFormInput): void {
    input;
  }
}
