import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { AppConfiguration, PulsarSearchArrayValue } from '../../app-configuration';
import { MidCalculatorService } from '../../core/mid-calculator.service';
import { AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { ValidatorsService } from '../../core/validators.service';
import {LowCommonInput, LowFormInput} from '../types';
import {
  MatSnackBar, MatSnackBarConfig,
  MatSnackBarRef,
  TextOnlySnackBar
} from '@angular/material/snack-bar';
import { LowSubarrayService } from '../../core/low-subarray.service';
import { HttpErrorResponse } from '@angular/common/http';
import {catchError, Observable, of, Subject, takeUntil, tap} from 'rxjs';
import { LowSubarraysResponse } from '../../generated/api/low';
import {ContinuumPanelComponent} from "../continuum-panel/continuum-panel.component";
import {ZoomPanelComponent} from "../zoom-panel/zoom-panel.component";
import {PssPanelComponent} from "../pss-panel/pss-panel.component";
import helpers from '../../shared/utils/helpers';
import { MatSlideToggle } from '@angular/material/slide-toggle';
import {LowCalculationService} from "../../core/low-calculation.service";
import {ContinuumResults, PssResults, ZoomResults} from "../../core/types";

@Component({
  selector: 'app-low-calculator',
  templateUrl: './low-calculator.component.html',
  styleUrls: ['./low-calculator.component.scss']
})
export class LowCalculatorComponent
  implements OnInit, AfterViewInit, OnDestroy
{

  continnumResults$?: Observable<ContinuumResults>;
  zoomResults$?: Observable<ZoomResults>;
  pssResults$?: Observable<PssResults>;

  @ViewChild(ContinuumPanelComponent)
  contPanel!: ContinuumPanelComponent;
  @ViewChild(ZoomPanelComponent)
  zoomPanel!: ZoomPanelComponent;
  @ViewChild(PssPanelComponent)
  pssPanel!: PssPanelComponent;
  @ViewChild('sexDecToggle') sexDecToggle!: MatSlideToggle;
  toggleSexDecMode = false; // To control coordinate 'dec' postscript
  isPanelDisabled = true;
  raPlaceholder!: string;
  raTitle!: string;
  decPlaceholder!: string;
  decTitle!: string;
  pssPanelDisabled = true;

  // Zoom is only allowed for certain subarrays
  zoomModeDisabled = false;

  selectedSubarrayy!: LowSubarraysResponse;

  configuration = this.config.getConfig();

  pssArrays! : PulsarSearchArrayValue[];

  formInitialValues: any;

  lowForm = this.fb.group({
    subarray: this.fb.control<LowSubarraysResponse | null>(
      { value: null, disabled: true },
      []
    ),
    numStations: this.fb.control(512, [
      Validators.required,
      this.validatorService.validatorNumberMinMax(2, 512)
    ]),
    coordinates: this.fb.group({
      ra: this.fb.control<string | null>(
        "00:00:00.0",
        [
          Validators.required,
          this.validatorService.validatorRightAscension(false)
        ]
      ),
      dec: this.fb.control<string | null>(
        "00:00:00.0",
        [
          Validators.required,
          this.validatorService.validatorDeclination("Low", false)
        ]
      ),
      minElevation: this.fb.control(20, [
        Validators.required,
        this.validatorService.validatorElevationLow(15, "dec")
      ])
    })
  });

  snackbar!: MatSnackBarRef<TextOnlySnackBar>;
  panelContinuumOpened!: boolean;
  panelZoomOpened!: boolean;
  panelPssOpened!: boolean;

  private componentIsDestroyed$ = new Subject<boolean>();

  // made a property so that tests can override it as dur, setting it to zero
  // See https://github.com/angular/components/issues/19290
  private snackbarDuration = 5000;

  subarrays: LowSubarraysResponse[] = [];
  static backendFailureErrorMessage= "Communication error: subarray templates will not be available.";

  constructor(
    private validatorService: ValidatorsService,
    private config: AppConfiguration,
    private service: MidCalculatorService,
    private apiService: LowSubarrayService,
    private calculationService: LowCalculationService,
    private snackBar: MatSnackBar,
    private fb: FormBuilder
  ) {
  }

  ngAfterViewInit(): void {
    this.formInitialValues = this.lowForm.value;
    this.pssArrays = this.configuration.pulsarSearchArrays;

    // Validate elevation field if dec changes and is valid
    this.dec?.valueChanges
      .pipe(takeUntil(this.componentIsDestroyed$))
      .subscribe(() => {
        if (this.dec?.valid) {
          this.minElevation?.markAsTouched();
          this.minElevation?.updateValueAndValidity();
          this.minElevation!.enable();
        } else {
          this.minElevation!.disable();
        }
      });
  }

  paramChange() {
    // Add param update message to all relevant result panels.
    this.contPanel?.paramChange();
    this.zoomPanel?.paramChange();
    this.pssPanel?.paramChange();
  }

  get numStations() {
    return this.lowForm.get('numStations');
  }

  get ra() {
    return this.lowForm.get('coordinates.ra');
  }

  get dec() {
    return this.lowForm.get('coordinates.dec');
  }

  get subarray() {
    return this.lowForm.get('subarray');
  }

  get minElevation() {
    return this.lowForm.get('coordinates.minElevation');
  }

  resetRaDec() {
    this.toggleSexDecMode = false;
    this.sexDecToggle.checked = false;
    const raConfig = this.configuration.midCalcFormFields?.find((e: any) => e.field == 'fieldRightAscension');
    this.ra?.setValue(raConfig!.defaultValue);
    this.raPlaceholder = raConfig!.placeholder!;
    this.raTitle = raConfig!.description!;
    this.ra?.setValidators([this.validatorService.validatorRightAscension(false), Validators.required]);
    this.ra?.updateValueAndValidity();

    const decConfig = this.configuration.midCalcFormFields?.find((e: any) => e.field == 'fieldDeclination');
    this.dec?.setValue(decConfig!.defaultValue);
    this.decPlaceholder = decConfig!.placeholder!;
    this.decTitle = decConfig!.description!;
    this.dec?.setValidators([this.validatorService.validatorDeclination("Low", false), Validators.required]);
    this.dec?.updateValueAndValidity();
  }

  /**
   * This clears down the entire form
   */
  onReset() {
    this.resetRaDec(); // Reset radec first or defaults are lost.

    this.lowForm.reset(this.formInitialValues);
    this.initialiseSubarray();
    this.lowForm.markAsUntouched();

    this.contPanel?.resetForm();
    this.zoomPanel?.resetForm();
    this.pssPanel?.resetForm();
  }

  /**
   * This clears down child forms.
   */
  clearResults() {
    this.contPanel?.clearResultPanel();
    this.zoomPanel?.clearResultPanel();
    this.pssPanel?.clearResultPanel();
  }

  showErrorModal(errorMessage: string) {
    const config: MatSnackBarConfig = {
      politeness: 'assertive',
      verticalPosition: 'top',
      panelClass: ['snackbar-error']
    };

    if (this.snackbarDuration >= 0) {
      config.duration = this.snackbarDuration;
    }

    this.snackbar = this.snackBar.open(errorMessage, undefined, config);
  }

  getErrorMessage(control: AbstractControl | null) {
    return this.validatorService.getErrorMessage(control);
  }

  isCalculateBtnEnabled(): boolean {
    // The button should be enabled if the common form plus all the open accordions are valid, and at least one accordion should be open
    const contPanelInvalid = this.panelContinuumOpened && this.contPanel && this.contPanel.continuumForm && !this.contPanel.continuumForm.valid;
    const zoomPanelInvalid = this.panelZoomOpened && this.zoomPanel && this.zoomPanel.zoomForm && !this.zoomPanel.zoomForm.valid;
    const pssPanelInvalid = this.panelPssOpened && this.pssPanel && this.pssPanel.pssForm && !this.pssPanel.pssForm.valid;

    const anyPanelOpened = this.panelContinuumOpened || this.panelZoomOpened || this.panelPssOpened;

    return this.lowForm.valid && !contPanelInvalid && !zoomPanelInvalid && !pssPanelInvalid && anyPanelOpened;
  }

  isResetBtnEnabled(): boolean {
    const contPanelDirty = this.contPanel?.continuumForm?.dirty;
    const zoomPanelDirty = this.zoomPanel?.zoomForm?.dirty;
    const pssPanelDirty = this.pssPanel?.pssForm?.dirty;

    return this.lowForm.dirty || contPanelDirty || zoomPanelDirty || pssPanelDirty;
  }

  ngOnInit() {
    this.service.setConfig(this.configuration);
    // Set up the dynamic coordinate control properties.
    const rightAscensionConfig = this.configuration.midCalcFormFields?.find((e: any) => e.field == 'fieldRightAscension');
    const declinationConfig = this.configuration.midCalcFormFields?.find((e: any) => e.field == 'fieldDeclination');

    this.raPlaceholder = rightAscensionConfig?.placeholder || '';
    this.raTitle = rightAscensionConfig?.description || '';
    this.decPlaceholder = declinationConfig?.placeholder || '';
    this.decTitle = declinationConfig?.description || '';

    this.validatorService.setConfig(this.configuration);
    this.panelContinuumOpened = false;
    this.panelZoomOpened = false;
    this.panelPssOpened = false;
    this.getSubarrayData();
  }

  ngOnDestroy() {
    this.dispose();
  }

  getSubarrayData() {
    // as the subarray query is resolved, set the selected subarray to
    // AA4 if present, otherwise the last of the items
    this.apiService.subarrays$
      .pipe(takeUntil(this.componentIsDestroyed$))
      .subscribe((subarrays) => {
        this.subarrays = subarrays;
        this.initialiseSubarray();
      });

    // disable subarray selection if subarray query failed
    this.apiService.subQueryFailed$
      .pipe(takeUntil(this.componentIsDestroyed$))
      .subscribe((failed) => {
        if (!failed) {
          this.lowForm.controls.subarray.enable();
        } else {
          this.lowForm.controls.subarray.disable();
          this.showErrorModal(
            LowCalculatorComponent.backendFailureErrorMessage
          );
        }
      });
  }

  initialiseSubarray() {
    const subarrays = this.subarrays;
    // subarray query succeeded but no subarrays returned?
    if (subarrays.length < 1) {
      return;
    }

    // Put the array AA* in its correct position.
    const aa_star = subarrays.findIndex((a) => a.name === 'LOW_AAstar_all');
    const aa_core_star = subarrays.findIndex((a) => a.name === 'LOW_AAstar_core_only');
    const aa_core_two = subarrays.findIndex((a) => a.name === 'LOW_AA2_core_only') + 1;

    if(aa_star >= 0  && aa_core_star >= 0  && aa_core_two >= 0)
    {
      subarrays.splice(aa_core_two,0,subarrays[aa_star]);
      subarrays.splice(aa_core_two + 1,0,subarrays[aa_core_star]);
      subarrays.splice(aa_star,2);
    }

    // Set the default array on view
    const nAA4 = subarrays.findIndex((element) => element.label == 'AA4');
    const defaultSubarray = subarrays[nAA4 == -1 ? subarrays.length - 1 : nAA4];
    this.onSelectSubarray(defaultSubarray);
    this.lowForm.controls.subarray.markAsUntouched();
  }

  onSelectSubarray(selected: LowSubarraysResponse) {
    this.selectedSubarrayy = selected;
    this.checkIfZoomModeDisabled(selected);
    const nStations = selected.n_stations;
    this.lowForm.patchValue({
      subarray: selected,
      numStations: nStations
    });

    if (selected.name == 'Custom') {
      this.numStations?.enable();
      this.contPanel?.disableImageWeighting();
      this.zoomPanel?.disableImageWeighting();
    } else {
      this.numStations?.disable();
      this.contPanel?.enableImageWeighting();
      this.zoomPanel?.enableImageWeighting();
    }

    this.lowForm.controls.numStations.updateValueAndValidity();
    this.pssPanelDisabled = this.pssAvailable(this.subarray);
  }

  pssAvailable(subarray: AbstractControl<LowSubarraysResponse | null, LowSubarraysResponse | null> | null): boolean {
    const arrayAllowed = this.pssArrays.some((item: { label: string | undefined; }) => item.label === subarray?.value?.label);
    this.panelPssOpened ? this.panelPssOpened = arrayAllowed : null;
    return !arrayAllowed;
  }

  onSubmit() {
    this.lowForm.updateValueAndValidity();
    if (!this.lowForm.valid) {
      this.showErrorModal('Form is invalid');
      return;
    }
    if (!this.panelContinuumOpened && !this.panelZoomOpened && !this.panelPssOpened) {
      this.showErrorModal('At least once panel must be opened');
      return;
    }

    const lowCommonData: LowCommonInput = {
      num_stations: this.numStations!.value!,
      subarray: this.subarray?.value!.name as LowCommonInput['subarray'],
      ra_str: this.sexDecToggle.checked ? helpers.format.degRaToSexagesimal(this.ra!.value!) : this.ra!.value!,
      dec_str: this.sexDecToggle.checked ? helpers.format.degDecToSexagesimal(this.dec!.value!) : this.dec!.value!,
      min_elevation: this.minElevation?.value ?? 20
    };

    const lowFormInput = {
      common: lowCommonData
    } as LowFormInput;

    if (this.panelContinuumOpened) {
      lowFormInput.continuum = this.contPanel.getFormData();
    }

    if (this.panelZoomOpened) {
      lowFormInput.zoom = this.zoomPanel.getFormData();
    }

    if (this.panelPssOpened) {
      lowFormInput.pss = this.pssPanel.getFormData();
    }

    this.calculateAndDisplay(lowFormInput);

  }

  private calculateAndDisplay(lowFormInput: LowFormInput) {
    // Start the loading indicator in all the output panels for user feedback.
    this.loadingContinuumResult(true);
    this.loadingZoomResult(true);
    this.loadingPssResult(true);
    // TODO this doesn't need to be passed through all the calculate methods. Refactor to just give to the child components
    const integrationTimesDifferent = this.compareEnteredIntegrationTimes();

     if (lowFormInput.continuum) {
      this.continnumResults$ = this.calculationService.calculateContinuumResult(lowFormInput, integrationTimesDifferent)
        .pipe(
          tap(() => this.loadingContinuumResult(false)),
          catchError((error) => this.errorRoute(error))
        );
     }

    if (lowFormInput.zoom) {
      this.zoomResults$ = this.calculationService.calculateZoomResult(lowFormInput, integrationTimesDifferent)
        .pipe(
          tap(() => this.loadingZoomResult(false)),
          catchError((error) => this.errorRoute(error))
        );
    }

    if (lowFormInput.pss) {
      this.pssResults$ = this.calculationService.calculatePssResult(lowFormInput, integrationTimesDifferent)
        .pipe(
          tap(() => this.loadingPssResult(false)),
          catchError((error) => this.errorRoute(error))
        );

    }
  }

  errorRoute = (response: HttpErrorResponse) => {
    let errorMsg = response.message;
    if (response.error && response.error.detail) {
      errorMsg = response.error.detail;
    }
    this.showErrorModal(errorMsg);
    this.clearResults();
    return of();
  };

  loadingContinuumResult(show: boolean) {
    if (this.panelContinuumOpened) {
      this.contPanel?.loadingResults(show);
    }
  }

  loadingZoomResult(show: boolean) {
    if (this.panelZoomOpened) {
      this.zoomPanel?.loadingResults(show);
    }
  }

  loadingPssResult(show: boolean) {
    if (this.panelPssOpened) {
      this.pssPanel?.loadingResults(show);
    }
  }

  private dispose() {
    this.componentIsDestroyed$.next(true);
    this.componentIsDestroyed$.complete();
  }

  onRaDecBlur(field: string) {
    if (field === "ra" && this.ra !== undefined) {
      if( this.sexDecToggle.checked ){
        this.ra?.setValue(helpers.format.decRaDecFormat(this.ra.value!));
      } else {
        this.ra?.setValue(helpers.format.raDecFormat(this.ra.value!));
      }
    }

    if (field === "dec" && this.dec !== undefined) {
      if( this.sexDecToggle.checked ){
        this.dec?.setValue(helpers.format.decRaDecFormat(this.dec.value!));
      } else {
        this.dec?.setValue(helpers.format.raDecFormat(this.dec.value!));
      }
    }
  }

  onSexDecChange(event: any) {
    if (event.checked) {
      this.toggleSexDecMode = true;
      const raConfig = this.configuration.midCalcFormFields?.find((e: any) => e.field == 'fieldRightAscensionDec');
      this.ra?.setValue(raConfig!.defaultValue);
      this.raPlaceholder = raConfig!.placeholder!;
      this.raTitle = raConfig!.description!;
      this.ra?.setValidators([this.validatorService.validatorRightAscension(true), Validators.required]);
      this.ra?.updateValueAndValidity();

      const decConfig = this.configuration.midCalcFormFields?.find((e: any) => e.field == 'fieldDeclinationDec');
      this.dec?.setValue(decConfig!.defaultValue);
      this.decPlaceholder = decConfig!.placeholder!;
      this.decTitle = decConfig!.description!;
      this.dec?.setValidators([this.validatorService.validatorDeclination("Low", true), Validators.required]);
      this.dec?.updateValueAndValidity();
    } else {
      this.resetRaDec();
    }
  }

  /**
   * @return true if integration times in multiple open accordions are different. Otherwise return false.
   */
  compareEnteredIntegrationTimes(): boolean {
    const durations: number[] = [];
    this.panelContinuumOpened ? durations.push(Number(this.contPanel.getFormData()?.duration)) : null;
    this.panelZoomOpened ? durations.push(Number(this.zoomPanel.getFormData()?.duration)) : null;
    this.panelPssOpened ? durations.push(Number(this.pssPanel.getFormData()?.duration)) : null;

    return durations.length > 1 && durations.some(duration => duration !== durations[0]);
  }

  /**
   * Zoom should be disabled for the earlier subarrays, and if open the zoom panel should be closed
   *
   * @param subarray that has just been selected
   **/
  checkIfZoomModeDisabled(subarray: LowSubarraysResponse): void {
    const shouldBeDisabled = ['LOW_AA05_all', 'LOW_AA1_all'].includes(subarray.name);
    this.zoomModeDisabled = shouldBeDisabled;
    // If the accordion is open, close it if the mode is disabled.
    if (this.panelZoomOpened) {
      this.panelZoomOpened = !shouldBeDisabled;
    }
  }
}
