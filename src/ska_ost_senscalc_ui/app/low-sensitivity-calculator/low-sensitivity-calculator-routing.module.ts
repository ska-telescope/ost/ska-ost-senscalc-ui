import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LowCalculatorComponent} from "./low-calculator/low-calculator.component";

const routes: Routes = [
  {
    path: '',
    component: LowCalculatorComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  // providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  exports: [RouterModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LowSensitivityCalculatorRoutingModule {
}
