import {ComponentFixture, TestBed} from '@angular/core/testing';
import {PssPanelComponent} from './pss-panel.component';
import {UntypedFormBuilder, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {TestbedHarnessEnvironment} from "@angular/cdk/testing/testbed";
import {MatInputHarness} from "@angular/material/input/testing";
import {MatFormFieldHarness} from "@angular/material/form-field/testing";
import {HarnessLoader} from "@angular/cdk/testing";
import {ValidatorsService} from "../../core/validators.service";
import {HttpClientModule} from "@angular/common/http";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {PssResultsComponent} from "./pss-results/pss-results.component";
import {Component} from "@angular/core";
import {MatSelectModule} from "@angular/material/select";

describe('PssPanelComponent', () => {
  let fixture: ComponentFixture<PssPanelComponent>;
  let component: PssPanelComponent;
  let loader: HarnessLoader;

  const getFormInputHarness = async (input_name: string): Promise<MatInputHarness> => {
    // Allow asterix in label in case the field is mandatory
    const input_reg = new RegExp(input_name + "( *)");

    const control = await loader.getHarness(
      MatFormFieldHarness.with({ floatingLabelText: input_reg })
    );
    return await control.getControl() as MatInputHarness;
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PssPanelComponent, MockPssResultsComponent ],
      imports: [
        HttpClientModule,
        NoopAnimationsModule,
        MatSnackBarModule,
        MatFormFieldModule,
        MatSelectModule,
        MatInputModule,
        ReactiveFormsModule,
        FormsModule
      ],
      providers: [UntypedFormBuilder, { provide: PssResultsComponent, useValue: MockPssResultsComponent }, ValidatorsService]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PssPanelComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(fixture.componentInstance).toBeTruthy();
  });

  it('should have a required Integration Time field with a default value', async () => {
    const input_field = await getFormInputHarness('Integration Time');
    expect(await input_field.isRequired()).toBeTruthy();
    expect(component.integrationTime?.value).toBe(1);
  });

  it('should have a required Central Frequency field with a default value', async () => {
    const input_field = await getFormInputHarness('Central Frequency');
    expect(await input_field.isRequired()).toBeTruthy();
    expect(component.centralFrequency?.value).toBe(200);
  });

  it('should have a required Bandwidth field with a default value', async () => {
    const input_field = await getFormInputHarness('Bandwidth');
    expect(component.totalBandwidth?.value).toBe(118.52);
  });

  it('should have a required Channel Width field with a default value', async () => {
    const input_field = await getFormInputHarness('Channel Width');
    expect(component.channelWidth?.value).toBe(14.5);
  });

  it('should have a required Dispersion Measure field with a default value', async () => {
    const input_field = await getFormInputHarness('Dispersion Measure');
    expect(await input_field.isRequired()).toBeTruthy();
    expect(component.dispersionMeasure?.value).toBe(14);
  });

  it('should reset all the fields and call the result component reset function', async () => {
    const integrationTimeField = await getFormInputHarness('Integration Time');
    await integrationTimeField.setValue("3");

    const spy = jest.spyOn(component.output, 'resetForm');

    component.resetForm();

    expect(component.pssForm.pristine);
    expect(component.integrationTime?.value).toBe(1);
    expect(spy).toHaveBeenCalled();
  });

  it('should return data from the pss form when getFormData is called', async () => {
    const int_time_input = await getFormInputHarness('Integration Time');
    await int_time_input.setValue("5");

    const cent_freq_input = await getFormInputHarness('Central Frequency');
    await cent_freq_input.setValue("123");

    const result = fixture.componentInstance.getFormData();

    expect(result.duration).toBe('5');
    expect(result.frequency.toString()).toBe("123");
  });

});

@Component({
  selector: 'app-pss-results',
  template: '',
  providers: [
    {
      provide: PssResultsComponent,
      useClass: MockPssResultsComponent
    }
  ]
})
class MockPssResultsComponent  {
  displayResultZoomPanel(response: any): void {
    response;
  }
  loadingResult(show: boolean): void { show; }
  clearResult(): void { }
  paramChange(): void { }
  suppliedChanged(): void { }
  resetForm(): void {}
}
