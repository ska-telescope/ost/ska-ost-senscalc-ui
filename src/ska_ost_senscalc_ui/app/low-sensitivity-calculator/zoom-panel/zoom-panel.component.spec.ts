import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZoomPanelComponent } from './zoom-panel.component';
import {UntypedFormBuilder, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {TestbedHarnessEnvironment} from "@angular/cdk/testing/testbed";
import {MatInputHarness} from "@angular/material/input/testing";
import {MatFormFieldHarness} from "@angular/material/form-field/testing";
import {HarnessLoader} from "@angular/cdk/testing";
import {ValidatorsService} from "../../core/validators.service";
import {HttpClientModule} from "@angular/common/http";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {ZoomResultsComponent} from "./zoom-results/zoom-results.component";
import {Component} from "@angular/core";
import {MatSelectModule} from "@angular/material/select";

describe('ZoomPanelComponent', () => {
  let fixture: ComponentFixture<ZoomPanelComponent>;
  let component: ZoomPanelComponent;
  let loader: HarnessLoader;

  const getFormInputHarness = async (input_name: string): Promise<MatInputHarness> => {
    // Allow asterix in label in case the field is mandatory
    const input_reg = new RegExp(input_name + "( *)");

    const control = await loader.getHarness(
      MatFormFieldHarness.with({ floatingLabelText: input_reg })
    );
    return await control.getControl() as MatInputHarness;
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZoomPanelComponent, MockZoomResultsComponent ],
      imports: [
        HttpClientModule,
        NoopAnimationsModule,
        MatSnackBarModule,
        MatFormFieldModule,
        MatSelectModule,
        MatInputModule,
        ReactiveFormsModule,
        FormsModule
      ],
      providers: [UntypedFormBuilder, { provide: ZoomResultsComponent, useValue: MockZoomResultsComponent }, ValidatorsService]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ZoomPanelComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(fixture.componentInstance).toBeTruthy();
  });

  it('should generate the display values for the bandwidth and spectral resolution to the correct dp with the calculated velocity', () => {
    // The velocity is calculated using the default central frequency of 200 MHz
    const expected_values = [
      "24.4 kHz (36.6 km/s), 14.1 Hz (21.2 m/s)",
      "48.8 kHz (73.2 km/s), 28.3 Hz (42.4 m/s)",
      "97.7 kHz (146.4 km/s), 56.5 Hz (84.7 m/s)",
      "195.3 kHz (292.8 km/s), 113.0 Hz (169.4 m/s)",
      "390.6 kHz (585.5 km/s), 226.1 Hz (338.8 m/s)",
      "781.3 kHz (1171.1 km/s), 452.1 Hz (677.7 m/s)",
      "1562.5 kHz (2342.1 km/s), 904.2 Hz (1.4 km/s)",
      "3125.0 kHz (4684.3 km/s), 1808.4 Hz (2.7 km/s)"
    ];

    const results = component.bandwidthAndSpectralResolutionOptions
      .map(val =>
        component.bandwidthAndSpectralResolutionDisplayValue(val.spectralResolutionHz, val.totalBandwidthKhz));
    expect(results).toStrictEqual(expected_values);
  });

  it('should have a required Integration Time field with a default value', async () => {
    const input_field = await getFormInputHarness('Integration Time');
    expect(await input_field.isRequired()).toBeTruthy();
    expect(component.integrationTime?.value).toBe(1);
  });

  it('should have a required Central Frequency field with a default value', async () => {
    const input_field = await getFormInputHarness('Central Frequency');
    expect(await input_field.isRequired()).toBeTruthy();
    expect(component.centralFrequency?.value).toBe(200);
  });

  it('should have a required Bandwidth/Spectral Resolution field with a default value', async () => {
    const input_field = await getFormInputHarness('Bandwidth, Spectral Resolution');
    expect(await input_field.isRequired()).toBeTruthy();
    expect(component.bandwidthAndSpectralResolution?.value).toStrictEqual({spectralResolutionHz: 14.128508391203704, totalBandwidthKhz: 24.4140625});
    const displayValue = component.bandwidthAndSpectralResolutionDisplayValue(
      component.bandwidthAndSpectralResolution?.value.spectralResolutionHz,
      component.bandwidthAndSpectralResolution?.value.totalBandwidthKhz
    );
    expect(displayValue).toBe("24.4 kHz (36.6 km/s), 14.1 Hz (21.2 m/s)");
  });

  it('should have a required Spectral Averaging field with a default value', async () => {
    const input_field = await getFormInputHarness('Spectral Averaging');
    expect(await input_field.isRequired()).toBeTruthy();
    expect(component.spectralAveraging?.value).toBe(1);
  });

  it('should populate Effective Resolution field with a default value', () => {
    // The value is derived from the first spectral resolution which is generated at initialisation.
    // We want to display a value to 1 dp but store the exact value to send to the calculation.
    expect(component.effectiveSpectralResolutionHz?.value).toBe(14.128508391203704);
    expect(component.effectiveSpectralResolutionDisplayValue).toBe("14.1 Hz (21.2 m/s)");
  });

  it('should have a required Image Weighting field with a default value', async () => {
    const input_field = await getFormInputHarness('Image Weighting');
    expect(await input_field.isRequired()).toBeTruthy();
    expect(component.imageWeighting?.value).toBe("Uniform");
  });

  it('should have a Robust Value field with a value of 0 when Image Weighting is Briggs', async () => {
    // Set the Image Weighting to Briggs. Ideally would do this using the harness set the value on the UI element,
    // however it doesn't seem to work nicely with MatSelect.
    component.imageWeighting?.setValue("Briggs");
    component.weightingChanged();

    const input_field = await getFormInputHarness('Robust Value');
    expect(await input_field.isRequired()).toBeTruthy();
    expect(component.robust?.value).toBe(0);
  });

  it('should reset all the fields and call the result component reset function', async () => {
    const integrationTimeField = await getFormInputHarness('Integration Time');
    await integrationTimeField.setValue("3");

    const spy = jest.spyOn(component.output, 'resetForm');

    component.resetForm();

    expect(component.zoomForm.pristine);
    expect(component.integrationTime?.value).toBe(1);
    expect(spy).toHaveBeenCalled();
  });

  it('should recalculate effectiveSpectralResolution when spectralAveraging changes', () => {
    const spy = jest.spyOn(component, 'paramChange');
    // Again it would be better to test this using the harness, but it can't
    // set the field to something other than a string: https://github.com/angular/components/issues/23894
    component.spectralAveraging?.setValue(2);
    component.bandwidthAndSpectralResolution?.setValue({spectralResolutionHz: 14});
    component.spectralAveragingChanged();

    expect(spy).toHaveBeenCalled();
    expect(component.effectiveSpectralResolutionHz?.value).toBe(28);
  });

  it('should recalculate effectiveSpectralResolution when bandwidthAndSpectralResolution changes', () => {
    const spy = jest.spyOn(component, 'paramChange');
    // Again it would be better to test this using the harness, but it can't
    // set the field to something other than a string: https://github.com/angular/components/issues/23894
    component.bandwidthAndSpectralResolution?.setValue({spectralResolutionHz: 50});
    component.bandwidthAndSpectralResolutionChange();

    expect(spy).toHaveBeenCalled();
    expect(component.effectiveSpectralResolutionHz?.value).toBe(50);
  });

  it('should return data from the zoom form when getFormData is called', async () => {
    const int_time_input = await getFormInputHarness('Integration Time');
    await int_time_input.setValue("5");

    const cent_freq_input = await getFormInputHarness('Central Frequency');
    await cent_freq_input.setValue("123");

    const result = fixture.componentInstance.getFormData();

    expect(result.duration).toBe('5');
    expect(result.frequency.toString()).toBe("123");
  });

  it('should set the image weighting to Natural and disable the field', async () => {
    component.imageWeighting?.setValue("Briggs");
    component.disableImageWeighting();

    const input_field = await getFormInputHarness('Image Weighting');
    expect(await input_field.isDisabled()).toBeTruthy();
    expect(component.imageWeighting?.value).toBe('Natural');

    component.enableImageWeighting();
    expect(await input_field.isDisabled()).toBeFalsy();
  });
});

@Component({
  selector: 'app-zoom-results',
  template: '',
  providers: [
    {
      provide: ZoomResultsComponent,
      useClass: MockZoomResultsComponent
    }
  ]
})
class MockZoomResultsComponent  {
  displayResultZoomPanel(response: any): void {
    response;
  }
  loadingResult(show: boolean): void { show; }
  clearResult(): void { }
  paramChange(): void { }
  suppliedChanged(): void { }
  resetForm(): void {}
}
