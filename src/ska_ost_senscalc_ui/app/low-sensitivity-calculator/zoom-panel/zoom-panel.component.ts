import {AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges,
  ViewChild
} from "@angular/core";
import {LowZoomInput} from '../types';
import {AbstractControl, UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {ValidatorsService} from '../../core/validators.service';
import {ZoomResultsComponent} from "./zoom-results/zoom-results.component";
import {LowSubarraysResponse} from "../../generated/api/low";
import helpers from "../../shared/utils/helpers";
import {ZoomResults} from "../../core/types";

@Component({
  selector: 'app-zoom-panel',
  templateUrl: './zoom-panel.component.html',
  styleUrls: ['./zoom-panel.component.scss']
})
export class ZoomPanelComponent implements OnInit, OnChanges, AfterViewInit {

  @Input() lowZoomResults?: ZoomResults | null;

  _selectedSubarray!: LowSubarraysResponse;

  @ViewChild(ZoomResultsComponent) output!: ZoomResultsComponent;

  @Input() set selectedSubarray(value: LowSubarraysResponse) {
    this._selectedSubarray = value;
  }

  formInitialValues: any;
  zoomForm: UntypedFormGroup;
  imageWeightingOptions = ['Natural', 'Uniform', 'Briggs'];
  defaultImageWeighting = 'Uniform';
  robustOptions = [-2, -1, 0, 1, 2];
  bandwidthAndSpectralResolutionOptions = this.generateBandwidthAndSpectralResolution(this._selectedSubarray);
  effectiveSpectralResolutionDisplayValue: string;
  centralFrequencyLimits = {min: 50, max: 350};
  spectralAveragingLimits = {min: 1, max: 864};

  @Output() changeEvent = new EventEmitter();
  showRobustness!: boolean;

  constructor(
    private validatorService: ValidatorsService,
    private fb: UntypedFormBuilder
  ) {
    const initialSpectralAveraging = 1;
    const initialEffectiveSpectralResolutionHz = this.bandwidthAndSpectralResolutionOptions[0].spectralResolutionHz * initialSpectralAveraging;
    this.zoomForm = this.fb.group({
      integrationTime: [1, [Validators.required, this.validatorService.validatorGeneral()]],
      centralFrequency: [200, [Validators.required, this.validatorService.validatorNumberMinMax(this.centralFrequencyLimits.min, this.centralFrequencyLimits.max)]],
      bandwidthAndSpectralResolution: [this.bandwidthAndSpectralResolutionOptions[0], [Validators.required, this.validatorService.lowZoomBandwidthValidator(this._selectedSubarray)]],
      spectralAveraging: [1, [Validators.required, this.validatorService.validatorNumberMinMax(this.spectralAveragingLimits.min, this.spectralAveragingLimits.max)]],
      effectiveSpectralResolutionHz: [initialEffectiveSpectralResolutionHz, [Validators.required]],
      imageWeighting: [this.defaultImageWeighting, [Validators.required]],
      robust: [0, []]
    });
    this.effectiveSpectralResolutionDisplayValue = this.effectiveResolutionDisplayValue(initialEffectiveSpectralResolutionHz);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['selectedSubarray']) {
      this.onSubarrayChange();
    }
  }

  ngOnInit(): void {
  }

  /**
   * This method is called when the subarray changes in the parent component, through ngChanges
   *
   * It handles any behaviour that needs to happen as a result of that change, for example
   * changing the allowed bandwidth values in the dropdown
   **/
  onSubarrayChange(): void {
      this.bandwidthAndSpectralResolutionOptions = this.generateBandwidthAndSpectralResolution(this._selectedSubarray);
      this.bandwidthAndSpectralResolution?.setValue(this.bandwidthAndSpectralResolutionOptions[0]);
      // Effective resolution depends on the bandwidthAndSpectralResolution, so if the latter changes we need to update the former
      this.calculateAndSetEffectiveSpectralResolution();
  }

  generateBandwidthAndSpectralResolution(subarray?: LowSubarraysResponse): Array<{spectralResolutionHz: number, totalBandwidthKhz: number}> {
    // The spectral resolutions for the zoom windows are given by (781250.0 * 32/27)/(4096 * 16) multiplied by increasing powers of 2
    // The allowed total bandwidths for zoom mode are then the chanel resolutions multiplied by the number of channels (1728), and converted to kHz
    const powers_of_two = subarray && ['LOW_AA2_all', 'LOW_AA2_core_only'].includes(subarray.name)
      ? [16, 32, 64, 128]
      : [1, 2, 4, 8, 16, 32, 64, 128];
    const baseSpectralResolutionHz = (781250 * 32/27) / 4096 / 16;
    return powers_of_two
      .map(multiplier => ({
        spectralResolutionHz: (multiplier * baseSpectralResolutionHz),
        totalBandwidthKhz: (multiplier * baseSpectralResolutionHz * 1728 * 1e-3)
      }));
  }

  bandwidthAndSpectralResolutionDisplayValue(spectralResolutionHz: number, totalBandwidthKhz:number) {
    const totalBandwidthVelocity = helpers.calculate.calculateVelocity(totalBandwidthKhz * 1e3, this.centralFrequency!.value * 1e6);
    const spectralResolutionVelocity = helpers.calculate.calculateVelocity(spectralResolutionHz, this.centralFrequency!.value * 1e6);

    return `${totalBandwidthKhz.toFixed(1)} kHz (${totalBandwidthVelocity}), ${spectralResolutionHz.toFixed(1)} Hz (${spectralResolutionVelocity})`;
  }

  ngAfterViewInit(): void {
    // Save the initial values for reset purposes.
    this.formInitialValues = this.zoomForm.value;
  }

  get integrationTime() {
    return this.zoomForm.get('integrationTime');
  }

  get centralFrequency() {
    return this.zoomForm.get('centralFrequency');
  }

  get bandwidthAndSpectralResolution() {
    return this.zoomForm.get('bandwidthAndSpectralResolution');
  }

  get spectralAveraging() {
    return this.zoomForm.get('spectralAveraging');
  }

  get effectiveSpectralResolutionHz() {
    return this.zoomForm.get('effectiveSpectralResolutionHz');
  }

  get imageWeighting() {
    return this.zoomForm.get('imageWeighting');
  }

  get robust() {
    return this.zoomForm.get('robust');
  }

  paramChange() {
    if (this.output) {
      this.output.paramChange();
    }
  }

  centralFrequencyChange() {
    // Only validate Bandwidth if Frequency is valid
    if (this.centralFrequency?.valid) {
      this.bandwidthAndSpectralResolution!.markAsTouched();
      this.bandwidthAndSpectralResolution!.updateValueAndValidity();
    }
    else {
      // Reset bandwidth errors if frequency is invalid
      this.bandwidthAndSpectralResolution!.setErrors(null);
    }
    this.calculateAndSetEffectiveSpectralResolution();
    this.paramChange();
  }

  bandwidthAndSpectralResolutionChange() {
    this.paramChange();
    this.calculateAndSetEffectiveSpectralResolution();
  }

  spectralAveragingChanged() {
    this.paramChange();
    this.calculateAndSetEffectiveSpectralResolution();
  }

  calculateAndSetEffectiveSpectralResolution() {
    const spectralResolutionHz = this.bandwidthAndSpectralResolution!.value.spectralResolutionHz * this.spectralAveraging?.value;

    this.effectiveSpectralResolutionDisplayValue = this.effectiveResolutionDisplayValue(spectralResolutionHz);
    this.effectiveSpectralResolutionHz?.setValue(spectralResolutionHz);
  }

  effectiveResolutionDisplayValue(spectralResolutionHz: number) {
    const spectralResolutionVelocity = helpers.calculate.calculateVelocity(spectralResolutionHz, this.centralFrequency?.value * 1e6);
    return `${spectralResolutionHz.toFixed(1)} Hz (${spectralResolutionVelocity})`;
  }

  weightingChanged(): void {
    this.paramChange();
    this.showRobustness = this.imageWeighting!.value == "Briggs";
  }

  getErrorMessage(control: AbstractControl | null) {
    return this.validatorService.getErrorMessage(control);
  }

  resetForm() {
    this.zoomForm.reset(this.formInitialValues);
    this.zoomForm.markAsUntouched();
    this.imageWeighting?.setValue(this.defaultImageWeighting);
    this.weightingChanged();
    this.output?.resetForm();
  }

  clearResultPanel() {
    this.loadingResults(false);
  }

  loadingResults(show: boolean) {
    this.output.loadingResults(show);
  }

  disableImageWeighting() {
    // In custom array mode, weighting should be set to natural and disabled as we can't calculate the weighting factors
    this.imageWeighting!.setValue('Natural');
    this.imageWeighting!.disable();
    this.weightingChanged();
  }

  enableImageWeighting() {
    this.imageWeighting!.enable();
    this.weightingChanged();
  }

  getFormData(): LowZoomInput {
    const data: LowZoomInput = {
      duration: this.integrationTime!.value,
      frequency: this.centralFrequency!.value,
      totalBandwidthKhz: this.bandwidthAndSpectralResolution!.value.totalBandwidthKhz,
      spectralResolutionHz: this.bandwidthAndSpectralResolution!.value.spectralResolutionHz,
      spectralAveraging: this.spectralAveraging!.value,
      effectiveSpectralResolutionHz: this.effectiveSpectralResolutionHz!.value,
      imageWeighting: this.imageWeighting!.value.toLowerCase()
    };
    if (this.showRobustness) {
      data.robust = this.robust!.value;
    }
    return data;
  }
}
