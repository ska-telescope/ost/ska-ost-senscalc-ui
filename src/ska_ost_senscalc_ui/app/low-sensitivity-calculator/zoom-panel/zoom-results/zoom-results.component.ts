import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import helpers from "../../../shared/utils/helpers";
import constants from "../../../shared/utils/constants";
import {ZoomResults} from "../../../core/types";

@Component({
  selector: 'app-zoom-results',
  templateUrl: './zoom-results.component.html',
  styleUrls: ['./zoom-results.component.scss']
})
export class ZoomResultsComponent implements OnInit, OnChanges {

  @Input() lowZoomResults?: ZoomResults | null;

  differentIntegrationTimeMessage = constants.messages.differentIntegrationTime;

  parametersChanged!: boolean;
  resultsHeaderMsg!: string;

  constructor() {}

  ngOnInit(): void {
    this.resetControlVariables();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['lowZoomResults']) {
      this.resetControlVariables();
    }
  }

  resetForm() {
    this.lowZoomResults = undefined;
    this.resetControlVariables();
  }

  resetControlVariables() {
    this.parametersChanged = false;
    this.loadingResults(false);
  }

  paramChange() {
    // Only want to warn after a calculation has been made and not
    // while user is entering at the start of a new calculation.
    if (this.lowZoomResults) {
      this.parametersChanged = true;
    }
  }

  loadingResults(loading: boolean): void {
    this.resultsHeaderMsg = helpers.result.header(loading);
  }

  sensLimitWarning() {
    return helpers.messages.sensLimitWarning();
  }
}
