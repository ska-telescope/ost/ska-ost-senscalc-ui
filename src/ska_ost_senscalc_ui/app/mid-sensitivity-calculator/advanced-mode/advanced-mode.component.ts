import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, UntypedFormBuilder, UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import {
  AppConfiguration,
  ArrayDropDownValue,
  Configuration,
  ConfigurationMidCalculatorFormField
} from "../../app-configuration";
import { ValidatorsService } from '../../core/validators.service';
import { AdvancedModeFormData } from '../types';
import {MatSlideToggleChange} from "@angular/material/slide-toggle";

@Component({
  selector: 'app-advanced-mode',
  templateUrl: './advanced-mode.component.html',
  styleUrls: ['./advanced-mode.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AdvancedModeComponent implements OnInit {

  configuration?: Configuration;
  advancedModeForm!: UntypedFormGroup;

  @Input() selectedsubarrayType!: ArrayDropDownValue;

  toggleEtaSystemEnabled = true;
  toggleEtaOtherEnabled = false;
  toggleTsysSKAEnabled = true;
  toggleTrcvSKAEnabled = false;
  toggleTsysMeerEnabled = true;
  toggleTrcvMeerEnabled = false;
  toggleTskyEnabled = false;
  toggleTgalEnabled = false;
  toggleTalphaEnabled = false;

  //form field configuration
  fieldToggleMode!: ConfigurationMidCalculatorFormField;
  fieldEtaSystem!: ConfigurationMidCalculatorFormField;
  fieldEtaPointing!: ConfigurationMidCalculatorFormField;
  fieldEtaCoherence!: ConfigurationMidCalculatorFormField;
  fieldEtaDigitisation!: ConfigurationMidCalculatorFormField;
  fieldEtaCorrelation!: ConfigurationMidCalculatorFormField;
  fieldEtaBandpass!: ConfigurationMidCalculatorFormField;
  fieldTsysSKA!: ConfigurationMidCalculatorFormField;
  fieldTrcvSKA!: ConfigurationMidCalculatorFormField;
  fieldTsplSKA!: ConfigurationMidCalculatorFormField;
  fieldTsysMeerKAT!: ConfigurationMidCalculatorFormField;
  fieldTrcvMeerKAT!: ConfigurationMidCalculatorFormField;
  fieldTsplMeerKAT!: ConfigurationMidCalculatorFormField;
  fieldTsky!: ConfigurationMidCalculatorFormField;
  fieldTgal!: ConfigurationMidCalculatorFormField;
  fieldTalpha!: ConfigurationMidCalculatorFormField;
  fieldEtaSKA!: ConfigurationMidCalculatorFormField;
  fieldEtaMeer!: ConfigurationMidCalculatorFormField;

  constructor(private config: AppConfiguration, private validators: ValidatorsService, private fb: UntypedFormBuilder) { }

  ngOnInit(): void {
    this.getDataFromConfiguration();
    this.initializeAdvancedModeForm();
  }

  checkSelectedSubarrayType(checkValue: string) {
    if (checkValue == 'ska' && this.selectedsubarrayType.n_ska != 0) {
      return true;
    }
    if (checkValue == 'meerkat' && this.selectedsubarrayType.n_meer != 0) {
      return true;
    }
    return false;
  }

  getDataFromConfiguration() {
    if (!this.configuration) {
      this.configuration = this.config.getConfig();
    }

    if (this.configuration?.advancedModeFields != null) {
      this.fieldToggleMode = this.getFormFieldByName('fieldToggleMode')!;
      this.fieldEtaSystem = this.getFormFieldByName('fieldEtaSystem')!;
      this.fieldEtaPointing = this.getFormFieldByName('fieldEtaPointing')!;
      this.fieldEtaCoherence = this.getFormFieldByName('fieldEtaCoherence')!;
      this.fieldEtaDigitisation = this.getFormFieldByName('fieldEtaDigitisation')!;
      this.fieldEtaCorrelation = this.getFormFieldByName('fieldEtaCorrelation')!;
      this.fieldEtaBandpass = this.getFormFieldByName('fieldEtaBandpass')!;
      this.fieldTsysSKA = this.getFormFieldByName('fieldTsysSKA')!;
      this.fieldTrcvSKA = this.getFormFieldByName('fieldTrcvSKA')!;
      this.fieldTsplSKA = this.getFormFieldByName('fieldTsplSKA')!;
      this.fieldTsysMeerKAT = this.getFormFieldByName('fieldTsysMeerKAT')!;
      this.fieldTrcvMeerKAT = this.getFormFieldByName('fieldTrcvMeerKAT')!;
      this.fieldTsplMeerKAT = this.getFormFieldByName('fieldTsplMeerKAT')!;
      this.fieldTsky = this.getFormFieldByName('fieldTsky')!;
      this.fieldTgal = this.getFormFieldByName('fieldTgal')!;
      this.fieldTalpha = this.getFormFieldByName('fieldTalpha')!;
      this.fieldEtaSKA = this.getFormFieldByName('fieldEtaSKA')!;
      this.fieldEtaMeer = this.getFormFieldByName('fieldEtaMeer')!;
    }
  }

  initializeAdvancedModeForm() {

    this.advancedModeForm = this.fb.group(
      {
        fieldEtaSystem: new UntypedFormControl({ value: null, disabled: false }, [this.validators.validatorEfficiency()]),
        fieldEtaPointing: new UntypedFormControl({ value: null, disabled: true }, [this.validators.validatorEfficiency()]),
        fieldEtaCoherence: new UntypedFormControl({ value: null, disabled: true }, [this.validators.validatorEfficiency()]),
        fieldEtaDigitisation: new UntypedFormControl({ value: null, disabled: true }, [this.validators.validatorEfficiency()]),
        fieldEtaCorrelation: new UntypedFormControl({ value: null, disabled: true }, [this.validators.validatorEfficiency()]),
        fieldEtaBandpass: new UntypedFormControl({ value: null, disabled: true }, [this.validators.validatorEfficiency()]),
        fieldTsysSKA: new UntypedFormControl(this.fieldTsysSKA ? this.fieldTsysSKA.defaultValue : null, [this.validators.validatorGeneral()]),
        fieldTrcvSKA: new UntypedFormControl({ value: this.fieldTrcvSKA ? this.fieldTrcvSKA.defaultValue : null, disabled: true }, [this.validators.validatorGeneral()]),
        fieldTsplSKA: new UntypedFormControl({ value: this.fieldTsplSKA ? this.fieldTsplSKA.defaultValue : null, disabled: true }, [this.validators.validatorGeneral()]),
        fieldTsysMeerKAT: new UntypedFormControl(this.fieldTsysMeerKAT ? this.fieldTsysMeerKAT.defaultValue : null, [this.validators.validatorGeneral()]),
        fieldTrcvMeerKAT: new UntypedFormControl({ value: this.fieldTrcvMeerKAT ? this.fieldTrcvMeerKAT.defaultValue : null, disabled: true }, [this.validators.validatorGeneral()]),
        fieldTsplMeerKAT: new UntypedFormControl({ value: this.fieldTsplMeerKAT ? this.fieldTsplMeerKAT.defaultValue : null, disabled: true }, [this.validators.validatorGeneral()]),
        fieldTsky: new UntypedFormControl({ value: this.fieldTsky ? this.fieldTsky.defaultValue : null, disabled: true }, [this.validators.validatorGeneral()]),
        fieldTgal: new UntypedFormControl({ value: this.fieldTgal ? this.fieldTgal.defaultValue : null, disabled: true }, [this.validators.validatorGeneral()]),
        fieldTalpha: new UntypedFormControl({ value: this.fieldTalpha ? this.fieldTalpha.defaultValue : null, disabled: true }, [this.validators.validatorGeneral()]),
        fieldEtaSKA: new UntypedFormControl(this.fieldEtaSKA ? this.fieldEtaSKA.defaultValue : null, [this.validators.validatorEfficiency()]),
        fieldEtaMeer: new UntypedFormControl(this.fieldEtaMeer ? this.fieldEtaMeer.defaultValue : null, [this.validators.validatorEfficiency()])
      }
    );
  }

  getFormFieldByName(name: string): ConfigurationMidCalculatorFormField | undefined {
    return this.configuration!.advancedModeFields!.find(
      (e: ConfigurationMidCalculatorFormField) => e.field == name
    )!;
  }

  /**
   * This method is called when either of the toggle for the eta fields are clicked.
   *
   * A user can either give a total system efficiency, or individual values for the other efficiencies
   * (the backend will then use these individual values, or defaults for ones not given, to calculate the
   * system efficiency).
   *
   * The behaviour of the toggles should therefore be that enabling system efficiency disables the individual ones,
   * and sets their values to null. And enabling the individual fields should disable the system efficiency. Setting
   * either of the toggles to off should disable and null that field.
   *
   * @param event which stores whether the toggle has been switched to on or off
   * @param fieldName the name of the toggled field, eg 'fieldEtaSystem'
   **/
  toggleEtaFields(event: MatSlideToggleChange, fieldName: string) {
    const otherEtaFields = ['fieldEtaPointing', 'fieldEtaCoherence', 'fieldEtaDigitisation', 'fieldEtaCorrelation', 'fieldEtaBandpass'];
    // First handle the case where one of the fields is toggled on
    if (event.checked) {
      if (fieldName === 'fieldEtaSystem') {
        this.toggleEtaOtherEnabled = false;
        this.toggleEtaSystemEnabled = true;
        otherEtaFields.forEach(field => {
          this.advancedModeForm.get(field)?.disable();
          this.advancedModeForm.get(field)?.setValue(null);
        });
        this.advancedModeForm.get('fieldEtaSystem')?.enable();
      } else {
        this.toggleEtaOtherEnabled = true;
        this.toggleEtaSystemEnabled = false;
        this.advancedModeForm.get('fieldEtaSystem')?.disable();
        this.advancedModeForm.get('fieldEtaSystem')?.setValue(null);
        otherEtaFields.forEach(field => {
          this.advancedModeForm.get(field)?.enable();
        });
      }
    } else { // Handle the case where one of the fields is toggled off
      if (fieldName === 'fieldEtaSystem') {
        this.toggleEtaSystemEnabled = false;
        this.advancedModeForm.get('fieldEtaSystem')?.disable();
        this.advancedModeForm.get('fieldEtaSystem')?.setValue(null);
      } else {
        this.toggleEtaOtherEnabled = false;
        otherEtaFields.forEach(field => {
          this.advancedModeForm.get(field)?.disable();
          this.advancedModeForm.get(field)?.setValue(null);
        });
      }
    }

  }

  toggleTempFields(event: MatSlideToggleChange, fieldName: string, postfix: string) {
    if (fieldName === 'fieldTsys' + postfix) {
      if (event.checked) {
        if (postfix === 'SKA') {
          this.toggleTsysSKAEnabled = true;
          this.toggleTrcvSKAEnabled = false;
        } else {
          this.toggleTsysMeerEnabled = true;
          this.toggleTrcvMeerEnabled = false;
        }
        this.toggleTskyEnabled = false;
        this.toggleTgalEnabled = false;
        this.toggleTalphaEnabled = false;
        this.advancedModeForm.get('fieldTsys' + postfix)!.enable();
        this.advancedModeForm.get('fieldTsys' + postfix)!.setValue(null);
        this.advancedModeForm.get('fieldTrcv' + postfix)!.disable();
        this.advancedModeForm.get('fieldTspl' + postfix)!.disable();
        this.advancedModeForm.get('fieldTrcv' + postfix)!.setValue(null);
        this.advancedModeForm.get('fieldTspl' + postfix)!.setValue(null);
        this.advancedModeForm.get('fieldTsky')!.disable();
        this.advancedModeForm.get('fieldTsky')!.setValue(null);
        this.advancedModeForm.get('fieldTgal')!.disable();
        this.advancedModeForm.get('fieldTgal')!.setValue(null);
        this.advancedModeForm.get('fieldTalpha')!.disable();
        this.advancedModeForm.get('fieldTalpha')!.setValue(null);
      } else {
        if (postfix === 'SKA') {
          this.toggleTsysSKAEnabled = false;
          this.toggleTrcvSKAEnabled = false;
        } else {
          this.toggleTsysMeerEnabled = false;
          this.toggleTrcvMeerEnabled = false;
        }
        this.advancedModeForm.get('fieldTsys' + postfix)!.disable();
        this.advancedModeForm.get('fieldTsys' + postfix)!.setValue(null);
      }
    } else if (fieldName === 'fieldTrcv' + postfix) {
      if (event.checked) {
        if (postfix === 'SKA') {
          this.toggleTsysSKAEnabled = false;
          this.toggleTrcvSKAEnabled = true;
        } else {
          this.toggleTsysMeerEnabled = false;
          this.toggleTrcvMeerEnabled = true;
        }
        this.advancedModeForm.get('fieldTsys' + postfix)!.disable();
        this.advancedModeForm.get('fieldTsys' + postfix)!.setValue(null);
        this.advancedModeForm.get('fieldTrcv' + postfix)!.enable();
        this.advancedModeForm.get('fieldTspl' + postfix)!.enable();
        this.advancedModeForm.get('fieldTrcv' + postfix)!.setValue(null);
        this.advancedModeForm.get('fieldTspl' + postfix)!.setValue(null);
        this.advancedModeForm.get('fieldTsky')!.disable();
        this.advancedModeForm.get('fieldTsky')!.setValue(null);
      } else {
        if (postfix === 'SKA') {
          this.toggleTsysSKAEnabled = false;
          this.toggleTrcvSKAEnabled = false;
        } else {
          this.toggleTsysMeerEnabled = false;
          this.toggleTrcvMeerEnabled = false;
        }

        this.advancedModeForm.get('fieldTrcv' + postfix)!.disable();
        this.advancedModeForm.get('fieldTspl' + postfix)!.disable();
        this.advancedModeForm.get('fieldTrcv' + postfix)!.setValue(null);
        this.advancedModeForm.get('fieldTspl' + postfix)!.setValue(null);
      }
    }
  }

  toggleSkyFields(event: MatSlideToggleChange, fieldToToggle: string) {
    if (event.checked) {
      let fieldToDisable1 = '';
      let fieldToDisable2 = '';
      if (fieldToToggle === 'fieldTsky') {
        this.toggleTgalEnabled = false;
        this.toggleTalphaEnabled = false;
        this.toggleTskyEnabled = true;
        fieldToDisable1 = 'fieldTgal';
        fieldToDisable2 = 'fieldTalpha';
      } else if (fieldToToggle === 'fieldTgal') {
        this.toggleTgalEnabled = true;
        this.toggleTalphaEnabled = false;
        this.toggleTskyEnabled = false;
        fieldToDisable1 = 'fieldTsky';
        fieldToDisable2 = 'fieldTalpha';
      } else {
        this.toggleTalphaEnabled = true;
        this.toggleTgalEnabled = false;
        this.toggleTskyEnabled = false;
        fieldToDisable1 = 'fieldTsky';
        fieldToDisable2 = 'fieldTgal';
      }
      this.toggleTsysSKAEnabled = false;
      this.toggleTsysMeerEnabled = false;

      this.advancedModeForm.get(fieldToToggle)!.enable();
      this.advancedModeForm.get(fieldToDisable1)!.disable();
      this.advancedModeForm.get(fieldToDisable1)!.setValue(null);
      this.advancedModeForm.get(fieldToDisable2)!.disable();
      this.advancedModeForm.get(fieldToDisable2)!.setValue(null);
      this.advancedModeForm.get('fieldTsysSKA')!.disable();
      this.advancedModeForm.get('fieldTsysSKA')!.setValue(null);
      this.advancedModeForm.get('fieldTsysMeerKAT')!.disable();
      this.advancedModeForm.get('fieldTsysMeerKAT')!.setValue(null);
    } else {
      this.advancedModeForm.get(fieldToToggle)!.disable();
      this.advancedModeForm.get(fieldToToggle)!.setValue(null);
    }
  }

  getAdvancedFormData(): any {
    let formData: AdvancedModeFormData | null = null;

    if (this.advancedModeForm.valid) {
      formData = {
        etaSystem: this.advancedModeForm.get('fieldEtaSystem')!.value,
        etaPointing: this.advancedModeForm.get('fieldEtaPointing')!.value,
        etaCoherence: this.advancedModeForm.get('fieldEtaCoherence')!.value,
        etaDigitisation: this.advancedModeForm.get('fieldEtaDigitisation')!.value,
        etaCorrelation: this.advancedModeForm.get('fieldEtaCorrelation')!.value,
        etaBandpass: this.advancedModeForm.get('fieldEtaBandpass')!.value,
        tSysSka: this.advancedModeForm.get('fieldTsysSKA')!.value,
        tRxSka: this.advancedModeForm.get('fieldTrcvSKA')!.value,
        tSplSka: this.advancedModeForm.get('fieldTsplSKA')!.value,
        tSysMeer: this.advancedModeForm.get('fieldTsysMeerKAT')!.value,
        tRxMeer: this.advancedModeForm.get('fieldTrcvMeerKAT')!.value,
        tSplMeer: this.advancedModeForm.get('fieldTsplMeerKAT')!.value,
        tSkySka: this.advancedModeForm.get('fieldTsky')!.value,
        tGalSka: this.advancedModeForm.get('fieldTgal')!.value,
        tSkyMeer: this.advancedModeForm.get('fieldTsky')!.value,
        tGalMeer: this.advancedModeForm.get('fieldTgal')!.value,
        alpha: this.advancedModeForm.get('fieldTalpha')!.value,
        etaSka: this.advancedModeForm.get('fieldEtaSKA')!.value,
        etaMeer: this.advancedModeForm.get('fieldEtaMeer')!.value
      };
    }
    return formData;
  }

  getErrorMessage(fieldName: string) {
    const control: AbstractControl | null = this.advancedModeForm.get(fieldName);
    return this.validators.getErrorMessage(control);
  }

  resetForm() {
    this.advancedModeForm.reset();
    this.advancedModeForm.markAsUntouched();
  }
}
