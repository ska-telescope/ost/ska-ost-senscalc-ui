import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, SimpleChange } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from "@angular/material/card";
import { MatMenuModule } from '@angular/material/menu';
import { ContinuumPanelComponent } from './continuum-panel.component';
import { HttpClientModule } from '@angular/common/http';
import { MatTabsModule } from '@angular/material/tabs';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";

import { ConfigurationDropDownValue } from '../../app-configuration';
import * as config from '../../../assets/configuration.json';
import { MidCalculatorService } from '../../core/mid-calculator.service';
import { ContinuumResultsComponent } from './continuum-results/continuum-results.component';
import helpers from "../../shared/utils/helpers";

describe('ContinuumTabComponent', () => {
  let component: ContinuumPanelComponent;
  let fixture: ComponentFixture<ContinuumPanelComponent>;
  let midCalcService: MockMidCalculatorService;

  beforeEach(async () => {
    midCalcService = new MockMidCalculatorService();

    await TestBed.configureTestingModule({
      declarations: [
        ContinuumPanelComponent,
        MockContinuumResultsComponent
      ],
      imports: [
        FontAwesomeModule,
        FormsModule,
        MatCardModule,
        ReactiveFormsModule,
        HttpClientModule,
        MatMenuModule,
        MatSelectModule,
        MatTabsModule,
        MatFormFieldModule,
        MatInputModule,
        NoopAnimationsModule
      ],
      providers: [
        { provide: MidCalculatorService, useValue: midCalcService }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContinuumPanelComponent);
    component = fixture.componentInstance;
    component.configuration = config;
    component._subarrayType = {
      name: 'MID_AA4_all',
      label: 'AA4',
      n_meer: 10,
      n_ska: 20
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('continuum form invalid when empty', () => {
    expect(component.continuumForm.valid).toBeFalsy();
  });

  it('select central frequency', () => {
    const value: ConfigurationDropDownValue = {
      type: 'frequency',
      value: 'GHz',
      operator: '*',
      multiplier: 1000000000
    };
    component.selectCentralFrequency(value);
    expect(component.selectedCentralFrequency).toBe(value);
  });

  it('select bandwidth', () => {
    const value: ConfigurationDropDownValue = {
      type: 'frequency',
      value: 'GHz',
      operator: '*',
      multiplier: 1000000000
    };
    component.selectBandwidth(value);
    expect(component.selectedBandwidth).toBe(value);
  });

  it('select Integration Time', () => {
    const value: ConfigurationDropDownValue = {
      type: 'time',
      value: 's',
      operator: null,
      multiplier: null
    };
    component.selectIntegrationTime(value);
    expect(component.selectedIntegrationTime).toBe(value);
  });

  it('select Sensitivity', () => {
    const value: ConfigurationDropDownValue = {
      type: 'sensitivity',
      value: 'Jy/beam',
      operator: null,
      multiplier: null
    };
    component.selectSensitivity(value);
    expect(component.selectedSensitivity).toBe(value);
  });

  it('should supplied field change call method and set value for dependant control', () => {
    component.suppliedChangedContPanel('IntegrationTime');
    fixture.detectChanges();
    expect(component.continuumForm.get('fieldIntegrationTime')?.enabled).toBe(true);
    expect(component.continuumForm.get('fieldSensitivity')?.disabled).toBe(true);

    component.suppliedChangedContPanel('Sensitivity');
    fixture.detectChanges();
    expect(component.continuumForm.get('fieldIntegrationTime')?.disabled).toBe(true);
    expect(component.continuumForm.get('fieldSensitivity')?.enabled).toBe(true);
  });

  it('should continuum tab component call ngOnChanges', async () => {
    await fixture.whenStable().then(() => {
      component.ngOnChanges({});
      fixture.detectChanges();
      component.continuumForm.get('fieldCentralFrequency')?.setValue('test');
      component.continuumForm.get('fieldBandwidth')?.setValue('test');
      component.continuumForm.get('fieldCentralFrequency')?.updateValueAndValidity();
      component.continuumForm.get('fieldBandwidth')?.updateValueAndValidity();
    });
  });

  it('should reset fieldCentralFrequency and fieldBandwidth when observingMode changes', async () => {
    await fixture.whenStable().then(() => {
      component.ngOnChanges({observingMode: new SimpleChange("band 1", "band 2", true)});
      fixture.detectChanges();
      expect(component.continuumForm.get('fieldCentralFrequency')?.value).toBe(-1);// getDefaultFromConfigurationFile returns -1 when the config file isn't available for the tests
      expect(component.continuumForm.get('fieldCentralFrequency')?.touched).toBe(false);
      expect(component.continuumForm.get('fieldBandwidth')?.value).toBe(-1);
      expect(component.continuumForm.get('fieldBandwidth')?.touched).toBe(false);
    });
  });

  it('reset continuum tab', async () => {
    await fixture.whenStable().then(() => {
      component.resetContinuumForm();
      fixture.detectChanges();
      expect(component.continuumForm.get('fieldBandwidth')?.value).toBe(0.435);
      expect(component.continuumForm.get('fieldSupplied')?.value).toBe('IntegrationTime');
      expect(component.continuumForm.get('fieldIntegrationTime')?.value).toBe(600);
      expect(component.continuumForm.get('fieldSensitivity')?.value).toBe(1);
    });
  });

  it('set observingMode', () => {
    component.observingMode = '';
    expect(component._observingMode).toBe('');
  });

  it('no configuration', () => {
    component.configuration = undefined;
    component.getformFieldsDataFromConfig();
    expect(component.configuration).toBe(undefined);
  });

  it('get form data', () => {
    component.continuumForm.get('fieldCentralFrequency')?.setErrors(null);
    component.continuumForm.updateValueAndValidity();
    component.selectedIntegrationTime = {
      multiplier: 1,
      operator: '*',
      type: 'time',
      value: 's'
    };
    const exp = {
      "frequency": 0,
      "bandwidth": 435000000,
      "integrationTime": 600,
      "selectedSensitivityUnit": "Jy/beam",
      "n_subbands": 1,
      "spectralAveragingFactor": 1,
      "spectralResolution": "13.44",
      "weighting": "uniform",
      "taper": 0,
      "calculator_mode": "continuum"
    };
    expect(component.getContinuumFormData()).toStrictEqual(exp);

    component.selectedSensitivity = {
      multiplier: 1,
      operator: null,
      type: 'sensitivity',
      value: 'Jy/beam'
    };
    component.continuumForm.get('fieldIntegrationTime')?.disable();
    component.continuumForm.get('fieldSensitivity')?.enable();
    component.continuumForm.get('fieldSensitivity')?.setValue(100);
    const exp2 = {
      "frequency": 0,
      "bandwidth": 435000000,
      "sensitivity": 100,
      "n_subbands": 1,
      "spectralAveragingFactor": 1,
      "spectralResolution": "13.44",
      "weighting": "uniform",
      "taper": 0,
      "calculator_mode": "continuum",
      "selectedSensitivityUnit": 'Jy/beam'
    };
    expect(component.getContinuumFormData()).toStrictEqual(exp2);
  });

  it('getFieldSupplied', () => {
    const input = {
      integration_time: 1
    };
    const input2 = {
    };
    expect(component.getFieldSupplied(input)).toBe('IntegrationTime');
    expect(component.getFieldSupplied(input2)).toBe('IntegrationTime');
  });

  it('weightingChanged', () => {
    component.weightingChanged('robust');
    fixture.detectChanges();
    expect(component.showRobustness).toBe(true);

    component.weightingChanged('uniform');
    fixture.detectChanges();
    expect(component.showRobustness).toBe(false);

    component.weightingChanged('natural');
    fixture.detectChanges();
    expect(component.showRobustness).toBe(false);
  });

  it('spectralAveragingChanged', () => {
    jest.spyOn(helpers.calculate, 'calculateVelocity').mockReturnValue('615.1 m/s');
    component.continuumForm.get('fieldResolution')?.setValue('13.44');
    component.spectralAveragingChanged(1);
    fixture.detectChanges();
    expect(component.continuumForm.get('fieldEffectiveResolution')?.value).toBe('13.44 kHz (615.1 m/s)');
  });

  it('should disable the correct fields if weighting is disabled', () => {
    component.disableWeightingFields();
    expect(component.continuumForm.get('fieldImageWeighting')?.enabled).toBe(false);
    expect(component.continuumForm.get('fieldImageWeighting')?.value).toBe('natural');
    expect(component.continuumForm.get('fieldTapering')?.enabled).toBe(false);
    expect(component.continuumForm.get('fieldTapering')?.value).toBe(0);
  });

});

class MockMidCalculatorService extends MidCalculatorService {
}

@Component({
  selector: 'app-continuum-results',
  template: '',
  providers: [
    {
      provide: ContinuumResultsComponent,
      useClass: MockContinuumResultsComponent
    }
  ]
})
class MockContinuumResultsComponent extends ContinuumResultsComponent {
  override suppliedChangedContResult() {}
}
