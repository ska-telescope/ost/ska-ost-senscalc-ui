import {
  AfterViewInit,
  Component,
  Input,
  OnChanges,
  OnInit, SimpleChanges,
  ViewChild
} from '@angular/core';
import {
  UntypedFormGroup,
  UntypedFormBuilder,
  UntypedFormControl,
  Validators,
  AbstractControl
} from '@angular/forms';
import {
  AppConfiguration,
  Configuration,
  ConfigurationMidCalculatorFormField,
  ConfigurationDropDownValue,
  ConfigurationImageWeightingValue,
  ConfigurationSpectralAvgDropDownValue, ArrayDropDownValue, MidSubarrayId
} from '../../app-configuration';
import { ValidatorsService } from '../../core/validators.service';
import { faCaretDown, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import {ContinuumFormData} from '../types';
import { ContinuumResultsComponent } from './continuum-results/continuum-results.component';
import { MidCalculatorService } from '../../core/mid-calculator.service';
import helpers from "../../shared/utils/helpers";
import {ContinuumResults} from "../../core/types";

@Component({
  selector: 'app-continuum-panel',
  templateUrl: './continuum-panel.component.html',
  styleUrls: ['./continuum-panel.component.scss']
})
export class ContinuumPanelComponent implements OnInit, OnChanges, AfterViewInit {
  @ViewChild(ContinuumResultsComponent) output!: ContinuumResultsComponent;
  // Nominal build
  _observingMode!: string;
  _subarrayType!: ArrayDropDownValue;

  @Input() continuumResults?: ContinuumResults | null;

  @Input() set observingMode(value: string) {
    this._observingMode = value;
  }

  @Input() set subarrayType(value: ArrayDropDownValue) {
    this._subarrayType = value;
  }

  continuumForm!: UntypedFormGroup;
  configuration?: Configuration;
  //form field configuration
  fieldCentralFrequency!: ConfigurationMidCalculatorFormField;
  fieldBandwidth!: ConfigurationMidCalculatorFormField;
  fieldResolution!: ConfigurationMidCalculatorFormField;
  fieldNoOfSubBands!: ConfigurationMidCalculatorFormField;
  fieldIntegrationTime!: ConfigurationMidCalculatorFormField;
  fieldSensitivity!: ConfigurationMidCalculatorFormField;
  fieldSupplied!: ConfigurationMidCalculatorFormField;
  fieldSpectralAveraging!: ConfigurationMidCalculatorFormField;
  fieldEffectiveResolution!: ConfigurationMidCalculatorFormField;
  fieldImageWeighting!: ConfigurationMidCalculatorFormField;
  fieldRobust!: ConfigurationMidCalculatorFormField;
  fieldTapering!: ConfigurationMidCalculatorFormField;
  //defaults
  defaultSupplied!: any;
  defaultFrequencyDropdownOptions!: ConfigurationDropDownValue[];
  defaultTimeDropdownOptions!: ConfigurationDropDownValue[];
  defaultSensitivityDropdownOptions!: ConfigurationDropDownValue[];
  selectedCentralFrequency!: ConfigurationDropDownValue;
  selectedBandwidth!: ConfigurationDropDownValue;
  selectedIntegrationTime!: ConfigurationDropDownValue;
  selectedSensitivity!: ConfigurationDropDownValue;
  contFreqInput!: number;
  contBandInput!: number;
  continuumFormInitialValues: any;
  defaultSpectralAveraging!: ConfigurationSpectralAvgDropDownValue;
  spectralAveragingOptions!: ConfigurationSpectralAvgDropDownValue[];
  defaultSelectedImageWeighting!: ConfigurationImageWeightingValue;
  showRobustness!: boolean;
  //icons
  faCaretDown: IconDefinition = faCaretDown;
  // Defines the robustness display spacing.
  imageWeightingStyle!: string;

  constructor(
    private config: AppConfiguration,
    private fb: UntypedFormBuilder,
    private validatorService: ValidatorsService,
    public midCalcService: MidCalculatorService
  ) {
    this.configuration = this.config.getConfig();
  }

  getImageClassName(): void {
    this.imageWeightingStyle = this.showRobustness ? 'width: 63%;' : 'width: 97%;';
  }

  ngAfterViewInit(): void {
    // Save the initial values for reset purposes.
    this.continuumFormInitialValues = this.continuumForm.value;
  }

  paramChangeContPanel() {
    // Add param update message to all relevant result panels.
    // Note: only one panel at this time.
    this.output.paramChangeContResult();
  }

  freqParamChange() {
    this.updateFrequencyAndBandwidthValidity();
    this.setEffectiveResolution();
    this.paramChangeContPanel();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.configuration?.midCalcFormFields) {
      const fieldObservingMode = this.configuration.midCalcFormFields.find(
        (e: ConfigurationMidCalculatorFormField) =>
          e.field == 'fieldObservingMode'
      );

      this.setDefaultFrequencyDropdownOptions();

      if (fieldObservingMode) {

        const mode = fieldObservingMode.defaultValue.find(
          (e: any) => e.mode === this._observingMode
        );

        const contFreqInputValues = mode?.contFreqInput;
        this.contFreqInput = helpers.configuration.getDefaultFromConfigurationFile(contFreqInputValues, this._subarrayType);

        const defaultContBandwidthHz = this.validatorService.midMaxContBandwidthHzForSubarray.get(this._subarrayType.label as MidSubarrayId);
        const contBandInputValues = mode?.contBandInput;
        const configBandDefaultGHz = helpers.configuration.getDefaultFromConfigurationFile(contBandInputValues, this._subarrayType);

        // Use the max allowed bandwidth if the default bandwidth is larger than the allowed value
        this.contBandInput = (defaultContBandwidthHz && defaultContBandwidthHz * 1.e-9 < configBandDefaultGHz) ? defaultContBandwidthHz * 1.e-9 : configBandDefaultGHz;

        if (this.continuumForm) {
          // If the band changes or the array configuration changes and the user hasn't updated any fields,
          // we want to set the continuum frequency and bandwidth to the default values for that band.
          // However, if the user has edited the fields then changes the subarray, we do not want to overwrite their changes
          const subarrayChangedButFrequencyUntouched = changes['subarrayType'] && !this.continuumForm.get('fieldCentralFrequency')?.touched;
          const subarrayChangedButBandwidthUntouched = changes['subarrayType'] && !this.continuumForm.get('fieldBandwidth')?.touched;
          this.updateFrequencyAndBandwidthValidity();
          if (changes['observingMode'] || subarrayChangedButFrequencyUntouched) {
            // Set the frequency value to the default for the given band
            this.continuumForm.get('fieldCentralFrequency')!.setValue(this.contFreqInput);
            // Reset the frequency unit back to default
            this.selectedCentralFrequency = this.defaultFrequencyDropdownOptions.find(
              (e: ConfigurationDropDownValue) => e.selected
            )!;
            this.updateFrequencyAndBandwidthValidity();
            this.continuumForm.get('fieldCentralFrequency')!.markAsUntouched();
          }
          if (changes['observingMode'] || subarrayChangedButBandwidthUntouched) {
            this.continuumForm.get('fieldBandwidth')!.setValue(this.contBandInput);
            this.selectedBandwidth = this.defaultFrequencyDropdownOptions.find(
              (e: ConfigurationDropDownValue) => e.selected
            )!;
            this.updateBandwidthValidity();
            this.continuumForm.get('fieldBandwidth')!.markAsUntouched();
          }

          // check if the chosen taper value is allowed for the current subarray
          // If taper value is not allowed (large taper values are not allowed for
          // some subarrays), set to the largest allowed value
          const tapers = this.getTaperValues();
          const tapering = this.continuumForm.get('fieldTapering');
          if (tapering && !tapers.includes(tapering.value)) {
            tapering.setValue(tapers[tapers.length - 1]);
          }
          this.setEffectiveResolution();
        }
      }
    }
  }

  ngOnInit(): void {
    this.getImageClassName();
    this.getformFieldsDataFromConfig();
    this.initializeFormContPanel();
    // this.suppliedChanged(this.continuumForm.get('fieldSupplied')?.value);
    this.setEffectiveResolution();
  }

  getTaperValues(): number[] {
    if (this._subarrayType) {
      return helpers.configuration.getTaperValuesForSubarray(this.fieldTapering, this._subarrayType.label);
    }
    return [];
  }

  getformFieldsDataFromConfig() {
    if (!this.configuration) {
      this.configuration = this.config.getConfig();
    }

    if (this.configuration && this.configuration.midCalcFormFields != null) {

      this.fieldCentralFrequency = this.configuration.midCalcFormFields.find(
        (e: ConfigurationMidCalculatorFormField) => e.field == 'fieldCentralFrequency'
      )!;
      this.fieldBandwidth = this.configuration.midCalcFormFields.find(
        (e: ConfigurationMidCalculatorFormField) => e.field == 'fieldBandwidth'
      )!;
      this.fieldResolution = this.configuration.midCalcFormFields.find(
        (e: ConfigurationMidCalculatorFormField) => e.field == 'fieldResolution'
      )!;
      this.fieldNoOfSubBands = this.configuration.midCalcFormFields.find(
        (e: ConfigurationMidCalculatorFormField) => e.field == 'fieldNoOfSubBands'
      )!;
      this.fieldSupplied = this.configuration.midCalcFormFields.find(
        (e: ConfigurationMidCalculatorFormField) => e.field == 'fieldSupplied'
      )!;
      this.fieldIntegrationTime = this.configuration.midCalcFormFields.find(
        (e: ConfigurationMidCalculatorFormField) =>
          e.field == 'fieldIntegrationTime'
      )!;
      this.fieldSensitivity = this.configuration.midCalcFormFields.find(
        (e: ConfigurationMidCalculatorFormField) =>
          e.field == 'fieldSensitivity'
      )!;
      this.fieldImageWeighting = this.configuration.midCalcFormFields.find(
        (e: ConfigurationMidCalculatorFormField) =>
          e.field == 'fieldImageWeighting'
      )!;
      this.fieldTapering = this.configuration.midCalcFormFields.find(
        (e: ConfigurationMidCalculatorFormField) =>
          e.field == 'fieldTapering'
      )!;
      this.fieldRobust = this.configuration.midCalcFormFields.find(
        (e: ConfigurationMidCalculatorFormField) => e.field == 'fieldRobust'
      )!;
      this.fieldSpectralAveraging = this.configuration.midCalcFormFields.find(
        (e: ConfigurationMidCalculatorFormField) => e.field === 'fieldSpectralAveraging'
      )!;
      this.fieldEffectiveResolution = this.configuration.midCalcFormFields.find(
        (e: ConfigurationMidCalculatorFormField) => e.field === 'fieldEffectiveResolution'
      )!;

      if (this.fieldSupplied) {
        this.defaultSupplied = this.fieldSupplied.defaultValue.find(
          (e: any) => e.selected
        );
      }

      if (this.fieldImageWeighting) {
        this.defaultSelectedImageWeighting =
          this.fieldImageWeighting.defaultValue.find((e: ConfigurationImageWeightingValue) => e.selected);
      }
    }

    if (this.configuration?.dropDownValues) {
      this.setDefaultFrequencyDropdownOptions();

      if (this.defaultFrequencyDropdownOptions) {
          this.selectedCentralFrequency =
            this.defaultFrequencyDropdownOptions.find(
              (e: ConfigurationDropDownValue) => e.selected
            )!;
          this.selectedBandwidth = this.defaultFrequencyDropdownOptions.find(
            (e: ConfigurationDropDownValue) => e.selected
          )!;
        }

      this.setDefaultTimeDropdownOptions();

      if (this.defaultTimeDropdownOptions) {
        this.selectedIntegrationTime = this.defaultTimeDropdownOptions.find(
          (e: ConfigurationDropDownValue) => e.selected
        )!;
      }

      this.setDefaultSensitivityDropdownOptions();

      if (this.defaultSensitivityDropdownOptions) {
        this.selectedSensitivity = this.defaultSensitivityDropdownOptions.find(
          (e: ConfigurationDropDownValue) => e.selected
        )!;
      }
    }

    if (this.configuration?.spectralAveragingValues) {
      this.spectralAveragingOptions = this.configuration.spectralAveragingValues;
      this.defaultSpectralAveraging = this.spectralAveragingOptions.find((e: ConfigurationSpectralAvgDropDownValue) => e.selected)!;
    }
  }

  setDefaultSensitivityDropdownOptions() {
    // We want to list the Janskys in order then the Kelvins
    if (this.configuration?.dropDownValues) {
      const janskyOptions = this.configuration.dropDownValues
        .filter((e: ConfigurationDropDownValue) => e.type === 'sensitivity' && e.value.includes("Jy/beam"))
        .sort(this.unitMagnitudeCompareFn);

      const kelvinOptions = this.configuration.dropDownValues
        .filter((e: ConfigurationDropDownValue) => e.type === 'sensitivity' && e.value.includes("K"))
        .sort(this.unitMagnitudeCompareFn);

      this.defaultSensitivityDropdownOptions = janskyOptions.concat(kelvinOptions);
    }
  }

  setDefaultFrequencyDropdownOptions() {
    if (this.configuration?.dropDownValues) {
      this.defaultFrequencyDropdownOptions = this.configuration.dropDownValues
        .filter((e: ConfigurationDropDownValue) => e.type === 'frequency')
        .sort(this.unitMagnitudeCompareFn);
    }
  }

  setDefaultTimeDropdownOptions() {
    if (this.configuration?.dropDownValues) {
      this.defaultTimeDropdownOptions = this.configuration.dropDownValues
        .filter((e: ConfigurationDropDownValue) => e.type === 'time')
        .sort(this.unitMagnitudeCompareFn);
    }
  }

  unitMagnitudeCompareFn(a: ConfigurationDropDownValue, b: ConfigurationDropDownValue) {
    // This function is used to sort the units in descending order, taking the inverse of the
    // multiplier if the operator is division.
    const first = a.operator == "/" ? (1 / (a.multiplier ?? 1)) : (a.multiplier ?? 1);
    const second = b.operator == "/" ? (1 / (b.multiplier ?? 1)) : (b.multiplier ?? 1);
    return second - first;
  }

  initializeFormContPanel() {
    this.continuumForm = this.fb.group(
      {
        fieldCentralFrequency: new UntypedFormControl(
          this.contFreqInput ? this.contFreqInput : null,
          [
            Validators.required,
            this.validatorService.centralFrequencyValidator(
              this.selectedCentralFrequency,
              this._observingMode,
              this._subarrayType
            )
          ]
        ),
        fieldBandwidth: new UntypedFormControl(
          this.fieldBandwidth ? this.fieldBandwidth.defaultValue : null,
          [Validators.required]
        ),
        fieldResolution: new UntypedFormControl(
          this.fieldResolution ? this.fieldResolution.defaultValue : null
        ),
        fieldNoOfSubBands: new UntypedFormControl(
          this.fieldNoOfSubBands ? this.fieldNoOfSubBands.defaultValue.value : null,
          [
            this.validatorService.validatorInteger(),
            this.validatorService.validatorNumberMinMax(
              this.fieldNoOfSubBands.defaultValue.min,
              this.fieldNoOfSubBands.defaultValue.max
            )
          ]
        ),
        fieldSpectralAveraging: new UntypedFormControl(
          this.defaultSpectralAveraging
            ? this.defaultSpectralAveraging.value
            : null
        ),
        fieldEffectiveResolution: new UntypedFormControl(
          null,
          []
        ),
        fieldImageWeighting: new UntypedFormControl(
          this.defaultSelectedImageWeighting
            ? this.defaultSelectedImageWeighting.value
            : null
        ),
        fieldTapering: new UntypedFormControl(0, [Validators.required]),
        fieldRobust: new UntypedFormControl(0),
        fieldSupplied: new UntypedFormControl(
          this.defaultSupplied ? this.defaultSupplied.value : null
        ),
        fieldIntegrationTime: new UntypedFormControl(
          this.fieldIntegrationTime
            ? this.fieldIntegrationTime.defaultValue
            : null,
          [Validators.required, this.validatorService.validatorGeneral()]
        ),
        fieldSensitivity: new UntypedFormControl(
          this.fieldSensitivity ? this.fieldSensitivity.defaultValue : null,
          [Validators.required, this.validatorService.validatorGeneral()]
        )
      },
      {
        validators: [
          this.validatorService.midContinuumBandwidthValidator(
            this.selectedCentralFrequency,
            this.selectedBandwidth,
            this._observingMode,
            this._subarrayType
          ),
          this.validatorService.subBandBandwidthValidator(this.selectedBandwidth)
        ]
      }
    );
  }

  updateFrequencyAndBandwidthValidity() {
    this.continuumForm.get('fieldCentralFrequency')?.setValidators(
      this.validatorService.centralFrequencyValidator(
        this.selectedCentralFrequency,
        this._observingMode,
        this._subarrayType
      )
    );
    this.continuumForm.get('fieldCentralFrequency')?.markAsTouched();
    this.continuumForm.get('fieldCentralFrequency')?.updateValueAndValidity();

    // Only validate bandwidth if central frequency is valid
    if (this.continuumForm.get('fieldCentralFrequency')?.valid) {
      this.updateBandwidthValidity();
    }
    else {
      this.continuumForm.get('fieldBandwidth')?.setErrors(null);
    }
  }

  updateBandwidthValidity() {
    this.continuumForm.setValidators([
      this.validatorService.midContinuumBandwidthValidator(
        this.selectedCentralFrequency,
        this.selectedBandwidth,
        this._observingMode,
        this._subarrayType
      ),
      this.validatorService.subBandBandwidthValidator(this.selectedBandwidth)
    ]);
    this.continuumForm.get('fieldBandwidth')?.markAsTouched();
    this.continuumForm.updateValueAndValidity();
  }

  // TODO: Consider changing this field name to better reflect the fact this actual represents the Units drop down field
  selectCentralFrequency(selectedFrequency: ConfigurationDropDownValue) {
    this.selectedCentralFrequency = selectedFrequency;
    if (this.continuumForm) {
      this.continuumForm.get('fieldCentralFrequency')?.markAsTouched();
      this.updateFrequencyAndBandwidthValidity();
    }
  }

  // TODO: Consider changing this field name to better reflect the fact this actual represents the Units drop down field
  selectBandwidth(selectedBandwidth: ConfigurationDropDownValue) {
    this.selectedBandwidth = selectedBandwidth;
    if (this.continuumForm) {
      this.continuumForm.get('fieldBandwidth')?.markAsTouched();
      this.updateBandwidthValidity();
    }
  }

  selectIntegrationTime(selectedIntegrationTime: ConfigurationDropDownValue) {
    this.paramChangeContPanel();
    this.selectedIntegrationTime = selectedIntegrationTime;
    this.continuumForm.get('fieldIntegrationTime')?.markAsTouched();
  }

  selectSensitivity(selectedSelected: ConfigurationDropDownValue) {
    this.paramChangeContPanel();
    this.selectedSensitivity = selectedSelected;
    this.continuumForm.get('fieldSensitivity')?.markAsTouched();
  }

  suppliedChangedContPanel(value: any): void {
    const disableSensitivity = () => {
      this.continuumForm.get('fieldIntegrationTime')?.enable();
      this.continuumForm.get('fieldSensitivity')?.disable();
      const defaultSens = this.configuration!.midCalcFormFields!.find(e => e.field === 'fieldSensitivity')!.defaultValue;
      this.continuumForm.get('fieldSensitivity')?.setValue(defaultSens);
    };
    const disableIntegrationTime = () => {
      this.continuumForm.get('fieldSensitivity')?.enable();
      this.continuumForm.get('fieldIntegrationTime')?.disable();
      const defaultInt = this.configuration!.midCalcFormFields!.find(e => e.field === 'fieldIntegrationTime')!.defaultValue;
      this.continuumForm.get('fieldIntegrationTime')?.setValue(defaultInt);
    };

    if (value === 'IntegrationTime') {
      disableSensitivity();
      this.output.suppliedChangedContResult('IntegrationTime');
    } else {
      disableIntegrationTime();
      this.output.suppliedChangedContResult('Sensitivity');
    }
  }

  weightingChanged(value: any): void {
    this.output.paramChangeContResult();

    const showRobustOption = this.fieldImageWeighting.defaultValue.find(
      (e: any) => e.robust
    );
    if (showRobustOption && showRobustOption.value === value) {
      this.showRobustness = true;
    } else {
      this.showRobustness = false;
    }

    this.getImageClassName();
  }

  continuumBandwidthChanged(): void {
    this.output.paramChangeContResult();
    // If the bandwidth changes we need to revalidate the sub-band bandwidth
    this.continuumForm.get('fieldNoOfSubBands')?.updateValueAndValidity();
  }

  getErrorMessage(fieldName: string) {
    const control: AbstractControl | null = this.continuumForm.get(fieldName);
    return this.validatorService.getErrorMessage(control);
  }

  resetContinuumForm(): void {
    this.getformFieldsDataFromConfig();
    this.continuumForm.reset(this.continuumFormInitialValues);
    this.suppliedChangedContPanel(this.defaultSupplied.value);
    this.weightingChanged(this.defaultSelectedImageWeighting.value);
    this.continuumForm.markAsUntouched();
    this.output.resetContResult();
  }

  getContinuumFormData(): ContinuumFormData {
    this.continuumForm.updateValueAndValidity();

    let centralFreqScaled = this.continuumForm.get(
      'fieldCentralFrequency'
    )?.value;
    if (
      this.selectedCentralFrequency?.multiplier &&
      this.selectedCentralFrequency?.operator
    ) {
      centralFreqScaled = this.validatorService.getScaledValue(
        this.continuumForm.get('fieldCentralFrequency')?.value,
        this.selectedCentralFrequency?.multiplier,
        this.selectedCentralFrequency?.operator
      );
    }

    let bandwidthScaled = this.continuumForm.get('fieldBandwidth')?.value;
    if (
      this.selectedBandwidth?.multiplier &&
      this.selectedBandwidth?.operator
    ) {
      bandwidthScaled = this.validatorService.getScaledValue(
        this.continuumForm.get('fieldBandwidth')?.value,
        this.selectedBandwidth?.multiplier,
        this.selectedBandwidth?.operator
      );
    }

    let integrationTimeScaled = this.continuumForm.get(
      'fieldIntegrationTime'
    )?.value;
    if (
      this.selectedIntegrationTime?.multiplier &&
      this.selectedIntegrationTime?.operator
    ) {
      integrationTimeScaled = this.validatorService.getScaledValue(
        this.continuumForm.get('fieldIntegrationTime')?.value,
        this.selectedIntegrationTime?.multiplier,
        this.selectedIntegrationTime?.operator
      );
    }

    let sensitivityScaled = this.continuumForm.get('fieldSensitivity')?.value;
    if (
      this.selectedSensitivity?.multiplier &&
      this.selectedSensitivity?.operator
    ) {
      sensitivityScaled = this.validatorService.getScaledValue(
        this.continuumForm.get('fieldSensitivity')?.value,
        this.selectedSensitivity?.multiplier,
        this.selectedSensitivity?.operator
      );
    }

    const spectralResolution = (this.continuumForm.get('fieldResolution')!.value).split(' ')[0];
    const spectralAveragingFactor = this.continuumForm.get('fieldSpectralAveraging')?.value;

    const formData: ContinuumFormData = {
      frequency: centralFreqScaled,
      bandwidth: bandwidthScaled,
      n_subbands: this.continuumForm.get('fieldNoOfSubBands')?.value,
      weighting: this.continuumForm.get('fieldImageWeighting')?.value,
      calculator_mode: 'continuum',
      taper: this.continuumForm.get('fieldTapering')?.value,
      selectedSensitivityUnit:  this.selectedSensitivity.value,
      spectralResolution,
      spectralAveragingFactor
    };

    const briggsVal = this.configuration!.midCalcFormFields!.find((e: any) => e.field === 'fieldImageWeighting')!.defaultValue.find((f: any) => f.option === 'Briggs').value;
    if (formData.weighting === briggsVal) {
      formData.robustness = this.continuumForm.get('fieldRobust')?.value;
    }

    if (this.continuumForm.get('fieldIntegrationTime')?.enabled) {
      formData.integrationTime = integrationTimeScaled;
    } else if (this.continuumForm.get('fieldSensitivity')?.enabled) {
      formData.sensitivity = sensitivityScaled;
    }
    return formData;
  }

  getFieldSupplied(inputData: any): string {
    if (!inputData?.integration_time) {
      return this.continuumForm.get('fieldSupplied')?.value;
    } else {
      return 'IntegrationTime';
    }
  }

  setEffectiveResolution(): void {
    const spectralAverage = this.continuumForm.get('fieldSpectralAveraging')?.value;
    const resolution = +(this.continuumForm.get('fieldResolution')!.value).split(' ')[0];
    const centralFreq = this.validatorService.getScaledValue(this.continuumForm.get('fieldCentralFrequency')!.value, this.selectedCentralFrequency.multiplier!, this.selectedCentralFrequency.operator!);
    const velocity = '(' + helpers.calculate.calculateVelocity(resolution * spectralAverage * 1000, centralFreq) + ')';
    this.continuumForm.get('fieldEffectiveResolution')?.setValue(`${(spectralAverage * resolution)} kHz ${velocity}`);
  }

  spectralAveragingChanged(spectralAverage: any): void {
    this.output.paramChangeContResult();

    const resolution = +(this.continuumForm.get('fieldResolution')!.value).split(' ')[0];
    const centralFreq = this.validatorService.getScaledValue(this.continuumForm.get('fieldCentralFrequency')!.value, this.selectedCentralFrequency.multiplier!, this.selectedCentralFrequency.operator!);
    const velocity = '(' + helpers.calculate.calculateVelocity(resolution * spectralAverage * 1000, centralFreq) + ')';
    this.continuumForm.get('fieldEffectiveResolution')?.setValue(`${(spectralAverage * resolution)} kHz ${velocity}`);
  }

  getResolutionDisplayValue(resolution: any): string {
    const centralFreq = this.validatorService.getScaledValue(this.continuumForm.get('fieldCentralFrequency')!.value, this.selectedCentralFrequency.multiplier!, this.selectedCentralFrequency.operator!);
    const resolutionValue = resolution.split(' ')[0] * 1000;
    const velocity = '(' + helpers.calculate.calculateVelocity(resolutionValue, centralFreq) + ')';
    return velocity;
  }

  loadingResults(show: boolean): void {
    this.output.loadingResults(show);
  }

  getScaledFrequency(): number {
    if (this.continuumForm && this.selectedCentralFrequency) {
      const scaledFreq = this.validatorService.getScaledValue(this.continuumForm.get('fieldCentralFrequency')!.value, this.selectedCentralFrequency.multiplier!, this.selectedCentralFrequency.operator!);
      return scaledFreq / 1000000000;
    }
    return 0;
  }

  disableWeightingFields() {
    // In custom array mode, weighting should be set to natural and disabled as we can't calculate
    // the weighting factors, there should be no tapering, and sensitivities should only be available in Jy/beam.
    this.continuumForm.get('fieldImageWeighting')?.setValue('natural');
    this.weightingChanged('natural');
    this.continuumForm.get('fieldImageWeighting')?.disable();
    this.continuumForm.get('fieldTapering')?.setValue(0);
    this.continuumForm.get('fieldTapering')?.disable();
    this.defaultSensitivityDropdownOptions = this.configuration!.dropDownValues!
      .filter((e: ConfigurationDropDownValue) => e.type === 'sensitivity' && !e.value.includes('K'))
      .sort(this.unitMagnitudeCompareFn);
    // Set the selected unit back to Jy/beam, incase it was in K before the custom array was selected
    this.selectedSensitivity = this.defaultSensitivityDropdownOptions.find((e: ConfigurationDropDownValue) => e.selected)!;
  }

  enableWeightingFields() {
    this.continuumForm.get('fieldImageWeighting')?.enable();
    this.continuumForm.get('fieldTapering')?.enable();
    this.setDefaultSensitivityDropdownOptions();
    // Call weightingChanged to ensure the robustness and style is correct
    this.weightingChanged(this.continuumForm.get('fieldImageWeighting')?.value);
  }
}
