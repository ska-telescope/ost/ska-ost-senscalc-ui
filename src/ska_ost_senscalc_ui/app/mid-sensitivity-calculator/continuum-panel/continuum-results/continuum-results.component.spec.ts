import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ContinuumResultsComponent } from './continuum-results.component';
import { MatCardModule } from '@angular/material/card';

describe('ContinuumResultsComponent', () => {
  let component: ContinuumResultsComponent;
  let fixture: ComponentFixture<ContinuumResultsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ContinuumResultsComponent],
      imports: [MatCardModule]
    }).compileComponents();

    fixture = TestBed.createComponent(ContinuumResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('When created', () => {
    it('has the expected default values', () => {
      expect(component).toBeTruthy();
      expect(component.parametersChanged).toBe(false);
      expect(component.integrationTimeSupplied).toBe(true);
      expect(component.continuumResults).toBe(undefined);
    });
  });

  it('loading results should indicate loading', () => {
    component.loadingResults(true);
    expect(component.resultsHeaderMsg).toBe('Calculating Results...');
  });

  it('param change should set warning flag', () => {
    component.continuumResults = {warnings: {}};
    component.parametersChanged = false;
    component.paramChangeContResult();
    expect(component.parametersChanged).toBe(true);
  });

  it('supplied changes continuum result should register viewing mode', () => {
    component.integrationTimeSupplied = false;
    component.suppliedChangedContResult('IntegrationTime');
    expect(component.integrationTimeSupplied).toBe(true);
    component.integrationTimeSupplied = true;
    component.suppliedChangedContResult('Sensitivity');
    expect(component.integrationTimeSupplied).toBe(false);
  });

});
