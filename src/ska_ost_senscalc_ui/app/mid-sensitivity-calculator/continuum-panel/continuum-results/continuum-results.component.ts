import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import helpers from "../../../shared/utils/helpers";
import constants from "../../../shared/utils/constants";
import {ContinuumResults} from "../../../core/types";

@Component({
  selector: 'app-continuum-results',
  templateUrl: './continuum-results.component.html',
  styleUrls: ['./continuum-results.component.scss']
})
export class ContinuumResultsComponent implements OnInit, OnChanges {

  @Input() nSubBands!: number;
  @Input() continuumResults?: ContinuumResults | null;

  differentIntegrationTimeMessage = constants.messages.differentIntegrationTime;

  resultsHeaderMsg = helpers.result.header(false);
  integrationTimeSupplied = true; // Flag is true if time is supplied, ie if sensitivity is being calculated
  parametersChanged = false; // Message to say the params have changed since the previous results were calculated
  // The sensLimitReached warning should be hidden if the user changes the 'Supplied' drop down
  // but we can't just overwrite the sensLimitReached variable to false as we need to retain the info
  // in case the user switches back, hence the need for this variable
  suppliedSwitched!: boolean;

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['continuumResults']) {
      this.resetControlVariables();
    }
  }

  resetContResult(): void {
    this.continuumResults = undefined;
    this.resetControlVariables();
  }

  resetControlVariables() {
    this.suppliedSwitched = false;
    this.parametersChanged = false;
    this.loadingResults(false);
  }

  /**
   * This method is called when the user changes whether they want to calculate sensitivity by supplying an integration time
   * or integration time by supplying a sensitivity. It handles any resetting of fields, etc.
   *
   * @param value - the field the user is supplying, either IntegrationTime or Sensitivity
   **/
  suppliedChangedContResult(value: string) {
    // Switch result values that are displayed.
    this.integrationTimeSupplied = value === 'IntegrationTime';
    // The aim is that that changing the 'Supplied' field will flip this bool,
    // and changing the field back to the original value will leave it in the original state
    this.suppliedSwitched = !this.suppliedSwitched;
  }

  paramChangeContResult() {
    // Only want to warn after a calculation has been made and not
    // while user is entering at the start of a new calculation.
    if (this.continuumResults) {
      this.parametersChanged = true;
    }
  }

  /**
   * This method defines the behaviour when the results are loading, ie the UI is waiting for a
   * response from the API, and a message should be displayed
   *
   * @param loading Set to true if the results are currently loading and false if not.
   **/
  loadingResults(loading: boolean): void {
    this.resultsHeaderMsg = helpers.result.header(loading);
  }

  sensLimitWarning() {
    return helpers.messages.sensLimitWarning(this.continuumResults!.warnings!.sensLimitReached!.value);
  }
}
