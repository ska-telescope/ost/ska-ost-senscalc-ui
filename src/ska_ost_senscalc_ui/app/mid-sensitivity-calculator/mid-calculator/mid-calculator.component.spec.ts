import {BehaviorSubject} from 'rxjs';
import { Component, Input} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  UntypedFormBuilder,
  FormsModule,
  ReactiveFormsModule,
  FormBuilder,
  FormGroup
} from "@angular/forms";
import { MatMenuModule } from '@angular/material/menu';
import { HarnessLoader} from "@angular/cdk/testing";
import { MatSnackBarHarness } from "@angular/material/snack-bar/testing";
import { MatSelectHarness } from "@angular/material/select/testing";
import { MatButtonHarness } from "@angular/material/button/testing";
import { MatInputHarness } from "@angular/material/input/testing";
import { MatCardModule} from "@angular/material/card";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatFormFieldHarness } from "@angular/material/form-field/testing";
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AdvancedModeComponent } from '../advanced-mode/advanced-mode.component';
import { MatSelectModule } from "@angular/material/select";
import { MatInputModule} from "@angular/material/input";
import { TestbedHarnessEnvironment } from "@angular/cdk/testing/testbed";

import {AppConfiguration, ArrayDropDownValue} from '../../app-configuration';

import { MidCalculatorComponent } from './mid-calculator.component';
import * as config from '../../../assets/configuration.json';
import { MidCalculatorService } from '../../core/mid-calculator.service';
import { MidSubarrayService } from "../../core/mid-subarray.service";
import { MidSubarraysResponse} from "../../generated/api/mid";
import { ContinuumPanelComponent } from "../continuum-panel/continuum-panel.component";
import { ZoomPanelComponent } from "../zoom-panel/zoom-panel.component";
import { MatExpansionPanelHarness } from "@angular/material/expansion/testing";
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

describe('MidCalculatorComponent', () => {
  let component: MidCalculatorComponent;
  let loader: HarnessLoader;
  let fixture: ComponentFixture<MidCalculatorComponent>;
  const formBuilder: UntypedFormBuilder = new UntypedFormBuilder();
  let service: MockService;

  // apiService is deprecated, to be replaced by the OpenAPI code generated client (midApi) soon
  // apiService is used for sensitivity calculation requests
  // midApi is used for subarrays and weighting requests
  let midApi: MockMidApi;

  beforeEach(async () => {
    midApi = new MockMidApi();
    service = new MockService();
    await TestBed.configureTestingModule({
      declarations: [
        MidCalculatorComponent,
        ContinuumPanelStubComponent,
        AdvancedModeStubComponent,
        ZoomPanelStubComponent
      ],
      imports: [
        FormsModule,
        HttpClientModule,
        MatButtonToggleModule,
        MatCardModule,
        MatExpansionModule,
        MatInputModule,
        MatMenuModule,
        MatSelectModule,
        MatSnackBarModule,
        NoopAnimationsModule,
        MatSlideToggleModule,
        ReactiveFormsModule
      ],
      providers: [
        AppConfiguration,
        { provide: UntypedFormBuilder, useValue: formBuilder },
        { provide: MidSubarrayService, useValue: midApi },
        { provide: MidCalculatorService, useValue: service }]
    }).compileComponents();
  });

  beforeEach(() => {

    fixture = TestBed.createComponent(MidCalculatorComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    component.configuration = config;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  const dummyArray = {
    name: 'foo',
    label: 'bar',
    n_ska: 0,
    n_meer: 0
  };

  describe('when restrictedFrequencyMessage() is called', () => {
    it.each([
      {band: 'Band 1',  subarray: {...dummyArray, n_meer: 1}, expected: '13.5-m antennas only cover 0.58-1.015 GHz'},
      {band: 'Band 2',  subarray: {...dummyArray, n_meer: 1}, expected: '13.5-m antennas only cover 0.95-1.67 GHz'},
      {band: 'Band 1',  subarray: {...dummyArray, n_meer: 0}, expected: ''},
      {band: 'Band 2',  subarray: {...dummyArray, n_meer: 0}, expected: ''},
      {band: 'Band 5a', subarray: {...dummyArray, n_meer: 1}, expected: ''},
      {band: 'Band 5b', subarray: {...dummyArray, n_meer: 1}, expected: ''},
      {band: 'Band 5a', subarray: {...dummyArray, n_meer: 0}, expected: ''},
      {band: 'Band 5b', subarray: {...dummyArray, n_meer: 0}, expected: ''}
    ])('returns "$expected" for band="$band" with $subarray.n_meer MeerKAT dishes.', ({band, subarray, expected}) => {
      component.onObservingModeChange(band);
      component.selectedSubarray = subarray;
      expect(component.restrictedFrequencyMessage()).toEqual(expected);
    });
  });

  describe('when isSharedArray is called', () => {
    it('returns true for a full SKA+MeerKAT subarray', () => {
      const subarray = {
        name: 'foo',
        label: 'bar',
        n_ska: 133,
        n_meer: 64
      };
      component.selectedSubarray = subarray;
      expect(component.isSharedArray()).toBeTruthy();
    });

    it('returns true for any mixed subarray with one or more SKA dishes and MeerKAT dishes', () => {
      const subarray = {
        name: 'foo',
        label: 'bar',
        n_ska: 1,
        n_meer: 1
      };
      component.selectedSubarray = subarray;
      expect(component.isSharedArray()).toBeTruthy();
    });

    it('returns false for a pure SKA subarray', () => {
      const subarray = {
        name: 'foo',
        label: 'bar',
        n_ska: 1,
        n_meer: 0
      };
      component.selectedSubarray = subarray;
      expect(component.isSharedArray()).toBeFalsy();
    });

    it('returns false for a pure MeerKAT subarray', () => {
      const subarray = {
        name: 'foo',
        label: 'bar',
        n_ska: 0,
        n_meer: 1
      };
      component.selectedSubarray = subarray;
      expect(component.isSharedArray()).toBeFalsy();
    });

    it('returns false for an empty subarray', () => {
      const subarray = {
        name: 'foo',
        label: 'bar',
        n_ska: 0,
        n_meer: 0
      };
      component.selectedSubarray = subarray;
      expect(component.isSharedArray()).toBeFalsy();
    });
  });

  describe('when isMeerKATAllowed is called',() => {
    it.each(['Band 1', 'Band 2'])('return true for %s', (band) => {
      component.form.get('fieldObservingMode')?.setValue(band);
      expect(component.isMeerKATAllowed()).toBeTruthy();
    });

    it.each(['Band 5a', 'Band 5b'])('return false for %s', (band) => {
      component.form.get('fieldObservingMode')?.setValue(band);
      expect(component.isMeerKATAllowed()).toBeFalsy();
    });
  });

  it('change observing mode subject', () => {
    const newMode = 'MODE_A';
    component.onObservingModeChange(newMode);
    service.getObservingMode().subscribe(data => {
      expect(data).toBe(newMode);
    });
  });

  it('reset form', () => {
    component.form.get('fieldWeatherPWV')?.setValue(5);
    component.form.updateValueAndValidity();
    expect(component.form.get('fieldWeatherPWV')?.value).toBe(5);
    component.onReset();
    expect(component.form.get('fieldWeatherPWV')?.value).toBe(10);
  });

  it('no configuration available', () => {
    component.configuration = undefined;
    component.getDataFromConfiguration();
    expect(component.configuration).toBe(undefined);
  });

  it('show error modal', async () => {
    // disable snackbar duration. See https://github.com/angular/components/issues/19290
    fixture.componentInstance['snackbarDuration'] = -1;

    // use the root loader to get the harnesses for the entire document
    // because the snack bar is rendered outside of the component
    const rootLoader = TestbedHarnessEnvironment.documentRootLoader(fixture);

    const errorMessage = 'foo';
    component.showErrorModal(errorMessage);
    const dialog = await rootLoader.getHarness<MatSnackBarHarness>(
      MatSnackBarHarness
    );
    expect(await dialog.getMessage()).toContain(errorMessage);
  });

  it('isIncludeConfusionNoise with various parameters should return true/false', () => {
    expect(component.isIncludeConfusionNoise({
      weighting: 'natural',
      taper: 0,
      array_configuration: "AA4"
    })).toBe(false);

    expect(component.isIncludeConfusionNoise({
      weighting: 'natural',
      taper: 1,
      array_configuration: "custom"
    })).toBe(false);

    expect(component.isIncludeConfusionNoise({
      weighting: 'robust',
      robustness: 2,
      taper: 0,
      array_configuration: "AA4"
    })).toBe(false);

    expect(component.isIncludeConfusionNoise({
      weighting: 'robust',
      robustness: 2,
      taper: 0,
      array_configuration: "AA4"
    })).toBe(false);

    expect(component.isIncludeConfusionNoise({
      weighting: 'uniform',
      taper: 0,
      array_configuration: "AA4"
    })).toBe(true);
  });

  it('setupFormGroup should create form object', () => {
    component.setupFormGroup();
    expect(component.form).toBeTruthy();
  });

  it('showErrorModal', () => {
    component.showErrorModal('error message');
    expect(component.snackbar).toBeTruthy();
  });

  describe('When the page loads', () => {
    it('displays an error modal if the subarray query fails', async () => {
      // disable snackbar duration. See https://github.com/angular/components/issues/19290
      fixture.componentInstance["snackbarDuration"] = -1;

      midApi.subQueryFailed$.next(true);

      // use the root loader to get the harnesses for the entire document
      // because the snack bar is rendered outside of the component
      const rootLoader = TestbedHarnessEnvironment.documentRootLoader(fixture);

      const dialog = await rootLoader.getHarness<MatSnackBarHarness>(MatSnackBarHarness);
      expect(await dialog.getMessage()).toMatch(/Communication error/i);
    });
  });

  describe('Disable Calculate button when', () => {
    let calculateButton: MatButtonHarness;

    beforeEach(async () => {
      calculateButton = await loader.getHarness(
        MatButtonHarness.with({text: 'Calculate'})
      );

      // Set up the common form so that it is valid for the
      // start of each test
      const subarrays: MidSubarraysResponse[] = [
        {
          name: 'foo',
          label: 'bar',
          n_meer: 10,
          n_ska: 20
        }
      ];
      midApi.subarrays$.next(subarrays);
      component.setupFormGroup();
      component.initialiseSubarrays();
    });

    it('there is an invalid field in the common form', async () => {
      const continuumPanel = await loader.getHarness(
        MatExpansionPanelHarness.with({selector: "#continuumExpansionPanel"})
      );
      await continuumPanel.expand();
      expect(await continuumPanel.isExpanded()).toBeTruthy();
      expect(await calculateButton.isDisabled()).toBeFalsy();
      const raFormField = await loader.getHarness(
        MatInputHarness.with({ selector: "#txtRightAscension"})
      );
      await raFormField.setValue("0:0:0:0:0");
      expect(await calculateButton.isDisabled()).toBeTruthy();
    });

    it('there is an invalid field in the continuum form', async () => {
      const continuumPanel = await loader.getHarness(
        MatExpansionPanelHarness.with({selector: "#continuumExpansionPanel"})
      );
      await continuumPanel.expand();
      expect(await continuumPanel.isExpanded()).toBeTruthy();
      expect(await calculateButton.isDisabled()).toBeFalsy();

      // set an error on the continuum form
      component.contPanel?.continuumForm.setErrors({required: true});
      expect(await calculateButton.isDisabled()).toBeTruthy();
    });

    it('there is an invalid field in the zoom form', async () => {
      const zoomPanel = await loader.getHarness(
        MatExpansionPanelHarness.with({selector: "#zoomExpansionPanel"})
      );
      await zoomPanel.expand();
      expect(await zoomPanel.isExpanded()).toBeTruthy();
      expect(await calculateButton.isDisabled()).toBeFalsy();

      // set an error on the continuum form
      component.zoomPanel?.lineForm.setErrors({required: true});
      expect(await calculateButton.isDisabled()).toBeTruthy();
    });
  });

  describe('When the subarray selector is presented', () => {
    let selectHarness: MatSelectHarness;
    let resetButton: MatButtonHarness;
    let nskaHarness: MatInputHarness;
    let nmeerHarness: MatInputHarness;
    let skaFormField: MatFormFieldHarness;
    let meerkatFormField: MatFormFieldHarness;

    beforeEach(async () => {
      selectHarness = await loader.getHarness(
        MatSelectHarness.with({selector: "#midSubarraySelect"})
      );
      resetButton = await loader.getHarness(
        MatButtonHarness.with({text: 'Reset'})
      );
      nskaHarness = await loader.getHarness(
        MatInputHarness.with({selector: '[name="fieldnSKA"]'})
      );
      skaFormField = await loader.getHarness(
        MatFormFieldHarness.with({ selector: '[title="Number of 15-m antennas"]'})
      );
      nmeerHarness = await loader.getHarness(
        MatInputHarness.with({selector: '[name="fieldnMeer"]'})
      );
      meerkatFormField = await loader.getHarness(
        MatFormFieldHarness.with({ selector: '[title="Number of 13.5-m antennas"]'})
      );
    });

    it('presents subarrays by label', async () => {
      const subarrays: MidSubarraysResponse[] = [
        {
          name: 'foo',
          label: 'bar',
          n_meer: 10,
          n_ska: 20
        }
      ];
      midApi.subarrays$.next(subarrays);

      await selectHarness.open();
      const options = await selectHarness.getOptions();
      const labels = await Promise.all(options.map(async (o) => o.getText()));
      expect(labels).toEqual(expect.arrayContaining(['bar']));
    });

    it('selects the AA4 subarray by default', async () => {
      midApi.subarrays$.next([
        {
          name: 'foo',
          label: 'foo',
          n_meer: 10,
          n_ska: 20
        },
        {
          name: 'MID_AA4_all',
          label: 'bar',
          n_meer: 10,
          n_ska: 20
        },
        {
          name: 'baz',
          label: 'baz',
          n_meer: 10,
          n_ska: 20
        }
      ]);

      const selected = await selectHarness.getValueText();
      expect(selected).toEqual('bar');
    });

    it.each([
      {
        name: 'MID_AA05_all',
        label: 'label 1',
        n_ska: 1, n_meer: 1
      },
      {
        name: 'MID_AA1_all',
        label: 'label 1',
        n_ska: 1, n_meer: 1
      },
      {
        name: 'MID_AA2_all',
        label: 'label 1',
        n_ska: 1, n_meer: 1
      }
    ])('disables zoom mode for the subarray',  (subarray) => {
        midApi.subarrays$.next([subarray]);

      expect(component.zoomModeDisabled).toBeTruthy();
      expect(component.panelZoomOpened).toBeFalsy();
    });

    it.each(['Band 5a', 'Band 5b'])('selects the AA4 15-m only subarray by default on %s', async (band) => {
      midApi.subarrays$.next([
        {
          name: 'foo',
          label: 'foo',
          n_meer: 0,
          n_ska: 20
        },
        {
          name: 'MID_AA4_all',
          label: 'bar',
          n_meer: 10,
          n_ska: 20
        },
        {
          name: 'MID_AA4_SKA_only',
          label: 'baz',
          n_meer: 0,
          n_ska: 20
        }
      ]);

      const rxSelector = await loader.getHarness<MatSelectHarness>(
        MatSelectHarness.with({ selector: '#midBandSelect' })
      );
      await rxSelector.open();
      await rxSelector.clickOptions({ text: new RegExp(band) });

      const selected = await selectHarness.getValueText();
      expect(selected).toEqual('baz');
    });

    it('puts the array AA* and AA*/AA4 in their correct positions.', () => {
      midApi.subarrays$.next([
        {
          name: 'MID_AAstar_all',
          label: 'bar',
          n_meer: 10,
          n_ska: 20
        },
        {
          name: 'MID_AA2_all',
          label: 'bar',
          n_meer: 10,
          n_ska: 20
        }
      ]);

      expect(component.subarrayOptions[0].name).toEqual('MID_AA2_all');
      expect(component.subarrayOptions[1].name).toEqual('MID_AAstar_all');
    });

    it('resets to the AA4 subarray when AA4 is available', async () => {
      // Overwrite the subarrays set in the 'setValidForm' function
      midApi.subarrays$.next([
        {
          name: 'foo',
          label: 'foo',
          n_meer: 10,
          n_ska: 20
        },
        {
          name: 'MID_AA4_all',
          label: 'bar',
          n_meer: 10,
          n_ska: 20
        },
        {
          name: 'baz',
          label: 'baz',
          n_meer: 10,
          n_ska: 20
        }
      ]);

      await selectHarness.clickOptions({text: 'foo'});
      expect(await selectHarness.getValueText()).toBe('foo');

      expect(await resetButton.isDisabled()).toBe(false);
      await resetButton.click();
      expect(await selectHarness.getValueText()).toBe('bar');
    });

    it('is disabled if the backend query fails', async () => {
      midApi.subQueryFailed$.next(true);
      expect(await selectHarness.isDisabled()).toBe(true);
    });

    it('disables dish widgets unless custom subarray is selected', async () => {
      midApi.subarrays$.next([
        {
          name: 'foo',
          label: 'foo',
          n_meer: 10,
          n_ska: 20
        },
        {
          name: 'Custom',
          label: 'Custom',
          n_meer: 10,
          n_ska: 20
        }
      ]);

      await selectHarness.clickOptions({ text: 'foo' });
      expect(await nmeerHarness.isDisabled()).toBeTruthy();
      expect(await nskaHarness.isDisabled()).toBeTruthy();

      await selectHarness.clickOptions({ text: 'Custom' });
      expect(await nmeerHarness.isDisabled()).toBeFalsy();
      expect(await nskaHarness.isDisabled()).toBeFalsy();
    });

    it('updates dish numbers when a subarray is selected', async () => {
      midApi.subarrays$.next([
        {
          name: 'foo',
          label: 'foo',
          n_meer: 10,
          n_ska: 20
        },
        {
          name: 'bar',
          label: 'bar',
          n_meer: 30,
          n_ska: 40
        }
      ]);

      await selectHarness.clickOptions({ text: 'foo' });
      expect(await nmeerHarness.getValue()).toBe("10");
      expect(await nskaHarness.getValue()).toBe("20");

      await selectHarness.clickOptions({ text: 'bar' });
      expect(await nmeerHarness.getValue()).toBe("30");
      expect(await nskaHarness.getValue()).toBe("40");
    });

    it('displays an error if no receptors are selected', async () => {
      midApi.subarrays$.next([
        {
          name: 'Custom',
          label: 'Custom',
          n_meer: 10,
          n_ska: 20
        }
      ]);

      await selectHarness.open();
      await selectHarness.clickOptions({ text: 'Custom' });
      expect(await skaFormField.hasErrors()).toBeFalsy();
      expect(await meerkatFormField.hasErrors()).toBeFalsy();

      await nmeerHarness.setValue('0');
      expect(await skaFormField.hasErrors()).toBeFalsy();
      expect(await meerkatFormField.hasErrors()).toBeFalsy();

      await nskaHarness.setValue('0');
      await nmeerHarness.setValue('0');
      expect(await skaFormField.hasErrors()).toBeTruthy();
      expect(await meerkatFormField.hasErrors()).toBeTruthy();

      await nskaHarness.setValue('1');
      await nmeerHarness.setValue('0');
      expect(await skaFormField.hasErrors()).toBeTruthy();
      expect(await meerkatFormField.hasErrors()).toBeTruthy();

      await nskaHarness.setValue('0');
      await nmeerHarness.setValue('1');
      expect(await skaFormField.hasErrors()).toBeTruthy();
      expect(await meerkatFormField.hasErrors()).toBeTruthy();
    });

    it('allows receptor type to be zero in XOR fashion', async () => {
      midApi.subarrays$.next([
        {
          name: 'Custom',
          label: 'Custom',
          n_meer: 10,
          n_ska: 20
        }
      ]);

      await selectHarness.clickOptions({ text: 'Custom' });
      expect(await skaFormField.hasErrors()).toBeFalsy();
      expect(await meerkatFormField.hasErrors()).toBeFalsy();

      await nmeerHarness.setValue('1');
      await nskaHarness.setValue('1');
      expect(await skaFormField.hasErrors()).toBeFalsy();
      expect(await meerkatFormField.hasErrors()).toBeFalsy();

      await nmeerHarness.setValue('0');
      await nskaHarness.setValue('2');
      expect(await skaFormField.hasErrors()).toBeFalsy();
      expect(await meerkatFormField.hasErrors()).toBeFalsy();

      await nmeerHarness.setValue('2');
      await nskaHarness.setValue('0');
      expect(await skaFormField.hasErrors()).toBeFalsy();
      expect(await meerkatFormField.hasErrors()).toBeFalsy();
    });
  });

  describe('When observing band selection changes', () => {
    let subarraySelector: MatSelectHarness;
    let rxSelector: MatSelectHarness;

    async function subarrayContainsMeerKATOptions() {
      await subarraySelector.open();
      const subarrayOptions = await subarraySelector.getOptions();
      const meerNumbers = await Promise.all(subarrayOptions.map(async (o) => {
        await o.click();
        if (await o.getText() !== 'Custom') {
          const meerValue = await loader.getHarness<MatInputHarness>(
            MatInputHarness.with({ selector: '[name="fieldnMeer"]' })
          );
          return await meerValue.getValue();
        } else {
          return '0';
        }
      }));
      return !meerNumbers.every(v => v === '0');
    }

    beforeEach(async () => {
      subarraySelector = await loader.getHarness<MatSelectHarness>(
        MatSelectHarness.with({ selector: '#midSubarraySelect' })
      );

      rxSelector = await loader.getHarness<MatSelectHarness>(
        MatSelectHarness.with({ selector: '#midBandSelect' })
      );
      const subarrays: MidSubarraysResponse[] = [
        {
          name: 'foo',
          label: 'foo',
          n_ska: 10,
          n_meer: 0
        },
        {
          name: 'bar',
          label: 'bar',
          n_ska: 0,
          n_meer: 10
        },
        {
          name: 'Custom',
          label: 'Custom',
          n_ska: 10,
          n_meer: 10
        }
      ];
      midApi.subarrays$.next(subarrays);
    });

    it.each(['Band 5a', 'Band 5b'])('excludes sub-arrays with MeerKAT dishes for %s', async (band) => {
      await rxSelector.open();
      await rxSelector.clickOptions({ text: new RegExp(band) });
      expect(await subarrayContainsMeerKATOptions()).toBeFalsy();
      await subarraySelector.open();
      const nSubOptions = (await subarraySelector.getOptions()).length;
      // Result should always include Custom array as well
      expect(nSubOptions).toBe(2);
    });

    it.each(['Band 5a', 'Band 5b'])('includes Custom array but with 0 MeerKAT dishes', async (band) => {
      midApi.subarrays$.next([{
        name: 'Custom',
        label: 'Custom',
        n_ska: 10,
        n_meer: 10
      }]);
      await rxSelector.open();
      await rxSelector.clickOptions({ text: new RegExp(band) });
      expect(await subarrayContainsMeerKATOptions()).toBeFalsy();
      await subarraySelector.open();
      await subarraySelector.clickOptions({ text: 'Custom' });
      const meerKATInput = await loader.getHarness<MatInputHarness>(
        MatInputHarness.with({ selector: '[name="fieldnMeer"]' })
      );
      const SKAInput = await loader.getHarness<MatInputHarness>(
        MatInputHarness.with({ selector: '[name="fieldnSKA"]' })
      );
      expect(await meerKATInput.getValue()).toBe("0");
      expect(await meerKATInput.isDisabled()).toBeTruthy();
      expect(await SKAInput.isDisabled()).toBeFalsy();

      await rxSelector.open();
      await rxSelector.clickOptions({ text: new RegExp('Band 1') });
      expect(await meerKATInput.isDisabled()).toBeFalsy();
      expect(await SKAInput.isDisabled()).toBeFalsy();
    });

    it.each(['Band 1', 'Band 2'])('includes sub-arrays with MeerKAT dishes for %s', async (band) => {
      await rxSelector.open();
      await rxSelector.clickOptions({ text: new RegExp(band) });
      expect(await subarrayContainsMeerKATOptions()).toBeTruthy();
      await subarraySelector.open();
      const nSubOptions = (await subarraySelector.getOptions()).length;
      expect(nSubOptions).toBe(3);
    });
  });

  describe.each(['fieldRightAscension', 'fieldDeclination'])('onRaDecBlur format correctly for %s', (field: string) => {
    it.each(['00:00:00', '00:00:00.', '00', '00:', '00:0', '0:0:0'])('onRaDecBlur format %s to 00:00:00.0', (input: string) => {
      component.form.get(field)?.setValue(input);
      component.onRaDecBlur(field);
      expect(component.form.get(field)?.value).toBe('00:00:00.0');
    });

    it.each(['aaa', '2.'])('onRaDecBlur does not format input %s', (input: string) => {
      component.form.get(field)?.setValue(input);
      component.onRaDecBlur(field);
      expect(component.form.get(field)?.value).toBe(input);
    });
  });

  it('onSexDecChange toggle change', () => {
    component.onSexDecChange({ checked: true });
    expect(component.form.get('fieldRightAscension')?.value).toBe('0.0');
    expect(component.form.get('fieldDeclination')?.value).toBe('0.0');
    component.onSexDecChange({ checked: false });
    expect(component.form.get('fieldRightAscension')?.value).toBe('00:00:00.0');
    expect(component.form.get('fieldDeclination')?.value).toBe('00:00:00.0');
  });

  describe('compareEnteredIntegrationTimes', () => {
    it('should return false if no accordions are open', () => {
      expect(component.compareEnteredIntegrationTimes()).toBe(false);
    });

    it('should return false if only the zoom accordion is open', async () => {
      const zoomPanel = await loader.getHarness(
        MatExpansionPanelHarness.with({selector: "#zoomExpansionPanel"})
      );
      await zoomPanel.expand();
      expect(component.compareEnteredIntegrationTimes()).toBe(false);
    });

    it('should return false if only the continuum accordion is open', async () => {
      const contPanel = await loader.getHarness(
        MatExpansionPanelHarness.with({selector: "#continuumExpansionPanel"})
      );
      await contPanel.expand();
      expect(component.compareEnteredIntegrationTimes()).toBe(false);
    });

    it('should return false if both accordions are open and have the same integration times', async () => {
      const zoomPanel = await loader.getHarness(
        MatExpansionPanelHarness.with({selector: "#zoomExpansionPanel"})
      );
      await zoomPanel.expand();
      const contPanel = await loader.getHarness(
        MatExpansionPanelHarness.with({selector: "#continuumExpansionPanel"})
      );
      await contPanel.expand();
      expect(component.compareEnteredIntegrationTimes()).toBe(false);
    });

    it('should return true if both accordions are open and have different integration times', async () => {
      const zoomPanel = await loader.getHarness(
        MatExpansionPanelHarness.with({selector: "#zoomExpansionPanel"})
      );
      await zoomPanel.expand();
      const contPanel = await loader.getHarness(
        MatExpansionPanelHarness.with({selector: "#continuumExpansionPanel"})
      );
      await contPanel.expand();

      jest
        .spyOn(
          (component as any).contPanel,
          'getContinuumFormData'
        )
        .mockReturnValue({integrationTime: 2});

      expect(component.compareEnteredIntegrationTimes()).toBe(true);
    });
  });
});

class MockMidApi {
  subarrays$ = new BehaviorSubject<MidSubarraysResponse[]>([]);

  subQueryFailed$: BehaviorSubject<boolean> = new BehaviorSubject(false);
}

class MockService extends MidCalculatorService {

}

@Component({
  selector: 'app-advanced-mode',
  template: '',
  providers: [
    {
      provide: AdvancedModeComponent,
      useClass: AdvancedModeStubComponent
    }
  ]
})
class AdvancedModeStubComponent {
  getFormData() {
    return {};
  }
}

@Component({
  selector: 'app-continuum-panel',
  template: '',
  providers: [
    {
      provide: ContinuumPanelComponent,
      useClass: ContinuumPanelStubComponent
    },
    FormBuilder
  ]
})
export class ContinuumPanelStubComponent {
  @Input() observingMode = '';

  @Input() subarrayType: ArrayDropDownValue = {
    name: 'foo',
    label: 'foo',
    n_ska: 10,
    n_meer: 20
  };
  formBuilder: FormBuilder;
  continuumForm: FormGroup;

  constructor() {
    this.formBuilder = new FormBuilder();
    this.continuumForm = this.formBuilder.group({foo: "bar", integration_time: 1});
  }

  paramChangeContPanel() {}

  resetContinuumForm() {}

  clearResultContPanel() {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  loadingResultContPanel(show: boolean) {}

  getContinuumFormData() {
    return {formData: {integration_time: 1}};
  }
}

@Component({
  selector: 'app-zoom-panel',
  template: '',
  providers: [
    {
      provide: ZoomPanelComponent,
      useClass: ZoomPanelStubComponent
    }
  ]
})
export class ZoomPanelStubComponent {
  @Input() observingMode = '';

  @Input() subarrayType: ArrayDropDownValue = {
    name: 'foo',
    label: 'foo',
    n_ska: 10,
    n_meer: 20
  };
  formBuilder: FormBuilder;
  lineForm: FormGroup;

  constructor() {
    this.formBuilder = new FormBuilder();
    this.lineForm = this.formBuilder.group({foo: "bar"});
  }

  paramChange() {}

  displayZoomPanelError() {}

  resetZoomForm() {}

  clearResultContPanel() {}

  getZoomFormData() {
    return {integrationTime: 1};
  }
}
