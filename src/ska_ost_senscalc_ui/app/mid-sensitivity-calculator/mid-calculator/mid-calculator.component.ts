import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {
  AppConfiguration,
  ArrayDropDownValue,
  Configuration,
  ConfigurationDropDownValue,
  ConfigurationMidCalculatorFormField, MidSubarrayId
} from '../../app-configuration';
import { MidCalculatorService } from '../../core/mid-calculator.service';
import {
  AbstractControl,
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators
} from '@angular/forms';
import { ValidatorsService } from '../../core/validators.service';
import { faCaretDown, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import {MidFormInput} from '../types';
import {
  MatSnackBar,
  MatSnackBarRef,
  TextOnlySnackBar
} from '@angular/material/snack-bar';
import { AdvancedModeComponent } from '../advanced-mode/advanced-mode.component';
import { SharedService } from '../../shared/service/shared.service';
import {
  MidSubarrayService
} from '../../core/mid-subarray.service';
import { ContinuumPanelComponent } from '../continuum-panel/continuum-panel.component';
import { ZoomPanelComponent } from '../zoom-panel/zoom-panel.component';
import helpers from '../../shared/utils/helpers';
import constants from '../../shared/utils/constants';
import {catchError, Observable, of, Subscription, tap} from "rxjs";
import { MatSlideToggle } from '@angular/material/slide-toggle';
import {MidCalculationService} from "../../core/mid-calculation.service";
import {ContinuumResults, ZoomResults} from "../../core/types";

@Component({
  selector: 'app-mid-calculator',
  templateUrl: './mid-calculator.component.html',
  styleUrls: ['./mid-calculator.component.scss']
})
export class MidCalculatorComponent
  implements OnInit, AfterViewInit, OnDestroy
{

  continuumResults$?: Observable<ContinuumResults>;
  zoomResults$?: Observable<ZoomResults>;

  configuration?: Configuration;
  form!: UntypedFormGroup;
  selectedObservingMode!: string;

  //form field configuration
  fieldRightAscension!: ConfigurationMidCalculatorFormField;
  fieldRightAscensionDecDef!: ConfigurationMidCalculatorFormField;
  fieldRightAscensionSexDef!: ConfigurationMidCalculatorFormField;
  fieldDeclination!: ConfigurationMidCalculatorFormField;
  fieldDeclinationDecDef!: ConfigurationMidCalculatorFormField;
  fieldDeclinationSexDef!: ConfigurationMidCalculatorFormField;
  fieldObservingMode!: ConfigurationMidCalculatorFormField;
  fieldWeatherPWV!: ConfigurationMidCalculatorFormField;
  fieldElevation!: ConfigurationMidCalculatorFormField;
  fieldArrayConfig!: ConfigurationMidCalculatorFormField;
  fieldnSKA!: ConfigurationMidCalculatorFormField;
  fieldnMeer!: ConfigurationMidCalculatorFormField;

  //defaults
  defaultObservingMode!: any;
  defaultTimeDropdownOptions!: ConfigurationDropDownValue[];
  formInitialValues: any;

  subarrayOptions: ArrayDropDownValue[] = [];
  selectedSubarray: ArrayDropDownValue = {
    name: '',
    label: '',
    n_meer: 0,
    n_ska: 0
  };
  subarrayQueryFailed = false;
  static backendFailureErrorMessage= "Communication error: subarray templates will not be available.";
  // Zoom is only allowed for certain subarrays
  zoomModeDisabled = false;

  //icons
  faCaretDown: IconDefinition = faCaretDown;

  //active snackbar
  snackbar!: MatSnackBarRef<TextOnlySnackBar>;
  panelContinuumOpened!: boolean;
  panelZoomOpened!: boolean;

  toggleAdvancedMode = false;
  toggleSexDecMode = false;

  arrayMessage!: string;

  private subscriptions: Subscription[] = [];

  @ViewChild(ContinuumPanelComponent) contPanel: ContinuumPanelComponent | undefined;
  @ViewChild(ZoomPanelComponent) zoomPanel: ZoomPanelComponent | undefined;
  @ViewChild(AdvancedModeComponent) advancedMode: AdvancedModeComponent | undefined;
  @ViewChild('sexDecToggle') sexDecToggle!: MatSlideToggle;

  // made a property so that tests can override it as dur, setting it to zero
  // See https://github.com/angular/components/issues/19290
  private snackbarDuration = 5000;

  constructor(private config: AppConfiguration,
              private service: MidCalculatorService,
              private calculationService: MidCalculationService,
              private validators: ValidatorsService,
              private fb: UntypedFormBuilder,
              private snackBar: MatSnackBar,
              private sharedService: SharedService,
              private midApi: MidSubarrayService) {
                this.arrayMessage = constants.messages.arrayLoadFail;
              }

  ngAfterViewInit(): void {
  }

  ngOnInit(): void {
    this.panelContinuumOpened = false;
    this.panelZoomOpened = false;
    if (this.getDataFromConfiguration()) {
      this.setupFormGroup();
      this.getAdvancedModeToggle();
      this.service.getObservingMode().subscribe((mode: string) => {
        this.selectedObservingMode = mode;
      });
    }
    this.getSubarrayData();
  }

  ngOnDestroy() {
    this.dispose();
  }

  private dispose() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  getField = (fields: any, fieldName: string) => fields.find((e: ConfigurationMidCalculatorFormField) => e.field === fieldName)!;
  getDefaultValue = (field: any) => field ? field.defaultValue : null;

  /**
  * This method gets the configuration for each control and any default settings
  */
  getDataFromConfiguration(): boolean {
    if (!this.configuration) {
      this.configuration = this.config.getConfig();
    }
    if (this.configuration?.midCalcFormFields) {
      this.service.setConfig(this.configuration);
      this.validators.setConfig(this.configuration);
      this.fieldRightAscension = this.getField(this.configuration.midCalcFormFields, 'fieldRightAscension');
      this.fieldRightAscensionDecDef = this.getField(this.configuration.midCalcFormFields, 'fieldRightAscensionDec');
      this.fieldRightAscensionSexDef = Object.assign({},this.fieldRightAscension); // Deep copy
      this.fieldDeclination = this.getField(this.configuration.midCalcFormFields,'fieldDeclination');
      this.fieldDeclinationDecDef = this.getField(this.configuration.midCalcFormFields,'fieldDeclinationDec');
      this.fieldDeclinationSexDef = Object.assign({},this.fieldDeclination); // Deep copy
      this.fieldObservingMode = this.getField(this.configuration.midCalcFormFields, 'fieldObservingMode');
      this.fieldWeatherPWV = this.getField(this.configuration.midCalcFormFields, 'fieldWeatherPWV');
      this.fieldElevation = this.getField(this.configuration.midCalcFormFields, 'fieldElevation');
      this.fieldArrayConfig = this.getField(this.configuration.midCalcFormFields, 'fieldArrayConfig');
      this.fieldnSKA = this.getField(this.configuration.midCalcFormFields, 'fieldnSKA');
      this.fieldnMeer = this.getField(this.configuration.midCalcFormFields, 'fieldnMeer');

      if (this.configuration.dropDownValues) {
        this.defaultTimeDropdownOptions = this.configuration.dropDownValues.filter((e: ConfigurationDropDownValue) => e.type === 'time')!;
      }
      if (this.fieldObservingMode) {
        this.defaultObservingMode = this.fieldObservingMode.defaultValue.find((e: any) => e.selected);
        this.service.setObservingModeValue(this.defaultObservingMode.mode as string);
      }
      return true;
    }
    return false;
  }

  /**
  * This method setups the form with controls, default values and validators
  */
  setupFormGroup(): void {
    this.form = this.fb.group({
      fieldRightAscension: this.fb.control(this.getDefaultValue(this.fieldRightAscension), [Validators.required, this.validators.validatorRightAscension(false)]),
      fieldDeclination: this.fb.control(this.getDefaultValue(this.fieldDeclination), [Validators.required, this.validators.validatorDeclination("Mid", false)]),
      fieldObservingMode: this.fb.control(this.fieldObservingMode ? this.defaultObservingMode.mode : null, [Validators.required]),
      fieldWeatherPWV: this.fb.control(this.fieldWeatherPWV.defaultValue.value, [this.validators.validatorNumberMinMax(
        this.fieldWeatherPWV.defaultValue.min as number,
        this.fieldWeatherPWV.defaultValue.max as number)]),
      fieldElevation: this.fb.control(this.fieldElevation.defaultValue.value, [this.validators.validatorElevationMid(
        this.fieldElevation.defaultValue.min as number,
        this.fieldDeclination.field)]),
      fieldArrayConfig: this.fb.control(null, [Validators.required]),
      fieldnSKA: this.fb.control(null, [this.validators.validatorInteger(),
                                        this.validators.validatorNumberMinMax(0, 133),
                                        this.validators.validatorMinReceiverNum(this.fieldnSKA.field, this.fieldnMeer.field)]),
      fieldnMeer: this.fb.control(null, [this.validators.validatorInteger(),
                                         this.validators.validatorNumberMinMax(0, 64),
                                         this.validators.validatorMinReceiverNum(this.fieldnSKA.field, this.fieldnMeer.field)])
    });
    this.formInitialValues = this.form.value;
  }

  /**
   * This uses the API to get the subarray data out for the Array Configuration dropdown. If API is unreachable it will disable the Array Configuration field.
   */
  getSubarrayData() {
    this.subscriptions.push(
      this.midApi.subarrays$.subscribe(subarrays=> {
        this.subarrayOptions = subarrays;
        this.initialiseSubarrays();
      })
    );

    this.midApi.subQueryFailed$.subscribe((failed) => {
      this.subarrayQueryFailed = failed;
      if (failed) {
        this.form.controls['fieldArrayConfig'].disable();
        this.showErrorModal(
          MidCalculatorComponent.backendFailureErrorMessage
        );
      }
    });
  }

  initialiseSubarrays() {
    if (this.subarrayOptions.length < 1) {
      return;
    }

    // Put the following arrays in the correct position:
    //   AA* after AA2
    //   AA* (15-m antennas only) after AA*
    //   AA*/AA4 after AA4 (15-m antennas only)
    let aa_star = this.subarrayOptions.findIndex((a) => a.name === 'MID_AAstar_all');
    const aa_two = this.subarrayOptions.findIndex((a) => a.name === 'MID_AA2_all') + 1;
    if(aa_star >= 0  && aa_two >= 0)
    {
      this.subarrayOptions.splice(aa_two,0,this.subarrayOptions[aa_star]);
      this.subarrayOptions.splice(aa_star,1);
    }

    const aa_star_ska = this.subarrayOptions.findIndex((a) => a.name === 'MID_AAstar_SKA_only');
    aa_star = this.subarrayOptions.findIndex((a) => a.name === 'MID_AAstar_all') + 1;
    if(aa_star >= 0  && aa_star_ska >= 0)
    {
      this.subarrayOptions.splice(aa_star,0,this.subarrayOptions[aa_star_ska]);
      this.subarrayOptions.splice(aa_star_ska,1);
    }

    const aa_meerkat = this.subarrayOptions.findIndex((a) => a.name === 'MID_AA4_MeerKAT_only');
    const aa_ska = this.subarrayOptions.findIndex((a) => a.name === 'MID_AA4_SKA_only') + 1;
    if(aa_meerkat >= 0  && aa_ska >= 0)
    {
      this.subarrayOptions.splice(aa_ska,0,this.subarrayOptions[aa_meerkat]);
      this.subarrayOptions.splice(aa_meerkat,1);
    }

    const defaultSub = this.subarrayOptions.find(
      (a) => a.name === this.fieldArrayConfig.defaultValue[0]
    );

    // Set the default array on view
    this.onSelectSubarray(defaultSub ?? this.subarrayOptions[0]);
  }

  getArrayMessage(){
    return this.arrayMessage;
  }

  /**
   * This method returns true when the selected subarray configuration is a mixed array
   * containing MeerKAT and SKA1 dishes.
   */
  isSharedArray = (): boolean =>
    this.selectedSubarray?.n_meer > 0 && this.selectedSubarray?.n_ska > 0;

  /**
   * Provide additional information about restricted frequency range
   * when subarray includes 13.5m MeerKAT dishes.
   **/
  restrictedFrequencyMessage = (): string => {
    if (this.selectedSubarray?.n_meer > 0) {
      switch (this.selectedObservingMode) {
        case 'Band 1':
          return '13.5-m antennas only cover 0.58-1.015 GHz';
        case 'Band 2':
          return '13.5-m antennas only cover 0.95-1.67 GHz';
        default:
          return '';
      }
    } else {
      return '';
    }
  };

  /**
   * This method is used to set the selected Configuration Array dropdown value, clears the Custom fields when changing
   * @param subarray
   */
  onSelectSubarray(subarray: ArrayDropDownValue) {
    this.form.patchValue({
      fieldArrayConfig: subarray,
      fieldnSKA: subarray.n_ska,
      fieldnMeer: subarray.n_meer
    });

    this.selectedSubarray = subarray;

    if (subarray.name === 'Custom') {
      this.form.get('fieldnSKA')!.enable();

      // Disable the 'Number of 13.4-m antennas' field if Custom array is chosen
      // but the selected band does not allow MeerKAT antennas
      if (!this.isMeerKATAllowed()) {
        this.form.get('fieldnMeer')!.setValue(0);
        this.form.get('fieldnMeer')!.disable();
      }
      else {
      this.form.get('fieldnMeer')!.enable();
      }
      if (this.contPanel?.disableWeightingFields) {
        this.contPanel.disableWeightingFields();
      }
      if (this.zoomPanel?.disableWeightingFields) {
        this.zoomPanel?.disableWeightingFields();
      }
    }
    else {
      this.form.get('fieldnSKA')!.disable();
      this.form.get('fieldnMeer')!.disable();
      if (this.contPanel?.enableWeightingFields) {
        this.contPanel.enableWeightingFields();
      }
      if (this.zoomPanel?.enableWeightingFields) {
        this.zoomPanel.enableWeightingFields();
      }
    }

    this.checkIfZoomModeDisabled(subarray);
    this.paramChange();
  }

  /**
   * This method is used to cleanly show the errors on the UI
   * @param fieldName
   * @returns
   */
  getErrorMessage(fieldName: string) {
    const control: AbstractControl | null = this.form.get(fieldName);
    return this.validators.getErrorMessage(control);
  }

  /**
   * This passes the selected observing mode band to the observable
   * @param mode
   */
  onObservingModeChange(mode: string) {
    this.service.setObservingModeValue(mode);
    if (!this.isMeerKATAllowed()) {
      if (this.selectedSubarray.label === "Custom") {
        // If MeerKAT arrays are not allowed but mode is set to Custom,
        // disable the 'Number of 13.5-m antennas' field and set the value to 0
        this.form.get('fieldnMeer')!.disable();
        this.form.get('fieldnMeer')?.setValue(0);
      }
      else if (this.selectedSubarray.n_meer !== 0) {
        // This is an edge case where number of MeerkAT antennas is non-zero but user has chosen
        // a band which does not allow arrays with MeerKAT antennas. Set the array to one with
        // only SKA antennas instead.
        const defaultSKASub = this.subarrayOptions.find(
          (a) => a.name === this.fieldArrayConfig.defaultValue[1]
        );
        this.onSelectSubarray(defaultSKASub ?? this.subarrayOptions[0]);
      }
    }
    else {
      // Enable the 'Number of 13.5-m antennas' field in case a custom array is selected and it is currently disabled
      if (this.selectedSubarray.label == "Custom" && this.form.get('fieldnMeer')?.disabled) {
        this.form.get('fieldnMeer')!.enable();
        this.form.get('fieldnMeer')?.setValue(
          this.subarrayOptions.find((s: any) => s.label == "Custom")?.n_meer ?? 64);
      }
    this.paramChange();
    }
  }

  resetRaDec() {
    this.toggleSexDecMode = false;
    this.sexDecToggle.checked = false;
    this.form.get('fieldRightAscension')!.setValue(this.fieldRightAscensionSexDef.defaultValue);
    this.fieldRightAscension.placeholder = this.fieldRightAscensionSexDef.placeholder;
    this.fieldRightAscension.description = this.fieldRightAscensionSexDef.description;
    this.form.get('fieldRightAscension')!.setValidators([this.validators.validatorRightAscension(false), Validators.required]);
    this.form.get('fieldRightAscension')!.updateValueAndValidity();

    this.form.get('fieldDeclination')!.setValue(this.fieldDeclinationSexDef.defaultValue);
    this.fieldDeclination.placeholder = this.fieldDeclinationSexDef.placeholder;
    this.fieldDeclination.description = this.fieldDeclinationSexDef.description;
    this.form.get('fieldDeclination')!.setValidators([this.validators.validatorDeclination("Mid", false), Validators.required]);
    this.form.get('fieldDeclination')!.updateValueAndValidity();
  }

  resetForm() {
    this.contPanel?.resetContinuumForm();
    this.zoomPanel?.resetZoomForm();
    this.advancedMode?.resetForm();
    this.initialiseSubarrays();
    this.form.get('fieldElevation')!.disable();
  }

  /**
   * This clears down the entire form
   */
  onReset() {
    this.resetRaDec(); // Reset radec first or defaults are lost.
    this.getDataFromConfiguration();
    this.form.reset(this.formInitialValues); // Only resets field entries.
    this.form.markAsUntouched();
    this.resetForm();
  }

  loadingResult(show: boolean, isZoom: boolean | null) {
    if (!isZoom && this.panelContinuumOpened && this.contPanel) {
      this.contPanel.loadingResults(show);
    }
    if (isZoom && this.panelZoomOpened && this.zoomPanel) {
      this.zoomPanel.loadingResults(show);
    }
  }

  errorRoute(response: any) {
    const message = response?.error?.detail ?? response?.error?.message ?? "Unknown Error whilst making request.";
    this.showErrorModal(message);
    // When an error has returning, we no longer want to show the loading message
    this.loadingResult(false, false);
    this.loadingResult(false, true);
    return of();
  }

  onRaDecBlur(field: string) {
    if( this.sexDecToggle.checked ){
      this.form.get(field)?.setValue(helpers.format.decRaDecFormat(this.form.get(field)!.value));
    } else {
      this.form.get(field)?.setValue(helpers.format.raDecFormat(this.form.get(field)!.value));
    }

    if (this.form.get('fieldDeclination')?.valid) {
      this.form.get('fieldElevation')!.enable();
    }
  }

  onSexDecChange(event: any) {
    if (event.checked) {
      this.toggleSexDecMode = true;
      this.form.get('fieldRightAscension')!.setValue(this.fieldRightAscensionDecDef.defaultValue);
      this.fieldRightAscension.placeholder = this.fieldRightAscensionDecDef.placeholder;
      this.fieldRightAscension.description = this.fieldRightAscensionDecDef.description;
      this.form.get('fieldRightAscension')!.setValidators([this.validators.validatorRightAscension(true), Validators.required]);
      this.form.get('fieldRightAscension')!.updateValueAndValidity();

      this.form.get('fieldDeclination')!.setValue(this.fieldDeclinationDecDef.defaultValue);
      this.fieldDeclination.placeholder = this.fieldDeclinationDecDef.placeholder;
      this.fieldDeclination.description = this.fieldDeclinationDecDef.description;
      this.form.get('fieldDeclination')!.setValidators([this.validators.validatorDeclination("Mid", true), Validators.required]);
      this.form.get('fieldDeclination')!.updateValueAndValidity();
    } else {
      this.resetRaDec();
    }
  }

  // Custom function to cater for object creation order issue.
  antennaParamChange() {
    // This function is triggered if the number of SKA or meerkat antennas is changed,
    // which should only happen with a Custom array configuration
    if(this.selectedSubarray.label == "Custom"){
      const customSubarray: ArrayDropDownValue = {
        name: this.selectedSubarray.name,
        label: this.selectedSubarray.label,
        n_meer: this.form.get('fieldnMeer')!.value,
        n_ska: this.form.get('fieldnSKA')!.value
      };
      this.selectedSubarray = customSubarray;

      // Force check for zero antennas error.
      const fieldnSKA = this.form.get('fieldnSKA');
      fieldnSKA?.markAsTouched();
      fieldnSKA?.updateValueAndValidity();
      const fieldnMeer = this.form.get('fieldnMeer');
      fieldnMeer?.markAsTouched();
      fieldnMeer?.updateValueAndValidity();

      this.paramChange();
    }
  }

  decParamChange() {
    this.form.get('fieldElevation')!.disable();

    // Only validate Elevation field if Declination has a valid value
    if (this.form.get('fieldDeclination')?.valid) {
      const fieldElevationControl = this.form.get('fieldElevation');
      fieldElevationControl?.markAsTouched();
      fieldElevationControl?.updateValueAndValidity();
      this.form.get('fieldElevation')!.enable();
    } else {
      this.form.get('fieldElevation')!.disable();
    }
    this.paramChange();
  }

  paramChange() {
    // Add param update message to all relevant result panels.
    this.contPanel?.paramChangeContPanel();
    this.zoomPanel?.paramChange();
  }

  isCalculateBtnEnabled(): boolean {
    const contPanelInvalid = this.panelContinuumOpened && this.contPanel?.continuumForm && this.contPanel.continuumForm.invalid;
    const zoomPanelInvalid = this.panelZoomOpened && this.zoomPanel?.lineForm && this.zoomPanel.lineForm.invalid;
    const advancedFieldsInvalid = this.configuration?.advancedModeActive && this.toggleAdvancedMode && this.advancedMode?.advancedModeForm && this.advancedMode.advancedModeForm.invalid;

    const anyPanelOpened = this.panelContinuumOpened || this.panelZoomOpened;
    return this.form.valid && !contPanelInvalid && !zoomPanelInvalid && !advancedFieldsInvalid && anyPanelOpened;
  }

  isIncludeConfusionNoise(data: any): boolean {
    if (data) {
      const {weighting, taper, robustness, array_configuration} = data;
      // Delegate to the common helper method, which returns a string if weighting cannot be applied and null if it can
      return !helpers.option.beamNotApplicableMessageForMid(weighting, taper, robustness, array_configuration);
    }
    return false;
  }

  getAdvancedModeToggle() {
    this.sharedService.getAdvancedToggle().subscribe((toggle: boolean) => {
      this.toggleAdvancedMode = toggle;
    });
  }

  isMeerKATAllowed() {
    const currentBand = this.form.get('fieldObservingMode')?.value;
    const mode = this.fieldObservingMode.defaultValue.find(
      (e: any) => e.mode === currentBand
    );
    return helpers.configuration.hasDefaultInConfigurationFile(mode.contFreqInput, "meerkat");
  }

  /**
   * Zoom should be disabled for the earlier subarrays, and if open the zoom panel should be closed
   *
   * @param subarray that has just been selected
   **/
  checkIfZoomModeDisabled(subarray: ArrayDropDownValue): void {
    const shouldBeDisabled = ['MID_AA05_all', 'MID_AA1_all', 'MID_AA2_all'].includes(subarray.name);
    this.zoomModeDisabled = shouldBeDisabled;
    // If the accordion is open, close it if the mode is disabled.
    if (this.panelZoomOpened) {
      this.panelZoomOpened = !shouldBeDisabled;
    }
  }

  showArray(array: ArrayDropDownValue): boolean {
    if (array.n_meer > 0 && array.label !== "Custom") {
      return this.isMeerKATAllowed();
    }
    return true;
  }

  showErrorModal(errorMessage: string) {
    this.snackbar = this.snackBar.open(errorMessage, undefined, {
      duration: this.snackbarDuration,
      politeness: 'assertive',
      verticalPosition: 'top',
      panelClass: ['snackbar-error']
    });
  }

  /**
   * This is called when the submit button is clicked, and will call the relevant methods to call the
   * API endpoints, then combine and display the results.
   */
  onSubmit(): void {
    this.form.updateValueAndValidity();
    const midFormInput: MidFormInput = {
      common: {
        rx_band: this.form.get('fieldObservingMode')!.value,
        ra_str: this.sexDecToggle.checked ? helpers.format.degRaToSexagesimal(this.form.get('fieldRightAscension')!.value) : this.form.get('fieldRightAscension')!.value,
        dec_str: this.sexDecToggle.checked ? helpers.format.degDecToSexagesimal(this.form.get('fieldDeclination')!.value) : this.form.get('fieldDeclination')!.value,
        array_configuration: this.selectedSubarray.label as MidSubarrayId,
        n_ska: this.form.get('fieldnSKA')!.value,
        n_meer: this.form.get('fieldnMeer')!.value,
        pwv: this.form.get('fieldWeatherPWV')!.value,
        el: this.form.get('fieldElevation')!.value
      }
    };
    // TODO this doesn't need to be passed through all the calculate methods. Refactor to just give to the child components
    const integrationTimesDifferent = this.compareEnteredIntegrationTimes();
    if (this.panelZoomOpened && this.zoomPanel) {
      midFormInput.line = this.zoomPanel.getZoomFormData();
      if ((this.advancedMode && this.toggleAdvancedMode)) {
        midFormInput.advanced = this.advancedMode.getAdvancedFormData();
      }
      this.zoomResults$ = this.calculationService.calculateZoomResult(midFormInput, integrationTimesDifferent)
        .pipe(
          tap(() => this.loadingResult(false, true)),
          catchError((error) => this.errorRoute(error))
        );
    }

    if (this.panelContinuumOpened && this.contPanel) {
      midFormInput.continuum = this.contPanel.getContinuumFormData();
      if ((this.advancedMode && this.toggleAdvancedMode)) {
        midFormInput.advanced = this.advancedMode.getAdvancedFormData();
      }
      this.continuumResults$ = this.calculationService.calculateContinuumResult(midFormInput, integrationTimesDifferent)
        .pipe(
          tap(() => this.loadingResult(false, false)),
          catchError((error) => this.errorRoute(error))
        );
     }
  }

  /**
   * @return true if integration times have been entered in both accordions and they are different. Otherwise, return false.
   */
  compareEnteredIntegrationTimes(): boolean {
    if (this.panelContinuumOpened && this.contPanel && this.panelZoomOpened && this.zoomPanel) {
      const contData = this.contPanel.getContinuumFormData();
      const zoomData = this.zoomPanel.getZoomFormData();

      return !!contData?.integrationTime && !!zoomData?.integrationTime && (contData?.integrationTime != zoomData?.integrationTime);
    }
    return false;
  }
}
