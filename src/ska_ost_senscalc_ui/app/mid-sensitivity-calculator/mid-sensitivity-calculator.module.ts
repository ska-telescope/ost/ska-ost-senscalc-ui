import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MidSensitivityCalculatorRoutingModule } from './mid-sensitivity-calculator-routing.module';
import { MidCalculatorComponent } from './mid-calculator/mid-calculator.component';
import { ContinuumPanelComponent } from './continuum-panel/continuum-panel.component';
import { ZoomPanelComponent } from './zoom-panel/zoom-panel.component';
import { ContinuumResultsComponent } from './continuum-panel/continuum-results/continuum-results.component';
import { ZoomResultsComponent } from './zoom-panel/zoom-results/zoom-results.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgMaterialModule } from '../ng-material/ng-material.module';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { AdvancedModeComponent } from './advanced-mode/advanced-mode.component';

@NgModule({
  declarations: [
    MidCalculatorComponent,
    ContinuumPanelComponent,
    ZoomPanelComponent,
    ContinuumResultsComponent,
    ZoomResultsComponent,
    AdvancedModeComponent
  ],
  imports: [
    CommonModule,
    MidSensitivityCalculatorRoutingModule,
    FontAwesomeModule,
    NgMaterialModule,
    ReactiveFormsModule
  ],
  exports: [
    MidCalculatorComponent,
    ContinuumPanelComponent,
    ZoomPanelComponent,
    ContinuumResultsComponent,
    ZoomResultsComponent,
    AdvancedModeComponent
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' }
    }
  ]
})
export class MidSensitivityCalculatorModule { }
