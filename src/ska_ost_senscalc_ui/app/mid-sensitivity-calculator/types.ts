import { MidSubarrayId } from "../app-configuration";

// TODO use camel case and consistent names

export interface MidFormInput {
  common: MainFormData;
  continuum?: ContinuumFormData;
  line?: LineFormData;
  advanced?: AdvancedModeFormData;
}

export interface MainFormData {
    rx_band: 'Band 1' | 'Band 2' | 'Band 3' | 'Band 4' | 'Band 5a' | 'Band 5b';
    ra_str: string;
    dec_str: string;
    array_configuration: MidSubarrayId;
    n_meer: null | number;
    n_ska: null | number;
    pwv: string;
    el: string;
}

export interface LineFormData {
    selectedSensitivityUnit?: string;
    zoomBandwidth: number;
    zoomFrequency: number;
    integrationTime?: null | number;
    sensitivity?: null | number;
    weighting: string;
    robustness?: number;
    taper?: number;
    spectralAveragingFactor?: number;
    spectralResolution?: number;
    calculator_mode: string;
}

export interface ContinuumFormData {
    selectedSensitivityUnit?: string,
    frequency: number;
    bandwidth: number;
    n_subbands: string | number;
    integrationTime?: null | number;
    sensitivity?: null | number;
    spectralResolution: number;
    spectralAveragingFactor: number;
    weighting: 'natural' | 'briggs' | 'uniform';
    robustness?: -2 | -1 | 0 | 1 | 2;
    taper?: 0 | 0.25 | 1 | 4 | 16 | 64 | 256 | 1024;
    calculator_mode: "continuum" | "line";
}

export interface AdvancedModeFormData {
  // Be wary changing these, as they are sent directly to the API with the ... operator
    etaSystem: number;
    etaPointing: number;
    etaCoherence: number;
    etaDigitisation: number;
    etaCorrelation: number;
    etaBandpass: number;
    tSysSka: number;
    tRxSka: number;
    tSplSka: number;
    tSysMeer: number;
    tRxMeer: number;
    tSplMeer: number;
    tSkySka: number;
    tGalSka: number;
    tSkyMeer: number;
    tGalMeer: number;
    alpha: number;
    etaSka: number;
    etaMeer: number;
}
