import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { UntypedFormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatMenuModule } from '@angular/material/menu';
import { ZoomPanelComponent } from './zoom-panel.component';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import * as config from '../../../assets/configuration.json';
import { ConfigurationDropDownValue } from '../../app-configuration';
import { ValidatorsService } from '../../core/validators.service';
import { ZoomResultsComponent } from './zoom-results/zoom-results.component';
import { LineFormData } from '../types';
import { MidCalculatorService } from '../../core/mid-calculator.service';
import constants from '../../shared/utils/constants';
import helpers from "../../shared/utils/helpers";

describe('LineTabComponent', () => {
  let component: ZoomPanelComponent;
  let fixture: ComponentFixture<ZoomPanelComponent>;
  let service: MockValidatorService;
  let midCalcService: MockMidCalculatorService;

  beforeEach(async () => {
    midCalcService = new MockMidCalculatorService();
    service = new MockValidatorService();

    await TestBed.configureTestingModule({
      declarations: [
        ZoomPanelComponent,
        MockZoomResultsComponent
      ],
      imports: [
        NoopAnimationsModule,
        FontAwesomeModule,
        FormsModule,
        HttpClientModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatMenuModule,
        MatSelectModule,
        ReactiveFormsModule
      ],
      providers: [
        UntypedFormBuilder,
        { provide: ZoomResultsComponent, useValue: MockZoomResultsComponent },
        { provide: ValidatorsService, useValue: service },
        { provide: MidCalculatorService, useValue: midCalcService }
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoomPanelComponent);
    component = fixture.componentInstance;
    component._observingMode = 'Band 1';
    component._subarrayType = {
      name: 'MID_AA4_all',
      label: 'bar',
      n_meer: 10,
      n_ska: 20
    };
    component.configuration = config;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit & ngOnChanges should create', () => {
    component.ngOnInit();
    component.ngOnChanges({});
    component._observingMode = 'Band 1';
    component._subarrayType = {
      name: 'foo',
      label: 'bar',
      n_meer: 10,
      n_ska: 20
    };
    expect(component).toBeTruthy();
    component._observingMode = 'Band 1';
    component._subarrayType = {
      name: 'foo',
      label: 'bar',
      n_meer: 10,
      n_ska: 20
    };
    expect(component).toBeTruthy();
  });

  it('observingMode should set _observingMode to the same string', () => {
    const newMode = 'new mode';
    component.observingMode = newMode;
    expect(component._observingMode).toBe(newMode);
  });

  it('getDefaultValue', () => {
    expect(component.getDefaultValue(null)).toBe(null);
    expect(component.getDefaultValue({ defaultValue: 'defaultValue'})).toBe('defaultValue');
  });

  it('getValue', () => {
    expect(component.getValue(null)).toBe(null);
    expect(component.getValue({ value: 'value'})).toBe('value');
  });

  it('getFieldSupplied', () => {
    const emptyString = '';
    const expected = { value: 'result' };
    component.ngOnChanges({});
    fixture.detectChanges();

    component.lineForm.get('fieldSupplied')?.setValue(emptyString);
    expect(component.lineForm.get('fieldSupplied')?.value).toBe(emptyString);
    component.lineForm.get('fieldSupplied')?.updateValueAndValidity();
    expect(component.getFieldSupplied({ integration_time: '10 ms'})).toBe(constants.supplied.integrationTime);

    component.lineForm.get('fieldSupplied')?.setValue(expected);
    expect(component.lineForm.get('fieldSupplied')?.value).toBe(expected);
    component.lineForm.get('fieldSupplied')?.updateValueAndValidity();
    expect(component.getFieldSupplied({})).toEqual(expected);
  });

  it('getSupplied ', () => {
    const expected = { value: 'result' };
    component.lineForm.get('fieldSupplied')?.setValue(expected);
    expect(component.getSupplied()).toEqual(expected);
  });

  // it('paramChange should call output paramChange()', () => {
  //   jest.spyOn(component.output, 'paramChange');
  //   component.paramChange();
  //   fixture.detectChanges();
  //   expect(component.output.paramChange).toHaveBeenCalled();
  // });

  it('select zoom frequency', () => {
    const value: ConfigurationDropDownValue = {
      type: 'frequency',
      value: 'GHz',
      operator: '*',
      multiplier: 1000000000
    };
    component.selectZoomFrequency(value);
    expect(component.selectedZoomFrequency).toBe(value);
  });

  it('select line integration time', () => {
    const value: ConfigurationDropDownValue = {
      type: 'time',
      value: 's',
      operator: null,
      multiplier: null
    };
    component.selectIntegrationTime(value);
    expect(component.selectedIntegrationTime).toBe(value);
  });

  it('select line sensitivity', () => {
    const value: ConfigurationDropDownValue = {
      type: 'sensitivity',
      value: 'Jy/beam',
      operator: null,
      multiplier: null
    };
    component.selectSensitivity(value);
    expect(component.selectedSensitivity).toBe(value);
  });

  describe('suppliedChanged', () => {
    it('with default data should supplied field change call method and set value for dependant control in line tab', () => {
      component.suppliedChangedZoomPanel(constants.supplied.integrationTime);
      fixture.detectChanges();
      expect(component.lineForm.get('fieldIntegrationTime')?.enabled).toBe(true);
      expect(component.lineForm.get('fieldSensitivity')?.disabled).toBe(true);

      component.suppliedChangedZoomPanel('Sensitivity');
      fixture.detectChanges();
      expect(component.lineForm.get('fieldIntegrationTime')?.disabled).toBe(true);
      expect(component.lineForm.get('fieldSensitivity')?.enabled).toBe(true);
    });

    it('with null data should supplied field change call method and set value for dependant control in line tab', () => {
      component.lineForm.patchValue({ fieldIntegrationTime: null, fieldSensitivity: null });
      component.suppliedChangedZoomPanel(constants.supplied.integrationTime);
      fixture.detectChanges();
      expect(component.lineForm.get('fieldIntegrationTime')?.enabled).toBe(true);
      expect(component.lineForm.get('fieldSensitivity')?.disabled).toBe(true);

      component.suppliedChangedZoomPanel('Sensitivity');
      fixture.detectChanges();
      expect(component.lineForm.get('fieldIntegrationTime')?.disabled).toBe(true);
      expect(component.lineForm.get('fieldSensitivity')?.enabled).toBe(true);
    });
  });

  it('should line tab component call ngOnChanges', async () => {
    await fixture.whenStable().then(() => {
      component.ngOnChanges({});
      fixture.detectChanges();
      component.lineForm.get('fieldZoomFrequency')?.setValue('test');
      component.lineForm.get('fieldZoomResolution')?.setValue('test');
      component.lineForm.get('fieldZoomFrequency')?.updateValueAndValidity();
      component.lineForm.get('fieldZoomResolution')?.updateValueAndValidity();
      expect(component.lineForm.get('fieldZoomFrequency')?.value).toBe('test');
      expect(component.lineForm.get('fieldZoomResolution')?.value).toBe('test');
    });
  });

  it('reset line tab', async () => {
    await fixture.whenStable().then(() => {
      component.resetZoomForm();
      fixture.detectChanges();
      expect(component.lineForm.get('fieldZoomResolution')?.value).toBe(0.21);
      expect(component.lineForm.get('fieldSupplied')?.value).toBe(constants.supplied.integrationTime);
      expect(component.lineForm.get('fieldIntegrationTime')?.value).toBe(600);
      expect(component.lineForm.get('fieldSensitivity')?.value).toBe(1);
    });
  });

  it('getFieldSupplied', () => {
    const result = component.getFieldSupplied({ integration_time: 1 });
    fixture.detectChanges();
    expect(result).toBe(constants.supplied.integrationTime);
  });

  it('getZoomFormData', () => {
    component.selectedZoomFrequency = {
      type: 'frequency',
      value: 'GHz',
      operator: '*',
      multiplier: 1000000000
    };

    component.selectedIntegrationTime = {
      multiplier: 1,
      operator: '*',
      type: 'time',
      value: 's'
    };

    component.lineForm.get('fieldIntegrationTime')?.enable();
    component.lineForm.get('fieldSensitivity')?.disable();

    const lineForm1: LineFormData = {
      zoomBandwidth: 3125000,
      zoomFrequency: 700000000,
      integrationTime: 600,
      weighting: 'uniform',
      taper: 0,
      calculator_mode: 'line',
      selectedSensitivityUnit: 'Jy/beam',
      spectralResolution: 210
    };
    component.lineForm.get('fieldZoomFrequency')?.setErrors(null);
    component.lineForm.get('fieldZoomFrequency')?.setValue(0.7);
    expect(component.getZoomFormData()).toEqual(lineForm1);

    component.selectedSensitivity = {
      multiplier: 1,
      operator: null,
      type: 'sensitivity',
      value: 'Jy/beam'
    };
    component.lineForm.get('fieldIntegrationTime')?.disable();
    component.lineForm.get('fieldSensitivity')?.enable();
    component.lineForm.get('fieldSensitivity')?.setValue(1);
    const lineForm2: LineFormData = {
      zoomBandwidth: 3125000,
      zoomFrequency: 700000000,
      sensitivity: 1,
      weighting: 'uniform',
      taper: 0,
      calculator_mode: 'line',
      selectedSensitivityUnit: 'Jy/beam',
      spectralResolution: 210
    };
    expect(component.getZoomFormData()).toStrictEqual(lineForm2);
  });

  it('should disable the correct fields if weighting is disabled', () => {
    component.disableWeightingFields();
    expect(component.lineForm.get('fieldImageWeighting')?.enabled).toBe(false);
    expect(component.lineForm.get('fieldImageWeighting')?.value).toBe('natural');
    expect(component.lineForm.get('fieldTapering')?.enabled).toBe(false);
    expect(component.lineForm.get('fieldTapering')?.value).toBe(0);
  });

  it('should enable the correct fields if weighting is enabled', () => {
    component.enableWeightingFields();
    expect(component.lineForm.get('fieldImageWeighting')?.enabled).toBe(true);
    expect(component.lineForm.get('fieldTapering')?.enabled).toBe(true);
  });

  it('no configuration', () => {
    component.configuration = undefined;
    component.getformFieldsDataFromConfig();
    expect(component.configuration).toBe(undefined);
  });

  it('getFieldSupplied', () => {
    const input = {
      integration_time: 1
    };
    const input2 = {
    };
    expect(component.getFieldSupplied(input)).toBe(constants.supplied.integrationTime);
    expect(component.getFieldSupplied(input2)).toBe(constants.supplied.integrationTime);
  });

  it('weightingChanged', () => {
    component.weightingChanged('robust');
    fixture.detectChanges();
    expect(component.showRobustness).toBe(true);

    component.weightingChanged('uniform');
    fixture.detectChanges();
    expect(component.showRobustness).toBe(false);

    component.weightingChanged('natural');
    fixture.detectChanges();
    expect(component.showRobustness).toBe(false);
  });

  it('resolutionChanged', () => {
    jest.spyOn(helpers.calculate, 'calculateVelocity').mockReturnValue('9.7 m/s');
    component.lineForm.get('fieldZoomSpectralAveraging')?.setValue(1);
    component.lineForm.get('fieldZoomFrequency')?.setValue(6550000000);
    component.resolutionChanged(0.21);
    fixture.detectChanges();
    expect(component.lineForm.get('fieldEffectiveResolution')?.value).toBe('0.21 kHz (9.7 m/s)');
  });

  it('spectralAveragingChanged', () => {
    jest.spyOn(helpers.calculate, 'calculateVelocity').mockReturnValue('9.7 m/s');
    component.lineForm.get('fieldZoomResolution')?.setValue(0.21);
    component.spectralAveragingChanged(1);
    fixture.detectChanges();
    expect(component.lineForm.get('fieldEffectiveResolution')?.value).toBe('0.21 kHz (9.7 m/s)');
  });

  describe('getScaledFrequency', () => {
    it('with lineForm and selectedZoomFrequency should return values', () => {
      component.lineForm.get('fieldZoomFrequency')!.setValue(null);
      expect(component.getScaledFrequency()).toBe(0);
    });
    it('with lineForm only should return zero', () => {
      component.selectedZoomFrequency = { type: 'frequency', value: 'kHz', multiplier: 1000.0, operator: '*', selected: true } as ConfigurationDropDownValue;
      component.lineForm.get('fieldZoomFrequency')!.setValue(0.3);
      expect(component.getScaledFrequency()).toBe(3e-7);
    });
  });
});

class MockValidatorService extends ValidatorsService {

  constructor() {
    super();
    this.config = config;
  }
}

class MockMidCalculatorService extends MidCalculatorService {

}

@Component({
  selector: 'app-zoom-panel',
  template: '',
  providers: [
    {
      provide: ZoomResultsComponent,
      useClass: MockZoomResultsComponent
    }
  ]
})
class MockZoomResultsComponent extends ZoomResultsComponent {
  displayResultZoomPanel(response: any): void {
    response;
  }
  loadingResult(show: boolean): void { show; }
  clearResult(): void { }
  paramChange(): void { }
  suppliedChanged(): void { }
}
