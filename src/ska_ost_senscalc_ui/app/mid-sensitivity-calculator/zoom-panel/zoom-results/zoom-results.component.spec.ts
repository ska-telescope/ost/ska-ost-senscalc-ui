import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { ZoomResultsComponent } from './zoom-results.component';
import constants from '../../../shared/utils/constants';

describe('LineResultsComponent', () => {
  let component: ZoomResultsComponent;
  let fixture: ComponentFixture<ZoomResultsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ZoomResultsComponent],
      imports: [
        // Other imports...
        MatCardModule
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoomResultsComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should show that the results message are loading', () => {
    component.loadingResults(true);
    expect(component.resultsHeaderMsg).toBe('Calculating Results...');
  });

  it('suppliedChangedZoomResult should change the timeMode flag', () => {
    component.integrationTimeSupplied = true;
    component.suppliedChangedZoomResult('IntegrationTime123');
    expect(component.integrationTimeSupplied).toBeFalsy();
    component.suppliedChangedZoomResult(constants.supplied.integrationTime);
    expect(component.integrationTimeSupplied).toBeTruthy();
  });

  it('reset should reset the control variables', () => {
    component.zoomResults = {warnings: {}};
    component.suppliedSwitched = true;
    component.resetZoomResult();
    expect(component.zoomResults).toBe(undefined);
    expect(component.suppliedSwitched).toBeFalsy();
  });

  describe('paramChangeZoomResult', () => {
    it('should display param change', () => {
      component.zoomResults = {warnings: {}};
      component.parametersChanged = false;
      component.paramChangeZoomResult();
      expect(component.parametersChanged).toBe(true);
    });
  });

});
