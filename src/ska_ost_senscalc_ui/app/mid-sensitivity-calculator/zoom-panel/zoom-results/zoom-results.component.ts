import {Component, Input, OnChanges, OnInit} from '@angular/core';
import helpers from '../../../shared/utils/helpers';
import constants from "../../../shared/utils/constants";
import {ZoomResults} from "../../../core/types";

@Component({
  selector: 'app-zoom-results',
  templateUrl: './zoom-results.component.html',
  styleUrls: ['./zoom-results.component.scss']
})
export class ZoomResultsComponent implements OnInit, OnChanges {

  @Input() zoomResults?: ZoomResults | null;

  differentIntegrationTimeMessage = constants.messages.differentIntegrationTime;

  resultsHeaderMsg = helpers.result.header(false);
  integrationTimeSupplied = true; // Flag is true if time is supplied, ie if sensitivity is being calculated
  parametersChanged = false; // Message to say the params have changed since the previous results were calculated
  // The sensLimitReached warning should be hidden if the user changes the 'Supplied' drop down
  // but we can't just overwrite the sensLimitReached variable to false as we need to retain the info
  // in case the user switches back, hence the need for this variable
  suppliedSwitched!: boolean;

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges() {
    this.resetControlVariables();
  }

  resetZoomResult(): void {
    this.zoomResults = undefined;
    this.resetControlVariables();
  }

  resetControlVariables() {
    this.suppliedSwitched = false;
    this.parametersChanged = false;
    this.loadingResults(false);
  }

  /**
   * This method is called when the user changes whether they want to calculate sensitivity by supplying an integration time
   * or integration time by supplying a sensitivity. It is responsible for resetting fields and removing warnings.
   *
   * @param value - the field the user is supplying, either IntegrationTime or Sensitivity
   **/
  suppliedChangedZoomResult(value: string): void {
    this.integrationTimeSupplied = helpers.supplied.isIntegrationTime(value);
    // The aim is that changing the 'Supplied' field will flip this bool,
    // and changing the field back to the original value will leave it in the original state
    this.suppliedSwitched = !this.suppliedSwitched;
  }

  paramChangeZoomResult(): void {
    // Only want to warn after a calculation has been made and not
    // while user is entering at the start of a new calculation.
    if(this.zoomResults) {
      this.parametersChanged = true;
    }
  }

  /**
   * This method defines the behaviour when the results are loading, ie the UI is waiting for a
   * response from the API, and a message should be displayed
   *
   * @param loading Set to true if the results are currently loading and false if not.
   **/
  loadingResults(loading: boolean): void {
    this.resultsHeaderMsg = helpers.result.header(loading);
  }

  sensLimitWarning() {
    // For the time -> sensitivity calculation, we don't show the number in the message to be consistent with continuum
    return helpers.messages.sensLimitWarning(this.zoomResults!.warnings!.sensLimitReached!.value);
  }
}
