import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { FooterComponent } from './footer.component';
import {MatToolbarModule} from "@angular/material/toolbar";

const config = {
  currentVersion: "0.0.0",
  validationErrors: [],
  calculatorModes: [],
  advancedModeActive: false,
  pulsarSearchArrays: [{"label": "AA2 (core only)"},{"label": "AA* (core only)"},{"label": "AA4 (core only)"}]
};

describe('FooterComponent', () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        MatToolbarModule
      ],
      declarations: [
        FooterComponent
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
    component.config = config;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.config).toEqual(config);
  });

  it('should have copyright', () => {
    const el: HTMLElement = fixture.debugElement.query(By.css('#footerCopyright')).nativeElement as HTMLElement;
    expect(el.innerHTML).toContain('SKAO');
  });

  it('check year', () => {
    expect(component.getYear()).toContain("20");
  });
});
