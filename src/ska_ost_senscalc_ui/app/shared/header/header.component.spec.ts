import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Location } from '@angular/common';
import { HeaderComponent } from './header.component';
import { Router } from "@angular/router";

import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { MatSlideToggleHarness } from "@angular/material/slide-toggle/testing";
import { MatButtonToggleHarness } from '@angular/material/button-toggle/testing';
import { MatButtonToggleModule} from "@angular/material/button-toggle";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatInputModule } from "@angular/material/input";
import { MatIconModule } from "@angular/material/icon";

import * as config from '../../../assets/configuration.json';
import { AppComponent } from '../../app.component';

describe('HeaderComponent', () => {
  let fixture: ComponentFixture<HeaderComponent>;
  let loader: HarnessLoader;

  let component: HeaderComponent;

  // helper functions to get material slide toggle harnesses
  const getAdvancedModeToggle = async () => {
    return loader.getHarness(
      MatSlideToggleHarness.with({name: 'advancedModeToggle'})
    );
  };

  const getMidLowToggle = async () => {
    return {
      'mid': await loader.getHarness(MatButtonToggleHarness.with({text: 'MID'})),
      'low': await loader.getHarness(MatButtonToggleHarness.with({text: 'LOW'}))
    };
  };

  beforeEach(async () => {

    // Provide a mock implementation of getPropertyValue to prevent the
    // TypeErrors seen when toggling the Material slide toggle.
    //
    //   TypeError: window.getComputedStyle(...).getPropertyValue is not a function
    //
    Object.defineProperty(window, 'getComputedStyle', {
      value: () => ({
        getPropertyValue: () => {
          return '';
        }
      })
    });

    await TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        RouterTestingModule.withRoutes([
          {
            path: 'test-low-endpoint',
            component: HeaderComponent,
            data: {offerAdvancedMode: false}
          },
          {
            path: 'test-mid-endpoint',
            component: HeaderComponent,
            data: {offerAdvancedMode: true}
          }
        ]),
        MatSlideToggleModule,
        MatButtonToggleModule,
        MatToolbarModule,
        MatInputModule,
        MatIconModule
      ],
      providers: [Location],
      declarations: [HeaderComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(HeaderComponent);

    component = fixture.componentInstance;
    component.config = {
      currentVersion: "",
      documentationMidURL: "",
      documentationLowURL: "",
      skaoHomepage: "",
      validationErrors: [],
      advancedModeActive: true,
      pulsarSearchArrays: [{"label": "AA2 (core only)"},{"label": "AA* (core only)"},{"label": "AA4 (core only)"}]
    };

    fixture.detectChanges();
    loader = TestbedHarnessEnvironment.loader(fixture);

    // start in mid URL by default
    fixture.componentInstance.location.go('/mid');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should start with config available', () => {
    expect(component.config).not.toBeNull();
  });

  it('should offer advanced mode slide toggle if in advanced mode', () => {
    component.config.advancedModeActive = false;
    fixture.detectChanges();
    component.ngOnInit();
    const debugElement: DebugElement = fixture.debugElement;
    let advancedToggle = debugElement.query(By.css('[data-testid="advanced-toggle-div"]'));
    expect(component.offerAdvancedMode()).toBeFalsy();
    expect(advancedToggle.styles['visibility']).toEqual('hidden');

    component.config.advancedModeActive = true;
    fixture.detectChanges();
    component.ngOnInit();
    advancedToggle = debugElement.query(By.css('[data-testid="advanced-toggle-div"]'));
    expect(component.offerAdvancedMode()).toBeTruthy();
    expect(advancedToggle.styles['visibility']).toEqual('visible');
  });

  it('should hide advanced mode slide toggle in LOW mode', async () => {
    const debugElement: DebugElement = fixture.debugElement;
    const advancedToggle = debugElement.query(By.css('[data-testid="advanced-toggle-div"]'));
    // ensure we're in MID mode to begin with by toggling to MID
    const buttons = await getMidLowToggle();
    await buttons.mid.check();
    // advanced mode toggle should be visible in MID mode
    expect(advancedToggle.styles['visibility']).toEqual('visible');

    // ... but hidden when toggled to LOW mode
    await buttons.low.check();
    expect(advancedToggle.styles['visibility']).toEqual('hidden');
  });

  it('modifies component state according to URL', async () => {
    const buttons = await getMidLowToggle();
    // The location path defaults to mid in the test setup
    expect(await buttons.mid.isChecked()).toBeTruthy();
    expect(await buttons.low.isChecked()).toBeFalsy();

    fixture.componentInstance.location.go('/low');
    expect(await buttons.low.isChecked()).toBeTruthy();
    expect(await buttons.mid.isChecked()).toBeFalsy();
  });

  it('modifies component state when toggling telescope', async () => {
    const buttons = await getMidLowToggle();
    await buttons.low.check();
    expect(await buttons.low.isChecked()).toBeTruthy();
    expect(await buttons.mid.isChecked()).toBeFalsy();

    await buttons.mid.check();
    expect(await buttons.low.isChecked()).toBeFalsy();
    expect(await buttons.mid.isChecked()).toBeTruthy();
  });

  it('load real config', () => {
    component.config = config;
    fixture.detectChanges();
    component.ngOnInit();
  });

  it('modifies component state when toggling advanced Mode', async() => {
    const advancedModeToggle = await getAdvancedModeToggle();
    expect(component.isAdvancedModeActive).toBeFalsy();

    await advancedModeToggle.toggle();
    expect(component.isAdvancedModeActive).toBeTruthy();

    await advancedModeToggle.toggle();
    expect(component.isAdvancedModeActive).toBeFalsy();
  });

  it('navigates to appropriate route when toggled', async () => {
    // begin in MID mode
    const toggleButtons = await getMidLowToggle();

    const router = TestBed.inject(Router);
    const navigateSpy = jest.spyOn(router, 'navigate');

    await toggleButtons.low.check();
    expect(navigateSpy).toHaveBeenCalledWith(['low']);

    await toggleButtons.mid.check();
    expect(navigateSpy).toHaveBeenCalledWith(['mid']);
  });

  it('isDarkMode', () => {
    expect(component.isDarkMode()).toBeFalsy();
    localStorage.setItem(AppComponent.LOCAL_STORAGE_TITLE, AppComponent.DARK_THEME_CLASS);
    expect(component.isDarkMode()).toBeTruthy();
  });

  it('should swap the theme', () => {
    // Start off with the component in light mode
    localStorage.removeItem(AppComponent.LOCAL_STORAGE_TITLE);
    expect(component.isDarkMode()).toBeFalsy();
    const mockDarkMode = jest.spyOn(component.sharedService.darkMode, 'emit');
    const mockLightMode = jest.spyOn(component.sharedService.lightMode, 'emit');
    // Swap the theme, should now have emitted a light mode event
    component.swapTheme();
    expect(mockDarkMode).toHaveBeenCalled();
    // Mock actually setting light mode, as this is done in he App component
    localStorage.setItem(AppComponent.LOCAL_STORAGE_TITLE, AppComponent.DARK_THEME_CLASS);
    // Swap the theme again, should now have emitted a dark mode event
    component.swapTheme();
    expect(mockLightMode).toBeCalledWith(true);
  });
});
