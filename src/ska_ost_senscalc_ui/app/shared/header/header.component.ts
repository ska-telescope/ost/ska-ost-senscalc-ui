import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { AppComponent } from '../../app.component';
import { Router } from '@angular/router';
import { IconDefinition, faCaretDown, faQuestionCircle, faDraftingCompass } from '@fortawesome/free-solid-svg-icons';
import { AppConfiguration, Configuration } from '../../app-configuration';
import { SharedService } from '../service/shared.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public faQuestionCircle: IconDefinition = faQuestionCircle;
  public faCaretDown: IconDefinition = faCaretDown;
  public faDraftingCompass: IconDefinition = faDraftingCompass;
  public config!: Configuration;
  isAdvancedModeActive = false;

  constructor(
    private configService: AppConfiguration,
    public sharedService: SharedService,
    private router: Router,
    public location: Location
  ) { }

  ngOnInit() {
    const serviceConfig = this.configService.getConfig();
    if (serviceConfig) {
      this.config = serviceConfig;
    }
  }

  async changeCalculatorMode(target: string) {
    await this.router.navigate([target]);
  }

  isMidSelected() {
    return this.location.path() === "/mid";
  }

  offerAdvancedMode() {
    if (!this.config.advancedModeActive) {
      return false;
    }
    return this.isMidSelected();
  }

  selectAdvancedMode() {
    this.isAdvancedModeActive = !this.isAdvancedModeActive;
    this.sendToggleChangeEvent();
  }

  getApplicationContextService() {
    return this.configService;
  }

  sendToggleChangeEvent() {
    this.sharedService.handleToggleChange(this.isAdvancedModeActive);
  }

  goToDocumentation() {
    window.open(this.isMidSelected() ? this.config.documentationMidURL: this.config.documentationLowURL, "_blank");
  }

  isDarkMode(): boolean {
    const currentTheme = localStorage.getItem(AppComponent.LOCAL_STORAGE_TITLE);
    if (currentTheme) {
      return true;
    }
    return false;
  }

  swapTheme(): void {
    if (this.isDarkMode()) {
      this.selectLightTheme();
    } else {
      this.selectDarkTheme();
    }
  }

  selectLightTheme() {
    this.sharedService.lightMode.emit(true);
  }

  selectDarkTheme() {
    this.sharedService.darkMode.emit(true);
  }
}
