import { EventEmitter, Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  public lightMode = new EventEmitter<boolean>();
  public darkMode = new EventEmitter<boolean>();

  private toggleAdvancedMode = new BehaviorSubject(false);

  constructor() { }

  handleToggleChange(toggle: boolean) {
    this.toggleAdvancedMode.next(toggle);
  }

  getAdvancedToggle() {
    return this.toggleAdvancedMode;
  }
}
