const constants = {
  form: {
    invalid: "Form is invalid"
  },

  supplied: {
    integrationTime: 'IntegrationTime',
    sensitivity: 'Sensitivity'
  },

  status: {
    success: 'success',
    fail: 'fail'
  },

  messages: {
    arrayLoadFail: 'Subarray selection not available',
    differentIntegrationTime: 'Warning: the integration times differ in other accordions.'
  },

  resultHeader: {
    loading: 'Calculating Results...',
    ready: 'Results'
  },

  result: {
    nonGaussianBeam: "N/A (non-Gaussian beam)",
    noBeamAvailable: "N/A (no beam information available)"
  },

  values: {
    minGaussianBeamLow: 1.5
  }
};

export default constants;
