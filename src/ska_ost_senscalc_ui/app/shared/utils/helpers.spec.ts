import constants from './constants';
import helpers from './helpers';
import { ArrayDropDownValue } from "../../app-configuration";
import {LowContinuumWeightingResult} from "../../generated/api/low";

describe('helpers', () => {
  describe('calculate', () => {
    describe('subBandtoFrequencyArray', () => {
      it('with non-positive number of sub-bands should return empty array', () => {
        expect(helpers.calculate.subBandtoFrequencyArray(-1, 1.234, 3.421)).toStrictEqual([]);
        expect(helpers.calculate.subBandtoFrequencyArray(0, 1.234, 3.421)).toStrictEqual([]);
      });
      it('subBandtoFrequencyArray with specific parameters should return different array', () => {
        expect(helpers.calculate.subBandtoFrequencyArray(1, 1.234, 3.421)).toStrictEqual([ 3.421 ]);
        expect(helpers.calculate.subBandtoFrequencyArray(4, 2, 8)).toStrictEqual([
          7.25, 7.75, 8.25, 8.75
        ]);
        expect(helpers.calculate.subBandtoFrequencyArray(10, 2, 8)).toStrictEqual([
          7.1, 7.3, 7.5, 7.7, 7.9, 8.1, 8.3, 8.5, 8.7, 8.9
        ]);
      });
    });

    it('calculates velocity and returns the correct precision and units', () => {
      expect(helpers.calculate.calculateVelocity(200, 1, 2)).toBe('59958491.60 km/s');
      expect(helpers.calculate.calculateVelocity(200, -1)).toBe('0.0 m/s');
      expect(helpers.calculate.calculateVelocity(10000, 1, 3)).toBe('2997924580.000 km/s');
      expect(helpers.calculate.calculateVelocity(0.0002, 1)).toBe('60.0 km/s');
      expect(helpers.calculate.calculateVelocity(-0.02, 4)).toBe('-1498962.3 m/s');
      expect(helpers.calculate.calculateVelocity(-0.123, 0)).toBe('0.0 m/s');
    });

    it('convertValueWithUnit calculation should return value', () => {
      expect(helpers.calculate.thermalSensitivity(11.2910, 7.1299, 109.00)).toBe(0.08032172021515531);
      expect(helpers.calculate.thermalSensitivity(1.3945, 7.1209, 19.00)).toBe(NaN);
    });

    it('sensitivityOnUnit calculation should return value', () => {
      expect(helpers.calculate.sensitivityOnUnit('K', 876.1299, 100.00)).toBe(8.761299000000001);
      expect(helpers.calculate.sensitivityOnUnit('mK', 976.1299, 10.00)).toBe(97.61299);
      expect(helpers.calculate.sensitivityOnUnit('uK', 476.1299, 10.00)).toBe(47.61299);
      expect(helpers.calculate.sensitivityOnUnit('MHz', 76.1299, 109.00)).toBe(76.1299);
    });

    it('sqrtOfSumSqs calculation should return value', () => {
      expect(helpers.calculate.sqrtOfSumSqs(0, 1.0)).toBe(1.0);
      expect(helpers.calculate.sqrtOfSumSqs(0, -1.0)).toBe(1.0);
      expect(helpers.calculate.sqrtOfSumSqs(1.0, 1.0)).toBe(1.4142135623730951);
    });

    it('should convertBeamValueDegreesToDisplayValue in arcsecs', () => {
      expect(helpers.format.convertBeamValueDegreesToDisplayValue(0.0, 0.0)).toBe('0.000" x 0.000"');
      expect(helpers.format.convertBeamValueDegreesToDisplayValue(5.0, 2.34, 1)).toBe(  '18000.0" x 8424.0"');
    });

    it('should calculate beam size in arcseconds correctly', () => {
      const beam_min_scaled = 0.0010792161880542248;
      const beam_maj_scaled= 0.0008405281736775013;
      const precision = 1;
      expect(helpers.calculate.calculateBeamSizeInArcseconds(beam_min_scaled, beam_maj_scaled, precision)).toBe(11.7);
    });

    it('should return true if beam size is smaller than min beam size limit', () => {
      // min beam size limit is currently set to 1.5 (constants.values.minGaussianBeamLow)
      const weightingResponse: LowContinuumWeightingResult = {
        weighting_factor: 1.00001218198349,
        sbs_conv_factor: 189423.14682290884,
        confusion_noise: {
          value: 2.3993667341630888e-05,
          limit_type: 'value'
        },
        // trying to get a very small beam size here => this should give us a beam size of 0.4 arcseconds
        beam_size: {beam_maj_scaled:  0.00017240449664253948, beam_min_scaled: 0.00015285889104342477, beam_pa: 43.447752922342616}
      };
      expect(helpers.calculate.minBeamSizeCheck(weightingResponse, 1)).toBe(true);
    });

    it('should return false if beam size is bigger than min beam size limit', () => {
      // min beam size limit is currently set to 1.5 (constants.values.minGaussianBeamLow)
      const weightingResponse: LowContinuumWeightingResult = {
        weighting_factor: 1.00001218198349,
        sbs_conv_factor: 189423.14682290884,
        confusion_noise: {
          value: 2.3993667341630888e-05,
          limit_type: 'value'
        },
        // trying to get a big beam size here => this should give us a beam size of 32701.2 arcseconds
        beam_size: {beam_maj_scaled:  0.053034207070936895, beam_min_scaled: 0.04757198793256391, beam_pa: -110.65020308794567}
      };
      expect(helpers.calculate.minBeamSizeCheck(weightingResponse, 1)).toBe(false);
    });
});

  describe('format', () => {

    it('should convert the time to the correct units', () => {
      expect(helpers.format.convertTimeToDisplayUnit({value: 1, unit: 's'})).toBe("1.00 s");
      expect(helpers.format.convertTimeToDisplayUnit({value: 1e-4, unit: 's'})).toBe("100.00 us");
      expect(helpers.format.convertTimeToDisplayUnit({value: 1e-7, unit: 's'})).toBe("100.00 ns");
      expect(helpers.format.convertTimeToDisplayUnit({value: 1e-2, unit: 's'})).toBe("10.00 ms");
    });

    it('should convert the sensitivity to the correct units', () => {
      expect(helpers.format.convertSensitivityToDisplayValue(0)).toBe("0.00 Jy/beam");
      expect(helpers.format.convertSensitivityToDisplayValue(1)).toBe("1.00 uJy/beam");
      expect(helpers.format.convertSensitivityToDisplayValue(12)).toBe("12.00 uJy/beam");
      expect(helpers.format.convertSensitivityToDisplayValue(123)).toBe("123.00 uJy/beam");
      expect(helpers.format.convertSensitivityToDisplayValue(1234)).toBe("1.23 mJy/beam");
      expect(helpers.format.convertSensitivityToDisplayValue(12345)).toBe("12.35 mJy/beam");
      expect(helpers.format.convertSensitivityToDisplayValue(123456)).toBe("123.46 mJy/beam");
      expect(helpers.format.convertSensitivityToDisplayValue(1234567)).toBe("1.23 Jy/beam");
      expect(helpers.format.convertSensitivityToDisplayValue(12345678)).toBe("12.35 Jy/beam");
      expect(helpers.format.convertSensitivityToDisplayValue(123456789)).toBe("123.46 Jy/beam");
    });

    it('should convert the sensitivity to the correct units', () => {
      expect(helpers.format.convertSensitivityUnit(1)).toBe("1.00 uJy");
      expect(helpers.format.convertSensitivityUnit(12)).toBe("12.00 uJy");
      expect(helpers.format.convertSensitivityUnit(123)).toBe("123.00 uJy");
      expect(helpers.format.convertSensitivityUnit(1234)).toBe("1.23 mJy");
      expect(helpers.format.convertSensitivityUnit(12345)).toBe("12.35 mJy");
      expect(helpers.format.convertSensitivityUnit(123456)).toBe("123.46 mJy");
      expect(helpers.format.convertSensitivityUnit(1234567)).toBe("1.23 Jy");
      expect(helpers.format.convertSensitivityUnit(12345678)).toBe("12.35 Jy");
      expect(helpers.format.convertSensitivityUnit(123456789)).toBe("123.46 Jy");
    });

    it('should convert the sensitivity to Jy', () => {
      expect(helpers.format.convertSensitivityToJy(1, 'uJy/beam')).toBe(1e-6);
      expect(helpers.format.convertSensitivityToJy(1, 'nJy/beam')).toBe(1e-9);
      expect(helpers.format.convertSensitivityToJy(1, 'mJy/beam')).toBe(1e-3);
      expect(helpers.format.convertSensitivityToJy(1, 'Jy/beam')).toBe(1);
    });

    it('should convert the Kelvin values to the correct units', () => {
      expect(helpers.format.convertKelvinsToDisplayValue(1)).toBe("1.00 uK");
      expect(helpers.format.convertKelvinsToDisplayValue(12)).toBe("12.00 uK");
      expect(helpers.format.convertKelvinsToDisplayValue(123)).toBe("123.00 uK");
      expect(helpers.format.convertKelvinsToDisplayValue(1234)).toBe("1.23 mK");
      expect(helpers.format.convertKelvinsToDisplayValue(12345)).toBe("12.35 mK");
      expect(helpers.format.convertKelvinsToDisplayValue(123456)).toBe("123.46 mK");
      expect(helpers.format.convertKelvinsToDisplayValue(1234567)).toBe("1.23 K");
      expect(helpers.format.convertKelvinsToDisplayValue(12345678)).toBe("12.35 K");
      expect(helpers.format.convertKelvinsToDisplayValue(123456789)).toBe("123.46 K");
    });

    it("should convert poorly formatted coordinates to the correct format", () => {
      expect(helpers.format.raDecFormat("01:02:03.")).toBe("01:02:03.0");
      expect(helpers.format.raDecFormat("-67:00:00.")).toBe("-67:00:00.0");
    });

    it("should convert coord sexagesimal to decimal", () => {
      expect(helpers.format.sexRaToDeg("12:02:00")).toBe("180.5");
      expect(helpers.format.sexRaToDeg("12:00:00")).toBe("180");
      expect(helpers.format.sexDecToDeg("-30:30:00")).toBe("-30.5");
      expect(helpers.format.sexDecToDeg("-30:00:00")).toBe("-30");
    });

    it("should convert coord decimal to sexagesimal", () => {
      expect(helpers.format.degRaToSexagesimal("180.5")).toBe("12:02:00.00");
      expect(helpers.format.degRaToSexagesimal("180.0")).toBe("12:00:00.00");
      expect(helpers.format.degDecToSexagesimal("-30.5")).toBe("-30:30:00.00");
      expect(helpers.format.degDecToSexagesimal("-30.0")).toBe("-30:00:00.00");
    });

    it("should convert poorly formatted decimal coordinates to the correct format", () => {
      expect(helpers.format.decRaDecFormat("1")).toBe("1.0");
      expect(helpers.format.decRaDecFormat("1.")).toBe("1.0");
      expect(helpers.format.decRaDecFormat("1.0")).toBe("1.0");
      expect(helpers.format.decRaDecFormat(".0")).toBe("0.0");
      expect(helpers.format.decRaDecFormat("aaa")).toBe("aaa"); // Do not format rubbish
      expect(helpers.format.decRaDecFormat("1.222")).toBe("1.222"); // Keep supplied precicion
    });
  });

  describe('option', () => {
    it('beamNotApplicableMessageForMid with param should return the N/A message or null', () => {
      expect(helpers.option.beamNotApplicableMessageForMid('natural', 0, 0, "Custom")).toBe(constants.result.noBeamAvailable);
      expect(helpers.option.beamNotApplicableMessageForMid('natural', 0, 0, "AA4")).toBe(constants.result.nonGaussianBeam);
      expect(helpers.option.beamNotApplicableMessageForMid('natural', 0, 2, "AA4")).toBe(constants.result.nonGaussianBeam);
      expect(helpers.option.beamNotApplicableMessageForMid('robust', 0, 2, "AA4")).toBe(constants.result.nonGaussianBeam);
      expect(helpers.option.beamNotApplicableMessageForMid('robust', 0, -1, "AA4")).toBe(undefined);
      expect(helpers.option.beamNotApplicableMessageForMid('uniform', 0, 0, "AA4")).toBe(undefined);
      expect(helpers.option.beamNotApplicableMessageForMid('natural', 1, 0, "AA4")).toBe(undefined);
    });
  });

  describe('result', () => {
    it('header with param should return different string', () => {
      expect(helpers.result.header(true)).toBe(constants.resultHeader.loading);
      expect(helpers.result.header(false)).toBe(constants.resultHeader.ready);
    });
  });

  describe('status', () => {
    it('isFail should return true or false', () => {
      expect(helpers.status.isFail(constants.status.fail)).toBe(true);
      expect(helpers.status.isFail(constants.status.success)).toBe(false);
    });

    it('isSuccess should return true or false', () => {
      expect(helpers.status.isSuccess(constants.status.fail)).toBe(false);
      expect(helpers.status.isSuccess(constants.status.success)).toBe(true);
    });
  });

  describe('supplied', () => {
    it('isIntegrationTime should return true or false', () => {
      expect(helpers.supplied.isIntegrationTime(constants.supplied.integrationTime)).toBe(true);
      expect(helpers.supplied.isIntegrationTime(constants.supplied.sensitivity)).toBe(false);
    });

    it('isSensitivity should return true or false', () => {
      expect(helpers.supplied.isSensitivity(constants.supplied.integrationTime)).toBe(false);
      expect(helpers.supplied.isSensitivity(constants.supplied.sensitivity)).toBe(true);
    });
  });

  describe('configuration', () => {
    const meerkatSub: ArrayDropDownValue = {
        "name": "MID_AA4_MeerKAT_only",
        "label": "AA4 (13.5 m antennas only)",
        "n_ska": 0,
        "n_meer": 64
    };

    const skaSub: ArrayDropDownValue = {
        "name": "MID_AA4_SKA_only",
        "label": "AA4 (15 m antennas only)",
        "n_ska": 133,
        "n_meer": 0
    };

    const mixedSub: ArrayDropDownValue = {
      "name": "MID_AA4_all",
      "label": "AA4",
      "n_ska": 133,
      "n_meer": 64
    };

    const configDefaults = [
      {"type": "ska", "value": 0.7},
      {"type": "meerkat", "value": 0.435},
      {"type": "mixed", "value": 0.435}
    ];

    it("getDefaultValueFromCOnfigurationFile should return -1 when config or subarray is not provided", () => {
      expect(helpers.configuration.getDefaultFromConfigurationFile(null, meerkatSub)).toBe(-1);
    });

    it("getDefaultValueFromCOnfigurationFile should return -1 when subarray does not exist in given config", () => {
      expect(helpers.configuration.getDefaultFromConfigurationFile([{'bar': 'foo'}], meerkatSub)).toBe(-1);
    });

    it.each([
      [ configDefaults[1].type, meerkatSub, configDefaults[1].value],
      [configDefaults[0].type, skaSub, configDefaults[0].value],
      [configDefaults[2].type, mixedSub, configDefaults[2].value]]
    )("getDefaultValueFromCOnfigurationFile should return default value for subarray type %p", (
      type, subarray, expectedDefault
    ) => {
      expect(helpers.configuration.getDefaultFromConfigurationFile(configDefaults, subarray)).toBe(expectedDefault);
    });

    it("hasDefaultInConfigurationFile should return false if given array type does not exist in the configuration list", () => {
      expect(helpers.configuration.hasDefaultInConfigurationFile([{"type": "ska", "value": 0.7}], 'meerkat')).toBeFalsy();
    });

    it("hasDefaultInConfigurationFile should return false if given array type does not exist in the configuration list", () => {
      expect(helpers.configuration.hasDefaultInConfigurationFile([{"type": "meerkat", "value": 0.435}], 'meerkat')).toBeTruthy();
      expect(helpers.configuration.hasDefaultInConfigurationFile(configDefaults, 'meerkat')).toBeTruthy();
      expect(helpers.configuration.hasDefaultInConfigurationFile(configDefaults, 'ska')).toBeTruthy();
      expect(helpers.configuration.hasDefaultInConfigurationFile(configDefaults, 'mixed')).toBeTruthy();
    });

    const taperingConfig = {
      "field": "fieldTapering",
      "title": "Tapering",
      "description": "Select a u,v taper (defined as arcsec in the image plane)",
      "placeholder": "",
      "defaultValue" : [
        [0, 1, 2, 3],
        {
          "AA0.5": [0, 1],
          "AA1": [0]
        }
      ]
    };

    it("getTaperValuesForSubarray returns correct values from config", () => {
      expect(
        helpers.configuration.getTaperValuesForSubarray(taperingConfig, 'Test')
      ).toStrictEqual([0, 1, 2, 3]);
      expect(
        helpers.configuration.getTaperValuesForSubarray(taperingConfig, 'AA0.5')
      ).toStrictEqual([0, 1]);
      expect(
        helpers.configuration.getTaperValuesForSubarray(taperingConfig, 'AA1')
      ).toStrictEqual([0]);
    });

    it('should indicate a zero confusion noise false sensitivity', () => {
      expect(helpers.calculate.sensitivityLimitCheck(0,55.54)).toBe(false);
    });

    it('should indicate a in range true sensitivity', () => {
      expect(helpers.calculate.sensitivityLimitCheck(40.0,55.54)).toBe(true);
    });

    it('should indicate a out of range false sensitivity', () => {
      expect(helpers.calculate.sensitivityLimitCheck(10.0,55.54)).toBe(false);
    });

    it('sensitivity limit reached warning', () => {
      const limitMessage = "Warning: You are approaching the confusion limit (0.00 Jy/beam) given the synthesized beam-size and frequency.";
      expect(helpers.messages.sensLimitWarning("0.00 Jy/beam")).toBe(limitMessage);
    });
  });
});
