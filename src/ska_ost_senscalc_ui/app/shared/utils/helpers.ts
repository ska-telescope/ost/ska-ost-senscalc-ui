import constants from './constants';
import { ArrayDropDownValue } from "../../app-configuration";
import { LowContinuumWeightingResult, LowSingleWeightingZoomResult } from "../../generated/api/low";
import {MidFormInput} from "../../mid-sensitivity-calculator/types";

interface ValueWithUnit {
  value?: number,
  unit?: string
}

const helpers = {
  calculate: {
    subBandtoFrequencyArray: (numberOfSubBands: number,
      bandwidth: number,
      centralFrequency: number): number[] => {
      return numberOfSubBands > 0
        ? Array.from({ length: numberOfSubBands }, (_, i) =>
          (((i / numberOfSubBands) + (1 / (numberOfSubBands * 2))) * bandwidth + (centralFrequency - bandwidth / 2)))
        : [];
    },

    /**
     * Calculates the velocity and returns a string with sensible units
     * **/
    calculateVelocity(resolutionHz: number, frequencyHz: number, precision = 1): string {
      const speedOfLight = 299792458;
      const velocity = frequencyHz > 0 ? (resolutionHz / frequencyHz) * speedOfLight : 0;
      if (velocity < 1000) {
        return velocity.toFixed(precision) + " m/s";
      } else {
        return (velocity / 1000).toFixed(precision) + " km/s";
      }
    },

    sensitivityOnUnit: (unit: string, sensitivity: number, conversion_factor: number): number =>
      ['K', 'mK', 'uK'].includes(unit) ? (sensitivity / conversion_factor) : sensitivity,

    sqrtOfSumSqs: (value1: number, value2: number): number => Math.sqrt(value1 ** 2 + value2 ** 2),

    thermalSensitivity: (sensitivity: number, confusion_noise: number, weighting_factor: number): number =>
      Math.sqrt((sensitivity * sensitivity) - (confusion_noise * confusion_noise)) / weighting_factor,

    /**
     * Checks whether the total sensitivity is approaching confusion noise limit.
     * The confusion noise limit is when the total sensitivty equals the confusion noise, so we
     * define approaching as any sensitivity within 2 * confusion noise
     *
     * @param {number | string | typeof NaN} confusionNoise - the calculated confusion noise for the observation
     * can be a numeric value or a string such as "N/A (no beam information available)
     * @param {number | string} sensitivity - the calculated sensitivity
     * @returns {boolean} - True if the sensitivity is near the limit - False otherwise
     **/
    sensitivityLimitCheck(confusionNoise: number | string | typeof NaN, sensitivity: number | string): boolean {
      // We check the type first. If it's a number and greater than 0, we then check if approaching the limit
      if (!isNaN(Number(confusionNoise)) && Number(confusionNoise) > 0) {
        if (Number(sensitivity) > Number(confusionNoise) && Number(sensitivity) < Number(confusionNoise) * 2) {
          return true;
        }
        return false;
      } else {
        return false;
      }
    },

    /**
    * Calculate the beam size by multiplying minor and major beam converted into arcseconds
    *
    * @param {number} beam_min_scaled - The minor beam size in degrees
    * @param {number} beam_maj_scaled - The major beam size in degrees
    * @param {number} precision - The number of decimal places to display the result to
    * @returns {number} - The calculated beam size in square arcseconds
    */
    calculateBeamSizeInArcseconds(beam_min_scaled: number, beam_maj_scaled: number, precision: number): number {
      const beamSizeinArcseconds = Number((beam_maj_scaled * 3600).toFixed(precision)) * Number((beam_min_scaled * 3600).toFixed(precision));
      return Number(beamSizeinArcseconds.toFixed(1));
    },

    /**
    * Checks if the beam size is smaller than the minimum beam size limit
    *
    * @param {LowWeightingResult} weightingResponse - response from the weighting calculation
    * @param {number} precision - The number of decimal places to display the result to
    * @returns {boolean} - True if smaller than min beam size - False otherwise
    */
    minBeamSizeCheck(weightingResponse: LowContinuumWeightingResult | LowSingleWeightingZoomResult, precision: number): boolean {
      const minBeamSize:number = constants.values.minGaussianBeamLow;
      const arcsecsBeamSize: number = this.calculateBeamSizeInArcseconds(weightingResponse.beam_size.beam_min_scaled, weightingResponse.beam_size.beam_maj_scaled, precision);
      return arcsecsBeamSize < minBeamSize;
    }

  },

  format: {

    /**
     * Converts the sensitivity to a sensible unit,
     * e.g. display 12.3 mJy/beam instead of 1234 uJy/beam
     *
     * @param sensitivity the sensitivity returned from the API in uJy/beam
     * @param precision the number of d.p. to display the result to
     * @returns {string} the sensitivity as a string with the correct units and precision
     * **/
    convertSensitivityToDisplayValue(sensitivity: number, precision = 2): string {
      sensitivity = Number(sensitivity);
      if (sensitivity == 0) {
        return '0.00 Jy/beam';
      }
      if (sensitivity < 1) {
        // For < 1 uJy/beam, display the value in nJy/beam
        return `${(sensitivity * 1e3).toFixed(precision)} nJy/beam`;
      }
      if (sensitivity < 1e3) {
        // For 0 - 999 uJy/beam, display the value in uJy/beam
        return `${sensitivity.toFixed(precision)} uJy/beam`;
      }
      if (sensitivity < 1e6) {
        // For 1000 - 999999 uJy/beam, display the value in mJy/beam
        return `${(sensitivity / 1e3).toFixed(precision)} mJy/beam`;
      }
      // For values above 999999 uJy/beam, display the value in Jy/beam
      return `${(sensitivity / 1e6).toFixed(precision)} Jy/beam`;
    },

    /**
     * Converts a time in seconds to a sensible unit,
     * e.g. display 1 ms instead of 0.001 s
     *
     * @param time in seconds
     * @param precision the number of d.p. to display the result to
     * @returns {string} the time as a string with the correct units and precision
     * **/
    convertTimeToDisplayUnit(time: ValueWithUnit, precision = 2): string {
      if (time.unit != 's') {
        // TODO make this function smarter so it can handle all units
        throw Error('Only seconds supported currently in convertTimeToDisplayUnit');
      }
      const timeS = Number(time.value);
      if (timeS > 1e-1) {
        return `${timeS.toFixed(precision)} s`;
      }
      if (timeS > 1e-4) {
        return `${(timeS * 1e3).toFixed(precision)} ms`;
      }
      if (timeS > 1e-7) {
        return `${(timeS * 1e6).toFixed(precision)} us`;
      }
      return `${(timeS * 1e9).toFixed(precision)} ns`;
    },

    convertSensitivityToJy(inputSensitivity: number, unit: string): number {
      // This method is currently only called for Custom subarrays when only Jy units are allowed.
      // As part of the refactoring of these helpers make it smarter/safer
      if (unit == 'mJy/beam') {
        return inputSensitivity * 1e-3;
      }
      if (unit == 'uJy/beam') {
        return inputSensitivity * 1e-6;
      }
      if (unit == 'nJy/beam') {
        return inputSensitivity * 1e-9;
      }
      return inputSensitivity;
    },

    /**
     * Converts the sensitivity to a sensible unit,
     * e.g. display 12.3 mJy instead of 1234 uJy
     *
     * @param sensitivity the sensitivity returned from the API in uJy
     * @param precision the number of d.p. to display the result to
     * @returns {string} the sensitivity as a string with the correct units and precision
     * **/
    convertSensitivityUnit(sensitivity: number, precision = 2): string {
      if (sensitivity < 1e3) {
        // For 0 - 999 uJy, display the value in uJy
        return `${sensitivity.toFixed(precision)} uJy`;
      }
      if (sensitivity < 1e6) {
        // For 1000 - 999999 uJy, display the value in mJy
        return `${(sensitivity / 1e3).toFixed(precision)} mJy`;
      }
      // For values above 999999 uJy, display the value in Jy
      return `${(sensitivity / 1e6).toFixed(precision)} Jy`;
    },

    /**
     * Converts a value in uK to a string with sensible units,
     * e.g. display 10.4 mK instead of 10400 uK
     *
     * @param value in uK
     * @param precision the number of d.p. to display the result to
     * @returns {string} the value as a string with the correct units and precision
     * **/
    convertKelvinsToDisplayValue(value: number, precision = 2) {
      if (value < 1e3) {
        // For 0 - 999 uK, display the value in uK
        return `${value.toFixed(precision)} uK`;
      }
      if (value < 1e6) {
        // For 1000 - 999999 uK, display the value in mK
        return `${(value / 1e3).toFixed(precision)} mK`;
      }
      // For values above 999999 uK, display the value in K
      return `${(value / 1e6).toFixed(precision)} K`;
    },

    /**
      * Converts a minor and major beam in degrees (as returned by the backend)
     * into a formatted string in arcsecs eg '4.6" x 7.9"'
      *
      * @param beam_min_scaled in degrees
      * @param beam_maj_scaled in degrees
      * @param precision the number of d.p. to display the result to
      * @returns {string} a string of the format '4.6" x 7.9"'
      * **/
    convertBeamValueDegreesToDisplayValue(beam_maj_scaled: number, beam_min_scaled: number, precision = 3): string {
      return `${(beam_maj_scaled * 3600).toFixed(precision)}" x ${(beam_min_scaled * 3600).toFixed(precision)}"`;
    },

    /**
  * Formats a Decimal RA or DEC
  *
  * @param field Decimal RA or DEC field to format
  * @returns {string} Formatted decimal RA or DEC value'
  * **/
    decRaDecFormat(field: string): string {
      const number = parseFloat(field);
      if (Number.isNaN(number)) { return field; }
      return Number.isInteger(number) ? number.toFixed(1) : number.toString();
    },

    /**
      * Formats a RA or DEC
      *
      * @param field RA or DEC field to format
      * @returns {string} Formatted RA or DEC value'
      * **/
    raDecFormat(field: string): string {

      // Split the input string into parts using ':'
      const parts = field.split(':').map(part => part.trim());

      //Pre validation check as we should not format if any of the parts are untenable.
      // Check the first value is a two digit integer.
      if (!(/^[-+]?\d{1,2}$/.test(parts[0]))) { return field; }
      // If the first entry ends in a colon, check the second value is a two digit integer and is not empty.
      if ((parts.length > 1 && parts[1] != '') && !(/^\d{1,2}$/.test(parts[1]))) { return field; }
      // If the second entry ends in a colon, check the third value is a two digit float and is not empty.
      if ((parts.length > 2 && parts[2] != '') && !(/^\d{1,2}(\.\d*)?\.?$/.test(parts[2]))) { return field; }
      // Anything more, just bail. This could be checked first but is an unlikely option.
      if (parts.length > 3) { return field; }

      // Pad missing parts with zeros and ensure they are valid numbers
      const hrDeg = parseInt(parts[0]) || 0;
      const minutes = parseInt(parts[1]) || 0;
      const seconds = parseFloat(parts[2]) || 0;

      // Format hours/degrees and minutes/arcminutes with leading zeros
      const formattedHrDeg = hrDeg.toString().padStart(2, '0');
      const formattedMinutes = minutes.toString().padStart(2, '0');

      // Split out the seconds integer part
      const intSeconds = Math.floor(seconds);

      // Preformat seconds as if there is a mantissa value of any length, padding values below 10 with zero.
      let formattedSeconds = seconds < 10 ? '0' + seconds.toString() : seconds.toString();

      // Reformat seconds for a zero padding, if the entry ends in a full stop or is just a whole number.
      if (intSeconds == seconds) {
        formattedSeconds = intSeconds.toString().padStart(2, '0') + ".0";
      }

      // Construct the padded astronomical declination string
      const paddedSexagesimal = `${formattedHrDeg}:${formattedMinutes}:${formattedSeconds}`;

      return paddedSexagesimal;
    },

    /**
     * Converts a Declination string value in degrees to its sexagesimal equivalent.
     *
     * It returns a Declination sexagesimal value.
     *
     * @param value Declination in decimal degrees.
    **/
    degDecToSexagesimal(value: string): string {
      const decDeg = parseFloat(value);

      const sign = decDeg >= 0 ? '+' : '-';
      const absDecDeg = Math.abs(decDeg);
      const degrees = Math.floor(absDecDeg);
      const decMinutes = (absDecDeg - degrees) * 60;
      const minutes = Math.floor(decMinutes);
      const seconds = (decMinutes - minutes) * 60;

      const sexagesimal = `${sign}${String(degrees).padStart(2, '0')}:${String(minutes).padStart(2, '0')}:${seconds.toFixed(2).padStart(5, '0')}`;

      return sexagesimal;
    },

    /**
     * Converts a declination string value in sexagesimal to its decimal degree equivalent.
     *
     * It returns a declination decimal degree value.
     *
     * @param value Declination in sexagesimal format.
    **/
    sexDecToDeg(value: string): string {
      const sign = value.startsWith('-') ? -1 : 1;
      const [degrees, minutes, seconds] = value.slice(0).split(':').map(Number);
      const decimalDegrees = sign * (Math.abs(degrees) + minutes / 60 + seconds / 3600);
      return decimalDegrees.toString();
    },

    /**
     * Converts a right ascension string value in degrees to its sexagesimal equivalent.
     *
     * It returns a right ascension sexagesimal value.
     *
     * @param value right ascension in decimal degrees.
    **/
    degRaToSexagesimal(value: string): string {
      const decimalDegrees = parseFloat(value);
      const hours = Math.floor(decimalDegrees / 15);
      const remainingDegrees = decimalDegrees - hours * 15;
      const minutes = Math.floor(remainingDegrees * 4);
      const remainingMinutes = remainingDegrees * 4 - minutes;
      const seconds = remainingMinutes * 60;

      const sexagesimalString = `${String(hours).padStart(2, '0')}:${String(minutes).padStart(2, '0')}:${seconds.toFixed(2).padStart(5, '0')}`;
      return sexagesimalString;
    },

    /**
     * Converts a right ascension string value in sexagesimal to its decimal degree equivalent.
     *
     * It returns a right ascension decimal degree value.
     *
     * @param value right ascension in sexagesimal format.
    **/
    sexRaToDeg(value: string): string {
      const [hours, minutes, seconds] = value.split(':').map(Number);
      const decDegrees = (hours + minutes / 60 + seconds / 3600) * 15;
      return decDegrees.toString();
    },

    displayValueWithUnit(valueWithUnit: { value?: number, unit?: string }, decimalPlaces = 1) {
      // TODO this is a temp fix as we don't like how the astropy unit has been serialised.
      //  We should change this in the backend in BTN-2282
      return (valueWithUnit.unit == 'rad / m2')
        ? `${valueWithUnit.value?.toFixed(decimalPlaces)} rad/m&sup2`
        : `${valueWithUnit.value?.toFixed(decimalPlaces)} ${valueWithUnit.unit}`;
    }
  },

  option: {

    /**
     * This function returns a message that should be displayed if weighting cannot be applied to the calculation, either because the beam
     * is non Gaussian or the beam cannot be simulated.
     *
     * @returns a string message if the weighting cannot be applied or null if the weighting can be applied
     * **/
    beamNotApplicableMessageForMid(weighting: string, taper: number, robustness: number, array_configuration: string): string | undefined {
      if (array_configuration.toLowerCase() == 'custom') {
        return constants.result.noBeamAvailable;
      }

      if ((weighting === 'natural' && taper == 0) ||
        (weighting === 'robust' && robustness === 2 && taper == 0)) {
        return constants.result.nonGaussianBeam;
      }
      return undefined;
    },

    continuumBeamNonGaussian(midFormInput: MidFormInput): boolean {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      const briggsOrRobust = midFormInput.continuum!.weighting === 'robust' || midFormInput.continuum!.weighting === 'briggs';
      return (midFormInput.continuum!.weighting === 'natural' && midFormInput.continuum!.taper! == 0) ||
        (briggsOrRobust && midFormInput.continuum!.robustness === 2 && midFormInput.continuum!.taper! == 0);
    },

    zoomBeamNonGaussian(midFormInput: MidFormInput): boolean {
      const briggsOrRobust = midFormInput.line!.weighting === 'robust' || midFormInput.line!.weighting === 'briggs';
      return (midFormInput.line!.weighting === 'natural' && midFormInput.line!.taper! == 0) ||
        (briggsOrRobust && midFormInput.line!.robustness === 2 && midFormInput.line!.taper! == 0);
    },

    /**
     * This function returns a message that should be displayed if weighting cannot be applied to the calculation, either because the beam
     * is non Gaussian or the beam cannot be simulated.
     *
     * @returns true if weighting results should be shown, ie the beam is Gaussian
     * **/
    showBeamAndSBSForLow(weighting: string, robustness: number): boolean {
      // We want hide the beam and SBS values if weighting is natural or if weighting is robust with a value of 2
      return !(weighting.toLowerCase() == 'natural' || (weighting.toLowerCase() == 'briggs' && robustness == 2));
    }
  },

  result: {
    header: (loading: boolean): string => loading ? constants.resultHeader.loading : constants.resultHeader.ready
  },

  status: {
    isFail: (value: string): boolean => value === constants.status.fail,

    isSuccess: (value: string): boolean => value === constants.status.success
  },

  supplied: {
    isIntegrationTime: (value: string): boolean => value === constants.supplied.integrationTime,

    isSensitivity: (value: string): boolean => value === constants.supplied.sensitivity
  },

  configuration: {
    getDefaultFromConfigurationFile(config: any, subarrayType: ArrayDropDownValue) {
      if (!config) {
        return -1;
      }

      const hasSKA = subarrayType?.n_ska > 0;
      const hasMeerkat = subarrayType?.n_meer > 0;

      let key: string;
      if (hasMeerkat && !hasSKA) {
        key = "meerkat";
      } else if (hasSKA && !hasMeerkat) {
        key = "ska";
      } else {
        key = "mixed";
      }

      if (!this.hasDefaultInConfigurationFile(config, key)) {
        return -1;
      }
      return config.find((e: any) => e.type === key)?.value;
    },

    hasDefaultInConfigurationFile(config: any, key: string): boolean {
      return !!config.find((e: any) => e.type === key);
    },

    getTaperValuesForSubarray(config: any, subarrayName: string): number[] {
      const nonDefaultTaperSubarrays = Object.keys(config.defaultValue[1]);
      if (nonDefaultTaperSubarrays.includes(subarrayName)) {
        return config.defaultValue[1][subarrayName];
      }
      return config.defaultValue[0];
    }
  },

  messages: {
    sensLimitWarning(confusionNoise: string | null = null): string {
      return confusionNoise
        ? `Warning: You are approaching the confusion limit (${confusionNoise}) given the synthesized beam-size and frequency.`
        : 'Warning: You are approaching the confusion limit given the synthesized beam-size and frequency.';
    },

    sensLimitError(confusionNoise: string): string {
      return `Error: the entered sensitivity is less than or equal to the confusion noise (${confusionNoise})`;
    }
  }
};

export default helpers;
